<?
include_once("common.php");	
//https://stackoverflow.com/questions/14087116/extract-address-from-string
//http://nominatim.openstreetmap.org/reverse?format=json&lat=23.016310&lon=72.505042&zoom=18&addressdetails=1
$PickUpAddress = "SG Hwy Service Rd, Prahlad Nagar, Ahmedabad, Gujarat 380015, India";
$DropOffAddress = "SG Hwy Service Rd, Prahlad Nagar, Ahmedabad, Gujarat 380015, India";

//https://www.google.co.in/maps/place/SG+Hwy+Service+Rd,+Prahlad+Nagar,+Ahmedabad,+Gujarat+380015/@23.0149581,72.5017284,17z/data=!3m1!4b1!4m5!3m4!1s0x395e9b26d505429f:0xef12dd24af7a1e15!8m2!3d23.0149581!4d72.5039171
$passengerLat = "23.0149621";
$passengerLon = "72.5017287";
//getcountrycitystatefromaddress($address);

$address_data['CheckAddress'] = $PickUpAddress;
$allowed_ans = checkRestrictedArea($address_data,"No");
$address_data['CheckAddress'] = $DropOffAddress;
$allowed_ans_drop = checkRestrictedArea($address_data,"Yes");

function checkRestrictedArea($address_data,$DropOff){
		global $generalobj, $obj;
		$ssql = "";
		if($DropOff == "No"){
			$ssql.= " AND (eRestrictType = 'Pick Up' OR eRestrictType = 'All')";
			}else{
			$ssql.= " AND (eRestrictType = 'Drop Off' OR eRestrictType = 'All')";
		}
		if(!empty($address_data)){
			$pickaddrress = strtolower($address_data['CheckAddress']);
			$pickaddrress = preg_replace('/\d/', '', $pickaddrress);
			$pickaddrress = preg_replace('/\s+/', '', $pickaddrress); 
			//$pickArr = explode(',',$pickaddrress);
			$pickArr = array_map('trim',array_filter(explode(',',$pickaddrress)));
			$sqlaa = "SELECT cr.vCountry,ct.vCity,st.vState,replace(rs.vAddress, ' ','') as vAddress FROM `restricted_negative_area` AS rs
			LEFT JOIN country as cr ON cr.iCountryId = rs.iCountryId
			LEFT JOIN state as st ON st.iStateId = rs.iStateId
			LEFT JOIN city as ct ON ct.iCityId = rs.iCityId
			WHERE eType='Allowed'".$ssql;
			$allowed_data = $obj->MySQLSelect($sqlaa);
			$allowed_ans = 'No';
			if(!empty($allowed_data)){
				foreach($allowed_data as $rds){
					$alwd_country = $alwd_state = $alwd_city = $alwd_address = 'allowed';
					if($rds['vCountry'] != ""){
						//if($rds['vCountry'] == $address_data['countryId']){
						if(in_array(strtolower($rds['vCountry']),$pickArr)){
							$alwd_country = 'allowed';
							}else {
							$alwd_country = 'Disallowed';
						}
					}
					if($rds['vState'] != ""){
						if(in_array(strtolower($rds['vState']),$pickArr)){
							$alwd_state = 'allowed';
							}else {
							$alwd_state = 'Disallowed';
						}
					}
					if($rds['vCity'] != ""){
						if(in_array(strtolower($rds['vCity']),$pickArr)){
							$alwd_city = 'allowed';
							}else{
							$alwd_city = 'Disallowed';
						}
					}
					if($rds['vAddress'] != ""){
						if(strstr(strtolower($pickaddrress), strtolower($rds['vAddress']))){
							$alwd_address = 'allowed';
							}else{
							$alwd_address = 'Disallowed';
						}
					}
					if($alwd_country == 'allowed' && $alwd_state == 'allowed' && $alwd_city == 'allowed' && $alwd_address == 'allowed'){
						$allowed_ans = 'Yes';
						break;
					}
				}
			}    
			
			if($allowed_ans == 'No') {
				//$sqlas = "SELECT * FROM `restricted_negative_area` WHERE (iCountryId='".$address_data['countryId']."' OR iStateId='".$address_data['stateId']."' OR iCityId='".$address_data['cityId']."') AND eType='Disallowed' AND (eRestrictType = 'Pick Up' OR eRestrictType = 'All')";
				$sqlas = "SELECT cr.vCountry,ct.vCity,st.vState,replace(rs.vAddress, ' ','') as vAddress FROM `restricted_negative_area` AS rs
				LEFT JOIN country as cr ON cr.iCountryId = rs.iCountryId
                LEFT JOIN state as st ON st.iStateId = rs.iStateId
                LEFT JOIN city as ct ON ct.iCityId = rs.iCityId
				WHERE eType='Disallowed'".$ssql;
				$restricted_data = $obj->MySQLSelect($sqlas);
				$allowed_ans = 'Yes';
				if(!empty($restricted_data)){
					foreach($restricted_data as $rds){
						$alwd_country = $alwd_state = $alwd_city = $alwd_address = 'Disallowed';   
						if($rds['vCountry'] != ""){                                               
							if(in_array(strtolower($rds['vCountry']),$pickArr)){
								$alwd_country = 'Disallowed';
								}else {
								$alwd_country = 'allowed';
							}
						}
						if($rds['vState'] != ""){
							if(in_array(strtolower($rds['vState']),$pickArr)){
								$alwd_state = 'Disallowed';
								}else {
								$alwd_state = 'allowed';
							}  
						}
						if($rds['vCity'] != ""){
							if(in_array(strtolower($rds['vCity']),$pickArr)){
								$alwd_city = 'Disallowed';
								}else{
								$alwd_city = 'allowed';
							}   
						}
						if($rds['vAddress'] != ""){      
							if(strstr(strtolower($pickaddrress), strtolower($rds['vAddress']))){
								$alwd_address = 'Disallowed';
								}else{
								$alwd_address = 'allowed';
              } 
						}
						if($alwd_country == 'Disallowed' && $alwd_state == 'Disallowed' && $alwd_city == 'Disallowed' && $alwd_address == "Disallowed"){
							$allowed_ans = 'No';
							break;
						}
					}  
				}
			}
		}
		return $allowed_ans;  
}

function get_data($url) {
 $ch = curl_init();
 $timeout = 5;
 curl_setopt($ch, CURLOPT_URL, $url);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
 $data = curl_exec($ch);
 curl_close($ch); echo "<pre>";print_r($data);exit;
 return json_decode($data,true);
}
/*
https://stackoverflow.com/questions/27535809/extracting-city-and-zipcode-from-string-in-php
$array = array();
$array[0] = "123 Main Street, New Haven, CT 06518";
$array[1] = "123 Main Street, New Haven, CT";
$array[2] = "123 Main Street, New Haven,                            CT 06511";
$array[3] = "New Haven,CT 66554, United States";
$array[4] = "New Haven, CT06513";
$array[5] = "06513";
$array[6] = "123 Main    Street, New Haven CT 06518, united states";

$array[7] = "1253 McGill College, Montreal, QC H3B 2Y5"; // google Montreal  / Canada
$array[8] = "1600 Amphitheatre Parkway, Mountain View, CA 94043"; // google CA  / US
$array[9] = "20 West Kinzie St., Chicago, IL 60654"; // google IL / US
$array[10] = "405 Rue Sainte-Catherine Est, Montreal, QC"; // Montreal address shows hyphened street names
$array[11] = "48 Pirrama Road, Pyrmont, NSW 2009"; // google Australia
*/
?>
