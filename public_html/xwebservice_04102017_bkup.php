<?php
	//date_default_timezone_set('Asia/Kolkata');
	
	@session_start();
	$_SESSION['sess_hosttype'] = 'ufxall';
  $inwebservice = "1";
	error_reporting(0);
	include_once('include_taxi_webservices.php');
	include_once(TPATH_CLASS.'configuration.php');
	
	require_once('assets/libraries/stripe/config.php');
	require_once('assets/libraries/stripe/stripe-php-2.1.4/lib/Stripe.php');
	require_once('assets/libraries/pubnub/autoloader.php');
	include_once(TPATH_CLASS .'Imagecrop.class.php');
	include_once(TPATH_CLASS .'twilio/Services/Twilio.php');
	include_once('generalFunctions04102017.php');
	include_once('send_invoice_receipt.php');
	$PHOTO_UPLOAD_SERVICE_ENABLE = "Yes";
	$host_arr = array();
	$host_arr = explode(".",$_SERVER["HTTP_HOST"]);
	$host_system = $host_arr[0];
	if($host_system == "beautician"){
		$PHOTO_UPLOAD_SERVICE_ENABLE = "Yes";
	}
	if($host_system == "tutors"){
		$PHOTO_UPLOAD_SERVICE_ENABLE = "No";
	}
	$uuid = "fg5k3i7i7l5ghgk1jcv43w0j41";
	
	/* creating objects */
	$thumb 		= new thumbnail;
	
	/* Get variables */
	$type 		= isset($_REQUEST['type'])?trim($_REQUEST['type']):'';
	
	/* Paypal supported Currency Codes */
	$currency_supported_paypal = array('AUD', 'BRL', 'CAD', 'CZK', 'DKK', 'EUR', 'HKD', 'HUF', 'ILS', 'JPY', 'MYR', 'MXN', 'TWD', 'NZD', 'NOK', 'PHP', 'PLN', 'GBP', 'RUB', 'SGD', 'SEK', 'CHF', 'THB', 'TRY', 'USD');
	
	$demo_site_msg="Edit / Delete Record Feature has been disabled on the Demo Application. This feature will be enabled on the main script we will provide you.";
	
	if($type==''){
		$type 		= isset($_REQUEST['function'])?trim($_REQUEST['function']):'';
	}
	$lang_label = array();
	$lang_code 	= '';
	
	/* general fucntions */
	if($type != "generalConfigData" && $type != "signIn" && $type != "isUserExist" && $type != "signup" && $type != "LoginWithFB" && $type != "sendVerificationSMS" && $type != "countryList" && $type != "changelanguagelabel" && $type != "requestResetPassword" && $type != "UpdateLanguageLabelsValue" && $type != "staticPage") {
		$tSessionId = isset($_REQUEST['tSessionId']) ? trim($_REQUEST['tSessionId']) : '';
		$GeneralMemberId = isset($_REQUEST['GeneralMemberId']) ? trim($_REQUEST['GeneralMemberId']) : '';
		$GeneralUserType = isset($_REQUEST['GeneralUserType']) ? trim($_REQUEST['GeneralUserType']) : '';
		if ($tSessionId == "" || $GeneralMemberId == "" || $GeneralUserType == "") {
			$returnArr['Action'] = "0";
			$returnArr['message'] = "SESSION_OUT";
			echo json_encode($returnArr);
			exit;
			} else {
			$userData = get_value($GeneralUserType == "Driver" ? "register_driver" : "register_user", $GeneralUserType == "Driver" ? "iDriverId as iMemberId,tSessionId" : "iUserId as iMemberId,tSessionId", $GeneralUserType == "Driver" ? "iDriverId" : "iUserId", $GeneralMemberId);
			if ($userData[0]['iMemberId'] != $GeneralMemberId || $userData[0]['tSessionId'] != $tSessionId) {
				$returnArr['Action'] = "0";
				$returnArr['message'] = "SESSION_OUT";
				echo json_encode($returnArr);
				exit;
			}
		}
	}  
	/* To Check App Version */
	$appVersion = isset($_REQUEST['AppVersion'])?trim($_REQUEST['AppVersion']):'';
	$Platform = isset($_REQUEST['Platform'])?trim($_REQUEST['Platform']):'Android';
	
	if($appVersion != ""){
		$UserType   = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		if($UserType == "Passenger"){
			$newAppVersion = $generalobj->getConfigurations("configurations", $Platform == "IOS"? "PASSENGER_IOS_APP_VERSION" :"PASSENGER_ANDROID_APP_VERSION");
			}else{
			$newAppVersion = $generalobj->getConfigurations("configurations",$Platform == "IOS"? "DRIVER_IOS_APP_VERSION" : "DRIVER_ANDROID_APP_VERSION");
		}
		$appVersion =  round($appVersion,2);
		if($newAppVersion != $appVersion && $newAppVersion > $appVersion){
			$returnArr['Action'] = "0";
			$returnArr['isAppUpdate'] = "true";
			$returnArr['message'] = "LBL_NEW_UPDATE_MSG";
			echo json_encode($returnArr);exit;
		}
	}
	
	if($type=="checkGetValue"){
		$check_payment = get_value('vehicle_type', '*', '', '');
		// print_r($check_payment);
		
		$row[0]['VehicleTypes'] = $check_payment;
		echo json_encode($row[0]);
	}
	
	function getPassengerDetailInfo($passengerID,$cityName){
		global $generalobj,$obj,$demo_site_msg,$PHOTO_UPLOAD_SERVICE_ENABLE,$parent_ufx_catid;
		
		$where = " iUserId = '".$passengerID."'";
		$data_version['iAppVersion']="2";
		$data_version['eLogout'] = 'No';
		$obj->MySQLQueryPerform("register_user",$data_version,'update',$where);
		
		$updateQuery = "UPDATE trip_status_messages SET eReceived='Yes' WHERE iUserId='".$passengerID."' AND eToUserType='Passenger'";
		$obj->sql_query($updateQuery);
		
		$sql = "SELECT * FROM `register_user` WHERE iUserId='$passengerID'";
		$row = $obj->MySQLSelect($sql);
		
		if (count($row) > 0) {
			
			
			$RIDER_EMAIL_VERIFICATION = $generalobj->getConfigurations("configurations", "RIDER_EMAIL_VERIFICATION");
			$RIDER_PHONE_VERIFICATION = $generalobj->getConfigurations("configurations", "RIDER_PHONE_VERIFICATION");
			
			if($RIDER_EMAIL_VERIFICATION == 'No'){
				$row[0]['eEmailVerified'] = "Yes";
			}
			
			if($RIDER_PHONE_VERIFICATION == 'No'){
				$row[0]['ePhoneVerified'] = "Yes";
			}
			
			## Check and update Device Session ID ##
			if($row[0]['tDeviceSessionId'] == ""){
				$random = substr( md5(rand()), 0, 7);
				$Update_Device_Session['tDeviceSessionId'] = session_id().time().$random;
				$Update_Device_Session_id = $obj->MySQLQueryPerform("register_user", $Update_Device_Session, 'update', $where);
				$row[0]['tDeviceSessionId'] = $Update_Device_Session['tDeviceSessionId'];
			}
			## Check and update Device Session ID ##
			## Check and update Session ID ##
			if($row[0]['tSessionId'] == ""){
				$Update_Session['tSessionId'] = session_id().time();
				$Update_Session_id = $obj->MySQLQueryPerform("register_user", $Update_Session, 'update', $where);
				$row[0]['tSessionId'] = $Update_Session['tSessionId'];
			}
			## Check and update Session ID ##
			
			if($row[0]['vImgName']!="" && $row[0]['vImgName']!="NONE"){
				$row[0]['vImgName']="3_".$row[0]['vImgName'];
			}
			//$row[0]['Passenger_Password_decrypt']= $generalobj->decrypt($row[0]['vPassword']);
			$row[0]['Passenger_Password_decrypt']= "";
			
			if($row[0]['eStatus']!="Active"){
				$returnArr['Action'] ="0";
				
				if($row[0]['eStatus'] !="Deleted"){
					$returnArr['message'] ="LBL_CONTACT_US_STATUS_NOTACTIVE_PASSENGER";
					}else {
					$returnArr['message'] ="LBL_ACC_DELETE_TXT";
				}
				echo json_encode($returnArr);exit;
			}
			
			$TripStatus = $row[0]['vTripStatus'];
			$TripID=$row[0]['iTripId'];
			
			if ($TripStatus != "NONE") {
				$TripID = $row[0]['iTripId'];
				$row_result_trips = getTripPriceDetails($TripID,$passengerID,"Passenger");
				
				$row[0]['TripDetails'] = $row_result_trips;
				
				
				$row[0]['DriverDetails'] = $row_result_trips['DriverDetails'];
				
				
				$row_result_trips['DriverCarDetails']['make_title'] = $row_result_trips['DriverCarDetails']['vMake'];
				$row_result_trips['DriverCarDetails']['model_title'] = $row_result_trips['DriverCarDetails']['vTitle'];
				$row[0]['DriverCarDetails'] = $row_result_trips['DriverCarDetails'];
				
				$sql = "SELECT vPaymentUserStatus FROM `payments` WHERE iTripId='$TripID'";
				$row_result_payments = $obj->MySQLSelect($sql);
				
				if(count($row_result_payments)>0){
					
					if($row_result_payments[0]['vPaymentUserStatus']!='approved'){
						$row[0]['PaymentStatus_From_Passenger']="Not Approved";
						}else{
						$row[0]['PaymentStatus_From_Passenger']="Approved";
					}
					
					}else{
					
					$row[0]['PaymentStatus_From_Passenger']="No Entry";
				}
				
				$sql = "SELECT iTripId,eUserType FROM `ratings_user_driver` WHERE iTripId='$TripID'";
				$row_result_ratings = $obj->MySQLSelect($sql);
				
				if(count($row_result_ratings)>0){
					
					$count_row_rating=0;
					$ContentWritten="false";
					while(count($row_result_ratings) > $count_row_rating){
						
						$UserType=$row_result_ratings[$count_row_rating]['eUserType'];
						
						if($UserType=="Passenger"){
							$ContentWritten="true";
							$row[0]['Ratings_From_Passenger']="Done";
							}else if($ContentWritten=="false"){
							$row[0]['Ratings_From_Passenger']="Not Done";
						}
						
						$count_row_rating++;
					}
					}else{
					
					$row[0]['Ratings_From_Passenger']="No Entry";
				}
			}
			
			$vehicleTypes = get_value('vehicle_type', '*', '', '',' ORDER BY iVehicleTypeId ASC');
			$vehicle_category = get_value('vehicle_category', 'iVehicleCategoryId, vLogo,vCategory_'.$row[0]['vLang'].' as vCategory', 'eStatus', 'Active');
			
			// $priceRatio=($obj->MySQLSelect("SELECT Ratio FROM currency WHERE vName='".$row[0]['vCurrencyPassenger']."' ")[0]['Ratio']);
			$priceRatio=get_value('currency', 'Ratio', 'vName', $row[0]['vCurrencyPassenger'],'','true');
			for($i=0;$i<1;$i++){
				$vehicleTypes[$i]['fPricePerKM']= round($vehicleTypes[$i]['fPricePerKM'] * $priceRatio,2);
				$vehicleTypes[$i]['fPricePerMin']= round($vehicleTypes[$i]['fPricePerMin'] * $priceRatio,2);
				$vehicleTypes[$i]['iBaseFare']= round($vehicleTypes[$i]['iBaseFare'] * $priceRatio,2);
				$vehicleTypes[$i]['fCommision']= round($vehicleTypes[$i]['fCommision'] * $priceRatio,2);
				$vehicleTypes[$i]['iMinFare']= round($vehicleTypes[$i]['iMinFare'] * $priceRatio,2);
				$vehicleTypes[$i]['FareValue']= round($vehicleTypes[$i]['fFixedFare'] * $priceRatio,2);
				$vehicleTypes[$i]['vVehicleType']= $vehicleTypes[$i]["vVehicleType_".$row[0]['vLang']];
			}
			$row[0]['VehicleTypes'] = $vehicleTypes;
			$row[0]['VehicleCategory'] = $vehicle_category;
			
			// $row[0]['PayPalConfiguration']=$generalobj->getConfigurations("configurations","PAYMENT_ENABLED");
			$row[0]['DefaultCurrencySign']=$generalobj->getConfigurations("configurations","DEFAULT_CURRENCY_SIGN");
			$row[0]['DefaultCurrencyCode']=$generalobj->getConfigurations("configurations","DEFAULT_CURRENCY_CODE");
			$row[0]['RESTRICTION_KM_NEAREST_TAXI']=$generalobj->getConfigurations("configurations","RESTRICTION_KM_NEAREST_TAXI");
			$row[0]['FACEBOOK_APP_ID']=$generalobj->getConfigurations("configurations","FACEBOOK_APP_ID");
			$row[0]['CONFIG_CLIENT_ID']=$generalobj->getConfigurations("configurations","CONFIG_CLIENT_ID");
			$row[0]['GOOGLE_SENDER_ID']=$generalobj->getConfigurations("configurations","GOOGLE_SENDER_ID");
			$row[0]['DRIVER_ARRIVED_MIN_TIME_PER_MINUTE']=$generalobj->getConfigurations("configurations","DRIVER_ARRIVED_MIN_TIME_PER_MINUTE");
			$row[0]['MOBILE_VERIFICATION_ENABLE']=$generalobj->getConfigurations("configurations","MOBILE_VERIFICATION_ENABLE");
			$row[0]['DRIVER_LOC_FETCH_TIME_INTERVAL']=$generalobj->getConfigurations("configurations","DRIVER_LOC_FETCH_TIME_INTERVAL");
			$row[0]['ONLINE_DRIVER_LIST_UPDATE_TIME_INTERVAL']=$generalobj->getConfigurations("configurations","ONLINE_DRIVER_LIST_UPDATE_TIME_INTERVAL");
			$row[0]['LOCATION_ACCURACY_METERS']=$generalobj->getConfigurations("configurations","LOCATION_ACCURACY_METERS");
			$row[0]['STRIPE_PUBLISH_KEY']=$generalobj->getConfigurations("configurations","STRIPE_PUBLISH_KEY");
			$row[0]['DRIVER_REQUEST_METHOD']=$generalobj->getConfigurations("configurations","DRIVER_REQUEST_METHOD");
			$row[0]['ENABLE_PUBNUB']=$generalobj->getConfigurations("configurations","ENABLE_PUBNUB");
			$row[0]['SITE_POLICE_CONTROL_NUMBER']=$generalobj->getConfigurations("configurations","SITE_POLICE_CONTROL_NUMBER");
			$row[0]['PUBNUB_PUBLISH_KEY'] = $generalobj->getConfigurations("configurations","PUBNUB_PUBLISH_KEY");
			$row[0]['PUBNUB_SUBSCRIBE_KEY'] = $generalobj->getConfigurations("configurations","PUBNUB_SUBSCRIBE_KEY");
			$row[0]['PUBNUB_SECRET_KEY'] = $generalobj->getConfigurations("configurations","PUBNUB_SECRET_KEY");
			$row[0]['REFERRAL_SCHEME_ENABLE'] = $generalobj->getConfigurations("configurations","REFERRAL_SCHEME_ENABLE");
			$row[0]['WALLET_ENABLE'] = $generalobj->getConfigurations("configurations","WALLET_ENABLE");
			$row[0]['APP_TYPE'] = $generalobj->getConfigurations("configurations","APP_TYPE");
			$row[0]['APP_PAYMENT_MODE'] = $generalobj->getConfigurations("configurations","APP_PAYMENT_MODE");
			$row[0]['WALLET_FIXED_AMOUNT_1'] = $generalobj->getConfigurations("configurations","WALLET_FIXED_AMOUNT_1");
			$row[0]['WALLET_FIXED_AMOUNT_2'] =$generalobj->getConfigurations("configurations","WALLET_FIXED_AMOUNT_2");
			$row[0]['WALLET_FIXED_AMOUNT_3'] = $generalobj->getConfigurations("configurations","WALLET_FIXED_AMOUNT_3");
			$row[0]['TOLL_COST_APP_ID'] = $generalobj->getConfigurations("configurations","TOLL_COST_APP_ID");
			$row[0]['TOLL_COST_APP_CODE'] = $generalobj->getConfigurations("configurations","TOLL_COST_APP_CODE");
			$row[0]['WALLET_MIN_BALANCE'] = $generalobj->getConfigurations("configurations","WALLET_MIN_BALANCE");
			//$row[0]['FETCH_TRIP_STATUS_TIME_INTERVAL'] = $generalobj->getConfigurations("configurations","FETCH_TRIP_STATUS_TIME_INTERVAL");
			$row[0]['FETCH_TRIP_STATUS_TIME_INTERVAL'] = fetchtripstatustimeinterval();
			$row[0]['RIDER_REQUEST_ACCEPT_TIME'] = $generalobj->getConfigurations("configurations","RIDER_REQUEST_ACCEPT_TIME");
			$row[0]['ENABLE_TOLL_COST'] = $row[0]['APP_TYPE'] != "UberX" ? $generalobj->getConfigurations("configurations","ENABLE_TOLL_COST") : "No";
			if($row[0]['APP_TYPE'] == "Ride"){
				$row[0]['FEMALE_RIDE_REQ_ENABLE'] = $generalobj->getConfigurations("configurations","FEMALE_RIDE_REQ_ENABLE");
				$row[0]['ENABLE_HAIL_RIDES']=$generalobj->getConfigurations("configurations","ENABLE_HAIL_RIDES");
				$row[0]['HANDICAP_ACCESSIBILITY_OPTION'] = $generalobj->getConfigurations("configurations","HANDICAP_ACCESSIBILITY_OPTION");
				
				}else{
				$row[0]['FEMALE_RIDE_REQ_ENABLE'] = "No";
				$row[0]['ENABLE_HAIL_RIDES'] = "No";
				$row[0]['HANDICAP_ACCESSIBILITY_OPTION'] = "No";
				// $row[0]['ENABLE_TOLL_COST'] = "No";
			}
			$user_available_balance = $generalobj->get_user_available_balance($passengerID,"Rider");
			$row[0]['user_available_balance'] = strval($generalobj->userwalletcurrency(0,$user_available_balance,$row[0]['vCurrencyPassenger']));
			
			// $row[0]['PHOTO_UPLOAD_SERVICE_ENABLE']=$PHOTO_UPLOAD_SERVICE_ENABLE;
			
			$row[0]['PHOTO_UPLOAD_SERVICE_ENABLE']=$row[0]['APP_TYPE'] == "UberX" ? $PHOTO_UPLOAD_SERVICE_ENABLE :"No";
			$row[0]['ENABLE_TIP_MODULE']=$generalobj->getConfigurations("configurations","ENABLE_TIP_MODULE");
			
			
			$host_arr = array();
			$host_arr = explode(".",$_SERVER["HTTP_HOST"]);
			$host_system = $host_arr[0];
			$parent_ufx_catid="0";
			if($host_system == "beautician"){
				$parent_ufx_catid="4";
			}
			if($host_system == "tutors"){
				$parent_ufx_catid="7";
			}
			$row[0]['UBERX_PARENT_CAT_ID'] = $parent_ufx_catid;
			if($row[0]['APP_TYPE'] == "UberX"){
				$row[0]['APP_DESTINATION_MODE'] = "None";
				$row[0]['ENABLE_TOLL_COST'] = "No";
				$row[0]['HANDICAP_ACCESSIBILITY_OPTION'] = "No";
				$row[0]['FEMALE_RIDE_REQ_ENABLE'] = "No";
				$row[0]['ENABLE_HAIL_RIDES'] = "No";
				$row[0]['ONLINE_DRIVER_LIST_UPDATE_TIME_INTERVAL']="5";
				}else{
				$row[0]['APP_DESTINATION_MODE'] = "Strict";
			}
			// $row[0]['ENABLE_DELIVERY_MODULE']=$generalobj->getConfigurations("configurations","ENABLE_DELIVERY_MODULE");
			$row[0]['ENABLE_DELIVERY_MODULE']=SITE_TYPE=="Demo"?$row[0]['eDeliverModule']:$generalobj->getConfigurations("configurations","ENABLE_DELIVERY_MODULE");
			$row[0]['PayPalConfiguration']=$row[0]['ENABLE_DELIVERY_MODULE'] == "Yes"?"Yes":$generalobj->getConfigurations("configurations","PAYMENT_ENABLED");
			
			
			// if($row[0]['ENABLE_DELIVERY_MODULE'] == "Yes"){
			// $row[0]['PayPalConfiguration'] = "Yes";
			// }
			$row[0]['CurrencyList'] = get_value('currency', '*', 'eStatus', 'Active');
			$row[0]['SITE_TYPE']=SITE_TYPE;
			$row[0]['RIIDE_LATER']=RIIDE_LATER;
			$row[0]['PROMO_CODE']=PROMO_CODE;
			$row[0]['SITE_TYPE_DEMO_MSG']=$demo_site_msg;
			$row[0]['CurrencySymbol']= get_value('currency', 'vSymbol', 'vName', $row[0]['vCurrencyPassenger'],'','true');
			$row[0]['LIST_DRIVER_LIMIT_BY_DISTANCE']=$generalobj->getConfigurations("configurations","LIST_DRIVER_LIMIT_BY_DISTANCE");
			
			$row[0]['DESTINATION_UPDATE_TIME_INTERVAL']=$generalobj->getConfigurations("configurations","DESTINATION_UPDATE_TIME_INTERVAL");
			$eUnit = getMemberCountryUnit($passengerID,"Passenger");
			$row[0]['eUnit']=$eUnit;
			$row[0]['SourceLocations']=getusertripsourcelocations($passengerID,"SourceLocation");
			$row[0]['DestinationLocations']=getusertripsourcelocations($passengerID,"DestinationLocation");
			/* fetch value */
			return $row[0];
			
			
			} else {
			
			$returnArr['Action'] ="0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
			
			echo json_encode($returnArr);exit;
			
			
		}
		
	}
	
	function getDriverDetailInfo($driverId,$fromSignIN=0){
		
		global $generalobj,$obj,$demo_site_msg,$PHOTO_UPLOAD_SERVICE_ENABLE;
		
		$where = " iDriverId = '".$driverId."'";
		$data_version['iAppVersion']="2";
		$data_version['eLogout'] = 'No';
		$obj->MySQLQueryPerform("register_driver",$data_version,'update',$where);
		
		$updateQuery = "UPDATE trip_status_messages SET eReceived='Yes' WHERE iDriverId='".$driverId."' AND eToUserType='Driver'";
		$obj->sql_query($updateQuery);
		
		$returnArr = array();
		
		$sql = "SELECT rd.*,cmp.eStatus as cmpEStatus,(SELECT dv.vLicencePlate From driver_vehicle as dv WHERE rd.iDriverVehicleId != '' AND rd.iDriverVehicleId !='0' AND dv.iDriverVehicleId = rd.iDriverVehicleId) as vLicencePlateNo FROM `register_driver` as rd,`company` as cmp WHERE rd.iDriverId='$driverId' AND cmp.iCompanyId=rd.iCompanyId";
		
		$Data = $obj->MySQLSelect($sql);
		
		if(count($Data) > 0)
		{
			
			$DRIVER_EMAIL_VERIFICATION = $generalobj->getConfigurations("configurations", "DRIVER_EMAIL_VERIFICATION");
			$DRIVER_PHONE_VERIFICATION = $generalobj->getConfigurations("configurations", "DRIVER_PHONE_VERIFICATION");
			
			if($DRIVER_EMAIL_VERIFICATION == 'No'){
				$Data[0]['eEmailVerified'] = "Yes";
			}
			
			if($DRIVER_PHONE_VERIFICATION == 'No'){
				$Data[0]['ePhoneVerified'] = "Yes";
			}
			
			
			## Check and update Device Session ID ##
			if($Data[0]['tDeviceSessionId'] == ""){
				$random = substr( md5(rand()), 0, 7);
				$Update_Device_Session['tDeviceSessionId'] = session_id().time().$random;
				$Update_Device_Session_id = $obj->MySQLQueryPerform("register_driver", $Update_Device_Session, 'update', $where);
				$Data[0]['tDeviceSessionId'] = $Update_Device_Session['tDeviceSessionId'];
			}
			## Check and update Device Session ID ##
			## Check and update Session ID ##
			if($Data[0]['tSessionId'] == ""){
				$Update_Session['tSessionId'] = session_id().time();
				$Update_Session_id = $obj->MySQLQueryPerform("register_driver", $Update_Session, 'update', $where);
				$Data[0]['tSessionId'] = $Update_Session['tSessionId'];
			}
			## Check and update Session ID ##
			// $Data[0]['Driver_Password_decrypt']= $generalobj->decrypt($Data[0]['vPassword']);
			$Data[0]['Driver_Password_decrypt']= "";
			
			if($Data[0]['vImage']!="" && $Data[0]['vImage']!="NONE"){
				$Data[0]['vImage']="3_".$Data[0]['vImage'];
			}
			
			if($Data[0]['iDriverVehicleId'] != '' && $Data[0]['iDriverVehicleId'] != '0'){
				$data_vehicle_arr=  get_value('driver_vehicle', 'iMakeId, iModelId', 'iDriverVehicleId', $Data[0]['iDriverVehicleId']);
				$Data[0]['vMake'] = get_value('make', 'vMake', 'iMakeId', $data_vehicle_arr[0]['iMakeId'],'','true');
				$Data[0]['vModel'] = get_value('model', 'vTitle', 'iModelId', $data_vehicle_arr[0]['iModelId'],'','true');
			}
			/*if($Data[0]['eStatus']!="active" || $Data[0]['cmpEStatus']!="Active"){
				
				$returnArr['Action'] ="0";
				
				if($Data[0]['cmpEStatus']!="Active"){
				$returnArr['message'] ="LBL_CONTACT_US_STATUS_NOTACTIVE_COMPANY";
				}else if($Data[0]['eStatus']=="Deleted"){
				$returnArr['message'] ="LBL_ACC_DELETE_TXT";
				}else{
				$returnArr['message']="LBL_CONTACT_US_STATUS_NOTACTIVE_DRIVER";
				}
				
				echo json_encode($returnArr);exit;
				
			}  */
			
			if($Data[0]['eStatus']=="Deleted"){
				
				$returnArr['Action'] ="0";
				$returnArr['message'] ="LBL_ACC_DELETE_TXT";
				
				echo json_encode($returnArr);exit;
			} 
			
			$TripStatus = $Data[0]['vTripStatus'];
			$Data[0]['RegistrationDate'] = date("Y-m-d", strtotime( $Data[0]['tRegistrationDate'] . ' -1 day ' ));
			if ($TripStatus != "NONE") {
				$TripID         =  $Data[0]['iTripId'];
				
				$row_result_trips = getTripPriceDetails($TripID,$driverId,"Driver");
				
				
				$Data[0]['TripDetails'] = $row_result_trips;
				$Data[0]['PassengerDetails'] = $row_result_trips['PassengerDetails'];
				
				$sql22 = "SELECT * FROM `trip_times` WHERE iTripId='$TripID'";
				$db_tripTimes = $obj->MySQLSelect($sql22);
				
				$totalSec = 0;
				$timeState = 'Pause';
				$iTripTimeId = '';
				foreach($db_tripTimes as $dtT){
					if($dtT['dPauseTime'] != '' && $dtT['dPauseTime'] != '0000-00-00 00:00:00') {
						$totalSec += strtotime($dtT['dPauseTime']) - strtotime($dtT['dResumeTime']);
						}else {
						$totalSec += strtotime(date('Y-m-d H:i:s')) - strtotime($dtT['dResumeTime']);
						$iTripTimeId = $dtT['iTripTimeId'];
						$timeState = 'Resume';
					}
				}
				
				// $diff = strtotime('2009-10-05 18:11:08') - strtotime('2009-10-05 18:07:13')
				
				$Data[0]['iTripTimeId'] = $iTripTimeId;
				$Data[0]['TotalSeconds'] = $totalSec;
				$Data[0]['TimeState'] = $timeState;
				
				$sql = "SELECT iTripId,eUserType FROM `ratings_user_driver` WHERE iTripId='$TripID'";
				$row_result_ratings = $obj->MySQLSelect($sql);
				
				if(count($row_result_ratings)>0){
					
					$count_row_rating=0;
					$ContentWritten="false";
					while(count($row_result_ratings) > $count_row_rating){
						
						$UserType=$row_result_ratings[$count_row_rating]['eUserType'];
						
						if($UserType == "Driver"){
							$ContentWritten="true";
							$Data[0]['Ratings_From_Driver']="Done";
							}else if($ContentWritten=="false"){
							$Data[0]['Ratings_From_Driver']="Not Done";
						}
						
						$count_row_rating++;
					}
					
					}else{
					
					$Data[0]['Ratings_From_Driver']="No Entry";
				}
			}
			
			$Data[0]['ABOUT_US_PAGE_DESCRIPTION']="";
			// $Data[0]['PayPalConfiguration']=$generalobj->getConfigurations("configurations","PAYMENT_ENABLED");
			$Data[0]['DefaultCurrencySign']=$generalobj->getConfigurations("configurations","DEFAULT_CURRENCY_SIGN");
			$Data[0]['DefaultCurrencyCode']=$generalobj->getConfigurations("configurations","DEFAULT_CURRENCY_CODE");
			$Data[0]['DRIVER_REFER_APP_SHARE_TXT']=$generalobj->getConfigurations("configurations","DRIVER_REFER_APP_SHARE_TXT");
			$Data[0]['MOBILE_VERIFICATION_ENABLE']=$generalobj->getConfigurations("configurations","MOBILE_VERIFICATION_ENABLE");
			$Data[0]['DRIVER_LOC_UPDATE_TIME_INTERVAL']=$generalobj->getConfigurations("configurations","DRIVER_LOC_UPDATE_TIME_INTERVAL");
			$Data[0]['ENABLE_PUBNUB']=$generalobj->getConfigurations("configurations","ENABLE_PUBNUB");
			$Data[0]['SITE_TYPE']=SITE_TYPE;
			$Data[0]['RIIDE_LATER']=RIIDE_LATER;
			$Data[0]['SITE_TYPE_DEMO_MSG']=$demo_site_msg;
			$Data[0]['vLicencePlateNo']=is_null($Data[0]['vLicencePlateNo'])== false ? $Data[0]['vLicencePlateNo']:'';
			$Data[0]['LOCATION_ACCURACY_METERS']=$generalobj->getConfigurations("configurations","LOCATION_ACCURACY_METERS");
			$Data[0]['PUBNUB_PUBLISH_KEY'] = $generalobj->getConfigurations("configurations","PUBNUB_PUBLISH_KEY");
			$Data[0]['PUBNUB_SUBSCRIBE_KEY'] = $generalobj->getConfigurations("configurations","PUBNUB_SUBSCRIBE_KEY");
			$Data[0]['PUBNUB_SECRET_KEY'] = $generalobj->getConfigurations("configurations","PUBNUB_SECRET_KEY");
			$Data[0]['REFERRAL_SCHEME_ENABLE'] = $generalobj->getConfigurations("configurations","REFERRAL_SCHEME_ENABLE");
			$Data[0]['WALLET_ENABLE'] = $generalobj->getConfigurations("configurations","WALLET_ENABLE");
			$Data[0]['APP_TYPE'] = $generalobj->getConfigurations("configurations","APP_TYPE");
			$Data[0]['APP_PAYMENT_MODE'] = $generalobj->getConfigurations("configurations","APP_PAYMENT_MODE");
			$Data[0]['SITE_POLICE_CONTROL_NUMBER']=$generalobj->getConfigurations("configurations","SITE_POLICE_CONTROL_NUMBER");
			$Data[0]['TOLL_COST_APP_ID']=$generalobj->getConfigurations("configurations","TOLL_COST_APP_ID");
			$Data[0]['TOLL_COST_APP_CODE']=$generalobj->getConfigurations("configurations","TOLL_COST_APP_CODE");
			$Data[0]['WALLET_MIN_BALANCE']=$generalobj->getConfigurations("configurations","WALLET_MIN_BALANCE");
			$Data[0]['DESTINATION_UPDATE_TIME_INTERVAL']=$generalobj->getConfigurations("configurations","DESTINATION_UPDATE_TIME_INTERVAL");
			//$Data[0]['FETCH_TRIP_STATUS_TIME_INTERVAL']=$generalobj->getConfigurations("configurations","FETCH_TRIP_STATUS_TIME_INTERVAL");
			$Data[0]['FETCH_TRIP_STATUS_TIME_INTERVAL']=fetchtripstatustimeinterval();
			$Data[0]['RIDER_REQUEST_ACCEPT_TIME']=$generalobj->getConfigurations("configurations","RIDER_REQUEST_ACCEPT_TIME");
			$Data[0]['ENABLE_TOLL_COST'] = $Data[0]['APP_TYPE'] != "UberX" ? $generalobj->getConfigurations("configurations","ENABLE_TOLL_COST") : "No";
			
			// if($Data[0]['APP_TYPE'] == "UberX"){
				// $Data[0]['WALLET_ENABLE'] = "No";
				// $Data[0]['APP_PAYMENT_MODE'] = "Cash";
			// }
			$Data[0]['STRIPE_PUBLISH_KEY']=$generalobj->getConfigurations("configurations","STRIPE_PUBLISH_KEY");
			$Data[0]['WALLET_FIXED_AMOUNT_1'] = $generalobj->getConfigurations("configurations","WALLET_FIXED_AMOUNT_1");
			$Data[0]['WALLET_FIXED_AMOUNT_2'] =$generalobj->getConfigurations("configurations","WALLET_FIXED_AMOUNT_2");
			$Data[0]['WALLET_FIXED_AMOUNT_3'] = $generalobj->getConfigurations("configurations","WALLET_FIXED_AMOUNT_3");
			// $Data[0]['APP_DESTINATION_MODE'] = $generalobj->getConfigurations("configurations","APP_DESTINATION_MODE");
			if($Data[0]['APP_TYPE'] == "UberX"){
				$Data[0]['APP_DESTINATION_MODE'] = "None";
				
				$Data[0]['ENABLE_TOLL_COST'] = "No";
				$Data[0]['HANDICAP_ACCESSIBILITY_OPTION'] = "No";
				$Data[0]['FEMALE_RIDE_REQ_ENABLE'] = "No";
				$Data[0]['ENABLE_HAIL_RIDES'] = "No";
				}else{
				$Data[0]['APP_DESTINATION_MODE'] = "Strict";
			}
			
			if($Data[0]['APP_TYPE'] == "Ride"){
				$Data[0]['FEMALE_RIDE_REQ_ENABLE'] = $generalobj->getConfigurations("configurations","FEMALE_RIDE_REQ_ENABLE");
				$Data[0]['ENABLE_HAIL_RIDES']=$generalobj->getConfigurations("configurations","ENABLE_HAIL_RIDES");
				$Data[0]['HANDICAP_ACCESSIBILITY_OPTION'] = $generalobj->getConfigurations("configurations","HANDICAP_ACCESSIBILITY_OPTION");
				// $Data[0]['ENABLE_TOLL_COST'] = $generalobj->getConfigurations("configurations","ENABLE_TOLL_COST");
				}else{
				$Data[0]['FEMALE_RIDE_REQ_ENABLE'] = "No";
				$Data[0]['ENABLE_HAIL_RIDES'] = "No";
				$Data[0]['HANDICAP_ACCESSIBILITY_OPTION'] = "No";
				// $Data[0]['ENABLE_TOLL_COST'] = "No";
			}
			// $Data[0]['PHOTO_UPLOAD_SERVICE_ENABLE'] = $PHOTO_UPLOAD_SERVICE_ENABLE;
			
			$Data[0]['PHOTO_UPLOAD_SERVICE_ENABLE']=$Data[0]['APP_TYPE'] == "UberX" ? $PHOTO_UPLOAD_SERVICE_ENABLE :"No";
			$Data[0]['ENABLE_TIP_MODULE']=$generalobj->getConfigurations("configurations","ENABLE_TIP_MODULE");
			$Data[0]['ENABLE_DELIVERY_MODULE']=SITE_TYPE=="Demo"?$Data[0]['eDeliverModule']:$generalobj->getConfigurations("configurations","ENABLE_DELIVERY_MODULE");
			$Data[0]['PayPalConfiguration']=$Data[0]['ENABLE_DELIVERY_MODULE'] == "Yes"?"Yes":$generalobj->getConfigurations("configurations","PAYMENT_ENABLED");
			// $Data[0]['CurrencyList']=($obj->MySQLSelect("SELECT * FROM currency"));
			$Data[0]['CurrencyList']=get_value('currency', '*', 'eStatus', 'Active');
			$Data[0]['UBERX_PARENT_CAT_ID'] = "0";
			$Data[0]['UBERX_SUB_CAT_ID'] = "0";
			
			$user_available_balance = $generalobj->get_user_available_balance($driverId,"Driver");
			$Data[0]['user_available_balance'] = strval($generalobj->userwalletcurrency(0,$user_available_balance,$Data[0]['vCurrencyDriver']));
			$Data[0]['CurrencySymbol']= get_value('currency', 'vSymbol', 'vName', $Data[0]['vCurrencyDriver'],'','true');
			
			$eUnit = getMemberCountryUnit($driverId,"Driver");
			$Data[0]['eUnit']=$eUnit;
			//$Data[0]['CurrentTime']= strval(date('Y-m-d'));
			
			// if($fromSignIN == 1){
			// $sql_checkLangCode = "SELECT  vCode FROM  language_master WHERE `eStatus` = 'Active' AND `eDefault` = 'Yes' ";
			// $Data_checkLangCode = $obj->MySQLSelect($sql_checkLangCode);
			
			
			// if($Data_checkLangCode[0]['vCode'] != $Data[0]['vLang']){
			// $Data[0]['changeLangCode'] ="Yes";
			// $Data[0]['UpdatedLanguageLabels'] = getLanguageLabelsArr($Data[0]['vLang'],"1");
			// }else{
			// $Data[0]['changeLangCode'] ="No";
			// }
			// }
			$str_date = @date('Y-m-d H:i:s', strtotime('-1 minutes'));
			
			$sql_request = "SELECT * FROM passenger_requests WHERE iDriverId='".$driverId."' AND dAddedDate > '".$str_date."' ";
			$data_requst = $obj->MySQLSelect($sql_request);
			
			$Data[0]['CurrentRequests'] = $data_requst;
			
			return $Data[0];
			} else {
			$returnArr['Action'] ="0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
			
			echo json_encode($returnArr);exit;
		}
	}
	
	
	
	/* function checkDistanceWithGoogleDirections($tripDistance,$startLatitude,$startLongitude,$endLatitude,$endLongitude){
		global $generalobj,$obj;
		
		$GOOGLE_API_KEY=$generalobj->getConfigurations("configurations","GOOGLE_SEVER_GCM_API_KEY");
		$url = "https://maps.googleapis.com/maps/api/directions/json?origin=".$startLatitude.",".$startLongitude."&destination=".$endLatitude.",".$endLongitude."&sensor=false&key=".$GOOGLE_API_KEY;
		try {
		$jsonfile = file_get_contents($url);
		} catch (ErrorException $ex) {
		echo "Failed";
		exit;
		}
		
		$jsondata = json_decode($jsonfile);
		$distance_google_directions=($jsondata->routes[0]->legs[0]->distance->value)/1000;
		
		$comparedDist=($distance_google_directions *85)/100;
		
		if($tripDistance>$comparedDist){
		return $tripDistance;
		}else{
		return round($distance_google_directions,2);
		}
	} */
	
	
	
	/* If no type found */
	if($type == '') {
		$result['result'] = 0;
		$result['message'] = 'Required parameter missing.';
		
		echo json_encode($result);
		exit;
	}
	
	/* function getLanguageLabelsArr($lCode = ''){
        global $obj;
		
		$sql = "SELECT  `vCode` FROM  `language_master` WHERE eStatus = 'Active' AND `eDefault` = 'Yes' ";
		$default_label = $obj->MySQLSelect($sql);
		
		if($lCode == ''){
		$lCode = (isset($default_label[0]['vCode']) && $default_label[0]['vCode'])?$default_label[0]['vCode']:'EN';
		}
		
		
        $sql = "SELECT  `vLabel` , `vValue`  FROM  `language_label`  WHERE  `vCode` = '".$lCode."' ";
        $all_label = $obj->MySQLSelect($sql);
		
        $x = array();
        for($i=0; $i<count($all_label); $i++){
		$vLabel = $all_label[$i]['vLabel'];
		
		$vValue = $all_label[$i]['vValue'];
		$x[$vLabel]=$vValue;
        }
		
		$sql = "SELECT  `vLabel` , `vValue`  FROM  `language_label_other`  WHERE  `vCode` = '".$lCode."' ";
        $all_label = $obj->MySQLSelect($sql);
		
        for($i=0; $i<count($all_label); $i++){
		$vLabel = $all_label[$i]['vLabel'];
		
		$vValue = $all_label[$i]['vValue'];
		$x[$vLabel]=$vValue;
        }
        $x['vCode'] = $lCode; // to check in which languge code it is loading
		
        return $x;
	} */
	
	/*-------------- For Luggage Lable default and as per user's Prefered language ----------------------- */
	if($type == 'language_label') {
		$lCode = isset($_REQUEST['vCode'])?clean(strtoupper($_REQUEST['vCode'])):''; // User's prefered language
		
		/* find default language of website set by admin */
		if($lCode == '') {
			$sql = "SELECT  `vCode` FROM  `language_master` WHERE eStatus = 'Active' AND `eDefault` = 'Yes' ";
			$default_label = $obj->MySQLSelect($sql);
			
			$lCode = (isset($default_label[0]['vCode']) && $default_label[0]['vCode'])?$default_label[0]['vCode']:'EN';
		}
		
		$sql = "SELECT  `vLabel` , `vValue`  FROM  `language_label`  WHERE  `vCode` = '".$lCode."' ";
		$all_label = $obj->MySQLSelect($sql);
		
		$x = array();
		for($i=0; $i<count($all_label); $i++){
			$vLabel = $all_label[$i]['vLabel'];
			
			
			$vValue = $all_label[$i]['vValue'];
			
			$x[$vLabel]=$vValue;
		}
		$x['vCode'] = $lCode; // to check in which languge code it is loading
		
		echo json_encode($x);
		exit;
	}
	##########################################################################
	## NEW WEBSERVICE START ##
	##########################################################################
	
	##########################################################################
    if($type == 'generalConfigData') {
		
		$UserType             = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
        $DataArr['LanguageLabels'] = getLanguageLabelsArr("","1");
        $DataArr['Action'] = "1";
		
		$defLangValues = get_value('language_master', 'vCode, vGMapLangCode, eDirectionCode as eType, vTitle', 'eDefault', 'Yes');
		
		$DataArr['DefaultLanguageValues']=$defLangValues[0];
		
		$defCurrencyValues = get_value('currency', 'vName, vSymbol, Ratio', 'eDefault', 'Yes');
		
		$DataArr['DefaultCurrencyValues']=$defCurrencyValues[0];
		
        if($UserType == "Passenger"){
			
			$DataArr['LINK_FORGET_PASS_PAGE_PASSENGER']=$tconfig["tsite_url"].$generalobj->getConfigurations("configurations","LINK_FORGET_PASS_PAGE_PASSENGER");
			
			$DataArr['CONFIG_CLIENT_ID']=$generalobj->getConfigurations("configurations","CONFIG_CLIENT_ID");
			
			}else{
            $DataArr['LINK_FORGET_PASS_PAGE_DRIVER']=$tconfig["tsite_url"].$generalobj->getConfigurations("configurations","LINK_FORGET_PASS_PAGE_DRIVER");
			$DataArr['LINK_SIGN_UP_PAGE_DRIVER']=$tconfig["tsite_url"].$generalobj->getConfigurations("configurations","LINK_SIGN_UP_PAGE_DRIVER");
			
		}
		$DataArr['FACEBOOK_APP_ID']=$generalobj->getConfigurations("configurations","FACEBOOK_APP_ID");
		$DataArr['LIST_CURRENCY']=get_value('currency', '*', 'eStatus', 'Active');
		$DataArr['LIST_LANGUAGES']=get_value('language_master', '*', 'eStatus', 'Active');
		$DataArr['MOBILE_VERIFICATION_ENABLE']=$generalobj->getConfigurations("configurations","MOBILE_VERIFICATION_ENABLE");
		$DataArr['GOOGLE_SENDER_ID']=$generalobj->getConfigurations("configurations","GOOGLE_SENDER_ID");
		$DataArr['REFERRAL_SCHEME_ENABLE'] = $generalobj->getConfigurations("configurations","REFERRAL_SCHEME_ENABLE");
		$DataArr['WALLET_ENABLE'] = $generalobj->getConfigurations("configurations","WALLET_ENABLE");
		$DataArr['SITE_TYPE']=SITE_TYPE;
		
		echo json_encode($DataArr);
        exit;
	}
	############################ country_list #############################
    if($type == 'countryList') {
		
        // $sql = "SELECT * FROM  `country` WHERE eStatus = 'Active' ";
        // $all_label = $obj->MySQLSelect($sql);
        // $returnArr['countryList'] = $all_label;
        // echo json_encode($returnArr);
        // exit;
		
		global $lang_label, $obj,$tconfig, $generalobj;
		
        $returnArr         = array();
		
        $counter=0;
        for($i=0;$i<26;$i++){
            $cahracter=  chr(65 + $i);
			
			$sql = "SELECT COU.* FROM country as COU WHERE COU.eStatus = 'Active' AND COU.vPhoneCode!='' AND COU.vCountryCode!='' AND COU.vCountry LIKE '$cahracter%' ORDER BY COU.vCountry";
            $db_rec = $obj->MySQLSelect($sql);
			
            if(count($db_rec) >0){
				
				$countryListArr = array();
				$subCounter=0;
				for($j=0;$j<count($db_rec);$j++){
					
					$countryListArr[$subCounter]=$db_rec[$j];
					$subCounter++;
				}
				
				if(count($countryListArr)>0){
                    $returnArr[$counter]['key'] = $cahracter;
                    $returnArr[$counter]['TotalCount'] = count($countryListArr);
                    $returnArr[$counter]['List'] = $countryListArr;
					
                    $counter++;
					
				}
			}
			
		}
		
        $countryArr['Action'] = 1;
        $countryArr['totalValues'] = count($returnArr);
        $countryArr['CountryList'] = $returnArr;
        echo json_encode($countryArr);
        exit;
	}
	
	###########################################################################
	
	if($type=="signup"){
		
		$fbid             = isset($_REQUEST["vFbId"]) ? $_REQUEST["vFbId"] : '';
		$Fname             = isset($_REQUEST["vFirstName"]) ? $_REQUEST["vFirstName"] : '';
		$Lname             = isset($_REQUEST["vLastName"]) ? $_REQUEST["vLastName"] : '';
		$email            = isset($_REQUEST["vEmail"]) ? $_REQUEST["vEmail"] : '';
		$email 			= strtolower($email);
		$phone_mobile     = isset($_REQUEST["vPhone"]) ? $_REQUEST["vPhone"] : '';
		$password         = isset($_REQUEST["vPassword"]) ? $_REQUEST["vPassword"] : '';
		$iGcmRegId        = isset($_REQUEST["vDeviceToken"]) ? $_REQUEST["vDeviceToken"] : '';
		$phoneCode = isset($_REQUEST["PhoneCode"]) ? $_REQUEST["PhoneCode"] : '';
		$CountryCode = isset($_REQUEST["CountryCode"]) ? $_REQUEST["CountryCode"] : '';
		$vInviteCode = isset($_REQUEST["vInviteCode"]) ? $_REQUEST["vInviteCode"] : '';
		$deviceType = isset($_REQUEST["vDeviceType"]) ? $_REQUEST["vDeviceType"] : 'Android';
		$vCurrency = isset($_REQUEST["vCurrency"]) ? $_REQUEST["vCurrency"] : '';
		$vLang = isset($_REQUEST["vLang"]) ? $_REQUEST["vLang"] : '';
		$user_type = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$eSignUpType = isset($_REQUEST["eSignUpType"]) ? $_REQUEST["eSignUpType"] : 'Normal';
		$vFirebaseDeviceToken = isset($_REQUEST["vFirebaseDeviceToken"]) ? $_REQUEST["vFirebaseDeviceToken"] : '';
		
		if($vCurrency=='')
		{
			$vCurrency=get_value('currency', 'vName', 'eDefault','Yes','','true');
		}
		if($vLang=='')
		{
			$vLang= get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		
		if($user_type == "Passenger"){
			$tblname = "register_user";
			$eRefType = "Rider";
			$Data_passenger['vPhoneCode']=$phoneCode;
			$Data_passenger['vCurrencyPassenger']=$vCurrency;
			$vImage = 'vImgName';
			$iMemberId = 'iUserId';
			}else{
			$tblname = "register_driver";
			$eRefType = "Driver";
			$Data_passenger['vCode']=$phoneCode;
			$Data_passenger['vCurrencyDriver'] = $vCurrency;
			$Data_passenger['iCompanyId'] = 1;
			$vImage = 'vImage';
			$iMemberId = 'iDriverId';
		} 
		
		//$sql = "SELECT * FROM `register_user` WHERE vEmail = '$email' OR vPhone = '$phone_mobile'";
		$sql    = "SELECT * FROM $tblname WHERE 1=1 AND IF('$email'!='',vEmail = '$email',0) OR IF('$phone_mobile'!='',vPhone = '$phone_mobile',0) OR IF('$fbid'!='',vFbId = '$fbid',0)";
		$check_passenger    = $obj->MySQLSelect($sql);
		
		//$Password_passenger = $generalobj->encrypt($password);
		if($password != ""){
			$Password_passenger = $generalobj->encrypt_bycrypt($password);
			}else{
			$Password_passenger = "";
		}
		
		
		if(count($check_passenger)>0){
			$returnArr['Action'] ="0";
			
			if($email==strtolower($check_passenger[0]['vEmail'])){
				$returnArr['message'] ="LBL_ALREADY_REGISTERED_TXT";
				}else {
				$returnArr['message'] ="LBL_MOBILE_EXIST";      
			}
			echo json_encode($returnArr);exit;
			}else{
			$check_inviteCode ="";
			$inviteSuccess= false;
			if($vInviteCode != ""){
				$check_inviteCode = $generalobj->validationrefercode($vInviteCode);
				if($check_inviteCode == "" || $check_inviteCode == "0" || $check_inviteCode == 0){
					$returnArr['Action'] = "0";
					$returnArr['message'] = "LBL_INVITE_CODE_INVALID";
					echo json_encode($returnArr);
					exit;
					}else{
					$inviteRes= explode("|",$check_inviteCode);
					$Data_passenger['iRefUserId'] = $inviteRes[0];
					$Data_passenger['eRefType'] = $inviteRes[1];
					$inviteSuccess = true;
				}
			}
			
			$Data_passenger['vFbId']=$fbid;
			$Data_passenger['vName']=$Fname;
			$Data_passenger['vLastName']=$Lname;
			$Data_passenger['vEmail']=$email;
			$Data_passenger['vPhone']=$phone_mobile;
			$Data_passenger['vPassword']=$Password_passenger;
			$Data_passenger['iGcmRegId']=$iGcmRegId;
			$Data_passenger['vFirebaseDeviceToken']=$vFirebaseDeviceToken;
			$Data_passenger['vLang']=$vLang;
			//$Data_passenger['vPhoneCode']=$phoneCode;
			$Data_passenger['vCountry']=$CountryCode;
			$Data_passenger['eDeviceType']=$deviceType;
			$Data_passenger['vRefCode'] = $generalobj->ganaraterefercode($eRefType);
			//$Data_passenger['vCurrencyPassenger']=$vCurrency;
			$Data_passenger['dRefDate'] =  Date('Y-m-d H:i:s');
			$Data_passenger['eSignUpType'] =$eSignUpType;
			if($eSignUpType == "Facebook" || $eSignUpType == "Google"){
				$Data_passenger['eEmailVerified'] ="Yes";
			}
			$random = substr( md5(rand()), 0, 7);
			$Data_passenger['tDeviceSessionId'] = session_id().time().$random;
			$Data_passenger['tSessionId'] = session_id().time();
			
			if(SITE_TYPE=='Demo')
			{
				$Data_passenger['eStatus'] = 'Active';
			}
			$id = $obj->MySQLQueryPerform($tblname,$Data_passenger,'insert');
			## Upload Image of Member if SignUp from Google, Facebook Or Twitter ##
			if($fbid != 0 || $fbid != ""){
				$UserImage = UploadUserImage($id,$user_type,$eSignUpType,$fbid);
				if($UserImage != ""){
					$where = " $iMemberId = '$id' ";
					$Data_update_image_member[$vImage] = $UserImage;
					$imageuploadid = $obj->MySQLQueryPerform($tblname,$Data_update_image_member,'update',$where);
				}   
			}
			## Upload Image of Member if SignUp from Google, Facebook Or Twitter ##
			
			$sql_checkLangCode = "SELECT  vCode FROM  language_master WHERE `eStatus` = 'Active' AND `eDefault` = 'Yes' ";
			$Data_checkLangCode = $obj->MySQLSelect($sql_checkLangCode);
			
			
			if($APP_TYPE == 'UberX'){
				if(strtolower($user_type)=='driver'){
					$query ="SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type`";
					$result = $obj->MySQLSelect($query);
					
					$Drive_vehicle['iDriverId'] = $id;
					$Drive_vehicle['iCompanyId'] = "1";
					$Drive_vehicle['iMakeId'] = "3";
					$Drive_vehicle['iModelId'] = "1";
					$Drive_vehicle['iYear'] = Date('Y');
					$Drive_vehicle['vLicencePlate'] = "My Services";
					$Drive_vehicle['eStatus'] = "Active";
					$Drive_vehicle['eCarX'] = "Yes";
					$Drive_vehicle['eCarGo'] = "Yes";		
					$Drive_vehicle['vCarType'] = $result[0]['countId'];
					$iDriver_VehicleId=$obj->MySQLQueryPerform('driver_vehicle',$Drive_vehicle,'insert');
					$sql = "UPDATE register_driver set iDriverVehicleId='".$iDriver_VehicleId."' WHERE iDriverId='".$id."'";
					$obj->sql_query($sql);
					
					if($ALLOW_SERVICE_PROVIDER_AMOUNT == "Yes"){
						$sql="select iVehicleTypeId,iVehicleCategoryId,eFareType,fFixedFare,fPricePerHour from vehicle_type where 1=1";
						$data_vehicles = $obj->MySQLSelect($sql);
						//echo "<pre>";print_r($data_vehicles);exit;
						
						if($data_vehicles[$i]['eFareType'] != "Regular")
						{
							for($i=0 ; $i < count($data_vehicles); $i++){
								$Data_service['iVehicleTypeId'] = $data_vehicles[$i]['iVehicleTypeId'];
								$Data_service['iDriverVehicleId'] = $iDriver_VehicleId;
								
								if($data_vehicles[$i]['eFareType'] == "Fixed"){
									$Data_service['fAmount'] = $data_vehicles[$i]['fFixedFare'];
								}
								else if($data_vehicles[$i]['eFareType'] == "Hourly"){
									$Data_service['fAmount'] = $data_vehicles[$i]['fPricePerHour'];
								}
								$data_service_amount = $obj->MySQLQueryPerform('service_pro_amount',$Data_service,'insert');
							}
						}
					}
				}
			}
			else
			{
				if(SITE_TYPE=='Demo')
				{
					$query ="SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type`";
					$result = $obj->MySQLSelect($query);
					$Drive_vehicle['iDriverId'] = $id;
					$Drive_vehicle['iCompanyId'] = "1";
					$Drive_vehicle['iMakeId'] = "5";
					$Drive_vehicle['iModelId'] = "18";
					$Drive_vehicle['iYear'] = "2014";
					$Drive_vehicle['vLicencePlate'] = "CK201";
					$Drive_vehicle['eStatus'] = "Active";
					$Drive_vehicle['eCarX'] = "Yes";
					$Drive_vehicle['eCarGo'] = "Yes";		
					$Drive_vehicle['vCarType'] = $result[0]['countId'];
					$iDriver_VehicleId=$obj->MySQLQueryPerform('driver_vehicle',$Drive_vehicle,'insert');
					$sql = "UPDATE register_driver set iDriverVehicleId='".$iDriver_VehicleId."' WHERE iDriverId='".$id."'";
					$obj->sql_query($sql);
				}
			}
			
			if ($id > 0) {
				if($inviteSuccess == true){
					$REFERRAL_AMOUNT = $generalobj->getConfigurations("configurations","REFERRAL_AMOUNT");
					$eFor = "Referrer";
					$tDescription = "Referral amount credited";
					$dDate = Date('Y-m-d H:i:s');
					$ePaymentStatus = "Unsettelled";
					//$generalobj->InsertIntoUserWallet($Data_passenger['iRefUserId'],$Data_passenger['eRefType'],$REFERRAL_AMOUNT,'Credit',0,$eFor,$tDescription,$ePaymentStatus,$dDate);
				}
				
				/*new added*/
				$returnArr['Action'] = "1";
				if($user_type == "Passenger"){
					$returnArr['message'] = getPassengerDetailInfo($id);
					}else{
					$returnArr['message'] = getDriverDetailInfo($id);
				}  
				
				echo json_encode($returnArr);
				
				$maildata['EMAIL'] = $email;
				$maildata['NAME'] = $Fname;
				$maildata['PASSWORD'] = $password;
				$maildata['SOCIALNOTES'] = '';
				
				if($user_type == "Passenger"){
	     			$generalobj->send_email_user("MEMBER_REGISTRATION_USER",$maildata);
					}else{
					$generalobj->send_email_user("DRIVER_REGISTRATION_USER",$maildata);
				}  
				} else {
				$returnArr['Action'] ="0";
				$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
				echo json_encode($returnArr);
				exit;
			}
		}
		
	}
	
	######################### isUserExist #############################
	
    if ($type == "isUserExist") {
		
        $Emid = isset($_REQUEST["Email"]) ? $_REQUEST["Email"] : '';
        $Phone = isset($_REQUEST["Phone"]) ? $_REQUEST["Phone"] : '';
        $fbid = isset($_REQUEST["fbid"]) ? $_REQUEST["fbid"] : '';
        
		/*if($fbid != ''){
			$sql    = "SELECT vEmail,vPhone,vFbId FROM `register_user` WHERE vEmail = '$Emid' OR vPhone = '$Phone' OR vFbId = '$fbid'";
    		}else{
			$sql    = "SELECT vEmail,vPhone,vFbId FROM `register_user` WHERE vEmail = '$Emid' OR vPhone = '$Phone'";
		} */
		$sql    = "SELECT vEmail,vPhone,vFbId FROM register_user WHERE 1=1 AND IF('$Emid'!='',vEmail = '$Emid',0) OR IF('$Phone'!='',vPhone = '$Phone',0) OR IF('$fbid'!='',vFbId = '$fbid',0)";
        $Data = $obj->MySQLSelect($sql);
		
        if ( count($Data) >0  )
        {
			
            $returnArr['Action']="0";
			
            if($Emid==$Data[0]['vEmail']){
				$returnArr['message'] ="LBL_ALREADY_REGISTERED_TXT";
				}else if($Phone==$Data[0]['vPhone']) {
				$returnArr['message'] ="LBL_MOBILE_EXIST";
				}else {
				$returnArr['message'] ="LBL_FACEBOOK_ACC_EXIST";
			}
		}
        else
        {
            $returnArr['Action']="1";
		}
		
        echo json_encode($returnArr);
	}
	###########################################################################
	
	if ($type == "signIn") {
		
		$Emid = isset($_REQUEST["vEmail"]) ? $_REQUEST["vEmail"] : '';
		$Emid = strtolower($Emid);
		$Password_user = isset($_REQUEST["vPassword"]) ? $_REQUEST["vPassword"] : '';
		$GCMID = isset($_REQUEST["vDeviceToken"]) ? $_REQUEST["vDeviceToken"] : '';
		$DeviceType = isset($_REQUEST["vDeviceType"]) ? $_REQUEST["vDeviceType"] : 'Android';
		$UserType   = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$vCurrency = isset($_REQUEST["vCurrency"]) ? $_REQUEST["vCurrency"] : '';
		$vLang = isset($_REQUEST["vLang"]) ? $_REQUEST["vLang"] : '';
		$vFirebaseDeviceToken = isset($_REQUEST["vFirebaseDeviceToken"]) ? $_REQUEST["vFirebaseDeviceToken"] : '';
		
		//$Password_user = $generalobj->encrypt($Password_user);
		
		if(SITE_TYPE == "Demo"){
			$tablename = ($UserType == 'Passenger')?"register_user":"register_driver"; 
			$iMemberId = ($UserType == 'Passenger')?"iUserId":"iDriverId";
			$iUserId = ($UserType == 'Passenger')?"36":"31";  
			$Member_Currency = ($UserType == 'Passenger')?"vCurrencyPassenger":"vCurrencyDriver";
			$Member_Image = ($UserType == 'Passenger')?"vImgName":"vImage";
			$Data_Update_Member['vName'] = ($UserType == 'Passenger')?"MAC":"Mark";
			$Data_Update_Member['vLastName'] = ($UserType == 'Passenger')?"ANDREW":"Bruno";
			$Data_Update_Member['vEmail'] = ($UserType == 'Passenger')?"rider@gmail.com":"driver@gmail.com";
			$Password_User = $generalobj->encrypt_bycrypt("123456");
			$Data_Update_Member['vPassword'] = $Password_User;
			$Data_Update_Member['vCountry'] = ($UserType == 'Passenger')?"US":"US";
			$Data_Update_Member['vLang'] = ($UserType == 'Passenger')?"EN":"EN";
			$Data_Update_Member['eStatus'] = ($UserType == 'Passenger')?"Active":"active";
			$Data_Update_Member[$Member_Currency] = ($UserType == 'Passenger')?"USD":"USD";
			$Data_Update_Member[$Member_Image] = ($UserType == 'Passenger')?"1504878922_81109.jpg":"1505208397_54463.jpg";
			$where = " $iMemberId = '".$iUserId."'";      
			$Update_Member_id = $obj->MySQLQueryPerform($tablename,$Data_Update_Member,'update',$where);
		}
		
		if($UserType == "Passenger"){
			$sql = "SELECT iUserId,eStatus,vLang,vTripStatus,vLang,vPassword FROM `register_user` WHERE vEmail='$Emid' OR vPhone = '$Emid'";
			$Data = $obj->MySQLSelect($sql);
			
			$iCabRequestId= get_value('cab_request_now', 'max(iCabRequestId)', 'iUserId',$Data[0]['iUserId'],'','true');
			$eStatus_cab= get_value('cab_request_now', 'eStatus', 'iCabRequestId',$iCabRequestId,'','true');
			if ( count($Data) > 0 ) {
				if($Data[0]['eStatus']=="Active"){
					# Check For Valid password #
					$hash = $Data[0]['vPassword'];
					$checkValidPass = $generalobj->check_password($Password_user, $hash);
					if($checkValidPass == 0){
						$returnArr['Action']="0";
						$returnArr['message'] ="LBL_WRONG_DETAIL";
						echo json_encode($returnArr);exit;
					}
					# Check For Valid password #
					
					$iUserId_passenger=$Data[0]['iUserId'];
					$where = " iUserId = '$iUserId_passenger' ";
					if($Data[0]['vLang'] == "" && $vLang == ""){
						$vLang= get_value('language_master', 'vCode', 'eDefault','Yes','','true');
						$Data_update_passenger['vLang']=$vLang;
					}
					if($vLang != ""){
						$Data_update_passenger['vLang']=$vLang;
						$Data[0]['vLang'] = $vLang; 
					}
					if($vCurrency != ""){
						$Data_update_passenger['vCurrencyPassenger']=$vCurrency;
					}
					if($GCMID!=''){
						$Data_update_passenger['iGcmRegId']=$GCMID;
						$Data_update_passenger['eDeviceType']=$DeviceType;
						$Data_update_passenger['tSessionId'] = session_id().time();
						$Data_update_passenger['vFirebaseDeviceToken']=$vFirebaseDeviceToken;
						if(SITE_TYPE == "Demo"){
							$Data_update_passenger['tRegistrationDate']=date('Y-m-d H:i:s');
						}
						$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
					}
					
					if($eStatus_cab == "Requesting"){
						$where1 = " iCabRequestId = '$iCabRequestId' ";
						$Data_update_cab_now['eStatus']="Cancelled";
						$id = $obj->MySQLQueryPerform("cab_request_now",$Data_update_cab_now,'update',$where1);
					}
					
					$sql_checkLangCode = "SELECT vCode FROM  language_master WHERE `eStatus` = 'Active' AND `eDefault` = 'Yes' ";
					$Data_checkLangCode = $obj->MySQLSelect($sql_checkLangCode);
					
					
					if($Data_checkLangCode[0]['vCode'] != $Data[0]['vLang']){
						$returnArr['changeLangCode'] ="Yes";
						$returnArr['UpdatedLanguageLabels'] = getLanguageLabelsArr($Data[0]['vLang'],"1");
						$returnArr['vLanguageCode'] = $Data[0]['vLang'];
						$returnArr['langType'] = get_value('language_master', 'eDirectionCode', 'vCode',$Data[0]['vLang'],'','true');
						$returnArr['vGMapLangCode'] = get_value('language_master', 'vGMapLangCode', 'vCode',$Data[0]['vLang'],'','true');
						}else{
						$returnArr['changeLangCode'] ="No";
					}
					
					$returnArr['Action'] = "1";
					$returnArr['message'] = getPassengerDetailInfo($Data[0]['iUserId'],'');
					echo json_encode($returnArr);
					
					createUserLog($UserType,"No",$Data[0]['iUserId'],"Android");
					}else{
					$returnArr['Action']="0";
  					if($Data[0]['eStatus'] !="Deleted"){
  						$returnArr['message'] ="LBL_CONTACT_US_STATUS_NOTACTIVE_PASSENGER";
						}else{
  						$returnArr['message'] ="LBL_ACC_DELETE_TXT";
					}
					echo json_encode($returnArr);
				}
			}
			else
			{
				$returnArr['Action']="0";
				$returnArr['message'] ="LBL_WRONG_DETAIL";
				echo json_encode($returnArr);
			}
			}else{
			
			//$sql = "SELECT rd.iDriverId,rd.eStatus,rd.vLang,cmp.eStatus as cmpEStatus FROM `register_driver` as rd,`company` as cmp WHERE ( rd.vEmail='$Emid' OR rd.vPhone = '$Emid' )  AND rd.vPassword='$Password_user' AND cmp.iCompanyId=rd.iCompanyId";
			$sql = "SELECT rd.iDriverId,rd.eStatus,rd.vLang,rd.vPassword,cmp.eStatus as cmpEStatus FROM `register_driver` as rd,`company` as cmp WHERE ( rd.vEmail='$Emid' OR rd.vPhone = '$Emid' ) AND cmp.iCompanyId=rd.iCompanyId";
			$Data = $obj->MySQLSelect($sql);
			
			if ( count($Data) > 0 ) {
				
				
				if($Data[0]['eStatus'] !="Deleted"){
					# Check For Valid password #
					$hash = $Data[0]['vPassword'];
					$checkValidPass = $generalobj->check_password($Password_user, $hash);
					if($checkValidPass == 0){
						$returnArr['Action']="0";
						$returnArr['message'] ="LBL_WRONG_DETAIL";
						echo json_encode($returnArr);exit;
					}
					# Check For Valid password #
					if($GCMID!=''){
						
						$iDriverId_driver=$Data[0]['iDriverId'];
						$where = " iDriverId = '$iDriverId_driver' ";
						
						if($Data[0]['vLang'] == "" && $vLang == ""){
							$vLang= get_value('language_master', 'vCode', 'eDefault','Yes','','true');
							$Data_update_driver['vLang']=$vLang;
						}
						if($vLang != ""){
							$Data_update_driver['vLang']=$vLang;
							$Data[0]['vLang'] = $vLang; 
						}
						if($vCurrency != ""){
							$Data_update_driver['vCurrencyDriver']=$vCurrency;
						}
						$Data_update_driver['vFirebaseDeviceToken']=$vFirebaseDeviceToken;
						$Data_update_driver['tSessionId'] = session_id().time();
						$Data_update_driver['iGcmRegId']=$GCMID;
						$Data_update_driver['eDeviceType']=$DeviceType;
						$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
					}
					// echo json_encode(getDriverDetailInfo($Data[0]['iDriverId'],1));
					
					$sql_checkLangCode = "SELECT  vCode FROM  language_master WHERE `eStatus` = 'Active' AND `eDefault` = 'Yes' ";
					$Data_checkLangCode = $obj->MySQLSelect($sql_checkLangCode);
					
					
					if($Data_checkLangCode[0]['vCode'] != $Data[0]['vLang']){
						$returnArr['changeLangCode'] ="Yes";
						$returnArr['UpdatedLanguageLabels'] = getLanguageLabelsArr($Data[0]['vLang'],"1");
						$returnArr['vLanguageCode'] = $Data[0]['vLang'];
						$returnArr['langType'] = get_value('language_master', 'eDirectionCode', 'vCode',$Data[0]['vLang'],'','true');
						$returnArr['vGMapLangCode'] = get_value('language_master', 'vGMapLangCode', 'vCode',$Data[0]['vLang'],'','true');
						}else{
						$returnArr['changeLangCode'] ="No";
					}
					
					$returnArr['Action'] = "1";
					$returnArr['message'] = getDriverDetailInfo($Data[0]['iDriverId'],1);
					echo json_encode($returnArr);
					
					createUserLog($UserType,"No",$Data[0]['iDriverId'],"Android");
					
					}else{
					$returnArr['Action']="0";
					$returnArr['message'] ="LBL_ACC_DELETE_TXT";
					echo json_encode($returnArr);
					exit;
				}
			}
			else
			{
				$returnArr['Action']="0";
				$returnArr['message'] ="LBL_WRONG_DETAIL";
				echo json_encode($returnArr);
				exit;
			}
		}
	}
	
	###########################################################################
	
	if ($type == "getDetail") {
		
		$iUserId  = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$GCMID = isset($_REQUEST["vDeviceToken"]) ? $_REQUEST["vDeviceToken"] : '';
		$deviceType = isset($_REQUEST["vDeviceType"]) ? $_REQUEST["vDeviceType"] : 'Android';
		$UserType   = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
		
		if($UserType == "Passenger"){
			$sql = "SELECT iGcmRegId,vTripStatus,vLang FROM `register_user` WHERE iUserId='$iUserId'";
			$Data = $obj->MySQLSelect($sql);
			
			$iCabRequestId= get_value('cab_request_now', 'max(iCabRequestId)', 'iUserId',$iUserId,'','true');
			$eStatus_cab= get_value('cab_request_now', 'eStatus', 'iCabRequestId',$iCabRequestId,'','true');
			if(count($Data)>0){
				
				## Check and update Session ID ##
				/*$where = " iUserId = '".$iUserId."'";
					$Update_Session['tSessionId'] = session_id().time();
				$Update_Session_id = $obj->MySQLQueryPerform("register_user", $Update_Session, 'update', $where);*/
				## Check and update Session ID ##
				
				$iGCMregID=$Data[0]['iGcmRegId'];
				$vTripStatus=$Data[0]['vTripStatus'];
				
				// if($GCMID!=''){
				
				// if($iGCMregID != $GCMID){
				// $where = " iUserId = '$iUserId' ";
				
				// $Data_update_passenger['iGcmRegId']=$GCMID;
				// $Data_update_passenger['eDeviceType']=$deviceType;
				
				// $id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
				// }
				
				// }
				
				
				if($GCMID != "" && $GCMID != $iGCMregID){
					$returnArr['Action'] = "0";
					$returnArr['message'] = "SESSION_OUT";
					echo json_encode($returnArr);
					exit;
				}
				
				if($Data[0]['vLang'] == ""){
					$where = " iUserId = '$iUserId' ";
					$vLang= get_value('language_master', 'vCode', 'eDefault','Yes','','true');
					$Data_update_passenger['vLang']=$vLang;
					$updateid = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
				}
				
				if($eStatus_cab == "Requesting"){
					$where = " iCabRequestId = '$iCabRequestId' ";
					
					$Data_update_cab_now['eStatus']="Cancelled";
					
					$id = $obj->MySQLQueryPerform("cab_request_now",$Data_update_cab_now,'update',$where);
				}
				
				$returnArr['Action'] = "1";
				$returnArr['message'] = getPassengerDetailInfo($iUserId,'');
				
				createUserLog($UserType,"Yes",$iUserId,"Android");
				
				}else{
				$returnArr['Action'] ="0";
				$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
			}
			
			echo json_encode($returnArr);
			}else{
			$sql = "SELECT iGcmRegId,vLang FROM `register_driver` WHERE iDriverId='$iUserId'";
			$Data = $obj->MySQLSelect($sql);
			
			if(count($Data)>0){
				
				$iGCMregID=$Data[0]['iGcmRegId'];
				
				## Check and update Session ID ##
				/*$where = " iDriverId = '$iUserId' ";
					$Update_Session['tSessionId'] = session_id().time();
				$Update_Session_id = $obj->MySQLQueryPerform("register_driver", $Update_Session, 'update', $where);  */
				## Check and update Session ID ##
				
				if($Data[0]['vLang'] == ""){
					$where = " iDriverId = '$iUserId' ";
					$vLang= get_value('language_master', 'vCode', 'eDefault','Yes','','true');
					$Data_update_driver['vLang']=$vLang;
					$updateid = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
				}
				
				// if($GCMID!=''){
				
				// if($iGCMregID!=$GCMID){
				// $where = " iDriverId = '$iUserId' ";
				
				// $Data_update_driver['iGcmRegId']=$GCMID;
				
				// $id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
				// }
				
				// }
				if($GCMID != "" && $GCMID != $iGCMregID){
					$returnArr['Action'] = "0";
					$returnArr['message'] = "SESSION_OUT";
					echo json_encode($returnArr);
					exit;
				}
				
				$returnArr['Action'] = "1";
				$returnArr['message'] = getDriverDetailInfo($iUserId);
				
				createUserLog($UserType,"Yes",$iUserId,"Android");
				
				}else{
				$returnArr['Action'] ="0";
				$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
			}
			
			echo json_encode($returnArr);
			
		}
		
		
		
	}
	
	###########################################################################
	
	if($type=="LoginWithFB"){
		
		$fbid             = isset($_REQUEST["iFBId"]) ? $_REQUEST["iFBId"] : '';
		$Fname             = isset($_REQUEST["vFirstName"]) ? $_REQUEST["vFirstName"] : '';
		$Lname             = isset($_REQUEST["vLastName"]) ? $_REQUEST["vLastName"] : '';
		$email            = isset($_REQUEST["vEmail"]) ? $_REQUEST["vEmail"] : '';
		$GCMID            = isset($_REQUEST["vDeviceToken"]) ? $_REQUEST["vDeviceToken"] : '';
		$vDeviceType            = isset($_REQUEST["vDeviceType"]) ? $_REQUEST["vDeviceType"] : 'Android';
		$eLoginType            = isset($_REQUEST["eLoginType"]) ? $_REQUEST["eLoginType"] : 'Facebook';
		$user_type   = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$vCurrency = isset($_REQUEST["vCurrency"]) ? $_REQUEST["vCurrency"] : '';
		$vLang = isset($_REQUEST["vLang"]) ? $_REQUEST["vLang"] : '';
		$vFirebaseDeviceToken = isset($_REQUEST["vFirebaseDeviceToken"]) ? $_REQUEST["vFirebaseDeviceToken"] : '';
		
		
		//$DeviceType = "Android";
		$DeviceType = $vDeviceType; 
		
		if($user_type == "Passenger"){
			$tblname = "register_user";
			$iMemberId = 'iUserId';
			$vCurrencyMember = "vCurrencyPassenger";
			$vImageFiled = 'vImgName';
			}else{
			$tblname = "register_driver";
			$iMemberId = 'iDriverId';
			$vCurrencyMember = "vCurrencyDriver";
			$vImageFiled = 'vImage';
		} 
		
		if($user_type == "Passenger"){
			$sql = "SELECT iUserId as iUserId,eStatus,vFbId,vLang,vTripStatus,eSignUpType,vImgName as vImage  FROM $tblname WHERE 1=1 AND IF('$email'!='',vEmail = '$email',0) OR IF('$fbid'!='',vFbId = '$fbid',0)";
			}else{
			$sql = "SELECT iDriverId as iUserId,eStatus,vFbId,vLang,vTripStatus,eSignUpType,vImage as vImage FROM $tblname WHERE 1=1 AND IF('$email'!='',vEmail = '$email',0) OR IF('$fbid'!='',vFbId = '$fbid',0)";
		}
		
		/*if($email != ''){
            $sql = "SELECT iUserId,eStatus,vFbId,vLang,vTripStatus FROM `register_user` WHERE vEmail='$email' OR vFbId='$fbid'";
			}else{
            $sql = "SELECT iUserId,eStatus,vFbId,vLang,vTripStatus FROM `register_user` WHERE vFbId='$fbid'";
		}   */
		$Data = $obj->MySQLSelect($sql);
		if($user_type == "Passenger"){
			$iCabRequestId= get_value('cab_request_now', 'max(iCabRequestId)', 'iUserId',$Data[0]['iUserId'],'','true');
			$eStatus_cab= get_value('cab_request_now', 'eStatus', 'iCabRequestId',$iCabRequestId,'','true');
		}
		if ( count($Data) > 0 ) {
			if($Data[0]['eStatus']=="Active" || ($user_type == "Driver" && $Data[0]['eStatus']!="Deleted")){
				
                $iUserId_passenger=$Data[0]['iUserId'];
                //$where = " iUserId = '$iUserId_passenger' ";
                $where = " $iMemberId = '$iUserId_passenger' ";
                if($Data[0]['vLang'] == "" && $vLang == ""){
					$vLang= get_value('language_master', 'vCode', 'eDefault','Yes','','true');
					$Data_update_passenger['vLang']=$vLang;
				}
                if($vLang != ""){
					$Data_update_passenger['vLang']=$vLang;
					$Data[0]['vLang'] = $vLang; 
				}
                if($vCurrency != ""){
					$Data_update_passenger[$vCurrencyMember]=$vCurrency;
				}
                
                ## Upload Image of Member if SignUp from Google, Facebook Or Twitter ##
                $vImage = $Data[0]['vImage'];
                if($fbid != 0 || $fbid != ""){
					$userid = $Data[0]['iUserId']; 
					$eSignUpType = $eLoginType;
					$UserImage = UploadUserImage($userid,$user_type,$eSignUpType,$fbid);
					if($UserImage != ""){
						$where = " $iMemberId = '$userid' ";
						$Data_update_image_member[$vImageFiled] = $UserImage;
						$imageuploadid = $obj->MySQLQueryPerform($tblname,$Data_update_image_member,'update',$where);
					}   
				}
                ## Upload Image of Member if SignUp from Google, Facebook Or Twitter ##
				
				if($GCMID!=''){
					
					$Data_update_passenger['iGcmRegId']=$GCMID;
					$Data_update_passenger['eDeviceType']=$DeviceType;
					$Data_update_passenger['vFbId']=$fbid;
					$Data_update_passenger['eSignUpType']=$eLoginType;
					$Data_update_passenger['tSessionId'] = session_id().time();
					$Data_update_passenger['vFirebaseDeviceToken']=$vFirebaseDeviceToken;
					/*if($Data[0]['vFbId'] =='' || $Data[0]['vFbId'] == "0"){
						$Data_update_passenger['vFbId']=$fbid;
					} */
					
					$id = $obj->MySQLQueryPerform($tblname,$Data_update_passenger,'update',$where);
				}
				
				
				if($user_type == "Passenger"){
					if($eStatus_cab == "Requesting"){
						$where1 = " iCabRequestId = '$iCabRequestId' ";
						$Data_update_cab_now['eStatus']="Cancelled";
						
						$id = $obj->MySQLQueryPerform("cab_request_now",$Data_update_cab_now,'update',$where1);
					}
				}  
				
				$sql_checkLangCode = "SELECT  vCode FROM  language_master WHERE `eStatus` = 'Active' AND `eDefault` = 'Yes' ";
				$Data_checkLangCode = $obj->MySQLSelect($sql_checkLangCode);
				
				
				if($Data_checkLangCode[0]['vCode'] != $Data[0]['vLang']){
					$returnArr['changeLangCode'] ="Yes";
					$returnArr['UpdatedLanguageLabels'] = getLanguageLabelsArr($Data[0]['vLang'],"1");
					$returnArr['vLanguageCode'] = $Data[0]['vLang'];
					$returnArr['langType'] = get_value('language_master', 'eDirectionCode', 'vCode',$Data[0]['vLang'],'','true');
					$returnArr['vGMapLangCode'] = get_value('language_master', 'vGMapLangCode', 'vCode',$Data[0]['vLang'],'','true');
					}else{
					$returnArr['changeLangCode'] ="No";
				}
				
				$returnArr['Action'] = "1";
				if($user_type == "Passenger"){
					$returnArr['message'] = getPassengerDetailInfo($Data[0]['iUserId'],'');
					createUserLog("Passenger","No",$Data[0]['iUserId'],"Android");
					}else{
					$returnArr['message'] = getDriverDetailInfo($Data[0]['iUserId'],'');
					createUserLog("Driver","No",$Data[0]['iUserId'],"Android");
				}  
				
				echo json_encode($returnArr);exit;
				
				}else{
				$returnArr['Action']="0";     
  				/*if($Data[0]['eStatus'] !="Deleted"){
  					$returnArr['message'] ="LBL_CONTACT_US_STATUS_NOTACTIVE_PASSENGER";
  					}else{
  					$returnArr['message'] ="LBL_ACC_DELETE_TXT";
				}*/
				if($Data[0]['eStatus'] =="Deleted"){
  					$returnArr['message'] ="LBL_ACC_DELETE_TXT";
				}
				echo json_encode($returnArr);exit;
			}
			
			}else{
			$returnArr['Action']="0";
			$returnArr['message'] ="DO_REGISTER";
			echo json_encode($returnArr);exit;
		}
	}
	
	########################### Get Available Taxi ##############################
	
	
	if ($type == "loadAvailableCab") {
		
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$passengerLat = isset($_REQUEST["PassengerLat"]) ? $_REQUEST["PassengerLat"] : '';
		$passengerLon = isset($_REQUEST["PassengerLon"]) ? $_REQUEST["PassengerLon"] : '';
		$iVehicleTypeId = isset($_REQUEST["iVehicleTypeId"]) ? $_REQUEST["iVehicleTypeId"] : '';
		$PickUpAddress = isset($_REQUEST["PickUpAddress"]) ? $_REQUEST["PickUpAddress"] : '';
		$geoCodeResult = isset($_REQUEST["currentGeoCodeResult"]) ? $_REQUEST["currentGeoCodeResult"] : '';
		$vTimeZone = isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		
		$address_data = fetch_address_geocode($PickUpAddress,$geoCodeResult);
		/*if($address_data['country_code'] == "US"){
			$address_data['city'] = "Brooklyn";
		}*/
		
		
		
		$cityId = get_value('city', 'iCityId', 'LOWER(vCity)', strtolower($address_data['city']),'',true);
		$country = get_value('country', 'iCountryId,vCountryCode', 'LOWER(vCountry)', strtolower($address_data['country']));
		$countryId = $country[0]['iCountryId'];
		$vCountryCode = $country[0]['vCountryCode'];
		$stateId = get_value('state', 'iStateId', 'LOWER(vState)', strtolower($address_data['state']),'',true);
		
		if($countryId == '') {
			$country = get_value('country', 'iCountryId', 'LOWER(vCountryCode)', strtolower($address_data['country_code']));
			$countryId = $country[0]['iCountryId'];
			$vCountryCode = $country[0]['vCountryCode'];
		}
		// echo $cityId;
		// echo "<br>";
		// echo $countryId;
		$address_data['cityId'] = $cityId;
		$address_data['countryId'] = $countryId;
		$address_data['stateId'] = $stateId;
		$address_data['PickUpAddress'] = $PickUpAddress;
		
		$DataArr = getOnlineDriverArr($passengerLat,$passengerLon,$address_data,"No");
		$Data = $DataArr['DriverList'];
		// print_r($Data);
		// die;
		
		$ALLOW_SERVICE_PROVIDER_AMOUNT = $generalobj->getConfigurations("configurations","ALLOW_SERVICE_PROVIDER_AMOUNT");
		
		$vLang=get_value('register_user', 'vLang', 'iUserId', $iUserId,'','true');
		$vCurrencyPassenger=get_value('register_user', 'vCurrencyPassenger', 'iUserId', $iUserId,'','true');
		$vCurrencySymbol=get_value('currency', 'vSymbol', 'vName', $vCurrencyPassenger,'','true');
		$i=0;
		while ( count($Data) > $i ) {
			if($Data[$i]['vImage']!="" && $Data[$i]['vImage']!="NONE"){
				$Data[$i]['vImage']="3_".$Data[$i]['vImage'];
			}
			$driverVehicleID = $Data[$i]['iDriverVehicleId'];
			
			$sql = "SELECT dv.*, make.vMake AS make_title, model.vTitle model_title FROM `driver_vehicle` dv, make, model
			WHERE dv.iMakeId = make.iMakeId
			AND dv.iModelId = model.iModelId
			AND iDriverVehicleId='$driverVehicleID'";
			$rows_driver_vehicle    = $obj->MySQLSelect($sql);
			$fAmount = 0;
			if($ALLOW_SERVICE_PROVIDER_AMOUNT == "Yes"){
				$sqlServicePro = "SELECT * FROM `service_pro_amount` WHERE iDriverVehicleId='".$rows_driver_vehicle[0]['iDriverVehicleId']."' AND iVehicleTypeId='".$iVehicleTypeId."'";
				$serviceProData = $obj->MySQLSelect($sqlServicePro);
				
				$vehicleTypeData = get_value('vehicle_type', 'eFareType,fPricePerHour,fFixedFare', 'iVehicleTypeId', $iVehicleTypeId);
				if($vehicleTypeData[0]['eFareType'] == "Fixed"){
					$fAmount = $vCurrencySymbol.$vehicleTypeData[0]['fFixedFare'];
					}else if($vehicleTypeData[0]['eFareType'] == "Hourly"){
					$fAmount = $vCurrencySymbol.$vehicleTypeData[0]['fPricePerHour']."/hour";
				}
				
				if(count($serviceProData) > 0){
					$fAmount = $serviceProData[0]['fAmount'];
					if($vehicleTypeData[0]['eFareType'] == "Fixed"){
						$fAmount = $vCurrencySymbol.$fAmount;
						}else if($vehicleTypeData[0]['eFareType'] == "Hourly"){
						$fAmount = $vCurrencySymbol.$fAmount."/hour";
					}
				}
				
				$rows_driver_vehicle[0]['fAmount'] = $fAmount;
			}
			
			$Data[$i]['DriverCarDetails'] = $rows_driver_vehicle[0];
			
			$i++;
		}
		$where = " iUserId='".$iUserId."'";
		$data['vLatitude']=$passengerLat;
		$data['vLongitude']=$passengerLon;
		$data['vRideCountry']=$vCountryCode;
		$data['tLastOnline']=@date("Y-m-d H:i:s");
		$obj->MySQLQueryPerform("register_user",$data,'update',$where);
		# Update User Location Date #
		Updateuserlocationdatetime($iUserId,"Passenger",$vTimeZone);    
		# Update User Location Date #     
		
		$returnArr['AvailableCabList'] = $Data;
		$returnArr['PassengerLat'] = $passengerLat;
		$returnArr['PassengerLon'] = $passengerLon;
		
		$sql23 = "SELECT * FROM `vehicle_type` WHERE (iCityId='".$cityId."' OR iCityId = '-1') AND (iStateId='".$stateId."' OR iStateId = '-1') AND (iCountryId='".$countryId."' OR iCountryId = '-1') ORDER BY iVehicleTypeId ASC";
		$vehicleTypes = $obj->MySQLSelect($sql23);
		
		// $vehicleTypes = get_value('vehicle_type', '*', '', '',' ORDER BY iVehicleTypeId ASC');
		
		$priceRatio=get_value('currency', 'Ratio', 'vName', $vCurrencyPassenger,'','true');
		for($i=0;$i<count($vehicleTypes);$i++){
			$vehicleTypes[$i]['fPricePerKM']= round($vehicleTypes[$i]['fPricePerKM'] * $priceRatio,2);
			$vehicleTypes[$i]['fPricePerMin']= round($vehicleTypes[$i]['fPricePerMin'] * $priceRatio,2);
			$vehicleTypes[$i]['iBaseFare']= round($vehicleTypes[$i]['iBaseFare'] * $priceRatio,2);
			$vehicleTypes[$i]['fCommision']= round($vehicleTypes[$i]['fCommision'] * $priceRatio,2);
			$vehicleTypes[$i]['iMinFare']= round($vehicleTypes[$i]['iMinFare'] * $priceRatio,2);
			$vehicleTypes[$i]['FareValue']= round($vehicleTypes[$i]['fFixedFare'] * $priceRatio,2);
			$vehicleTypes[$i]['vVehicleType']= $vehicleTypes[$i]["vVehicleType_".$vLang];
		}
		
		$returnArr['VehicleTypes'] = $vehicleTypes;
		$returnArr['CurrentCity'] = $address_data['city'];
		$returnArr['CurrentCountry'] = $address_data['country'];
		
		echo json_encode($returnArr);
	}
	
	###########################################################################
	###########################################################################
	if($type=="getDriverStates"){
		$driverId = isset($_REQUEST['iDriverId'])?clean($_REQUEST['iDriverId']):'';
		$userType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):'Driver';
		
		$docUpload = 'Yes';
		$driverVehicleUpload = 'Yes';
		$driverStateActive = 'Yes';
		
		$sql1= "SELECT dm.doc_masterid masterid, dm.doc_usertype , dm.doc_name ,dm.ex_status,dm.status, COALESCE(dl.doc_id,  '' ) as doc_id,COALESCE(dl.doc_masterid, '') as masterid_list ,COALESCE(dl.ex_date, '') as ex_date,COALESCE(dl.doc_file, '') as doc_file, COALESCE(dl.status, '') as status FROM document_master dm left join (SELECT * FROM `document_list` where doc_userid='".$driverId."' ) dl on dl.doc_masterid=dm.doc_masterid  
		where dm.doc_usertype='driver' and dm.status='Active' ";
		$db_document = $obj->MySQLSelect($sql1);
		
		if(count($db_document) > 0){
			for($i=0;$i<count($db_document);$i++){
				if($db_document[$i]['doc_file'] == ""){
					$docUpload = 'No';
				}
			}
		}
		
		if($APP_TYPE != 'UberX'){
			// echo $docUpload; die;
			$sql = "SELECT iDriverVehicleId from driver_vehicle WHERE iDriverId = '".$driverId."'";
			$db_drv_vehicle = $obj->MySQLSelect($sql);
			if(count($db_drv_vehicle) == 0){
				$driverVehicleUpload = 'No';
				}else if($driverVehicleUpload != 'No'){
				$test = array();
				# Check For Driver's selected vehicle's document are upload or not #
				$sql= "SELECT dl.*,dv.iDriverVehicleId FROM `driver_vehicle` AS dv LEFT JOIN document_list as dl ON dl.doc_userid=dv.iDriverVehicleId WHERE dv.iDriverId='$driverId'";
				$db_selected_vehicle = $obj->MySQLSelect($sql);
				if(count($db_selected_vehicle) > 0){
					for($i=0;$i<count($db_selected_vehicle);$i++){
						if($db_selected_vehicle[$i]['doc_file'] == ""){
							$test[] = '1';
						}
					}
				}
				if(count($test) == count($db_selected_vehicle)){
					$driverVehicleUpload = 'No';
				}
			}
			}else{
			$driverVehicleUpload = 'Yes';
		}
		
		$sql = "SELECT rd.eStatus as driverstatus,cmp.eStatus as cmpEStatus FROM `register_driver` as rd,`company` as cmp WHERE rd.iDriverId='".$driverId."' AND cmp.iCompanyId=rd.iCompanyId";
		$Data = $obj->MySQLSelect($sql);
		
		if(strtolower($Data[0]['driverstatus']) != "active" || strtolower($Data[0]['cmpEStatus']) != "active"){
			$driverStateActive = 'No';
		}
		
		$returnArr['Action'] = "1";
		$returnArr['IS_DOCUMENT_PROCESS_COMPLETED'] = $docUpload;
		$returnArr['IS_VEHICLE_PROCESS_COMPLETED'] = $driverVehicleUpload;
		$returnArr['IS_DRIVER_STATE_ACTIVATED'] = $driverStateActive;
		echo json_encode($returnArr);
	}
	###########################################################################
	
	
	if($type=="CheckPromoCode"){
		$promoCode = isset($_REQUEST['PromoCode'])?clean($_REQUEST['PromoCode']):'';
		$iUserId = isset($_REQUEST['iUserId'])?clean($_REQUEST['iUserId']):'';
		
		$curr_date=@date("Y-m-d");
		
		$promoCode = strtoupper($promoCode);
		//$sql = "SELECT * FROM coupon where eStatus = 'Active' AND vCouponCode = '".$promoCode."' AND iUsageLimit > iUsed AND (eValidityType = 'Permanent' OR dExpiryDate > '$curr_date')";
		//$sql = "SELECT * FROM coupon where eStatus = 'Active' AND vCouponCode = '".$promoCode."' AND iUsageLimit > iUsed ORDER BY iCouponId ASC LIMIT 0,1";
		$sql = "SELECT * FROM coupon where eStatus = 'Active' AND vCouponCode = '".$promoCode."' ORDER BY iCouponId ASC LIMIT 0,1";
		$data = $obj->MySQLSelect($sql);
		
		if(count($data)>0){
			$eValidityType = $data[0]['eValidityType'];
			$iUsageLimit = $data[0]['iUsageLimit'];
			$iUsed = $data[0]['iUsed'];
			if($iUsageLimit <= $iUsed){
				$returnArr['Action']="0";// code is invalid due to Usage Limit
				$returnArr["message"] = "LBL_PROMOCODE_COMPLETE_USAGE_LIMIT";
				echo json_encode($returnArr);exit;
			}
			if($eValidityType == "Permanent"){ 
				$returnArr['Action']="1"; // code is valid
				echo json_encode($returnArr);exit;
				}else{
				$dActiveDate = $data[0]['dActiveDate'];
				$dExpiryDate = $data[0]['dExpiryDate']; 
				if($dActiveDate <= $curr_date && $dExpiryDate >= $curr_date){
					$returnArr['Action']="1"; // code is valid
					echo json_encode($returnArr);exit;
					}else{
					$returnArr['Action']="0";// code is invalid due to expiration
					$returnArr["message"] = "LBL_PROMOCODE_EXPIRED";
					echo json_encode($returnArr);exit;
				}
			}
			}else{
			$returnArr['Action']="0";// code is invalid
			//$returnArr['Action']="01";// code is used by this user
			$returnArr["message"] = "LBL_INVALID_PROMOCODE";
			echo json_encode($returnArr);exit;
		}
		
	}
	
	###########################################################################
	
	if($type=='estimateFare'){
		$iUserId          = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$distance          = isset($_REQUEST["distance"]) ? $_REQUEST["distance"] : '';
		$time          = isset($_REQUEST["time"]) ? $_REQUEST["time"] : '';
		$SelectedCar          = isset($_REQUEST["SelectedCar"]) ? $_REQUEST["SelectedCar"] : '';
		
		$vCurrencyPassenger=get_value('register_user', 'vCurrencyPassenger', 'iUserId', $iUserId,'','true');
		$priceRatio=get_value('currency', 'Ratio', 'vName', $vCurrencyPassenger,'','true');
		
		$Fare_data=calculateFareEstimate($time,$distance,$SelectedCar,$iUserId,1);
		
		$Fare_data[0]['Distance']= $distance ==NULL ? "0" : strval(round($distance,2));
        $Fare_data[0]['Time']= $time == NULL ? "0" : strval(round($time,2));
		$Fare_data[0]['total_fare'] = number_format(round($Fare_data[0]['total_fare'] * $priceRatio,1),2);
		$Fare_data[0]['iBaseFare'] = number_format(round($Fare_data[0]['iBaseFare'] * $priceRatio,1),2);
		$Fare_data[0]['fPricePerMin'] = number_format(round($Fare_data[0]['fPricePerMin'] * $priceRatio,1),2);
		$Fare_data[0]['fPricePerKM'] = number_format(round($Fare_data[0]['fPricePerKM'] * $priceRatio,1),2);
		$Fare_data[0]['fCommision'] = number_format(round($Fare_data[0]['fCommision'] * $priceRatio,1),2);
		if($Fare_data[0]['MinFareDiff'] >0){
			$Fare_data[0]['MinFareDiff'] = number_format(round($Fare_data[0]['MinFareDiff'] * $priceRatio,1),2);
			}else{
			$Fare_data[0]['MinFareDiff'] = "0";
		}
		$Fare_data[0]['MinFareDiff'] = "0";
		$Fare_data[0]['Action'] = "1";
		
		echo json_encode($Fare_data[0]);
	}
	
	###########################################################################
	if($type=='estimateFareNew'){
		$iUserId          = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$distance          = isset($_REQUEST["distance"]) ? $_REQUEST["distance"] : '';
		$time          = isset($_REQUEST["time"]) ? $_REQUEST["time"] : '';
		$SelectedCar          = isset($_REQUEST["SelectedCar"]) ? $_REQUEST["SelectedCar"] : '';
		$StartLatitude    = isset($_REQUEST["StartLatitude"]) ? $_REQUEST["StartLatitude"] : '0.0';
		$EndLongitude    =isset($_REQUEST["EndLongitude"]) ? $_REQUEST["EndLongitude"] : '0.0';
		$iQty = isset($_REQUEST["iQty"]) ? $_REQUEST["iQty"] : '1';
		$PromoCode = isset($_REQUEST["PromoCode"]) ? $_REQUEST["PromoCode"] : ''; 
		
		$time = round(($time / 60),2);
		$distance = round(($distance / 1000),2);
		
		//$Fare_data=calculateFareEstimateAll($time,$distance,$SelectedCar,$iUserId,1);
		$Fare_data=calculateFareEstimateAll($time,$distance,$SelectedCar,$iUserId,1,"","",$PromoCode,1,0,0,0,"","Passenger",$iQty);
		
		/*$Fare_data[0]['Distance']= $distance ==NULL ? "0" : strval(round($distance,2));
			$Fare_data[0]['Time']= $time == NULL ? "0" : strval(round($time,2));
			$Fare_data[0]['total_fare'] = number_format(round($Fare_data[0]['total_fare'] * $priceRatio,1),2);
			$Fare_data[0]['iBaseFare'] = number_format(round($Fare_data[0]['iBaseFare'] * $priceRatio,1),2);
			$Fare_data[0]['fPricePerMin'] = number_format(round($Fare_data[0]['fPricePerMin'] * $priceRatio,1),2);
			$Fare_data[0]['fPricePerKM'] = number_format(round($Fare_data[0]['fPricePerKM'] * $priceRatio,1),2);
			$Fare_data[0]['fCommision'] = number_format(round($Fare_data[0]['fCommision'] * $priceRatio,1),2);
			if($Fare_data[0]['MinFareDiff'] >0){
			$Fare_data[0]['MinFareDiff'] = number_format(round($Fare_data[0]['MinFareDiff'] * $priceRatio,1),2);
			}else{
			$Fare_data[0]['MinFareDiff'] = "0";
			}
			$Fare_data[0]['MinFareDiff'] = "0";
		$Fare_data[0]['Action'] = "1";   */
		$returnArr["Action"] = "1";
		$returnArr["message"] = $Fare_data;
		echo json_encode($returnArr);
	}
	###########################################################################
	###########################################################################
	if($type=='getEstimateFareDetailsArr'){
		$iUserId          = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$distance          = isset($_REQUEST["distance"]) ? $_REQUEST["distance"] : '';
		$time          = isset($_REQUEST["time"]) ? $_REQUEST["time"] : '';
		$SelectedCar          = isset($_REQUEST["SelectedCar"]) ? $_REQUEST["SelectedCar"] : '';
		$StartLatitude    = isset($_REQUEST["StartLatitude"]) ? $_REQUEST["StartLatitude"] : '0.0';
		$EndLongitude    =isset($_REQUEST["EndLongitude"]) ? $_REQUEST["EndLongitude"] : '0.0';
		$promoCode = isset($_REQUEST['PromoCode'])?clean($_REQUEST['PromoCode']):'';
		$userType = isset($_REQUEST["UserType"]) ? $_REQUEST['UserType'] : '';
		$GeneralUserType = isset($_REQUEST['GeneralUserType']) ? trim($_REQUEST['GeneralUserType']) : '';
		if($userType == "" || $userType == NULL){
			$userType = $GeneralUserType;    
		}
		$curr_date=@date("Y-m-d");
		$time = round(($time / 60),2);
		$distance = round(($distance / 1000),2);
		$Fare_data=calculateFareEstimateAll($time,$distance,$SelectedCar,$iUserId,1,"","",$promoCode,1,0,0,0,"DisplySingleVehicleFare",$userType);
		$returnArr["Action"] = "1";
		$returnArr["message"] = $Fare_data;
		echo json_encode($returnArr);
	}
	###########################################################################
	
	if ($type == "updateUserProfileDetail") {
		
		$vName        = isset($_REQUEST["vName"]) ? $_REQUEST["vName"] : '';
		$vLastName        = isset($_REQUEST["vLastName"]) ? stripslashes($_REQUEST["vLastName"]) : '';
		$vPhone      = isset($_REQUEST["vPhone"]) ? $_REQUEST["vPhone"] : '';
		$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST['iMemberId'] : '';
		$phoneCode = isset($_REQUEST["vPhoneCode"]) ? $_REQUEST['vPhoneCode'] : '';
		$vCountry = isset($_REQUEST["vCountry"]) ? $_REQUEST['vCountry'] : '';
		$currencyCode = isset($_REQUEST["CurrencyCode"]) ? $_REQUEST['CurrencyCode'] : '';
		$languageCode = isset($_REQUEST["LanguageCode"]) ? $_REQUEST['LanguageCode'] : '';
		$userType = isset($_REQUEST["UserType"]) ? $_REQUEST['UserType'] : 'Passenger';
		$vEmail = isset($_REQUEST["vEmail"]) ? $_REQUEST['vEmail'] : '';
		$tProfileDescription = isset($_REQUEST["tProfileDescription"]) ? $_REQUEST['tProfileDescription'] : '';
		
		if($userType != "Driver"){
			$vEmail_userId_check =  get_value('register_user', 'iUserId', 'vEmail',$vEmail,'','true');
			$vPhone_userId_check =  get_value('register_user', 'iUserId', 'vPhone',$vPhone,'','true');
			
			$where = " iUserId = '$iMemberId'";
			$tableName="register_user";
			
			$Data_update_User['vPhoneCode']=$phoneCode;
			$Data_update_User['vCurrencyPassenger']=$currencyCode;
			$currentLanguageCode =  get_value('register_user', 'vLang', 'iUserId',$iMemberId,'','true');
			
			$vPhoneCode_orig =  get_value('register_user', 'vPhoneCode', 'iUserId',$iMemberId,'','true');
			$vPhone_orig =  get_value('register_user', 'vPhone', 'iUserId',$iMemberId,'','true');
			$vEmail_orig =  get_value('register_user', 'vEmail', 'iUserId',$iMemberId,'','true');
			}else{
			$vEmail_userId_check =  get_value('register_driver', 'iDriverId', 'vEmail',$vEmail,'','true');
			$vPhone_userId_check =  get_value('register_driver', 'iDriverId', 'vPhone',$vPhone,'','true');
			
			$where = " iDriverId = '$iMemberId'";
			$tableName="register_driver";
			
			$Data_update_User['vCode']=$phoneCode;
			$Data_update_User['vCurrencyDriver']=$currencyCode;
			$Data_update_User['tProfileDescription']=$tProfileDescription;
			$currentLanguageCode =  get_value('register_driver', 'vLang', 'iDriverId',$iMemberId,'','true');
			
			$vPhoneCode_orig =  get_value('register_driver', 'vCode', 'iDriverId',$iMemberId,'','true');
			$vPhone_orig =  get_value('register_driver', 'vPhone', 'iDriverId',$iMemberId,'','true');
			$vEmail_orig =  get_value('register_driver', 'vEmail', 'iDriverId',$iMemberId,'','true');
		}
		
		// $currentLanguageCode = ($obj->MySQLSelect("SELECT vLang FROM ".$tableName." WHERE".$where)[0]['vLang']);
		
		if($vEmail_userId_check != "" && $vEmail_userId_check != $iMemberId){
			$returnArr['Action']="0";
			$returnArr['message'] ="LBL_ALREADY_REGISTERED_TXT";
			echo json_encode($returnArr);exit;
		}
		if($vPhone_userId_check != "" && $vPhone_userId_check != $iMemberId){
			$returnArr['Action']="0";
			$returnArr['message'] ="LBL_MOBILE_EXIST";
			echo json_encode($returnArr);exit;
		}
		
		if($vPhone_orig != $vPhone || $vPhoneCode_orig != $phoneCode){
			$Data_update_User['ePhoneVerified'] = "No";
		}
		if($vEmail_orig != $vEmail){
			$Data_update_User['eEmailVerified'] = "No";
		}
		
		$Data_update_User['vName']=$vName;
		$Data_update_User['vLastName']=$vLastName;
		$Data_update_User['vPhone']=$vPhone;
		$Data_update_User['vCountry']=$vCountry;
		$Data_update_User['vLang']=$languageCode;
		if($vEmail != ""){
			$Data_update_User['vEmail']=$vEmail;
		}
		
		
		$id = $obj->MySQLQueryPerform($tableName,$Data_update_User,'update',$where);
		
		if($currentLanguageCode != $languageCode){
			$returnArr['changeLangCode'] ="Yes";
			$returnArr['UpdatedLanguageLabels'] = getLanguageLabelsArr($languageCode,"1");
			$returnArr['vLanguageCode'] = $languageCode;
			$returnArr['langType'] = get_value('language_master', 'eDirectionCode', 'vCode',$languageCode,'','true');
			$returnArr['vGMapLangCode'] = get_value('language_master', 'vGMapLangCode', 'vCode',$languageCode,'','true');
			}else{
			$returnArr['changeLangCode'] ="No";
		}
		if($userType != "Driver"){
			$returnArr['message'] = getPassengerDetailInfo($iMemberId,"");
			}else{
			$returnArr['message'] = getDriverDetailInfo($iMemberId);
		}
		if ($id >0) {
			$returnArr['Action']="1";
			} else {
			$returnArr['Action']="0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
		
	}
	
	###########################################################################
	
	if($type == "uploadImage"){
		global $generalobj,$tconfig;
		
		$iMemberId 	= isset($_REQUEST['iMemberId'])?clean($_REQUEST['iMemberId']):'';
		$memberType 	= isset($_REQUEST['MemberType'])?clean($_REQUEST['MemberType']):'';
		$image_name = $vImage	= isset($_FILES['vImage']['name'])?$_FILES['vImage']['name']:'';
		$image_object	= isset($_FILES['vImage']['tmp_name'])?$_FILES['vImage']['tmp_name']:'';
		$image_name = "123.jpg";
		
		if($memberType == "Driver"){
			$Photo_Gallery_folder = $tconfig['tsite_upload_images_driver_path']."/".$iMemberId."/";
			}else{
			$Photo_Gallery_folder = $tconfig['tsite_upload_images_passenger_path']."/".$iMemberId."/";
		}
		
		// echo $Photo_Gallery_folder."===";
		if(!is_dir($Photo_Gallery_folder))
		mkdir($Photo_Gallery_folder, 0777);
		
		// echo $tconfig["tsite_upload_images_member_size1"];exit;
		
		$vImageName = $generalobj->general_upload_image($image_object, $image_name, $Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"], '', '', '', 'Y', '', $Photo_Gallery_folder);
		
		if($vImageName != ''){
			if($memberType == "Driver"){
				$where = " iDriverId = '".$iMemberId."'";
				$Data_passenger['vImage']=$vImageName;
				$id = $obj->MySQLQueryPerform("register_driver",$Data_passenger,'update',$where);
				}else{
				$where = " iUserId = '".$iMemberId."'";
				$Data_passenger['vImgName']=$vImageName;
				$id = $obj->MySQLQueryPerform("register_user",$Data_passenger,'update',$where);
			}
			
			
			if($id > 0){
				$returnArr['Action']="1";
				if($memberType == "Driver"){
					$returnArr['message'] = getDriverDetailInfo($iMemberId);
					}else{
					$returnArr['message'] = getPassengerDetailInfo($iMemberId,"");
				}
				
				
				}else{
				$returnArr['Action']="0";
				$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
			}
			
			}else{
			$returnArr['Action']="0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
		
	}
	
	####################### getRideHistory #############################
	if ($type == "getRideHistory") {
  		global $generalobj;
		
  		$page        = isset($_REQUEST['page']) ? trim($_REQUEST['page']) : 1;
  		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
  		$eType = isset($_REQUEST["eType"]) ? $_REQUEST["eType"] : 'Ride';
  		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
  		$vLanguage=get_value('register_user', 'vLang', 'iUserId',$iUserId,'','true');
  		if($vLanguage == "" || $vLanguage == NULL){
  			$vLanguage = "EN";
		}
		
  		$per_page=10;
  		$sql_all  = "SELECT COUNT(iTripId) As TotalIds FROM trips WHERE  iUserId='$iUserId' AND (iActive='Canceled' || iActive='Finished')";
  		$data_count_all = $obj->MySQLSelect($sql_all);
  		$TotalPages = ceil($data_count_all[0]['TotalIds'] / $per_page);
		
  		$start_limit = ($page - 1) * $per_page;
  		$limit       = " LIMIT " . $start_limit . ", " . $per_page;
		
  		//$sql = "SELECT tripRate.vRating1 as TripRating,tr.* FROM `trips` as tr,`ratings_user_driver` as tripRate  WHERE  tr.iUserId='$iUserId' AND tr.eType='$eType' AND tripRate.iTripId=tr.iTripId AND tripRate.eUserType='$UserType' AND (tr.iActive='Canceled' || tr.iActive='Finished') ORDER BY tr.iTripId DESC" . $limit;
		$sql = "SELECT tr.* FROM `trips` as tr WHERE tr.iUserId='$iUserId' AND (tr.iActive='Canceled' || tr.iActive='Finished') ORDER BY tr.iTripId DESC" . $limit;
  		$Data = $obj->MySQLSelect($sql);
  		$totalNum = count($Data);
		
  		$i=0;
  		if ( count($Data) > 0 ) {
			
			while ( count($Data)> $i ) {
				
				$returnArr = getTripPriceDetails($Data[$i]['iTripId'],$iUserId,"Passenger");
				
				$sql = "SELECT count(iRatingId) AS Total FROM `ratings_user_driver` WHERE iTripId = '".$Data[$i]['iTripId']."' and eUserType = '$UserType'";
				$rating_check = $obj->MySQLSelect($sql);
				$returnArr['is_rating'] = 'No';
				if($rating_check[0]['Total'] > 0) {
					$returnArr['is_rating'] = 'Yes';
				}
				
				$Data[$i] = array_merge($Data[$i], $returnArr);
				$i++;
			}
			$returnData['message']=$Data;
			if ($TotalPages > $page) {
				$returnData['NextPage'] = "".($page + 1);
    			} else {
				$returnData['NextPage'] = "0";
			}
			$returnData['Action']="1";
			echo json_encode($returnData);
			
			}else{
  			$returnData['Action']="0";
  			$returnData['message']="LBL_NO_RIDES_TXT";
  			echo json_encode($returnData);
		}
		
	}
	
	/* if ($type == "getRideHistory") {
		global $generalobj;
		
		$page        = isset($_REQUEST['page']) ? trim($_REQUEST['page']) : 1;
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$eType = isset($_REQUEST["eType"]) ? $_REQUEST["eType"] : 'Ride';
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
		$vCurrencyPassenger=get_value('register_user', 'vCurrencyPassenger', 'iUserId', $iUserId,'','true');
		$currencySymbol = get_value('currency', 'vSymbol', 'vName', $vCurrencyPassenger,'','true');
		$vLanguage=get_value('register_user', 'vLang', 'iUserId',$iUserId,'','true');
		if($vLanguage == "" || $vLanguage == NULL){
		$vLanguage = "EN";
		}
		
		$per_page=10;
		$sql_all  = "SELECT COUNT(iTripId) As TotalIds FROM trips WHERE  iUserId='$iUserId' AND (iActive='Canceled' || iActive='Finished') AND eType='".$eType."'";
		$data_count_all = $obj->MySQLSelect($sql_all);
		$TotalPages = ceil($data_count_all[0]['TotalIds'] / $per_page);
		
		$start_limit = ($page - 1) * $per_page;
		$limit       = " LIMIT " . $start_limit . ", " . $per_page;
		
		$sql = "SELECT tripRate.vRating1 as TripRating,tr.* FROM `trips` as tr,`ratings_user_driver` as tripRate  WHERE  tr.iUserId='$iUserId' AND tr.eType='$eType' AND tripRate.iTripId=tr.iTripId AND tripRate.eUserType='$UserType' AND (tr.iActive='Canceled' || tr.iActive='Finished') ORDER BY tr.iTripId DESC" . $limit;
		$Data = $obj->MySQLSelect($sql);
		$totalNum = count($Data);
		
		$i=0;
		if ( count($Data) > 0 ) {
		
		$row = $Data;
		while ( count($row)> $i ) {
		
		$rows_driver_data    = array();
		$rows_driver_vehicle = array();
		$row_driver_id = $row[$i]['iDriverId'];
		$priceRatio = $row[$i]['fRatio_'.$vCurrencyPassenger];
		
		$petDetails_arr= get_value('user_pets', 'iPetTypeId,vTitle as PetName,vWeight as PetWeight, tBreed as PetBreed, tDescription as PetDescription', 'iUserPetId', $row[$i]['iUserPetId'],'','');
		
		if(count($petDetails_arr)>0){
		
		if($UserType == "Passenger"){
		$vLang = get_value('register_user', 'vLang', 'iUserId', $row[$i]['iUserId'],'','true');
		}else{
		$vLang = get_value('register_driver', 'vLang', 'iDriverId', $row[$i]['iDriverId'],'','true');
		}
		$petTypeName = get_value('pet_type', 'vTitle_'.$vLang, 'iPetTypeId', $petDetails_arr[0]['iPetTypeId'],'','true');
		$row[$i]['PetDetails']['PetName'] = $petDetails_arr[0]['PetName'];
		$row[$i]['PetDetails']['PetWeight'] = $petDetails_arr[0]['PetWeight'];
		$row[$i]['PetDetails']['PetBreed'] = $petDetails_arr[0]['PetBreed'];
		$row[$i]['PetDetails']['PetDescription'] = $petDetails_arr[0]['PetDescription'];
		$row[$i]['PetDetails']['PetTypeName'] = $petTypeName;
		}else{
		$row[$i]['PetDetails']['PetName'] = '';
		$row[$i]['PetDetails']['PetWeight'] = '';
		$row[$i]['PetDetails']['PetBreed'] = '';
		$row[$i]['PetDetails']['PetDescription'] = '';
		$row[$i]['PetDetails']['PetTypeName'] = '';
		}
		
		$row[$i]['CurrencySymbol']= $currencySymbol;
		
		$row[$i]['UserDebitAmount'] = "0";
		
		$sql = "SELECT iBalance, eType FROM `user_wallet` WHERE iTripId='".$row[$i]['iTripId']."'";
		$user_debit_data = $obj->MySQLSelect($sql);
		
		if(count($user_debit_data) > 0 && $user_debit_data[0]['eType'] == "Debit"){
		$row[$i]['UserDebitAmount'] = $user_debit_data[0]['iBalance'];
		}
		
		$vehicleType=$row[$i]['iVehicleTypeId'];
		
		$sql = "SELECT vVehicleType_".$vLanguage." as vVehicleType, iVehicleCategoryId FROM `vehicle_type`  WHERE  iVehicleTypeId='$vehicleType'";
		$vehicleType_data = $obj->MySQLSelect($sql);
		
		$row[$i]['vVehicleType']=$vehicleType_data[0]['vVehicleType'];
		$row[$i]['vVehicleCategory']=get_value('vehicle_category', 'vCategory_'.$vLanguage, 'iVehicleCategoryId',$vehicleType_data[0]['iVehicleCategoryId'],'','true');
		
		$startDate=$row[$i]['tStartDate'];
		$endDateOfTrip=$row[$i]['tEndDate'];
		
		$totalTimeInMinutes_trip=@round(abs(strtotime($startDate) - strtotime($endDateOfTrip)) / 60,2);
		
		$diff = @abs(strtotime($endDateOfTrip) - strtotime($startDate));
		$years = floor($diff / (365*60*60*24)); $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		
		// $row[$i]['TripTimeInMinutes']=$hours.':'.$minuts.':'.$seconds;
		
		// $FareOfDistance=round(($row[$i]['fPricePerKM']*$row[$i]['fDistance'])* $priceRatio,1);
		// $FareOfMinutes=($row[$i]['fPricePerMin']*$totalTimeInMinutes_trip) * $priceRatio;
		
		$row[$i]['TripFareOfMinutes']=strval(number_format(round($row[$i]['fPricePerMin']* $priceRatio,1),2));
		$row[$i]['TripFareOfDistance']=strval(number_format(round($row[$i]['fPricePerKM']* $priceRatio,1),2));
		$row[$i]['iFare']=strval(number_format(round($row[$i]['iFare'] * $priceRatio,1),2));
		
		if($row[$i]['fMinFareDiff'] > 0){
		$row[$i]['fMinFareDiff']=strval(number_format(round($row[$i]['fMinFareDiff'] * $priceRatio,1),2));
		}else{
		$row[$i]['fMinFareDiff']="0";
		}
		
		if($row[$i]['fDiscount'] != "" && $row[$i]['fDiscount'] != "0"&& $row[$i]['fDiscount'] != 0){
		$row[$i]['fDiscount']= strval(number_format(round($row[$i]['fDiscount'] * $priceRatio,1),2));
		}else{
		$row[$i]['fDiscount']= round($row[$i]['fDiscount'] * $priceRatio,1);
		}
		// $row[$i]['fDiscount']=strval(number_format(round($row[$i]['fDiscount'] * $row[$i]['fRatioPassenger'],1),2));
		$row[$i]['iBaseFare']=strval(number_format(round($row[$i]['iBaseFare'] * $priceRatio,1),2));
		$row[$i]['fCommision']= strval(number_format(round($row[$i]['fCommision']* $priceRatio,1),2));
		$row[$i]['tTripRequestDate']=date('dS M \a\t h:i a',strtotime($row[$i]['tTripRequestDate']));
		
		$totalTime=0;
		$hours= dateDifference($row[$i]['tStartDate'],$row[$i]['tEndDate'],'%h');
		$minutes= dateDifference($row[$i]['tStartDate'],$row[$i]['tEndDate'],'%i');
		$seconds= dateDifference($row[$i]['tStartDate'],$row[$i]['tEndDate'],'%s');
		
		if($hours>0){
		$totalTime = $hours*60;
		}if($minutes>0){
		$totalTime = $totalTime+$minutes;
		}
		$totalTime = $totalTime.".".$seconds;
		
		$row[$i]['TripTimeInMinutes']=$totalTime;
		
		$sql = "SELECT * FROM `register_driver` WHERE iDriverId='$row_driver_id'";
		$result_drivers = $obj->MySQLSelect($sql);
		if (count($result_drivers) > 0) {
		
		if($result_drivers[0]['vImage']!="" && $result_drivers[0]['vImage']!="NONE"){
		$result_drivers[0]['vImage']="3_".$result_drivers[0]['vImage'];
		}
		$row[$i]['DriverDetails'] = $result_drivers[0];
		
		$iDriverVehicleId = $row[$i]['iDriverVehicleId'];
		
		$sql = "SELECT make.vMake, model.vTitle, dv.*  FROM `driver_vehicle` dv, make, model WHERE dv.iDriverVehicleId='$iDriverVehicleId' AND dv.`iMakeId` = make.`iMakeId` AND dv.`iModelId` = model.`iModelId`";
		
		$row_driver_vehicle = $obj->MySQLSelect($sql);
		
		$row_driver_vehicle[0]['vModel']=$row_driver_vehicle[0]['vTitle'];
		$row[$i]['DriverCarDetails']   = $row_driver_vehicle[0];
		}
		$i++;
		}
		$returnData['message']=$row;
		if ($TotalPages > $page) {
		$returnData['NextPage'] = $page + 1;
		} else {
		$returnData['NextPage'] = "0";
		}
		$returnData['Action']="1";
		echo json_encode($returnData);
		
		}else{
		$returnData['Action']="0";
		$returnData['message']="LBL_NO_RIDES_TXT";
		echo json_encode($returnData);
		}
		
	} */
	
	###########################################################################
	
	if($type == 'staticPage') {
		$iPageId = isset($_REQUEST['iPageId'])?clean($_REQUEST['iPageId']):'';
        $iMemberId = isset($_REQUEST['iMemberId'])?clean($_REQUEST['iMemberId']):'';
        $vLangCode = isset($_REQUEST['vLangCode'])?clean($_REQUEST['vLangCode']):'';
        $appType = isset($_REQUEST['appType'])?clean($_REQUEST['appType']):''; // Passenger OR Driver
		
        $languageCode="";
        if($iMemberId != ""){
			if($appType == "Driver"){
				$languageCode = get_value('register_driver', 'vLang', 'iDriverId', $iMemberId,'','true');
				}else{
				$languageCode = get_value('register_user', 'vLang', 'iUserId', $iMemberId,'','true');
			}
			}else if($vLangCode != NULL && $vLangCode != ""){
			$check_lng = get_value('language_master', 'vTitle', 'vCode',$vLangCode,'','true');
			if($check_lng != NULL){
				$languageCode = $vLangCode;
			}
		}
		
		if($languageCode == ""){
			$languageCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
        $pageDesc = get_value('pages', 'tPageDesc_'.$languageCode, 'iPageId', $iPageId,'','true');
        // $meta['page_desc']=strip_tags($pageDesc);
        $meta['page_desc']=$pageDesc;
        echo json_encode($meta,JSON_UNESCAPED_UNICODE);
	}
	
	###########################################################################
	
	if($type=='sendContactQuery'){
		
		$UserType          = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : '';
		$UserId          = isset($_REQUEST["UserId"]) ? $_REQUEST["UserId"] : '';
		$message          = isset($_REQUEST["message"]) ? $_REQUEST["message"] : '';
		$subject          = isset($_REQUEST["subject"]) ? $_REQUEST["subject"] : '';
		
		if($UserType=='Passenger'){
			$sql="SELECT vName,vLastName,vPhone,vEmail FROM register_user WHERE iUserId=$UserId";
			
			$result_data = $obj->MySQLSelect($sql);
			
			}else if($UserType=='Driver'){
			$sql="SELECT vName,vLastName,vPhone,vEmail FROM register_driver WHERE iDriverId=$UserId";
			
			$result_data = $obj->MySQLSelect($sql);
			
		}
		
		$Data['vFirstName'] = $result_data[0]['vName'];
		$Data['vLastName'] = $result_data[0]['vLastName'];
		$Data['eSubject'] =  $subject;
		$Data['tSubject'] =  $message;
		$Data['vEmail'] = $result_data[0]['vEmail'];
		$Data['cellno'] = $result_data[0]['vPhone'];
		$id= $generalobj->send_email_user("CONTACTUS",$Data);
		
		if($id>0){
			$returnArr['Action']="1";
			$returnArr['message'] ="LBL_SENT_CONTACT_QUERY_SUCCESS_TXT";
			}else{
			$returnArr['Action']="0";
			$returnArr['message'] ="LBL_FAILED_SEND_CONTACT_QUERY_TXT";
		}
		echo json_encode($returnArr);
	}
	
	############################# GetFAQ ######################################
	if ($type == "getFAQ") {
		$status = "Active";
		
        $iMemberId = isset($_REQUEST['iMemberId'])?clean($_REQUEST['iMemberId']):'';
        $appType = isset($_REQUEST['appType'])?clean($_REQUEST['appType']):'';
		
        $languageCode="";
        if($appType == "Driver"){
            $languageCode =  get_value('register_driver', 'vLang', 'iDriverId',$iMemberId,'','true');
			}else{
			$languageCode =  get_value('register_user', 'vLang', 'iUserId',$iMemberId,'','true');
		}
		
		if($languageCode == ""){
			$languageCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		
        $sql = "SELECT * FROM `faq_categories` WHERE eStatus='$status' AND vCode='".$languageCode."' ORDER BY iDisplayOrder ASC ";
        $Data = $obj->MySQLSelect($sql);
		
        $i=0;
        if (count($Data) > 0) {
            $row = $Data;
            while (count($row)> $i) {
                $rows_questions = array();
                $iUniqueId = $row[$i]['iUniqueId'];
				
                $sql = "SELECT vTitle_" .$languageCode. " as vTitle,tAnswer_" .$languageCode. " as tAnswer FROM `faqs` WHERE iFaqcategoryId='".$iUniqueId."'";
                $row_questions = $obj->MySQLSelect($sql);
				
                $j=0;
                while (count($row_questions) > $j ) {
                    $rows_questions[$j] = $row_questions[$j];
                    $j++;
				}
                $row[$i]['Questions'] = $rows_questions;
                $i++;
			}
			
			$returnData['Action']="1";
			$returnData['message']=$row;
			}else{
			$returnData['Action']="0";
			$returnData['message']="LBL_FAQ_NOT_AVAIL";
		}
		
        echo json_encode($returnData);
	}
	###########################################################################
	
	if($type == 'getReceipt')
	{
        $iTripId = isset($_REQUEST['iTripId'])?clean($_REQUEST['iTripId']):'';
        $UserType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):''; //Passenger OR Driver
		
		$value = sendTripReceipt($iTripId);
		
		if($value == true || $value == "true" || $value == "1"){
			$returnArr['Action'] = "1";
			$returnArr['message']="LBL_CHECK_INBOX_TXT";
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_FAILED_SEND_RECEIPT_EMAIL_TXT";
		}
		echo json_encode($returnArr);
		exit;
		
	}
	
	###########################################################################
	
	if ($type == "cancelCabRequest") {
		$iUserId          = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$iCabRequestId          = isset($_REQUEST["iCabRequestId"]) ? $_REQUEST["iCabRequestId"] : '';
		
		if($iCabRequestId == ""){
			// $data = get_value('cab_request_now', 'max(iCabRequestId),eStatus', 'iUserId',$iUserId);
			$sql = "SELECT iCabRequestId, eStatus FROM cab_request_now WHERE iUserId='".$iUserId."' ORDER BY iCabRequestId DESC LIMIT 1 ";
			$data = $obj->MySQLSelect($sql);
			$iCabRequestId = $data[0]['iCabRequestId'];
			$eStatus = $data[0]['eStatus'];
			}else{
			$data = get_value('cab_request_now', 'eStatus', 'iCabRequestId',$iCabRequestId,'','true');
			$eStatus = $data[0]['eStatus'];
		}
		if($eStatus == "Requesting"){
			$where = " iCabRequestId='$iCabRequestId'";
			$Data_update_cab_request['eStatus']="Cancelled";
			
			$id = $obj->MySQLQueryPerform("cab_request_now",$Data_update_cab_request,'update',$where);
			
			
			if ($id) {
				$returnArr['Action'] = "1";
				$returnArr['message'] = "DO_RESET";
				}else{
				$returnArr['Action'] = "0";
				$returnArr['message'] = "LBL_REQUEST_CANCEL_FAILED_TXT";
			}
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message'] = "DO_RESTART";
		}
		
		echo json_encode($returnArr);
		
	}
	
	###########################################################################
	
	if ($type == "sendRequestToDrivers") {
		
		$driver_id_auto =isset($_REQUEST["driverIds"]) ? $_REQUEST["driverIds"] : '';
		$message        = isset($_REQUEST["message"]) ? $_REQUEST["message"] : '';
		$passengerId    = isset($_REQUEST["userId"]) ? $_REQUEST["userId"] : '';
		$cashPayment    =isset($_REQUEST["CashPayment"]) ? $_REQUEST["CashPayment"] : '';
		$selectedCarTypeID    = isset($_REQUEST["SelectedCarTypeID"]) ? $_REQUEST["SelectedCarTypeID"] : '';
		
		$eFemaleDriverRequest    = isset($_REQUEST["eFemaleDriverRequest"]) ? $_REQUEST["eFemaleDriverRequest"] : '';
		$eHandiCapAccessibility    = isset($_REQUEST["eHandiCapAccessibility"]) ? $_REQUEST["eHandiCapAccessibility"] : '';
		$PickUpLatitude    = isset($_REQUEST["PickUpLatitude"]) ? $_REQUEST["PickUpLatitude"] : '0.0';
		$PickUpLongitude    =isset($_REQUEST["PickUpLongitude"]) ? $_REQUEST["PickUpLongitude"] : '0.0';
		$PickUpAddress    =isset($_REQUEST["PickUpAddress"]) ? $_REQUEST["PickUpAddress"] : '';
		
		$DestLatitude    = isset($_REQUEST["DestLatitude"]) ? $_REQUEST["DestLatitude"] : '';
		$DestLongitude    =isset($_REQUEST["DestLongitude"]) ? $_REQUEST["DestLongitude"] : '';
		$DestAddress    =isset($_REQUEST["DestAddress"]) ? $_REQUEST["DestAddress"] : '';
		$promoCode    =isset($_REQUEST["PromoCode"]) ? $_REQUEST["PromoCode"] : '';
		$eType    =isset($_REQUEST["eType"]) ? $_REQUEST["eType"] : '';
		$iPackageTypeId    =isset($_REQUEST["iPackageTypeId"]) ? $_REQUEST["iPackageTypeId"] : '';
		$vReceiverName    =isset($_REQUEST["vReceiverName"]) ? $_REQUEST["vReceiverName"] : '';
		$vReceiverMobile    =isset($_REQUEST["vReceiverMobile"]) ? $_REQUEST["vReceiverMobile"] : '';      
		$tPickUpIns    =isset($_REQUEST["tPickUpIns"]) ? $_REQUEST["tPickUpIns"] : '';
		$tDeliveryIns    =isset($_REQUEST["tDeliveryIns"]) ? $_REQUEST["tDeliveryIns"] : '';
		$tPackageDetails    =isset($_REQUEST["tPackageDetails"]) ? $_REQUEST["tPackageDetails"] : '';
		$vDeviceToken    =isset($_REQUEST["vDeviceToken"]) ? $_REQUEST["vDeviceToken"] : '';
		$iUserPetId    =isset($_REQUEST["iUserPetId"]) ? $_REQUEST["iUserPetId"] : '0';
		$quantity    =isset($_REQUEST["Quantity"]) ? $_REQUEST["Quantity"] : '';
		$PickUpAddress = isset($_REQUEST["PickUpAddress"]) ? $_REQUEST["PickUpAddress"] : '';
		$fTollPrice = isset($_REQUEST["fTollPrice"]) ? $_REQUEST["fTollPrice"] : '';
		$vTollPriceCurrencyCode = isset($_REQUEST["vTollPriceCurrencyCode"]) ? $_REQUEST["vTollPriceCurrencyCode"] : '';
		$vTimeZone = isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		$eTollSkipped = isset($_REQUEST["eTollSkipped"]) ? $_REQUEST["eTollSkipped"] : 'Yes';
		$trip_status  = "Requesting";
		
		$iCabRequestId_cab_now= get_value('cab_request_now', 'max(iCabRequestId)', 'iUserId',$passengerId,'','true');
		$eStatus_cab_now= get_value('cab_request_now', 'eStatus', 'iCabRequestId',$iCabRequestId_cab_now,'','true');
		if($eStatus_cab_now == "Requesting"){
			$where_cab_now = " iCabRequestId = '$iCabRequestId_cab_now' ";
			$Data_update_cab_now['eStatus']="Cancelled";
			
			$obj->MySQLQueryPerform("cab_request_now",$Data_update_cab_now,'update',$where_cab_now);
		}
		
		checkmemberemailphoneverification($passengerId,"Passenger");
		
		$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		
		$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
		$userwaitinglabel = $languageLabelsArr['LBL_TRIP_USER_WAITING'];
		$alertMsg = $userwaitinglabel;
		
		$address_data = fetch_address_geocode($PickUpAddress);
		
		$cityId = get_value('city', 'iCityId', 'LOWER(vCity)', strtolower($address_data['city']),'',true);
		$country = get_value('country', 'iCountryId,vCountryCode', 'LOWER(vCountry)', strtolower($address_data['country']));
		$countryId = $country[0]['iCountryId'];
		$vCountryCode = $country[0]['vCountryCode'];
		$stateId = get_value('state', 'iStateId', 'LOWER(vState)', strtolower($address_data['state']),'',true);
		
		if($countryId == '') {
			$country = get_value('country', 'iCountryId,vCountryCode', 'LOWER(vCountryCode)', strtolower($address_data['country_code']));
			$countryId = $country[0]['iCountryId'];
			$vCountryCode = $country[0]['vCountryCode'];
		}
		
		$address_data['cityId'] = $cityId;
		$address_data['countryId'] = $countryId;
		$address_data['stateId'] = $stateId;
		$address_data['PickUpAddress'] = $PickUpAddress;
		$address_data['DropOffAddress'] = $DestAddress;                                                  
		
		$DataArr = getOnlineDriverArr($PickUpLatitude,$PickUpLongitude,$address_data,"Yes");
		$Data = $DataArr['DriverList'];
		if($DataArr['PickUpDisAllowed'] == "No" && $DataArr['DropOffDisAllowed'] == "No"){
			$returnArr['Action'] = "0";
  			$returnArr['message'] = "LBL_PICK_DROP_LOCATION_NOT_ALLOW";
  			echo json_encode($returnArr);
  			exit;
		}
		if($DataArr['PickUpDisAllowed'] == "Yes" && $DataArr['DropOffDisAllowed'] == "No"){
			$returnArr['Action'] = "0";
  			$returnArr['message'] = "LBL_DROP_LOCATION_NOT_ALLOW";
  			echo json_encode($returnArr);
  			exit;
		}
		if($DataArr['PickUpDisAllowed'] == "No" && $DataArr['DropOffDisAllowed'] == "Yes"){
			$returnArr['Action'] = "0";
  			$returnArr['message'] = "LBL_PICKUP_LOCATION_NOT_ALLOW";
  			echo json_encode($returnArr);
  			exit;
		}
		
		$iGcmRegId=get_value('register_user', 'iGcmRegId', 'iUserId',$passengerId,'','true');
		
		if($vDeviceToken != "" && $vDeviceToken != $iGcmRegId){
			$returnArr['Action'] = "0";
			$returnArr['message'] = "SESSION_OUT";
			echo json_encode($returnArr);
			exit;
		}
		
		$passengerFName = get_value('register_user', 'vName', 'iUserId',$passengerId,'','true');
		$passengerLName = get_value('register_user', 'vLastName', 'iUserId',$passengerId,'','true');
		$final_message['Message'] = "CabRequested";
		$final_message['sourceLatitude'] = strval($PickUpLatitude);
		$final_message['sourceLongitude'] = strval($PickUpLongitude);
		$final_message['PassengerId'] = strval($passengerId);
		$final_message['PName'] = $passengerFName. " " .$passengerLName;
		$final_message['PPicName'] = get_value('register_user', 'vImgName', 'iUserId',$passengerId,'','true');
		$final_message['PFId'] = get_value('register_user', 'vFbId', 'iUserId',$passengerId,'','true');
		$final_message['PRating'] = get_value('register_user', 'vAvgRating', 'iUserId',$passengerId,'','true');
		$final_message['PPhone'] = get_value('register_user', 'vPhone', 'iUserId',$passengerId,'','true');
		$final_message['PPhoneC'] = get_value('register_user', 'vPhoneCode', 'iUserId',$passengerId,'','true');
		$final_message['PPhone'] = '+'.$final_message['PPhoneC'].$final_message['PPhone'];
		$final_message['REQUEST_TYPE'] = $eType;
		$final_message['PACKAGE_TYPE'] = $eType == "Deliver"?get_value('package_type', 'vName', 'iPackageTypeId',$iPackageTypeId,'','true'):'';
		$final_message['destLatitude'] = strval($DestLatitude);
		$final_message['destLongitude'] = strval($DestLongitude);
		$final_message['MsgCode'] = strval(time().mt_rand(1000, 9999));
		//$final_message['Time']= strval(date('Y-m-d'));
		if($eType == "UberX"){
			$iVehicleCategoryId=get_value('vehicle_type', 'iVehicleCategoryId', 'iVehicleTypeId',$selectedCarTypeID,'','true');
			$vVehicleTypeName=get_value('vehicle_type', 'vVehicleType_'.$vLangCode, 'iVehicleTypeId',$selectedCarTypeID,'','true');
			if($iVehicleCategoryId != 0){
				$vVehicleCategoryName = get_value('vehicle_category', 'vCategory_'.$vLangCode, 'iVehicleCategoryId',$iVehicleCategoryId,'','true');
				$vVehicleTypeName = $vVehicleCategoryName . "-" . $vVehicleTypeName;
			}
			$final_message['SelectedTypeName']	= $vVehicleTypeName;
			}else{
			$final_message['SelectedTypeName'] = "";
		}
		
		
		$ePickStatus=get_value('vehicle_type', 'ePickStatus', 'iVehicleTypeId',$selectedCarTypeID,'','true');
		$eNightStatus=get_value('vehicle_type', 'eNightStatus', 'iVehicleTypeId',$selectedCarTypeID,'','true');
		
		$fPickUpPrice = 1;
		$fNightPrice = 1;
		
		$data_surgePrice = checkSurgePrice($selectedCarTypeID,"");
		
		if($data_surgePrice['Action'] == "0"){
			if($data_surgePrice['message'] == "LBL_PICK_SURGE_NOTE"){
				$fPickUpPrice=$data_surgePrice['SurgePriceValue'];
				}else{
				$fNightPrice=$data_surgePrice['SurgePriceValue'];
			}
		}
		$cmpMinutes = ceil((fetchtripstatustimeMAXinterval() + 60) / 60);
		$str_date = @date('Y-m-d H:i:s', strtotime('-'.$cmpMinutes.' minutes'));
        $sql = "SELECT iGcmRegId,eDeviceType,iDriverId FROM register_driver WHERE iDriverId IN (".$driver_id_auto.") AND tLocationUpdateDate > '$str_date' AND vAvailability='Available'";
        $result = $obj->MySQLSelect($sql);
		
		// echo "Res:count:".count($result);exit;
        if(count($result) == 0 || $driver_id_auto == "" || count($Data) == 0){
			$returnArr['Action'] = "0";
			$returnArr['message'] = "NO_CARS";
            echo json_encode($returnArr);
            exit;
		}
		
		if($cashPayment=='true'){
			$tripPaymentMode="Cash";
			}else{
			$tripPaymentMode="Card";
		}
		
		// $where = " iUserId = '$passengerId'";
		$where = "";
		
		// $Data_update_passenger['eStatus']=$trip_status;
		$Data_update_passenger['ePayType']=$tripPaymentMode;
		
		// if(($generalobj->getConfigurations("configurations","PAYMENT_ENABLED")) == 'Yes'){
		// $Data_update_passenger['vTripPaymentMode']=$tripPaymentMode;
		// }else{
		// $Data_update_passenger['vTripPaymentMode']="Cash";
		// }
		
		if($eTollSkipped=='No' || $fTollPrice != "")
		{
			$fTollPrice_Original = $fTollPrice;
			$vTollPriceCurrencyCode = strtoupper($vTollPriceCurrencyCode);
			$default_currency = get_value('currency', 'vName', 'eDefault', 'Yes','','true');
			$sql=" SELECT round(($fTollPrice/(SELECT Ratio FROM currency where vName='".$vTollPriceCurrencyCode."'))*(SELECT Ratio FROM currency where vName='".$default_currency."' ) ,2)  as price FROM currency  limit 1";
			$result_toll = $obj->MySQLSelect($sql);
			$fTollPrice = $result_toll[0]['price'];
			if($fTollPrice == 0 || $fTollPrice == "" || $fTollPrice == NULL){
				$fTollPrice = get_currency($vTollPriceCurrencyCode,$default_currency,$fTollPrice_Original);
			}
      if($fTollPrice == 0 || $fTollPrice == "" || $fTollPrice == NULL){
         $fTollPrice = $fTollPrice_Original;
      }
      
			$Data_update_passenger['fTollPrice']=$fTollPrice;
			$Data_update_passenger['vTollPriceCurrencyCode']=$vTollPriceCurrencyCode;
			$Data_update_passenger['eTollSkipped']=$eTollSkipped;
			}else{
			$Data_update_passenger['fTollPrice']="0";
			$Data_update_passenger['vTollPriceCurrencyCode']="";
			$Data_update_passenger['eTollSkipped']="No";
		}
		
		$Data_update_passenger['iUserId']=$passengerId;
		$Data_update_passenger['tMsgCode']=$final_message['MsgCode'];
		$Data_update_passenger['eStatus']='Requesting';
		$Data_update_passenger['vSourceLatitude']=$PickUpLatitude;
		$Data_update_passenger['vSourceLongitude']=$PickUpLongitude;
		$Data_update_passenger['tSourceAddress']=$PickUpAddress;
		$Data_update_passenger['vDestLatitude']=$DestLatitude;
		$Data_update_passenger['vDestLongitude']=$DestLongitude;
		$Data_update_passenger['tDestAddress']=$DestAddress;
		$Data_update_passenger['iVehicleTypeId']=$selectedCarTypeID;
		$Data_update_passenger['fPickUpPrice']=$fPickUpPrice;
		$Data_update_passenger['fNightPrice']=$fNightPrice;
		$Data_update_passenger['eType']=$eType;
		$Data_update_passenger['iPackageTypeId']= $eType == "Deliver"?$iPackageTypeId:'';
		$Data_update_passenger['vReceiverName']=$eType == "Deliver"?$vReceiverName:'';
		$Data_update_passenger['vReceiverMobile']=$eType == "Deliver"?$vReceiverMobile:'';
		$Data_update_passenger['tPickUpIns']=$eType == "Deliver"?$tPickUpIns:'';
		$Data_update_passenger['tDeliveryIns']=$eType == "Deliver"?$tDeliveryIns:'';
		$Data_update_passenger['tPackageDetails']=$eType == "Deliver"?$tPackageDetails:'';
		$Data_update_passenger['vCouponCode']=$promoCode;
		$Data_update_passenger['iQty']=$quantity;
		$Data_update_passenger['vRideCountry']=$vCountryCode;
		$Data_update_passenger['eFemaleDriverRequest']=$eFemaleDriverRequest;
		$Data_update_passenger['eHandiCapAccessibility']=$eHandiCapAccessibility;
		$Data_update_passenger['vTimeZone']=$vTimeZone;
		$Data_update_passenger['dAddedDate']=date("Y-m-d H:i:s");
		
		$insert_id = $obj->MySQLQueryPerform("cab_request_now",$Data_update_passenger,'insert');
		// $insert_id = mysql_insert_id();
		
		$final_message['iCabRequestId'] = $insert_id;
		$msg_encode  = json_encode($final_message,JSON_UNESCAPED_UNICODE);
		$ENABLE_PUBNUB = $generalobj->getConfigurations("configurations","ENABLE_PUBNUB");
		$PUBNUB_PUBLISH_KEY = $generalobj->getConfigurations("configurations","PUBNUB_PUBLISH_KEY");
		$PUBNUB_SUBSCRIBE_KEY = $generalobj->getConfigurations("configurations","PUBNUB_SUBSCRIBE_KEY");
		
		if($ENABLE_PUBNUB == "Yes" && $PUBNUB_PUBLISH_KEY != "" && $PUBNUB_SUBSCRIBE_KEY != ""){
			
			//$pubnub = new Pubnub\Pubnub($PUBNUB_PUBLISH_KEY, $PUBNUB_SUBSCRIBE_KEY);
			$pubnub = new Pubnub\Pubnub(array("publish_key" => $PUBNUB_PUBLISH_KEY,"subscribe_key" => $PUBNUB_SUBSCRIBE_KEY, "uuid" => $uuid));
			$filter_driver_ids = str_replace(' ', '', $driver_id_auto);
			$driverIds_arr = explode(",",$filter_driver_ids);
			
			$message= stripslashes(preg_replace("/[\n\r]/","",$message));
			
			$deviceTokens_arr_ios = array();
			$registation_ids_new = array();
			
			$sourceLoc = $PickUpLatitude.','.$PickUpLongitude;
			$destLoc = $DestLatitude.','.$DestLongitude;
			for($i=0;$i<count($driverIds_arr); $i++){
				// Add User Request
				$data_userRequest = array();
				$data_userRequest['iUserId'] = $passengerId;
				$data_userRequest['iDriverId'] = $driverIds_arr[$i];
				$data_userRequest['tMessage'] = $msg_encode;
				$data_userRequest['iMsgCode'] = $final_message['MsgCode'];
				$data_userRequest['dAddedDate'] = @date("Y-m-d H:i:s");
				$requestId = addToUserRequest2($data_userRequest);
				
				// Add Driver Request
				$data_driverRequest = array();
				$data_driverRequest['iDriverId'] = $driverIds_arr[$i];
				$data_driverRequest['iRequestId'] = $requestId;
				$data_driverRequest['iUserId'] = $passengerId;
				$data_driverRequest['iTripId'] = 0;
				$data_driverRequest['vMsgCode'] = $final_message['MsgCode'];
				$data_driverRequest['eStatus'] = "Timeout";
				$data_driverRequest['vStartLatlong'] = $sourceLoc;
				$data_driverRequest['vEndLatlong'] = $destLoc;
				$data_driverRequest['tStartAddress'] = $PickUpAddress;
				$data_driverRequest['tEndAddress'] = $DestAddress ;
				$data_driverRequest['tDate'] = @date("Y-m-d H:i:s");
				addToDriverRequest2($data_driverRequest);
				
				/* For PubNub Setting */
				$iAppVersion=get_value("register_driver", 'iAppVersion', "iDriverId",$driverIds_arr[$i],'','true');
				$eDeviceType=get_value("register_driver", 'eDeviceType', "iDriverId",$driverIds_arr[$i],'','true');
				$vDeviceToken=get_value("register_driver", 'iGcmRegId', "iDriverId",$driverIds_arr[$i],'','true');
				/* For PubNub Setting Finished */
				
				// if($iAppVersion > 1 && $eDeviceType == "Android"){
				
				$channelName = "CAB_REQUEST_DRIVER_".$driverIds_arr[$i];
				// $info = $pubnub->publish($channelName, $message);
				$info = $pubnub->publish($channelName, $msg_encode );
				
				// }else{
				// if($eDeviceType == "Android"){
				// array_push($registation_ids_new, $vDeviceToken);
				// }else{
				// array_push($deviceTokens_arr_ios, $vDeviceToken);
				// }
				// }
				
				if($eDeviceType != "Android"){
					array_push($deviceTokens_arr_ios, $vDeviceToken);
				}
				
			}
			
			// if(count($registation_ids_new) > 0){
			// $Rmessage = array("message" => $message);
			
			// $result = send_notification($registation_ids_new, $Rmessage,0);
			// }
			if(count($deviceTokens_arr_ios) > 0){
				// sendApplePushNotification(1,$deviceTokens_arr_ios,"",$alertMsg,0);
			}
			
			}else{
			$deviceTokens_arr_ios = array();
			$registation_ids_new = array();
			
			foreach ($result as $item) {
				if($item['eDeviceType'] == "Android"){
					array_push($registation_ids_new, $item['iGcmRegId']);
					}else{
					array_push($deviceTokens_arr_ios, $item['iGcmRegId']);
				}
				// Add User Request
				$data_userRequest = array();
				$data_userRequest['iUserId'] = $passengerId;
				$data_userRequest['iDriverId'] = $item['iDriverId'];
				$data_userRequest['tMessage'] = $msg_encode;
				$data_userRequest['iMsgCode'] = $final_message['MsgCode'];
				$data_userRequest['dAddedDate'] = @date("Y-m-d H:i:s");
				$requestId = addToUserRequest2($data_userRequest);
				
				// Add Driver Request
				$data_driverRequest = array();
				$data_driverRequest['iDriverId'] = $item['iDriverId'];
				$data_driverRequest['iRequestId'] = $requestId;
				$data_driverRequest['iUserId'] = $passengerId;
				$data_driverRequest['iTripId'] = 0;
				$data_driverRequest['eStatus'] = "Timeout";
				$data_driverRequest['vMsgCode'] = $final_message['MsgCode'];
				$data_driverRequest['vStartLatlong'] = $sourceLoc;
				$data_driverRequest['vEndLatlong'] = $destLoc;
				$data_driverRequest['tStartAddress'] = $PickUpAddress;
				$data_driverRequest['tEndAddress'] = $DestAddress ;
				$data_driverRequest['tDate'] = @date("Y-m-d H:i:s");
				addToDriverRequest2($data_driverRequest);
				// addToUserRequest($passengerId,$item['iDriverId'],$msg_encode,$final_message['MsgCode']);
				// addToDriverRequest($item['iDriverId'],$passengerId,0,"Timeout");
			}
			if(count($registation_ids_new) > 0){
				// $Rmessage = array("message" => $message);
				$Rmessage = array("message" => $msg_encode);
				
				$result = send_notification($registation_ids_new, $Rmessage,0);
				
			}
			if(count($deviceTokens_arr_ios) > 0){
				// sendApplePushNotification(1,$deviceTokens_arr_ios,$message,$alertMsg,1);
				sendApplePushNotification(1,$deviceTokens_arr_ios,$msg_encode,$alertMsg,0);
			}
		}
		
		$returnArr['Action'] = "1";
        echo json_encode($returnArr);
	}
	
	###########################################################################
	
	if ($type == "cancelTrip") {
		
		$iDriverId = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$iUserId   = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$iTripId   = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '';
		$userType   = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$driverComment   = isset($_REQUEST["Comment"]) ? $_REQUEST["Comment"] : '';
		$driverReason   = isset($_REQUEST["Reason"]) ? $_REQUEST["Reason"] : '';
		
		
		if($userType != "Driver"){
            $vTripStatus = get_value('register_user', 'vTripStatus', 'iUserId',$iUserId,'','true');
			
            if($vTripStatus != "Cancelled" && $vTripStatus != "Active" && $vTripStatus != "Arrived"){
				
				$returnArr['Action'] = "0";
				$returnArr['message'] = "DO_RESTART";
				echo json_encode($returnArr);
				exit;
			}
		}
		$tripCancelData = get_value('trips AS tr LEFT JOIN vehicle_type AS vt ON vt.iVehicleTypeId=tr.iVehicleTypeId', 'tr.vCouponCode,tr.vTripPaymentMode,tr.iUserId,tr.iFare,tr.vRideNo,tr.tTripRequestDate,vt.fCancellationFare,vt.iCancellationTimeLimit', 'iTripId',$iTripId);
		
		$currentDate =@date("Y-m-d H:i:s");
		$tTripRequestDate = $tripCancelData[0]['tTripRequestDate'];
		$fCancellationFare = 0;
		$eCancelChargeFailed = "No";
		$totalMinute=@round(abs(strtotime($currentDate) - strtotime($tTripRequestDate)) / 60,2);
		if($totalMinute >= $tripCancelData[0]['iCancellationTimeLimit'] && $userType != "Driver"){
			$fCancellationFare = $tripCancelData[0]['fCancellationFare'];
			$vTripPaymentMode = $tripCancelData[0]['vTripPaymentMode'];
			if($vTripPaymentMode == "Card" && $fCancellationFare > 0){
				$vStripeCusId = get_value('register_user', 'vStripeCusId', 'iUserId', $tripCancelData[0]['iUserId'],'','true');
				$currency = get_value('currency', 'vName', 'eDefault', 'Yes','','true');
				$price_new = $fCancellationFare * 100;
        $price_new = round($price_new);
				$description = "Payment received for cancelled trip number:".$tripCancelData[0]['vRideNo'];
				try{
					if($fCancellationFare > 0){
						$charge_create = Stripe_Charge::create(array(
						"amount" => $price_new,
						"currency" => $currency,
						"customer" => $vStripeCusId,
						"description" =>  $description
						));
						$details = json_decode($charge_create);
						$result = get_object_vars($details);
						if($fCancellationFare == 0 || ($result['status']=="succeeded" && $result['paid']=="1")){
							$pay_data['tPaymentUserID']=$result['id'];
							$pay_data['vPaymentUserStatus']="approved";
							$pay_data['iTripId']=$iTripId;
							$pay_data['iAmountUser']=$fCancellationFare;
							$obj->MySQLQueryPerform("payments",$pay_data,'insert');
							}else{
							$eCancelChargeFailed ='Yes';
						}
					}
					}catch(Exception $e){
					$error3 = $e->getMessage();
					$eCancelChargeFailed ='Yes';
				}
			}
		}
		$active_status  = "Canceled";
		if($userType != "Driver"){
			$message  = "TripCancelled";
			}else{
			$message  = "TripCancelledByDriver";
		}
		
		$couponCode=$tripCancelData[0]['vCouponCode'];
		
		if($couponCode != ''){
			$noOfCouponUsed = get_value('coupon', 'iUsed', 'vCouponCode', $couponCode,'','true');
			
			$where = " vCouponCode = '".$couponCode."'";
			$data_coupon['iUsed']=$noOfCouponUsed - 1;
			$obj->MySQLQueryPerform("coupon",$data_coupon,'update',$where);
		}
		
        $statusUpdate_user = "Not Assigned";
        $trip_status       = "Cancelled";
		
		$sql = "SELECT CONCAT(rd.vName,' ',rd.vLastName) AS driverName, tr.vRideNo FROM trips as tr,register_driver as rd WHERE tr.iTripId=rd.iTripId AND rd.iDriverId = '".$iDriverId."'";
		$result = $obj->MySQLSelect($sql);
		
		$message_arr = array();
        $message_arr['Message'] = $message;
		if($userType == "Driver"){
			$message_arr['Reason'] = $driverReason;
			$message_arr['isTripStarted'] = "false";
		}
        $message_arr['iTripId'] = $iTripId;
        $message_arr['iUserId'] = $iUserId;
		$message_arr['driverName'] = $result[0]['driverName'];
		$message_arr['vRideNo'] = $result[0]['vRideNo'];
		
        $message = json_encode($message_arr,JSON_UNESCAPED_UNICODE);
		
		#####################Add Status Message#########################
		$DataTripMessages['tMessage']= $message;
		$DataTripMessages['iDriverId']= $iDriverId;
		$DataTripMessages['iTripId']= $iTripId;
		$DataTripMessages['iUserId']= $iUserId;
		if($userType != "Driver"){
			$DataTripMessages['eFromUserType']= "Passenger";
			$DataTripMessages['eToUserType']= "Driver";
			}else{
			$DataTripMessages['eFromUserType']= "Driver";
			$DataTripMessages['eToUserType']= "Passenger";
		}
		$DataTripMessages['eReceived']= "No";
		$DataTripMessages['dAddedDate']= @date("Y-m-d H:i:s");
		
		$obj->MySQLQueryPerform("trip_status_messages",$DataTripMessages,'insert');
		################################################################
		
		$where = " iTripId = '$iTripId'";
		$Data_update_trips['iActive']=$active_status;
		$Data_update_trips['tEndDate']=@date("Y-m-d H:i:s");
		if($vTripPaymentMode == "Card" && $fCancellationFare > 0){
			$Data_update_trips['eCancelChargeFailed']=$eCancelChargeFailed;
			$Data_update_trips['fCancellationFare']=$fCancellationFare;
		}
		
		$Data_update_trips['eCancelledBy']=$userType;
		if($userType == "Driver"){
			$Data_update_trips['vCancelReason'] = $driverReason;
			$Data_update_trips['vCancelComment'] = $driverComment;
			$Data_update_trips['eCancelled'] = "Yes";
		}
		
		$id = $obj->MySQLQueryPerform("trips",$Data_update_trips,'update',$where);
		
		
		$where = " iUserId = '$iUserId'";
		$Data_update_passenger['vCallFromDriver']=$statusUpdate_user;
		$Data_update_passenger['vTripStatus']=$trip_status;
		
		$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
		
		
		$where = " iDriverId='$iDriverId'";
		// $Data_update_driver['iTripId']=$statusUpdate_user;
		$Data_update_driver['vTripStatus']=$trip_status;
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		
		
		/* For PubNub Setting */
		$tableName = $userType != "Driver"?"register_driver":"register_user";
		$iMemberId_VALUE = $userType != "Driver"?$iDriverId:$iUserId;
		$iMemberId_KEY = $userType != "Driver"?"iDriverId":"iUserId";
		$iAppVersion=get_value($tableName, 'iAppVersion', $iMemberId_KEY,$iMemberId_VALUE,'','true');
		$eDeviceType=get_value($tableName, 'eDeviceType', $iMemberId_KEY,$iMemberId_VALUE,'','true');
		/* For PubNub Setting Finished */
		
		
		$ENABLE_PUBNUB = $generalobj->getConfigurations("configurations","ENABLE_PUBNUB");
		$PUBNUB_PUBLISH_KEY = $generalobj->getConfigurations("configurations","PUBNUB_PUBLISH_KEY");
		$PUBNUB_SUBSCRIBE_KEY = $generalobj->getConfigurations("configurations","PUBNUB_SUBSCRIBE_KEY");
		
		$alertMsg = "Trip canceled";
		
		$vLangCode=get_value($tableName, 'vLang', $iMemberId_KEY,$iMemberId_VALUE,'','true');
        if($vLangCode == "" || $vLangCode == NULL){
			$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		
        $languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
		if($userType == "Driver"){
			$usercanceltriplabel = $languageLabelsArr['LBL_PREFIX_TRIP_CANCEL_DRIVER'].' '.$driverReason.' '.$languageLabelsArr['LBL_CANCEL_TRIP_BY_DRIVER_MSG_SUFFIX'];
		}else{
			$usercanceltriplabel = $languageLabelsArr['LBL_PASSENGER_CANCEL_TRIP_TXT'];
		}
		$alertMsg = $usercanceltriplabel;
		
		if($ENABLE_PUBNUB == "Yes"  && $PUBNUB_PUBLISH_KEY != "" && $PUBNUB_SUBSCRIBE_KEY != ""/*  && $iAppVersion > 1 && $eDeviceType == "Android" */){
			
			//$pubnub = new Pubnub\Pubnub($PUBNUB_PUBLISH_KEY, $PUBNUB_SUBSCRIBE_KEY);
			$pubnub = new Pubnub\Pubnub(array("publish_key" => $PUBNUB_PUBLISH_KEY,"subscribe_key" => $PUBNUB_SUBSCRIBE_KEY, "uuid" => $uuid));
			
			if($userType!="Driver"){
				$channelName = "DRIVER_".$iDriverId;
				}else{
				$channelName = "PASSENGER_".$iUserId;
			}
			
			$info = $pubnub->publish($channelName, $message);
			
		}
		
		if($userType !="Driver"){
			$sql = "SELECT iGcmRegId,eDeviceType,tLocationUpdateDate FROM register_driver WHERE iDriverId IN (".$iDriverId.")";
		}else{
			$sql = "SELECT iGcmRegId,eDeviceType,tLocationUpdateDate FROM register_user WHERE iUserId IN (".$iUserId.")";
		}
		
		$result = $obj->MySQLSelect($sql);
		
		$deviceTokens_arr_ios = array();
		$registation_ids_new = array();
		
		foreach ($result as $item) {
			if($item['eDeviceType'] == "Android"){
				array_push($registation_ids_new, $item['iGcmRegId']);
				}else{
				array_push($deviceTokens_arr_ios, $item['iGcmRegId']);
			}
		}
		
		$cmpMinutes = ceil((fetchtripstatustimeMAXinterval() + 60) / 60);
		$compare_date = @date('Y-m-d H:i:s', strtotime('-'.$cmpMinutes.' minutes'));

		$alertSendAllowed = false;
		
		if($ENABLE_PUBNUB == "Yes" && $PUBNUB_PUBLISH_KEY != "" && $PUBNUB_SUBSCRIBE_KEY != ""){
			$message = $alertMsg;
			$tLocUpdateDate = date("Y-m-d H:i:s",strtotime($result[0]['tLocationUpdateDate']));
			
			if($tLocUpdateDate < $compare_date){
				$alertSendAllowed = true;
			}
		}else{
			$alertSendAllowed = true;
		}
		
		if($alertSendAllowed == true){
			if(count($registation_ids_new) > 0){
				$Rmessage = array("message" => $message);
				
				$result = send_notification($registation_ids_new, $Rmessage,0);
			}
			if(count($deviceTokens_arr_ios) > 0){
				
				if($userType == "Driver"){
					sendApplePushNotification(0,$deviceTokens_arr_ios,$message,$alertMsg,0);
					}else{
					sendApplePushNotification(1,$deviceTokens_arr_ios,$message,$alertMsg,0);
				}
				
			}
		}
		
		
		// Code for Check last logout date is update in driver_log_report
		
		$driverId_log=get_value('trips', 'iDriverId', 'iTripId',$iTripId,'','true');
		$query = "SELECT * FROM driver_log_report WHERE iDriverId = '".$driverId_log."' ORDER BY iDriverLogId DESC LIMIT 0,1";
		$db_driver = $obj->MySQLSelect($query);
		if(count($db_driver) > 0) {
			$driver_lastonline = @date("Y-m-d H:i:s");
			$updateQuery = "UPDATE driver_log_report set dLogoutDateTime='".$driver_lastonline."' WHERE iDriverLogId = ".$db_driver[0]['iDriverLogId'];
			$obj->sql_query($updateQuery);
		}
		// Code for Check last logout date is update in driver_log_report Ends
		
		$returnArr['Action'] = "1";
        echo json_encode($returnArr);
		
	}
	
	###########################################################################
	
	if($type=="addDestination"){
		
		$userId     = isset($_REQUEST["UserId"]) ? $_REQUEST["UserId"] : '';
		$Latitude     = isset($_REQUEST["Latitude"]) ? $_REQUEST["Latitude"] : '';
		$Longitude     = isset($_REQUEST["Longitude"]) ? $_REQUEST["Longitude"] : '';
		$Address     = isset($_REQUEST["Address"]) ? $_REQUEST["Address"] : '';
		$userType     = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : '';
		$iDriverId     = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		
		if($userType !="Driver"){
			$sql = "SELECT ru.iTripId,tr.iDriverId,rd.vTripStatus as driverStatus,rd.iGcmRegId as regId,rd.eDeviceType as deviceType FROM register_user as ru,trips as tr,register_driver as rd WHERE ru.iUserId='$userId' AND tr.iTripId=ru.iTripId AND rd.iDriverId=tr.iDriverId";
			}else{
			$sql = "SELECT rd.iTripId,rd.vTripStatus as driverStatus,ru.iGcmRegId as regId,ru.eDeviceType as deviceType FROM trips as tr,register_driver as rd ,register_user as ru WHERE ru.iUserId='$userId' AND rd.iDriverId='$iDriverId'";
		}
		
		$data = $obj->MySQLSelect($sql);
		
		if(count($data) > 0){
			$driverStatus = $data[0]['driverStatus'];
			
			$where_trip = " iTripId = '".$data[0]['iTripId']."'";
			$Data_trips['tEndLat']=$Latitude;
			$Data_trips['tEndLong']=$Longitude;
			$Data_trips['tDaddress']=$Address;
			$id = $obj->MySQLQueryPerform("trips",$Data_trips,'update',$where_trip);
			
			if($driverStatus == "Active"){
				
				$where_passenger = " iUserId = '$userId'";
				$Data_passenger['tDestinationLatitude']=$Latitude;
				$Data_passenger['tDestinationLongitude']=$Longitude;
				$Data_passenger['tDestinationAddress']=$Address;
				$id = $obj->MySQLQueryPerform("register_user",$Data_passenger,'update',$where_passenger);
				
				}else{
				
				$message  = "DestinationAdded";
				if($userType !="Driver"){
					$alertMsg = "Destination is added by passenger.";
					}else{
					$alertMsg = "Destination is added by driver.";
				}
				$message_arr = array();
				$message_arr['Message'] = $message;
				$message_arr['DLatitude'] = $Latitude;
				$message_arr['DLongitude'] = $Longitude;
				$message_arr['DAddress'] = $Address;
				$message = json_encode($message_arr);
				
				/* For PubNub Setting */
				$tableName = $userType != "Driver"?"register_driver":"register_user";
				$iMemberId_VALUE = $userType != "Driver"?$iDriverId:$userId;
				$iMemberId_KEY = $userType != "Driver"?"iDriverId":"iUserId";
				$iAppVersion=get_value($tableName, 'iAppVersion', $iMemberId_KEY,$iMemberId_VALUE,'','true');
				$eDeviceType=get_value($tableName, 'eDeviceType', $iMemberId_KEY,$iMemberId_VALUE,'','true');
				/* For PubNub Setting Finished */
				
				$vLangCode=get_value($tableName, 'vLang', $iMemberId_KEY,$iMemberId_VALUE,'','true');
				if($vLangCode == "" || $vLangCode == NULL){
					$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
				}
				
				$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
				$lblValue = $userType != "Driver"?"LBL_DEST_ADD_BY_DRIVER":"LBL_DEST_ADD_BY_PASSENGER";
				$alertMsg = $languageLabelsArr[$lblValue];
				
				$ENABLE_PUBNUB = $generalobj->getConfigurations("configurations","ENABLE_PUBNUB");
				$PUBNUB_PUBLISH_KEY = $generalobj->getConfigurations("configurations","PUBNUB_PUBLISH_KEY");
				$PUBNUB_SUBSCRIBE_KEY = $generalobj->getConfigurations("configurations","PUBNUB_SUBSCRIBE_KEY");
				
				if($ENABLE_PUBNUB == "Yes"  && $PUBNUB_PUBLISH_KEY != "" && $PUBNUB_SUBSCRIBE_KEY != ""/*  && $iAppVersion > 1 && $eDeviceType == "Android" */){
					
					//$pubnub = new Pubnub\Pubnub($PUBNUB_PUBLISH_KEY, $PUBNUB_SUBSCRIBE_KEY);
					$pubnub = new Pubnub\Pubnub(array("publish_key" => $PUBNUB_PUBLISH_KEY,"subscribe_key" => $PUBNUB_SUBSCRIBE_KEY, "uuid" => $uuid));
					
					if($userType !="Driver"){
						$channelName = "DRIVER_".$iDriverId;
						}else{
						$channelName = "PASSENGER_".$userId;
					}
					
					$info = $pubnub->publish($channelName, $message);
					
				}
				
				$deviceTokens_arr_ios = array();
				$registation_ids_new = array();
				
				if($data[0]['deviceType'] == "Android" && $ENABLE_PUBNUB != "Yes"){
					array_push($registation_ids_new, $data[0]['regId']);
					
					$Rmessage = array("message" => $message);
					
					$result = send_notification($registation_ids_new, $Rmessage,0);
					}else if($data[0]['deviceType'] != "Android" ){
					array_push($deviceTokens_arr_ios, $data[0]['regId']);
					
					if($ENABLE_PUBNUB == "Yes"){
						$message = "";
					}
					
					if($message != ""){
						if($userType == "Driver"){
							sendApplePushNotification(0,$deviceTokens_arr_ios,$message,$alertMsg,0);
							}else{
							sendApplePushNotification(1,$deviceTokens_arr_ios,$message,$alertMsg,0);
						}
					}
				}
				
				
			}
			
			$returnArr['Action']="1";
			
			}else{
			$returnArr['Action']="0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
	}
	
	###################### getAssignedDriverLocation ##########################
	if($type == "getDriverLocations"){
		$iDriverId = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		
		$sql = "SELECT vLatitude, vLongitude,vTripStatus FROM `register_driver` WHERE iDriverId='$iDriverId'";
		$Data = $obj->MySQLSelect($sql);
		
		if ( count($Data) == 1 )
		{	$returnArr['Action'] = "1";
			$returnArr['vLatitude'] = $Data[0]['vLatitude'];
			$returnArr['vLongitude'] = $Data[0]['vLongitude'];
			$returnArr['vTripStatus'] = $Data[0]['vTripStatus'];
		}
		else
		{
			$returnArr['Action'] = "0";
			$returnArr['message'] = 'Not Found';
		}
		echo json_encode($returnArr);
		
	}
	
	###########################################################################
	
	if($type=='displayFare'){
		global $currency_supported_paypal,$generalobj;
		
		$iMemberId          = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$userType     = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
		
		$tableName = $userType != "Driver"?"register_user":"register_driver";
		$iMemberId_KEY = $userType != "Driver"?"iUserId":"iDriverId";
		$iTripId=get_value($tableName, 'iTripId', $iMemberId_KEY,$iMemberId,'','true');
		$ENABLE_TIP_MODULE=$generalobj->getConfigurations("configurations","ENABLE_TIP_MODULE");
		$vTripPaymentMode =get_value('trips', 'vTripPaymentMode', 'iTripId',$iTripId,'','true');
		if($vTripPaymentMode == "Card"){
			$result_fare['ENABLE_TIP_MODULE']  = $ENABLE_TIP_MODULE;
			}else{
			$result_fare['ENABLE_TIP_MODULE']  = "No";
		}
		$result_fare['FormattedTripDate']=date('dS M Y \a\t h:i a',strtotime($result_fare[0]['tStartDate']));
		$result_fare['PayPalConfiguration']="No";
		$result_fare['DefaultCurrencyCode']="USD";
		$result_fare['PaypalFare']=strval($result_fare[0]['TotalFare']);
		$result_fare['PaypalCurrencyCode']=$vCurrencyCode;
		$result_fare['APP_TYPE'] = $generalobj->getConfigurations("configurations","APP_TYPE");
		if($result_fare['APP_TYPE'] == "UberX"){
			$result_fare['APP_DESTINATION_MODE'] = "None";
			}else{
			$result_fare['APP_DESTINATION_MODE'] = "Strict";
		}
		// $result_fare['APP_DESTINATION_MODE'] = $generalobj->getConfigurations("configurations","APP_DESTINATION_MODE");
		$returnArr = gettrippricedetails($iTripId,$iMemberId,$userType, "DISPLAY");
		
		$result_fare = array_merge($result_fare, $returnArr);
		
		if(count($returnArr) > 0){
			$returnArr['Action'] = "1";
    		$returnArr['message'] = $result_fare;
			}else{
			$returnArr['Action'] = "0";
		}
		//echo "<pre>" ; print_r($returnArr); exit;
        echo json_encode($returnArr);
		
	}
	
	
	
	###########################################################################
	
    if($type=="submitRating"){
		
        //$iGeneralUserId = isset($_REQUEST["iGeneralUserId"]) ? $_REQUEST["iGeneralUserId"] : ''; // for both driver or passenger
        $iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : ''; // for both driver or passenger
        $tripID    = isset($_REQUEST["tripID"]) ? $_REQUEST["tripID"] : '';
        $rating  = isset($_REQUEST["rating"]) ? $_REQUEST["rating"] : '';
        $message   = isset($_REQUEST["message"]) ? $_REQUEST["message"] : '';
        $userType   = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger'; // Passenger or Driver
        $fAmount = isset($_REQUEST["fAmount"]) ? $_REQUEST["fAmount"] : '';
        $isCollectTip = isset($_REQUEST["isCollectTip"]) ? $_REQUEST["isCollectTip"] : ''; 
        if($isCollectTip == "" || $isCollectTip == NULL){
			$isCollectTip = "No";
		}
        
		$message = stripslashes($message);
		
        $sql = "SELECT * FROM `ratings_user_driver` WHERE iTripId = '$tripID' and eUserType = '$userType'";
        $row_check = $obj->MySQLSelect($sql);
		
		$ENABLE_TIP_MODULE=$generalobj->getConfigurations("configurations","ENABLE_TIP_MODULE");
		
        if(count($row_check) > 0){
            // $returnArr['Action'] = "0"; //LBL_RATING_EXIST
            // $returnArr['message'] = "LBL_ERROR_RATING_SUBMIT_AGAIN_TXT"; //LBL_RATING_EXIST
            $returnArr['Action'] = "1";
			$returnArr['message'] = "LBL_TRIP_FINISHED_TXT";
            echo json_encode($returnArr);  exit;
		    }else{
            
            # Code For Tip Charge #
            if($isCollectTip == "Yes" && $userType == "Passenger"){
                if($fAmount > 0){
					TripCollectTip($iMemberId,$tripID,$fAmount);
				}
			} 
            # Code For Tip Charge # 
			
            if($userType == "Passenger"){
				$iDriverId =get_value('trips', 'iDriverId', 'iTripId',$tripID,'','true');
				$tableName = "register_driver";
				$where = "iDriverId='".$iDriverId."'";
				$iMemberId = $iDriverId;
				}else{   
				
				$where_trip = " iTripId = '$tripID'";
				
				$Data_update_trips['eVerified']="Verified";
				
				$id = $obj->MySQLQueryPerform("trips",$Data_update_trips,'update',$where_trip);
				
				$iUserId =get_value('trips', 'iUserId', 'iTripId',$tripID,'','true');
				$tableName = "register_user";
				$where = "iUserId='".$iUserId."'";
				$iMemberId = $iUserId;
			}
            /* Insert records into ratings table*/
            $Data_update_ratings['iTripId']=$tripID;
            $Data_update_ratings['vRating1']=$rating;
            $Data_update_ratings['vMessage']=$message;
            $Data_update_ratings['eUserType']=$userType;
			
			$id = $obj->MySQLQueryPerform("ratings_user_driver",$Data_update_ratings,'insert');
			
			/* Set average rating for passenger OR Driver */
			// Driver gives rating to passenger and passenger gives rating to driver
			/*$average_rating = getUserRatingAverage($iMemberId,$userType);
				
				$sql = "SELECT vAvgRating FROM ".$tableName.' WHERE '.$where;
				$fetchAvgRating= $obj->MySQLSelect($sql);
				
				if($fetchAvgRating[0]['vAvgRating'] > 0){
				$average_rating = round(($fetchAvgRating[0]['vAvgRating'] + $rating) / 2,1);
				}else{
				$average_rating = round($fetchAvgRating[0]['vAvgRating'] + $rating,1);
			} */
			
            $Data_update['vAvgRating']=getUserRatingAverage($iMemberId,$userType);
			
            $id = $obj->MySQLQueryPerform($tableName,$Data_update,'update',$where);
			
            if ($id) {
                $returnArr['Action'] = "1";
				$returnArr['message'] = "LBL_TRIP_FINISHED_TXT";
				$vTripPaymentMode =get_value('trips', 'vTripPaymentMode', 'iTripId',$tripID,'','true');
				if($vTripPaymentMode == "Card"){
					$returnArr['ENABLE_TIP_MODULE']  = $ENABLE_TIP_MODULE;
					}else{
					$returnArr['ENABLE_TIP_MODULE']  = "No";
				}
                echo json_encode($returnArr);
				} else {
                $returnArr['Action'] = "0";
                $returnArr['message'] = "LBL_TRY_AGAIN_LATER_TXT";
                echo json_encode($returnArr);
			}
			
            if($userType == "Passenger"){
                sendTripReceipt($tripID);
				}else{
                sendTripReceiptAdmin($tripID);
			}
			// echo "come";
		}
		
		
	}
	
	###########################################################################
	
    if ($type == "updatePassword") {
        $user_id = isset($_REQUEST["UserID"]) ? $_REQUEST["UserID"] : '';
        $Upass        = isset($_REQUEST["pass"]) ? $_REQUEST["pass"] : '';
        $UserType = isset($_REQUEST["UserType"])?clean($_REQUEST["UserType"]):''; // UserType = Driver/Passenger
        $CurrentPassword  = isset($_REQUEST["CurrentPassword"]) ? $_REQUEST["CurrentPassword"] : '';
        if($UserType == "Passenger"){
			$tblname = "register_user";
			$vPassword=get_value('register_user', 'vPassword', 'iUserId',$user_id,'','true');
			}else{
			$tblname = "register_driver";
			$vPassword=get_value('register_driver', 'vPassword', 'iDriverId',$user_id,'','true');        
		}
        
        # Check For Valid password #
        if($CurrentPassword != ""){
			$hash = $vPassword;
			$checkValidPass = $generalobj->check_password($CurrentPassword, $hash);
			if($checkValidPass == 0){
				$returnArr['Action']="0";
				$returnArr['message'] ="LBL_WRONG_PASSWORD";
				echo json_encode($returnArr);exit;
			}
		}
        # Check For Valid password #  
		
        //$updatedPassword = $generalobj->encrypt($Upass);
        $updatedPassword = $generalobj->encrypt_bycrypt($Upass);
		
        $Data_update_user['vPassword']=$updatedPassword;
		
        if($UserType == "Passenger"){
			
            $where = " iUserId = '$user_id'";
            $id = $obj->MySQLQueryPerform("register_user",$Data_update_user,'update',$where);
			
            if ($id >0) {
				
                $returnArr['Action']="1";
                $returnArr['message']=getPassengerDetailInfo($user_id,"");
                echo json_encode($returnArr);
				
				} else {
				
                $returnArr['Action']="0";
                $returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
                echo json_encode($returnArr);
			}
			
			}else{
            $where = " iDriverId = '$user_id'";
            $id = $obj->MySQLQueryPerform("register_driver",$Data_update_user,'update',$where);
			
			
            if ($id >0) {
				
                $returnArr['Action']="1";
                $returnArr['message']=getDriverDetailInfo($user_id);
                echo json_encode($returnArr);
				
				} else {
				
                $returnArr['Action']="0";
                $returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
                echo json_encode($returnArr);
			}
		}
		
	}
	
	############################Send Sms Twilio####################################
	
	if($type=='sendVerificationSMS'){
		$mobileNo = isset($_REQUEST['MobileNo'])?clean($_REQUEST['MobileNo']):'';
		$mobileNo = str_replace('+','',$mobileNo);
		$iMemberId = isset($_REQUEST['iMemberId'])?clean($_REQUEST['iMemberId']):'';
		$userType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):'Passenger';
		$REQ_TYPE = isset($_REQUEST["REQ_TYPE"]) ? $_REQUEST['REQ_TYPE'] : '';
		
		$isdCode= $generalobj->getConfigurations("configurations","SITE_ISD_CODE");
		//$toMobileNum= "+".$mobileNo;
		if($userType == "Passenger"){
			$tblname = "register_user";
			$fields = 'iUserId, vPhone,vPhoneCode as vPhoneCode, vEmail, vName, vLastName';
			$condfield = 'iUserId';
			$vLangCode=get_value('register_user', 'vLang', 'iUserId',$iMemberId,'','true');
			}else{
			$tblname = "register_driver";
			$fields = 'iDriverId, vPhone,vCode as vPhoneCode, vEmail, vName, vLastName';
			$condfield = 'iDriverId';
			$vLangCode=get_value('register_driver', 'vLang', 'iDriverId',$iMemberId,'','true');
		}
		
		if($vLangCode == "" || $vLangCode == NULL){
			$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
		$prefix = $languageLabelsArr['LBL_VERIFICATION_CODE_TXT'];
		$verificationCode_sms = mt_rand(1000, 9999);
		$verificationCode_email = mt_rand(1000, 9999);
		$message = $prefix.' '.$verificationCode_sms;
		
		
		if($iMemberId == "" && $REQ_TYPE == "DO_PHONE_VERIFY"){
			$toMobileNum = "+".$mobileNo;
			}else{
			$sql="select $fields from $tblname where $condfield = '".$iMemberId."'";
			$db_member = $obj->MySQLSelect($sql);
			
			$Data_Mail['vEmail'] = isset($db_member[0]['vEmail'])?$db_member[0]['vEmail']:'';
			$vFirstName = isset($db_member[0]['vName'])?$db_member[0]['vName']:'';
			$vLastName = isset($db_member[0]['vLastName'])?$db_member[0]['vLastName']:'';
			$Data_Mail['vName'] = $vFirstName." ".$vLastName ;
			$Data_Mail['CODE'] = $verificationCode_email ;
			$mobileNo = $db_member[0]['vPhoneCode'].$db_member[0]['vPhone'];
			$toMobileNum = "+".$mobileNo;
		}
		
		
		$emailmessage = "";
		$phonemessage = "";
		if($REQ_TYPE == "DO_EMAIL_PHONE_VERIFY"){
			$sendemail = $generalobj->send_email_user("APP_EMAIL_VERIFICATION_USER",$Data_Mail);
			if($sendemail != true || $sendemail != "true" || $sendemail != "1"){
				$sendemail = 0;
			}
			$result = sendEmeSms($toMobileNum,$message);
			if($result ==0){
				$toMobileNum = "+".$isdCode.$mobileNo;
				$result = sendEmeSms($toMobileNum,$message);
			}
			
			$returnArr['Action'] ="1";
			if($sendemail == 0 && $result ==0){
				$returnArr['Action'] ="0";
				$returnArr['message'] = "LBL_ACC_VERIFICATION_FAILED";
				}else{
				$returnArr['message_sms'] = $result == 0?"LBL_MOBILE_VERIFICATION_FAILED_TXT":$verificationCode_sms;
				$returnArr['message_email'] = $sendemail == 0?"LBL_EMAIL_VERIFICATION_FAILED_TXT":$verificationCode_email;
			}
			echo json_encode($returnArr);exit;
			}else if($REQ_TYPE == "DO_PHONE_VERIFY"){
			$result = sendEmeSms($toMobileNum,$message);
			if($result ==0){
				$toMobileNum = "+".$isdCode.$mobileNo;
				$result = sendEmeSms($toMobileNum,$message);
			}
			
			if($result ==0){
				$returnArr['Action'] ="0";
				$returnArr['message'] ="LBL_MOBILE_VERIFICATION_FAILED_TXT";
				echo json_encode($returnArr);exit;
				}else{
				$returnArr['Action'] ="1";
				$returnArr['message'] =$verificationCode_sms;
				echo json_encode($returnArr);exit;
			}
			}else if($REQ_TYPE == "DO_EMAIL_VERIFY"){
			$sendemail = $generalobj->send_email_user("APP_EMAIL_VERIFICATION_USER",$Data_Mail);
			if($sendemail != true || $sendemail != "true" || $sendemail != "1"){
				$sendemail = 0;
			}
			if($sendemail == 0){
				$returnArr['Action'] ="0";
				$returnArr['message'] = "LBL_EMAIL_VERIFICATION_FAILED_TXT";
				echo json_encode($returnArr);exit;
				}else{
				$returnArr['Action'] ="1";
				$returnArr['message'] =  $Data_Mail['CODE'];
				echo json_encode($returnArr);exit;
			}
			}else if($REQ_TYPE == "EMAIL_VERIFIED"){
			$where = " ".$condfield." = '".$iMemberId."'";
			$Data['eEmailVerified']="Yes";
			$id = $obj->MySQLQueryPerform($tblname,$Data,'update',$where);
			
			if ($id) {
				$returnArr['Action'] ="1";
				$returnArr['message']  = "LBL_EMAIl_VERIFIED";
				
				if($userType == 'Passenger'){
					$returnArr['userDetails']['Action']="1";
					$returnArr['userDetails']['message']=getPassengerDetailInfo($iMemberId);
					}else {
					$returnArr['userDetails']['Action']="1";
					$returnArr['userDetails']['message']=getDriverDetailInfo($iMemberId);
				}
				echo json_encode($returnArr);exit;
				} else {
				$returnArr['Action'] ="0";
				$returnArr['message']  = "LBL_EMAIl_VERIFIED_ERROR";
				echo json_encode($returnArr);exit;
			}
			
			}else if($REQ_TYPE == "PHONE_VERIFIED"){
			
			$where = " ".$condfield." = '".$iMemberId."'";
			$Data['ePhoneVerified']="Yes";
			$id = $obj->MySQLQueryPerform($tblname,$Data,'update',$where);
			
			if ($id) {
				$returnArr['Action'] ="1";
				$returnArr['message']  = "LBL_PHONE_VERIFIED";
				if($userType == 'Passenger'){
					$returnArr['userDetails']['Action']="1";
					$returnArr['userDetails']['message']=getPassengerDetailInfo($iMemberId);
					}else {
					$returnArr['userDetails']['Action']="1";
					$returnArr['userDetails']['message']=getDriverDetailInfo($iMemberId);
				}
				echo json_encode($returnArr);exit;
				} else {
				$returnArr['Action'] ="0";
				$returnArr['message'] = "LBL_PHONE_VERIFIED_ERROR";
				echo json_encode($returnArr);exit;
			}
		}
		
		//	$returnArr['message'] =$verificationCode;
		//echo json_encode($returnArr);
	}
	
	/* if($type=='sendVerificationSMS'){
		$mobileNo = isset($_REQUEST['MobileNo'])?clean($_REQUEST['MobileNo']):'';
		$iMemberId = isset($_REQUEST['iMemberId'])?clean($_REQUEST['iMemberId']):'';
		$userType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):'Passenger';
		
		$isdCode= $generalobj->getConfigurations("configurations","SITE_ISD_CODE");
		$toMobileNum= "+".$mobileNo;
		$verificationCode = mt_rand(1000, 9999);
		
		if($iMemberId != ""){
		if($userType=="Passenger"){
		$vLangCode = get_value('register_user', 'vLang', 'iUserId',$iMemberId,'','true');
		}else{
		$vLangCode = get_value('register_driver', 'vLang', 'iDriverId',$iMemberId,'','true');
		}
		}else{
		$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		
		$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
		
		$prefix = $languageLabelsArr['LBL_VERIFICATION_CODE_TXT'];
		
		$message = $prefix.' '.$verificationCode;
		
		$result = sendEmeSms($toMobileNum,$message);
		if($result ==0){
		$toMobileNum = "+".$isdCode.$mobileNo;
		$result = sendEmeSms($toMobileNum,$message);
		}
		
		if($result ==0){
		$returnArr['Action'] ="0";
		$returnArr['message'] ="LBL_MOBILE_VERIFICATION_FAILED_TXT";
		}else{
		$returnArr['Action'] ="1";
		$returnArr['message'] =$verificationCode;
		}
		
		echo json_encode($returnArr);
	} */
	
	############################Send Sms Twilio END################################
	
	###########################################################################
	
	if ($type == "updateDriverStatus") {
		
		$iDriverId         = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$Status_driver    = isset($_REQUEST["Status"]) ? $_REQUEST["Status"] : '';
		$isUpdateOnlineDate    = isset($_REQUEST["isUpdateOnlineDate"]) ? $_REQUEST["isUpdateOnlineDate"] : '';
		$latitude_driver  = isset($_REQUEST["latitude"]) ? $_REQUEST["latitude"] : '';
		$longitude_driver = isset($_REQUEST["longitude"]) ? $_REQUEST["longitude"] : '';
		$iGCMregID = isset($_REQUEST["vDeviceToken"]) ? $_REQUEST["vDeviceToken"] : '';
		$vTimeZone = isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		
		checkmemberemailphoneverification($iDriverId,"Driver");
		
		if($iDriverId==''){
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
			echo json_encode($returnArr);
			exit;
		}
		
		$GCMID =get_value('register_driver', 'iGcmRegId', 'iDriverId',$iDriverId,'','true');
		if($GCMID != "" && $iGCMregID!="" && $GCMID != $iGCMregID){
			$returnArr['Action'] = "0";
			$returnArr['message'] = "SESSION_OUT";
			echo json_encode($returnArr);
			exit;
		}
		$COMMISION_DEDUCT_ENABLE=$generalobj->getConfigurations("configurations","COMMISION_DEDUCT_ENABLE");
		if($COMMISION_DEDUCT_ENABLE == 'Yes') {
			$vLang =get_value('register_driver', 'vLang', 'iDriverId',$iDriverId,'','true');
			if($vLang == "" || $vLang == NULL){
				$vLang = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
			}
			$languageLabelsArr= getLanguageLabelsArr($vLang,"1");
			$user_available_balance = $generalobj->get_user_available_balance($iDriverId,"Driver");
			$driverDetail = get_value('register_driver AS rd LEFT JOIN currency AS c ON c.vName=rd.vCurrencyDriver', 'rd.vCurrencyDriver,c.Ratio,c.vSymbol', 'rd.iDriverId',$iDriverId);
			$vCurrencyDriver =$driverDetail[0]['vCurrencyDriver'];
			$ratio =$driverDetail[0]['Ratio'];
			$currencySymbol=$driverDetail[0]['vSymbol'];
			$WALLET_MIN_BALANCE=$generalobj->getConfigurations("configurations","WALLET_MIN_BALANCE");
			if($WALLET_MIN_BALANCE > $user_available_balance){
				// $returnArr['Action'] = "0";
				$returnArr['message']="REQUIRED_MINIMUM_BALNCE";
				$returnArr['Msg']=str_replace('####', $currencySymbol.($WALLET_MIN_BALANCE * $ratio), $languageLabelsArr['LBL_REQUIRED_MINIMUM_BALNCE']);
				// echo json_encode($returnArr);
				// exit;
			}
		}
		
		// getDriverStatus($iDriverId);
		$APP_TYPE = $generalobj->getConfigurations("configurations", "APP_TYPE");
		$ssql = "";
		if($APP_TYPE == "UberX"){
			$ssql = "And dv.vCarType !=''";
		}
		
		$sql = "SELECT make.vMake, model.vTitle, dv.*, rd.iDriverVehicleId as iSelectedVehicleId FROM `driver_vehicle` dv, make, model, register_driver as rd WHERE dv.iDriverId='$iDriverId' AND rd.iDriverId='$iDriverId' AND dv.`iMakeId` = make.`iMakeId` AND dv.`iModelId` = model.`iModelId` AND dv.`eStatus`='Active'".$ssql ;
		
		$Data_Car = $obj->MySQLSelect($sql);
		
		if (count($Data_Car) > 0)
		{
			$status= "CARS_NOT_ACTIVE";
			
			$i=0;
			while(count($Data_Car)>$i){
				
				$eStatus=$Data_Car[$i]['eStatus'];
				if($eStatus == "Active"){
					$status= "CARS_AVAIL";
				}
				$i++;
			}
			
			if($status == "CARS_AVAIL" && ($Data_Car[0]['iSelectedVehicleId'] =="0" ||$Data_Car[0]['iSelectedVehicleId'] =="") ){
				// echo "SELECT_CAR";
				$returnArr['Action']="0";
				$returnArr['message']="LBL_SELECT_CAR_MESSAGE_TXT";
				echo json_encode($returnArr);
				exit;
				}else if($status == "CARS_NOT_ACTIVE"){
				// echo "CARS_NOT_ACTIVE";
				$returnArr['Action']="0";
				$returnArr['message']="LBL_INACTIVE_CARS_MESSAGE_TXT";
				echo json_encode($returnArr);
				exit;
			}
			
			
		}
		else
		{
			// echo "NO_CARS_AVAIL";
			$returnArr['Action']="0";
			$returnArr['message']="LBL_NO_CAR_AVAIL_TXT";
			echo json_encode($returnArr);
			exit;
		}
		
		$where = " iDriverId='".$iDriverId."'";
		if($Status_driver != ''){
			$Data_update_driver['vAvailability']=$Status_driver;
		}
		
		if($latitude_driver != '' && $longitude_driver != ''){
			$Data_update_driver['vLatitude']=$latitude_driver;
			$Data_update_driver['vLongitude']=$longitude_driver;
		}
		
		if($Status_driver == "Available"){
			$Data_update_driver['tOnline']=@date("Y-m-d H:i:s");
			// insert as online
			// Code for Check last logout date is update in driver_log_report
			$query = "SELECT * FROM driver_log_report WHERE dLogoutDateTime = '0000-00-00 00:00:00' AND iDriverId = '".$iDriverId."' ORDER BY iDriverLogId DESC LIMIT 0,1";
			$db_driver = $obj->MySQLSelect($query);
			if(count($db_driver) > 0) {
				$sql = "SELECT tLastOnline FROM register_driver WHERE iDriverId = '".$iDriverId."'";
				$db_drive_lastonline = $obj->MySQLSelect($sql);
				$driver_lastonline = $db_drive_lastonline[0]['tLastOnline'];
				$updateQuery = "UPDATE driver_log_report set dLogoutDateTime='".$driver_lastonline."' WHERE iDriverLogId = ".$db_driver[0]['iDriverLogId'];
				$obj->sql_query($updateQuery);
			}
			// Code for Check last logout date is update in driver_log_report Ends
			$vIP=get_client_ip();
			$curr_date= date('Y-m-d H:i:s');
			$sql = "INSERT INTO `driver_log_report` (`iDriverId`,`dLoginDateTime`,`vIP`) VALUES ('" . $iDriverId  . "','".$curr_date."','" . $vIP . "')";
			$insert_log = $obj->sql_query($sql);
		}
		
		if($Status_driver == "Not Available"){
			// update as offline
			$Data_update_driver['tLastOnline']=@date("Y-m-d H:i:s");
			$curr_date= date('Y-m-d H:i:s');
			$selct_query = "select * from driver_log_report WHERE iDriverId = '".$iDriverId."' order by `iDriverLogId` desc limit 0,1";
			$get_data_log = $obj->sql_query($selct_query);
			
			$update_sql = "UPDATE driver_log_report set dLogoutDateTime = '".$curr_date."' WHERE iDriverLogId ='".$get_data_log[0]['iDriverLogId']."'";
			$result = $obj->sql_query($update_sql);
		}
		
		if(($isUpdateOnlineDate == "true" && $Status_driver == "Available") || ($isUpdateOnlineDate == "" && $Status_driver == "") || $isUpdateOnlineDate == "true"){
			$Data_update_driver['tOnline']=@date("Y-m-d H:i:s");
			$Data_update_driver['tLastOnline']=@date("Y-m-d H:i:s");
		}
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		
		# Update User Location Date #
		Updateuserlocationdatetime($iDriverId,"Driver",$vTimeZone);    
		# Update User Location Date #
		
		if ($id) {
			$returnArr['Action']="1";
			echo json_encode($returnArr);
			} else {
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
			echo json_encode($returnArr);
		}
		
	}
	
	###########################################################################
	
	if($type == "LoadAvailableCars"){
		$iDriverId              = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		
		$sql = "SELECT register_driver.iDriverVehicleId as DriverSelectedVehicleId,make.vMake, model.vTitle, dv.* FROM `driver_vehicle` dv, make, model,register_driver WHERE dv.iDriverId='$iDriverId' AND register_driver.iDriverId = '$iDriverId' AND dv.`iMakeId` = make.`iMakeId` AND dv.`iModelId` = model.`iModelId` AND dv.`eStatus`='Active'";
		
		$Data_Car = $obj->MySQLSelect($sql);
		
		if (count($Data_Car) > 0)
		{
			$status= "CARS_NOT_ACTIVE";
			
			$i=0;
			while(count($Data_Car)>$i){
				
				$eStatus=$Data_Car[$i]['eStatus'];
				if($eStatus == "Active"){
					$status= "CARS_AVAIL";
				}
				$i++;
			}
			if($status == "CARS_NOT_ACTIVE"){
				$returnArr['Action']="0";
				$returnArr['message']="LBL_INACTIVE_CARS_MESSAGE_TXT";
				echo json_encode($returnArr);
				exit;
			}
			
			// $returnArr['carList'] = $Data_Car;
			
			// echo json_encode($returnArr);
			$returnArr['Action']="1";
			$returnArr['message']=$Data_Car;
			echo json_encode($returnArr);
		}
		else
		{
			$returnArr['Action']="0";
			$returnArr['message']="LBL_NO_CAR_AVAIL_TXT";
			echo json_encode($returnArr);
			exit;
		}
	}
	
	########################### Set Driver CarID ############################
	if ($type == "SetDriverCarID") {
		
		$iDriverId = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$Data['iDriverVehicleId'] = isset($_REQUEST["iDriverVehicleId"]) ? $_REQUEST["iDriverVehicleId"] : '';
		
		$where = " iDriverId = '".$iDriverId."'";
		
		$sql = $obj->MySQLQueryPerform("register_driver",$Data,'update',$where);
		if ($sql > 0) {
			$returnArr['Action']="1";
			echo json_encode($returnArr);
		}
		else
		{
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
			echo json_encode($returnArr);
		}
	}
	
	###########################################################################
	
	if($type == "GenerateTrip"){
		$passenger_id =  isset($_REQUEST["PassengerID"]) ? $_REQUEST["PassengerID"] : '';
		$driver_id      =  isset($_REQUEST["DriverID"]) ? $_REQUEST["DriverID"] : '';
		$iCabRequestId      =  isset($_REQUEST["iCabRequestId"]) ? $_REQUEST["iCabRequestId"] : '';
		$Source_point_latitude=  isset($_REQUEST["start_lat"]) ? $_REQUEST["start_lat"] : '';
		$Source_point_longitude=  isset($_REQUEST["start_lon"]) ? $_REQUEST["start_lon"] : '';
		$Source_point_Address= isset($_REQUEST["sAddress"]) ? $_REQUEST["sAddress"] : '';
		$GoogleServerKey= isset($_REQUEST["GoogleServerKey"]) ? $_REQUEST["GoogleServerKey"] : '';
		$iCabBookingId= isset($_REQUEST["iCabBookingId"]) ? $_REQUEST["iCabBookingId"] : '';
		$vMsgCode= isset($_REQUEST["vMsgCode"]) ? $_REQUEST["vMsgCode"] : '';
		$setCron= isset($_REQUEST["setCron"]) ? $_REQUEST["setCron"] : 'No';
		
		if($iCabBookingId !=""){
			$bookingData = get_value('cab_booking', 'iUserId,vSourceLatitude,vSourceLongitude,vSourceAddresss,eType', 'iCabBookingId', $iCabBookingId);
			$passenger_id = $bookingData[0]['iUserId'];
			$Source_point_latitude = $bookingData[0]['vSourceLatitude'];
			$Source_point_longitude = $bookingData[0]['vSourceLongitude'];
			$Source_point_Address = $bookingData[0]['vSourceAddresss'];
			$eType_cabbooking = $bookingData[0]['eType'];
		}
		
		$DriverMessage= "CabRequestAccepted";
		
		$TripRideNO = rand ( 10000000 , 99999999 );
        $TripVerificationCode = rand ( 1000 , 9999 );
		$Active= "Active";
		
		$vLangCode=get_value('register_user', 'vLang', 'iUserId',$passenger_id,'','true');
		if($vLangCode == "" || $vLangCode == NULL){
			$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		$vGMapLangCode=get_value('language_master', 'vGMapLangCode', 'vCode',$vLangCode,'','true');
		
		$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
		$tripdriverarrivlbl = $languageLabelsArr['LBL_DRIVER_ARRIVING'];
		
		if($Source_point_Address == ""){
			$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$Source_point_latitude.",".$Source_point_longitude."&key=".$GoogleServerKey."&language=".$vGMapLangCode;
			
			try {
				
				$jsonfile = file_get_contents($url);
				$jsondata = json_decode($jsonfile);
				$source_address=$jsondata->results[0]->formatted_address;
				
				$Source_point_Address = $source_address ;
				
				} catch (ErrorException $ex) {
				
				$returnArr['Action'] = "0";
				$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
				echo json_encode($returnArr);
				exit;
			}
		}
		
		if($Source_point_Address == ""){
            $returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
            echo json_encode($returnArr);
            exit;
		}
		
		$sql = "SELECT eStatus,ePayType,iVehicleTypeId,vDestLatitude,vDestLongitude,tDestAddress,vCouponCode,eType,iPackageTypeId,vReceiverName,vReceiverMobile,tPickUpIns,tDeliveryIns,tPackageDetails,fPickUpPrice,fNightPrice,iQty,vRideCountry,fTollPrice,vTollPriceCurrencyCode,eTollSkipped,vTimeZone FROM cab_request_now WHERE iUserId='$passenger_id' and iCabRequestId = '$iCabRequestId'";
		$check_row = $obj->MySQLSelect($sql);
		
		$eStatus = $check_row[0]['eStatus'];
		$eType = $check_row[0]['eType'];
		
		if($eType_cabbooking != ""){
			$eType = $eType_cabbooking;
			}else{
			$eType = $check_row[0]['eType'];
		}
		
		if ($eStatus == "Requesting" || $iCabBookingId != "" || $eType == "UberX") {   
			
			// $eStatus      = $check_row[0]['eStatus'];
			
			// if ($eStatus == "Requesting") {
			
			$where = " iCabRequestId = '$iCabRequestId'";
			
			$Data_update_cab_request['eStatus']='Complete';
			
			$obj->MySQLQueryPerform("cab_request_now",$Data_update_cab_request,'update',$where);
			$sql = "SELECT vCurrencyPassenger,iAppVersion,iUserPetId FROM `register_user` WHERE iUserId = '$passenger_id'";
			$Data_passenger_detail = $obj->MySQLSelect($sql);
			
			$sql = "SELECT iDriverVehicleId,vCurrencyDriver,iAppVersion,vName,vLastName FROM `register_driver` WHERE iDriverId = '$driver_id'";
			$Data_vehicle = $obj->MySQLSelect($sql);
			
			$CAR_id_driver     = $Data_vehicle[0]['iDriverVehicleId'];
			
			if($iCabBookingId !=""){
				$sql_booking = "SELECT vDestLatitude,vDestLongitude,tDestAddress,ePayType,iVehicleTypeId,eType,iPackageTypeId,vReceiverName,vReceiverMobile,tPickUpIns,tDeliveryIns,tPackageDetails,fPickUpPrice,fNightPrice,iUserPetId,vCouponCode,iQty,vRideCountry,fTollPrice,vTollPriceCurrencyCode,eTollSkipped, vTimeZone FROM cab_booking WHERE iCabBookingId='$iCabBookingId'";
				
				$data_booking = $obj->MySQLSelect($sql_booking);
				
				$iSelectedCarType = $data_booking[0]['iVehicleTypeId'];
				$vTripPaymentMode = $data_booking[0]['ePayType'];
				$tDestinationLatitude = $data_booking[0]['vDestLatitude'];
				$tDestinationLongitude = $data_booking[0]['vDestLongitude'];
				$tDestinationAddress = $data_booking[0]['tDestAddress'];
				$fPickUpPrice = $data_booking[0]['fPickUpPrice'];
				$fNightPrice = $data_booking[0]['fNightPrice'];
				
				$eType = $data_booking[0]['eType'];
				$iPackageTypeId = $data_booking[0]['iPackageTypeId'];
				$vReceiverName = $data_booking[0]['vReceiverName'];
				$vReceiverMobile = $data_booking[0]['vReceiverMobile'];
				$tPickUpIns = $data_booking[0]['tPickUpIns'];
				$tDeliveryIns = $data_booking[0]['tDeliveryIns'];
				$tPackageDetails = $data_booking[0]['tPackageDetails'];
				$iUserPetId = $data_booking[0]['iUserPetId'];
				$vCouponCode = $data_booking[0]['vCouponCode'];
				$iQty = $data_booking[0]['iQty'];
				$vRideCountry = $data_booking[0]['vRideCountry'];
				$fTollPrice = $data_booking[0]['fTollPrice'];
				$vTollPriceCurrencyCode = $data_booking[0]['vTollPriceCurrencyCode'];
				$eTollSkipped = $data_booking[0]['eTollSkipped'];
				$vTimeZone = $data_booking[0]['vTimeZone'];
				}else{
				$iSelectedCarType = $check_row[0]['iVehicleTypeId'];
				$vTripPaymentMode = $check_row[0]['ePayType'];
				$tDestinationLatitude = $check_row[0]['vDestLatitude'];
				$tDestinationLongitude = $check_row[0]['vDestLongitude'];
				$tDestinationAddress = $check_row[0]['tDestAddress'];
				$fPickUpPrice = $check_row[0]['fPickUpPrice'];
				$fNightPrice = $check_row[0]['fNightPrice'];
				
				$eType = $check_row[0]['eType'];
				$iPackageTypeId = $check_row[0]['iPackageTypeId'];
				$vReceiverName = $check_row[0]['vReceiverName'];
				$vReceiverMobile = $check_row[0]['vReceiverMobile'];
				$tPickUpIns = $check_row[0]['tPickUpIns'];
				$tDeliveryIns = $check_row[0]['tDeliveryIns'];
				$tPackageDetails = $check_row[0]['tPackageDetails'];
				$iUserPetId = $Data_passenger_detail[0]['iUserPetId'];
				$vCouponCode = $check_row[0]['vCouponCode'];
				$iQty = $check_row[0]['iQty'];
				$vRideCountry = $check_row[0]['vRideCountry'];
				$fTollPrice = $check_row[0]['fTollPrice'];
				$vTollPriceCurrencyCode = $check_row[0]['vTollPriceCurrencyCode'];
				$eTollSkipped = $check_row[0]['eTollSkipped'];
				$vTimeZone = $check_row[0]['vTimeZone'];
			}
			
			/* 	if($vRideCountry != "") {
				$newTimeZone = get_value('country', 'vTimeZone', 'LOWER(vCountry)', strtolower($vRideCountry),'',true);
				//$newTimeZone = $
				@date_default_timezone_set($newTimeZone);
				}
			*/	
			$Data_trips['vRideNo']=$TripRideNO;
			$Data_trips['iUserId']=$passenger_id;
			$Data_trips['iDriverId']=$driver_id;
			$Data_trips['tTripRequestDate']=@date("Y-m-d H:i:s");
			$Data_trips['tStartLat']=$Source_point_latitude;
			$Data_trips['tStartLong']=$Source_point_longitude;
			$Data_trips['tSaddress']=$Source_point_Address;
			$Data_trips['iActive']=$Active;
			$Data_trips['iDriverVehicleId']=$CAR_id_driver;
			$Data_trips['iVerificationCode']=$TripVerificationCode;
			$Data_trips['iVehicleTypeId']=$iSelectedCarType;
			$Data_trips['eFareType'] = get_value('vehicle_type', 'eFareType', 'iVehicleTypeId', $iSelectedCarType,'','true');
			$Data_trips['fVisitFee'] = get_value('vehicle_type', 'fVisitFee', 'iVehicleTypeId', $iSelectedCarType,'','true');
			
			$Data_trips['vTripPaymentMode']=$vTripPaymentMode;
			$Data_trips['tEndLat']=$tDestinationLatitude;
			$Data_trips['tEndLong']=$tDestinationLongitude;
			$Data_trips['tDaddress']=$tDestinationAddress;
			$Data_trips['fPickUpPrice']=$fPickUpPrice;
			$Data_trips['fNightPrice']=$fNightPrice;
			$Data_trips['iQty']=$iQty;
			
			$Data_trips['eType']=$eType;
			$Data_trips['iPackageTypeId']=$iPackageTypeId;
			$Data_trips['vReceiverName']=$vReceiverName;
			$Data_trips['vReceiverMobile']=$vReceiverMobile;
			$Data_trips['tPickUpIns']=$tPickUpIns;
			$Data_trips['tDeliveryIns']=$tDeliveryIns;
			$Data_trips['tPackageDetails']=$tPackageDetails;
			$Data_trips['iUserPetId']=$iUserPetId;
			$Data_trips['vCountryUnitRider']=getMemberCountryUnit($passenger_id,"Passenger");
			$Data_trips['vCountryUnitDriver']=getMemberCountryUnit($driver_id,"Driver");
			$Data_trips['fTollPrice']=$fTollPrice;
			$Data_trips['vTollPriceCurrencyCode']=$vTollPriceCurrencyCode;
			$Data_trips['eTollSkipped']=$eTollSkipped;
			$Data_trips['vTimeZone']=$vTimeZone;
			
			if($vCouponCode != ''){
				$Data_trips['vCouponCode']=$vCouponCode;
				
				$noOfCouponUsed = get_value('coupon', 'iUsed', 'vCouponCode', $vCouponCode,'','true');
				$where = " vCouponCode = '".$vCouponCode."'";
				$data_coupon['iUsed']=$noOfCouponUsed + 1;
				$obj->MySQLQueryPerform("coupon",$data_coupon,'update',$where);
			}
			
			$currencyList = get_value('currency', '*', 'eStatus', 'Active');
			
			for($i=0;$i<count($currencyList);$i++){
				$currencyCode = $currencyList[$i]['vName'];
				$Data_trips['fRatio_'.$currencyCode] = $currencyList[$i]['Ratio'];
			}
			
			$Data_trips['vCurrencyPassenger']=$Data_passenger_detail[0]['vCurrencyPassenger'];
			$Data_trips['vCurrencyDriver']=$Data_vehicle[0]['vCurrencyDriver'];
			// $Data_trips['fRatioPassenger']=($obj->MySQLSelect("SELECT Ratio FROM currency WHERE vName='".$check_row[0]['vCurrencyPassenger']."' ")[0]['Ratio']);
			$Data_trips['fRatioPassenger']=get_value('currency', 'Ratio', 'vName', $check_row[0]['vCurrencyPassenger'],'','true');
			// $Data_trips['fRatioDriver']=($obj->MySQLSelect("SELECT Ratio FROM currency WHERE vName='".$Data_vehicle[0]['vCurrencyDriver']."' ")[0]['Ratio']);
			$Data_trips['fRatioDriver']=get_value('currency', 'Ratio', 'vName', $Data_vehicle[0]['vCurrencyDriver'],'','true');
			
			$id = $obj->MySQLQueryPerform("trips",$Data_trips,'insert');
			$iTripId  = $id;
			$trip_status = "Active";
			
			$where1 = " iCabRequestId = '$iCabRequestId'";
			$Data_update_cab_request['iTripId']=$iTripId;
			$Data_update_cab_request['iDriverId']=$driver_id;
			$obj->MySQLQueryPerform("cab_request_now",$Data_update_cab_request,'update',$where1);
			#### Update Driver Request Status of Trip ####
			UpdateDriverRequest2($driver_id,$passenger_id,$iTripId,"Accept",$vMsgCode);
			#### Update Driver Request Status of Trip ####
			
			if($iCabBookingId !=""){
				$where = " iCabBookingId = '$iCabBookingId'";
				$data_update_booking['iTripId']=$iTripId;
				$data_update_booking['eStatus']="Completed";
				$obj->MySQLQueryPerform("cab_booking",$data_update_booking,'update',$where);
			}
			
			$where = " iUserId = '$passenger_id'";
			$Data_update_passenger['iTripId']=$iTripId;
			$Data_update_passenger['vTripStatus']=$trip_status;
			$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
			
			$where = " iDriverId = '$driver_id'";
			$Data_update_driver['iTripId']=$iTripId;
			$Data_update_driver['vTripStatus']=$trip_status;
			$Data_update_driver['vRideCountry']=$vRideCountry;
			$Data_update_driver['vAvailability']="Not Available";
			$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
			
			$message_arr = array();
			$message_arr['iDriverId'] = $driver_id;
			$message_arr['Message'] = $DriverMessage;
			$message_arr['iTripId'] = strval($iTripId);
			$message_arr['DriverAppVersion'] = strval($Data_vehicle[0]['iAppVersion']);
			if($iCabBookingId != ""){
				$message_arr['iCabBookingId'] = $iCabBookingId;
				$message_arr['iBookingId'] = $iCabBookingId;
			}
			$message_arr['iTripVerificationCode'] = $TripVerificationCode;
			
			$message = json_encode($message_arr);
			
			#####################Add Status Message#########################
			$DataTripMessages['tMessage']= $message;
			$DataTripMessages['iDriverId']= $driver_id;
			$DataTripMessages['iTripId']= $iTripId;
			$DataTripMessages['iUserId']= $passenger_id;
			$DataTripMessages['eFromUserType']= "Driver";
			$DataTripMessages['eToUserType']= "Passenger";
			$DataTripMessages['eReceived']= "No";
			$DataTripMessages['dAddedDate']= @date("Y-m-d H:i:s");
			
			$obj->MySQLQueryPerform("trip_status_messages",$DataTripMessages,'insert');
			################################################################
			
			if($setCron == 'Yes'){
				$passengerDetail = get_value('register_user', 'vName,vLastName,vPhone,vPhoneCode', 'iUserId', $passenger_id);
				$passengerName = $passengerDetail[0]['vName'].' '.$passengerDetail[0]['vLastName'];
				$vPhoneCode = $passengerDetail[0]['vPhoneCode'];
				$vPhone = $passengerDetail[0]['vPhone'];
				$driverName = $Data_vehicle[0]['vName'].' '.$Data_vehicle[0]['vLastName'];
				$messageEmail['details'] = '<p>Dear Administrator,</p>
				<p>Driver ( '.$driverName.' ) is assigned successfully for the following manual booking.</p>
				<p>Name: '.$passengerName.',</p>
				<p>Contact Number: +'.$vPhoneCode.$vPhone.'</p>';
				$mail = $generalobj->send_email_user('CRON_BOOKING_EMAIL',$messageEmail);
				$where_cabid2 = " iCabBookingId = '".$iCabBookingId."'";
				$Data_update2['eAssigned']= 'Yes';
				$Data_update2['iDriverId']= $driver_id;
				$id = $obj->MySQLQueryPerform("cab_booking",$Data_update2,'update',$where_cabid2); 
			}
			
			if ($iTripId > 0) {
				$ENABLE_PUBNUB = $generalobj->getConfigurations("configurations","ENABLE_PUBNUB");
				$PUBNUB_PUBLISH_KEY = $generalobj->getConfigurations("configurations","PUBNUB_PUBLISH_KEY");
				$PUBNUB_SUBSCRIBE_KEY = $generalobj->getConfigurations("configurations","PUBNUB_SUBSCRIBE_KEY");
				
				
				/* For PubNub Setting */
				$tableName = "register_user";
				$iMemberId_VALUE = $passenger_id;
				$iMemberId_KEY = "iUserId";
				$iAppVersion=get_value($tableName, 'iAppVersion', $iMemberId_KEY,$iMemberId_VALUE,'','true');
				$eDeviceType=get_value($tableName, 'eDeviceType', $iMemberId_KEY,$iMemberId_VALUE,'','true');
				/* For PubNub Setting Finished */
				
				$sql = "SELECT iGcmRegId,eDeviceType FROM register_user WHERE iUserId='$passenger_id'";
				$result = $obj->MySQLSelect($sql);
				$registatoin_ids = $result[0]['iGcmRegId'];
				$deviceTokens_arr_ios = array();
				$registation_ids_new = array();
				
				if($ENABLE_PUBNUB == "Yes"  && $PUBNUB_PUBLISH_KEY != "" && $PUBNUB_SUBSCRIBE_KEY != ""){
					//$pubnub = new Pubnub\Pubnub($PUBNUB_PUBLISH_KEY, $PUBNUB_SUBSCRIBE_KEY);
					$pubnub = new Pubnub\Pubnub(array("publish_key" => $PUBNUB_PUBLISH_KEY,"subscribe_key" => $PUBNUB_SUBSCRIBE_KEY, "uuid" => $uuid));
					$channelName = "PASSENGER_".$passenger_id;
					$info = $pubnub->publish($channelName, $message);
					if($result[0]['eDeviceType'] != "Android"){
						//$alertMsg = "Driver is arriving";
						$alertMsg = $tripdriverarrivlbl;
						array_push($deviceTokens_arr_ios, $result[0]['iGcmRegId']);	
						// sendApplePushNotification(0,$deviceTokens_arr_ios,"",$alertMsg,0);
					}	
					}else{
					if($result[0]['eDeviceType'] == "Android"){
						array_push($registation_ids_new, $result[0]['iGcmRegId']);
						$Rmessage         = array("message" => $message);
						$result = send_notification($registation_ids_new, $Rmessage,0);	
						}else{
						//$alertMsg = "Driver is arriving";
						$alertMsg = $tripdriverarrivlbl;
						array_push($deviceTokens_arr_ios, $result[0]['iGcmRegId']);
						sendApplePushNotification(0,$deviceTokens_arr_ios,$message,$alertMsg,0);
					}
				}
				
				$returnArr['Action'] = "1";
				$data['iTripId'] = $iTripId;
				$data['tEndLat'] = $tDestinationLatitude;
				$data['tEndLong'] = $tDestinationLongitude;
				$data['tDaddress'] = $tDestinationAddress;
				$data['PAppVersion'] = $Data_passenger_detail[0]['iAppVersion'];
				$data['eFareType'] = $Data_trips['eFareType'];
				$returnArr['APP_TYPE'] = $generalobj->getConfigurations("configurations","APP_TYPE");
				$returnArr['message']=$data;
				
				if($iCabBookingId !=""){
					$passengerData = get_value('register_user', 'vName,vLastName,vImgName,vFbId,vAvgRating,vPhone,vPhoneCode,iAppVersion', 'iUserId', $passenger_id);
					$returnArr['sourceLatitude'] = $Source_point_latitude;
					$returnArr['sourceLongitude'] = $Source_point_longitude;
					$returnArr['PassengerId'] = $passenger_id;
					$returnArr['PName'] = $passengerData[0]['vName'].' '.$passengerData[0]['vLastName'];
					$returnArr['PPicName'] = $passengerData[0]['vImgName'];
					$returnArr['PFId'] = $passengerData[0]['vFbId'];
					$returnArr['PRating'] = $passengerData[0]['vAvgRating'];
					$returnArr['PPhone'] = $passengerData[0]['vPhone'];
					$returnArr['PPhoneC'] = $passengerData[0]['vPhoneCode'];
					$returnArr['PAppVersion'] = $passengerData[0]['iAppVersion'];
					$returnArr['TripId'] = strval($iTripId);
					$returnArr['DestLocLatitude'] = $tDestinationLatitude;
					$returnArr['DestLocLongitude'] = $tDestinationLongitude;
					$returnArr['DestLocAddress'] = $tDestinationAddress;
				}
				echo json_encode($returnArr); exit;
				} else {
				$data['Action'] = "0";
				$data['message']="LBL_TRY_AGAIN_LATER_TXT";
				echo json_encode($data);
				exit;
			}
			
			/* }else{
				$returnArr['Action'] = "0";
				$returnArr['message']="LBL_CAR_REQUEST_CANCELLED_TXT";
				echo json_encode($returnArr);
			} */
		}
		else{
			if($eStatus == "Complete"){
				$returnArr['Action'] = "0";
				$returnArr['message']="LBL_FAIL_ASSIGN_TO_PASSENGER_TXT";
				}else if($eStatus == "Cancelled"){
				$returnArr['Action'] = "0";
				$returnArr['message']="LBL_CAR_REQUEST_CANCELLED_TXT";
			}
            echo json_encode($returnArr);
		}
	}
	
	###########################################################################
	
	if($type == "DriverArrived"){
		$iDriverId = isset($_REQUEST["iDriverId"])?$_REQUEST["iDriverId"]:'';
		
		if($iDriverId != '') {
			
			$vTripStatus=get_value('register_driver', 'vTripStatus', 'iDriverId',$iDriverId,'','true');
			if($vTripStatus == "Cancelled"){
				$returnArr['Action'] = "0";
				$returnArr['message']="DO_RESTART";
				echo json_encode($returnArr);exit;
			}
			
			$where = " iDriverId = '$iDriverId'";
			
			$Data_update_driver['vTripStatus']='Arrived';
			
			$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
			
			if($id>0){
				
				$sql = "SELECT CONCAT(rd.vName,' ',rd.vLastName) AS driverName, tr.vRideNo, tr.tEndLat,tr.tEndLong,tr.tDaddress,tr.iUserId,tr.eType,rd.iTripId,tr.eTollSkipped FROM trips as tr,register_driver as rd WHERE tr.iTripId=rd.iTripId AND rd.iDriverId = '".$iDriverId."'";
				$result = $obj->MySQLSelect($sql);
				
				// echo "<pre>"; print_r($result);  die;
				
				$returnArr['Action'] = "1";
				
				if($result[0]['iTripId'] != "") {
					// Update Trip Table
					$where1 = " iTripId = '".$result[0]['iTripId']."'";
					$Data_update_trips['tDriverArrivedDate']=date('Y-m-d H:i:s');
					$id = $obj->MySQLQueryPerform("trips",$Data_update_trips,'update',$where1);
				}
				
				if($result[0]['tEndLat'] != '' && $result[0]['tEndLong'] != ''){
					$data['DLatitude'] = $result[0]['tEndLat'];
					$data['DLongitude']=$result[0]['tEndLong'];
					$data['DAddress']=$result[0]['tDaddress'];
					}else{
					$data['DLatitude'] = "0";
					$data['DLongitude']="0";
					$data['DAddress']= "0";
				}
				$data['eTollSkipped']=$result[0]['eTollSkipped'];
				$returnArr['message'] = $data;
				// echo "UpdateSuccess";
				
				$ENABLE_PUBNUB = $generalobj->getConfigurations("configurations","ENABLE_PUBNUB");
				$PUBNUB_PUBLISH_KEY = $generalobj->getConfigurations("configurations","PUBNUB_PUBLISH_KEY");
				$PUBNUB_SUBSCRIBE_KEY = $generalobj->getConfigurations("configurations","PUBNUB_SUBSCRIBE_KEY");
				
				/* For PubNub Setting */
				$tableName = "register_user";
				$iMemberId_VALUE = $result[0]['iUserId'];
				$iMemberId_KEY = "iUserId";
				$iAppVersion=get_value($tableName, 'iAppVersion', $iMemberId_KEY,$iMemberId_VALUE,'','true');
				$eDeviceType=get_value($tableName, 'eDeviceType', $iMemberId_KEY,$iMemberId_VALUE,'','true');
				$iGcmRegId=get_value($tableName, 'iGcmRegId', $iMemberId_KEY,$iMemberId_VALUE,'','true');
				$vLangCode=get_value($tableName, 'vLang', $iMemberId_KEY,$iMemberId_VALUE,'','true');
				/* For PubNub Setting Finished */
				
				if($vLangCode == "" || $vLangCode == NULL){
					$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
				}
				
				$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
				$driverArrivedLblValue = $languageLabelsArr['LBL_DRIVER_ARRIVED_NOTIMSG'];
				$driverArrivedLblValue_ride = $languageLabelsArr['LBL_DRIVER_ARRIVED_TXT'];
				
				$deviceTokens_arr_ios = array();
				$registation_ids_new = array();
				$message = "";
				
				$message_arr['MsgType'] = "DriverArrived";
				$message_arr['iDriverId'] = $iDriverId;
				$message_arr['driverName'] = $result[0]['driverName'];
				$message_arr['vRideNo'] = $result[0]['vRideNo'];
				$message = json_encode($message_arr);
				
				if($ENABLE_PUBNUB == "Yes"  && $PUBNUB_PUBLISH_KEY != "" && $PUBNUB_SUBSCRIBE_KEY != ""/*  && $iAppVersion > 1 && $eDeviceType == "Android" */){
					//$pubnub = new Pubnub\Pubnub($PUBNUB_PUBLISH_KEY, $PUBNUB_SUBSCRIBE_KEY);
					$pubnub = new Pubnub\Pubnub(array("publish_key" => $PUBNUB_PUBLISH_KEY,"subscribe_key" => $PUBNUB_SUBSCRIBE_KEY, "uuid" => $uuid));
					$channelName = "PASSENGER_".$result[0]['iUserId'];
					
					$info = $pubnub->publish($channelName, $message);
				}
				
				#####################Add Status Message#########################
				$DataTripMessages['tMessage']= $message;
				$DataTripMessages['iDriverId']= $iDriverId;
				$DataTripMessages['iTripId']= $result[0]['iTripId'];
				$DataTripMessages['iUserId']= $result[0]['iUserId'];
				$DataTripMessages['eFromUserType']= "Driver";
				$DataTripMessages['eToUserType']= "Passenger";
				$DataTripMessages['eReceived']= "No";
				$DataTripMessages['dAddedDate']= @date("Y-m-d H:i:s");
				
				$obj->MySQLQueryPerform("trip_status_messages",$DataTripMessages,'insert');
				################################################################
				
				if($eDeviceType == "Android" && $ENABLE_PUBNUB != "Yes"){
					
					
					array_push($registation_ids_new, $iGcmRegId);
					$Rmessage         = array("message" => $message);
					$result = send_notification($registation_ids_new, $Rmessage,0);
					}else if($eDeviceType != "Android" ){
					if($ENABLE_PUBNUB == "Yes"){
						$message = "";
					}
					$eType = $result[0]['eType'];
					if($eType == "UberX"){
						$alertMsg = $languageLabelsArr['LBL_DELIVERY_DRIVER_TXT'].' '.$result[0]['driverName'].' '.$driverArrivedLblValue.$result[0]['vRideNo'];
						}else{
						$alertMsg = $driverArrivedLblValue_ride;
					}
					
					array_push($deviceTokens_arr_ios, $iGcmRegId);
					if($message != ""){
						sendApplePushNotification(0,$deviceTokens_arr_ios,$message,$alertMsg,0);
					}
				}
				
				}else{
				$returnArr['Action'] = "0";
				$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
				// echo "UpdateFailed";
			}
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	
	############################################################################
	
	if ($type == "updateDriverLocations") {
		
		$iDriverId              = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$latitude_driver  = isset($_REQUEST["latitude"]) ? $_REQUEST["latitude"] : '';
		$longitude_driver = isset($_REQUEST["longitude"]) ? $_REQUEST["longitude"] : '';
		$vTimeZone = isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		
		
		$where = " iDriverId='$iDriverId'";
		$Data_update_driver['vLatitude']=$latitude_driver;
		$Data_update_driver['vLongitude']=$longitude_driver;
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		# Update User Location Date #
		Updateuserlocationdatetime($iDriverId,"Driver",$vTimeZone);    
		# Update User Location Date #
		
		
		if ($id) {
			$returnArr['Action'] = "1";
			} else {
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	
	###########################################################################
	
	if ($type == "updateTripLocations") {
		
		$tripId     = isset($_REQUEST["TripId"]) ? $_REQUEST["TripId"] : '';
		$latitudes  = isset($_REQUEST['latList']) ? $_REQUEST['latList'] : '';
		$longitudes = isset($_REQUEST['lonList']) ? $_REQUEST['lonList'] : '';
		$iDriverId = isset($_REQUEST['iDriverId']) ? $_REQUEST['iDriverId'] : '';
		
		if($iDriverId != "" && $tripId == ""){
			
			$iTripId = get_value('register_driver', 'iTripId', 'iDriverId',$iDriverId,'','true');
			if($iTripId != ""){
				$tripId = $iTripId;
			}
		}
		
		if($tripId != '' && $latitudes != '' && $longitudes!=''){
			$latitudes = preg_replace("/[^0-9,.-]/", "", $latitudes);
			$longitudes =preg_replace("/[^0-9,.-]/", "", $longitudes);
			$id = processTripsLocations($tripId,$latitudes,$longitudes);
		}
		
		if($id > 0){
			$returnArr['Action'] = "1";
			}else{
			$returnArr['Action'] = "0";
		}
		
		echo json_encode($returnArr);
	}
	
	###########################################################################
	
	
	if ($type == "StartTrip") {
		
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$iDriverId =isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$TripID = isset($_REQUEST["TripID"]) ? $_REQUEST["TripID"] : '';
		$image_name = $vImage	= isset($_FILES['vImage']['name'])?$_FILES['vImage']['name']:'';
		$image_object	= isset($_FILES['vImage']['tmp_name'])?$_FILES['vImage']['tmp_name']:'';
		
		
		$startDateOfTrip=@date("Y-m-d H:i:s");
		$vLangCode=get_value('register_user', 'vLang', 'iUserId',$iUserId,'','true');
		if($vLangCode == "" || $vLangCode == NULL){
			$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		
		$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
		$tripstartlabel = $languageLabelsArr['LBL_DRIVER_START_NOTIMSG'];
		$tripstartlabel_ride = $languageLabelsArr['LBL_START_TRIP_DIALOG_TXT'];
		$message      = "TripStarted";
		
		$sql = "SELECT CONCAT(rd.vName,' ',rd.vLastName) AS driverName, tr.vRideNo FROM trips as tr,register_driver as rd WHERE tr.iTripId=rd.iTripId AND rd.iDriverId = '".$iDriverId."'";
		$result22 = $obj->MySQLSelect($sql);
		
		$verificationCode = rand ( 10000000 , 99999999 );
		
		$eType =get_value('trips', 'eType', 'iTripId',$TripID,'','true');
		$fVisitFee = get_value('trips', 'fVisitFee', 'iTripId', $TripID,'','true');  
		$eFareType = get_value('trips', 'eFareType', 'iTripId', $TripID,'','true');
		
		$message_arr = array();
        $message_arr['Message'] = $message;
        $message_arr['iDriverId'] = $iDriverId;
        $message_arr['iTripId'] = $TripID;
		$message_arr['driverName'] = $result22[0]['driverName'];
		$message_arr['vRideNo'] = $result22[0]['vRideNo'];
		if($eType == "Deliver"){
			$message_arr['VerificationCode'] = strval($verificationCode);
			}else{
			$message_arr['VerificationCode'] = "";
		}
		
        $message = json_encode($message_arr,JSON_UNESCAPED_UNICODE);
		
		#####################Add Status Message#########################
		$DataTripMessages['tMessage']= $message;
		$DataTripMessages['iDriverId']= $iDriverId;
		$DataTripMessages['iTripId']= $TripID;
		$DataTripMessages['iUserId']= $iUserId;
		$DataTripMessages['eFromUserType']= "Driver";
		$DataTripMessages['eToUserType']= "Passenger";
		$DataTripMessages['eReceived']= "No";
		$DataTripMessages['dAddedDate']= @date("Y-m-d H:i:s");
		
		$obj->MySQLQueryPerform("trip_status_messages",$DataTripMessages,'insert');
		################################################################
		
		//Update passenger Table
		$where = " iUserId = '$iUserId'";
		
		$Data_update_passenger['vTripStatus']='On Going Trip';
		
		$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
		
		//Update Driver Table
		$where = " iDriverId = '$iDriverId'";
		
		$Data_update_driver['vTripStatus']='On Going Trip';
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		
		
		$sql = "SELECT iGcmRegId,eDeviceType,iTripId,tLocationUpdateDate FROM register_user WHERE iUserId='$iUserId'";
        $result = $obj->MySQLSelect($sql);
		
        // $Curr_TripID=$result[0]['iTripId'];
		
        $where = " iTripId = '$TripID'";
		
        $Data_update_trips['iActive']='On Going Trip';
        $Data_update_trips['tStartDate']=$startDateOfTrip;
		
		/*Code for Upload StartImage of trip Start */
		if($image_name != ""){
			//$Photo_Gallery_folder = $tconfig['tsite_upload_trip_images_path']."/".$TripID."/";
			$Photo_Gallery_folder = $tconfig['tsite_upload_trip_images_path'];
			if(!is_dir($Photo_Gallery_folder))
			mkdir($Photo_Gallery_folder, 0777);
			$vFile = $generalobj->fileupload($Photo_Gallery_folder,$image_object,$image_name,$prefix='', $vaildExt="pdf,doc,docx,jpg,jpeg,gif,png");
			$vImageName = $vFile[0];
			$Data_update_trips['vBeforeImage']=$vImageName;
		}
		/*Code for Upload StartImage of trip End */
        $id = $obj->MySQLQueryPerform("trips",$Data_update_trips,'update',$where);
		
		
		if($id > 0){
			$returnArr['Action'] = "1";
			$returnArr['fVisitFee'] = $fVisitFee;
			
			$ENABLE_PUBNUB = $generalobj->getConfigurations("configurations","ENABLE_PUBNUB");
			$PUBNUB_PUBLISH_KEY = $generalobj->getConfigurations("configurations","PUBNUB_PUBLISH_KEY");
			$PUBNUB_SUBSCRIBE_KEY = $generalobj->getConfigurations("configurations","PUBNUB_SUBSCRIBE_KEY");
			
			/* For PubNub Setting */
			$tableName = "register_user";
			$iMemberId_VALUE = $iUserId;
			$iMemberId_KEY = "iUserId";
			$iAppVersion=get_value($tableName, 'iAppVersion', $iMemberId_KEY,$iMemberId_VALUE,'','true');
			$eDeviceType=get_value($tableName, 'eDeviceType', $iMemberId_KEY,$iMemberId_VALUE,'','true');
			/* For PubNub Setting Finished */
			
			$cmpMinutes = ceil((fetchtripstatustimeMAXinterval() + 60) / 60);
			$compare_date = @date('Y-m-d H:i:s', strtotime('-'.$cmpMinutes.' minutes'));
			
			$alertSendAllowed = false;

			if($eType == "UberX"){
				$alertMsg = $languageLabelsArr['LBL_DELIVERY_DRIVER_TXT'].' '.$result22[0]['driverName'].' '.$tripstartlabel.$result22[0]['vRideNo'];
			}else{
				$alertMsg = $tripstartlabel_ride;
			}
				
			if($ENABLE_PUBNUB == "Yes"  && $PUBNUB_PUBLISH_KEY != "" && $PUBNUB_SUBSCRIBE_KEY != ""/*  && $iAppVersion > 1 && $eDeviceType == "Android" */){
				
				//$pubnub = new Pubnub\Pubnub($PUBNUB_PUBLISH_KEY, $PUBNUB_SUBSCRIBE_KEY);
				$pubnub = new Pubnub\Pubnub(array("publish_key" => $PUBNUB_PUBLISH_KEY,"subscribe_key" => $PUBNUB_SUBSCRIBE_KEY, "uuid" => $uuid));
				
				$channelName = "PASSENGER_".$iUserId;
				
				$info = $pubnub->publish($channelName, $message);
				
				$message = $alertMsg;
				$tLocUpdateDate = date("Y-m-d H:i:s",strtotime($result[0]['tLocationUpdateDate']));
				if($tLocUpdateDate < $compare_date){
					$alertSendAllowed = true;
				}
				
			}else{
				$alertSendAllowed = true;
			}
			
			$deviceTokens_arr = array();
			
			if($alertSendAllowed == true){
				array_push($deviceTokens_arr, $result[0]['iGcmRegId']);
				
				if($result[0]['eDeviceType'] == "Android" ){
					$Rmessage = array("message" => $message);
					
					send_notification($deviceTokens_arr, $Rmessage,0);
				}else{
					sendApplePushNotification(0,$deviceTokens_arr,$message,$alertMsg,0);
				}
				
			}
			
			
			// Send SMS to receiver if trip type is delivery.
			if($eType == "Deliver"){
				$receiverMobile =get_value('trips', 'vReceiverMobile', 'iTripId',$TripID,'','true');
				$receiverMobile1 = "+".$receiverMobile;
				
				$where_trip_update = " iTripId = '$TripID'";
				$data_delivery['vDeliveryConfirmCode']=$verificationCode;
				$obj->MySQLQueryPerform("trips",$data_delivery,'update',$where);
				
				//$message_deliver = "SMS format goes here. Your verification code is ".$verificationCode." Please give this code to driver to end delivery process.";
				$message_deliver = deliverySmsToReceiver($TripID);
				$result = sendEmeSms($receiverMobile1,$message_deliver);
				if($result ==0){
					$isdCode= $generalobj->getConfigurations("configurations","SITE_ISD_CODE");
					$receiverMobile = "+".$isdCode.$receiverMobile;
					sendEmeSms($receiverMobile,$message_deliver);
				}
				
				$returnArr['message']=$verificationCode;
				$returnArr['SITE_TYPE']=SITE_TYPE;
			}
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		$returnArr['iTripTimeId'] = '';
		if($eFareType == 'Hourly'){
			$dTime = date('Y-m-d H:i:s');
			$Data_update['dResumeTime']=$dTime;
			$Data_update['iTripId']=$TripID;
			$id = $obj->MySQLQueryPerform("trip_times",$Data_update,'insert');
			$returnArr['iTripTimeId'] = $id;
		}
		echo json_encode($returnArr);
		
	}
	
	###########################################################################
	
	if($type == "ProcessEndTrip"){
		global $generalobj;
		$tripId     = isset($_REQUEST["TripId"]) ? $_REQUEST["TripId"] : '';
		$userId     = isset($_REQUEST["PassengerId"]) ? $_REQUEST["PassengerId"] : '';
		$driverId     = isset($_REQUEST["DriverId"]) ? $_REQUEST["DriverId"] : '';
		$latitudes  = isset($_REQUEST["latList"]) ? $_REQUEST["latList"] : '';
		$longitudes = isset($_REQUEST["lonList"]) ? $_REQUEST["lonList"] : '';
		$tripDistance     = isset($_REQUEST["TripDistance"]) ? $_REQUEST["TripDistance"] : '0';
		$dAddress        = isset($_REQUEST["dAddress"]) ? $_REQUEST["dAddress"] : '';
		// $currentCity= isset($_REQUEST["currentCity"]) ? $_REQUEST["currentCity"] : '';
		$destination_lat = isset($_REQUEST["dest_lat"]) ? $_REQUEST["dest_lat"] : '';
		$destination_lon = isset($_REQUEST["dest_lon"]) ? $_REQUEST["dest_lon"] : '';
		$isTripCanceled = isset($_REQUEST["isTripCanceled"]) ? $_REQUEST["isTripCanceled"] : '';
		$driverComment   = isset($_REQUEST["Comment"]) ? $_REQUEST["Comment"] : '';
		$driverReason   = isset($_REQUEST["Reason"]) ? $_REQUEST["Reason"] : '';
		$image_name = $vImage	= isset($_FILES['vImage']['name'])?$_FILES['vImage']['name']:'';
		$image_object	= isset($_FILES['vImage']['tmp_name'])?$_FILES['vImage']['tmp_name']:'';
		$fMaterialFee  = isset($_REQUEST["fMaterialFee"]) ? $_REQUEST["fMaterialFee"] : '';
		$fMiscFee  = isset($_REQUEST["fMiscFee"]) ? $_REQUEST["fMiscFee"] : '';
		$fDriverDiscount  = isset($_REQUEST["fDriverDiscount"]) ? $_REQUEST["fDriverDiscount"] : '';
		
		
		$Active="Finished";
		$vLangCode=get_value('register_user', 'vLang', 'iUserId',$userId,'','true');
		if($vLangCode == "" || $vLangCode == NULL){
			$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		
		$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
		$tripcancelbydriver = $languageLabelsArr['LBL_TRIP_CANCEL_BY_DRIVER'];
		$tripfinish = $languageLabelsArr['LBL_DRIVER_END_NOTIMSG'];
		$tripfinish_ride = $languageLabelsArr['LBL_TRIP_FINISH'];
		
		$message_arr = array();
		$message_arr['ShowTripFare'] = "true";
		if($isTripCanceled == "true"){
			$message  = "TripCancelledByDriver";
			}else{
			$message      = "TripEnd";
		}
		
		$sql = "SELECT CONCAT(rd.vName,' ',rd.vLastName) AS driverName,tr.vRideNo FROM trips as tr,register_driver as rd WHERE tr.iTripId=rd.iTripId AND rd.iDriverId = '".$driverId."'";
		$result22 = $obj->MySQLSelect($sql);
        
        $message_arr['Message'] = $message;
        $message_arr['iTripId'] = $tripId;
        $message_arr['iDriverId'] = $driverId;
		$message_arr['driverName'] = $result22[0]['driverName'];
		$message_arr['vRideNo'] = $result22[0]['vRideNo'];
		if($isTripCanceled == "true"){
			$message_arr['Reason'] = $driverReason;
			$message_arr['isTripStarted'] = "true";
		}
		
        $message = json_encode($message_arr,JSON_UNESCAPED_UNICODE);
		
		#####################Add Status Message#########################
		$DataTripMessages['tMessage']= $message;
		$DataTripMessages['iDriverId']= $driverId;
		$DataTripMessages['iTripId']= $tripId;
		$DataTripMessages['iUserId']= $userId;
		$DataTripMessages['eFromUserType']= "Driver";
		$DataTripMessages['eToUserType']= "Passenger";
		$DataTripMessages['eReceived']= "No";
		$DataTripMessages['dAddedDate']= @date("Y-m-d H:i:s");
		
		$obj->MySQLQueryPerform("trip_status_messages",$DataTripMessages,'insert');
		################################################################
		
		$couponCode=get_value('trips', 'vCouponCode', 'iTripId',$tripId,'','true');
		$discountValue = 0;
		$discountValueType= "cash";
		if( $couponCode != ''){
			$discountValue = get_value('coupon', 'fDiscount', 'vCouponCode', $couponCode,'','true');
			$discountValueType = get_value('coupon', 'eType', 'vCouponCode', $couponCode,'','true');
		}
		
		
		if($latitudes != '' && $longitudes != ''){
			processTripsLocations($tripId,$latitudes,$longitudes);
		}
		
		$vCurrencyDriver=get_value('register_driver', 'vCurrencyDriver', 'iDriverId', $driverId,'','true');
		$currencySymbolDriver = get_value('currency', 'vSymbol', 'vName', $vCurrencyDriver,'','true');
		
		$sql = "SELECT tStartDate,tEndDate,iVehicleTypeId,tStartLat,tStartLong,eFareType,fRatio_".$vCurrencyDriver." as fRatioDriver, vTripPaymentMode,fPickUpPrice,fNightPrice, eType, fTollPrice FROM trips WHERE iTripId='$tripId'";
		$trip_start_data_arr = $obj->MySQLSelect($sql);
		
		$tripDistance=calcluateTripDistance($tripId);
		
		$sourcePointLatitude=$trip_start_data_arr[0]['tStartLat'];
		$sourcePointLongitude=$trip_start_data_arr[0]['tStartLong'];
		$startDate=$trip_start_data_arr[0]['tStartDate'];
		$vehicleTypeID=$trip_start_data_arr[0]['iVehicleTypeId'];
		$eFareType=$trip_start_data_arr[0]['eFareType'];
		$eType=$trip_start_data_arr[0]['eType'];
		
		//$endDateOfTrip=@date("Y-m-d H:i:s");
		$endDateOfTrip = $trip_start_data_arr[0]['tEndDate'];
		if($endDateOfTrip == "0000-00-00 00:00:00"){
			$endDateOfTrip=@date("Y-m-d H:i:s");    
		}
		
		if($eFareType == 'Hourly'){
			$sql22 = "SELECT * FROM `trip_times` WHERE iTripId='$tripId'";
			$db_tripTimes = $obj->MySQLSelect($sql22);
			
			$totalSec = 0;
			$iTripTimeId = '';
			foreach($db_tripTimes as $dtT){
				if($dtT['dPauseTime'] != '' && $dtT['dPauseTime'] != '0000-00-00 00:00:00') {
					$totalSec += strtotime($dtT['dPauseTime']) - strtotime($dtT['dResumeTime']);
				}
			}
			$totalTimeInMinutes_trip=@round(abs($totalSec) / 60,2);
			}else {
			$totalTimeInMinutes_trip=@round(abs(strtotime($startDate) - strtotime($endDateOfTrip)) / 60,2);
		}
		if($totalTimeInMinutes_trip <= 1){
			$FinalDistance= $tripDistance;
			}else{
			$FinalDistance=checkDistanceWithGoogleDirections($tripDistance,$sourcePointLatitude,$sourcePointLongitude,$destination_lat,$destination_lon);
		}
		
		$tripDistance=$FinalDistance;
		
		$Fare_data=calculateFare($totalTimeInMinutes_trip,$tripDistance,$vehicleTypeID,$userId,1,$startDate,$endDateOfTrip,$couponCode,$tripId,$fMaterialFee,$fMiscFee,$fDriverDiscount);
		$where = " iTripId = '" . $tripId . "'";
		
		$Data_update_trips['tEndDate']=$endDateOfTrip;
		$Data_update_trips['tEndLat']=$destination_lat;
		$Data_update_trips['tEndLong']=$destination_lon;
		$Data_update_trips['tDaddress']=$dAddress;
		$Data_update_trips['iFare']=$Fare_data['total_fare'];
		$Data_update_trips['iActive']=$Active;
		$Data_update_trips['fDistance']=$tripDistance;
		$Data_update_trips['fDuration']=$totalTimeInMinutes_trip;
		$Data_update_trips['fPricePerMin']=$Fare_data['fPricePerMin'];
		$Data_update_trips['fPricePerKM']=$Fare_data['fPricePerKM'];
		$Data_update_trips['iBaseFare']=$Fare_data['iBaseFare'];
		$Data_update_trips['fCommision']=$Fare_data['fCommision'];
		$Data_update_trips['fDiscount']=$Fare_data['fDiscount'];
		$Data_update_trips['vDiscount'] =$Fare_data['vDiscount'] ;
		$Data_update_trips['fMinFareDiff']=$Fare_data['MinFareDiff'];
		$Data_update_trips['fSurgePriceDiff']=$Fare_data['fSurgePriceDiff'];
		$Data_update_trips['fWalletDebit']=$Fare_data['user_wallet_debit_amount'];
		$Data_update_trips['fTripGenerateFare']=$Fare_data['fTripGenerateFare'];
		$Data_update_trips['fMaterialFee']=$_REQUEST['fMaterialFee'];
		$Data_update_trips['fMiscFee']=$_REQUEST['fMiscFee'];
		$Data_update_trips['fDriverDiscount']=$_REQUEST['fDriverDiscount'];
		
		if($isTripCanceled == "true"){
			$Data_update_trips['vCancelReason'] = $driverReason;
			$Data_update_trips['vCancelComment'] = $driverComment;
			$Data_update_trips['eCancelled'] = "Yes";
		}
		
		/*Code for Upload AfterImage of trip Start */
		if($image_name != ""){
			//$Photo_Gallery_folder = $tconfig['tsite_upload_trip_images_path']."/".$TripID."/";
			$Photo_Gallery_folder = $tconfig['tsite_upload_trip_images_path'];
			if(!is_dir($Photo_Gallery_folder))
			mkdir($Photo_Gallery_folder, 0777);
			$vFile = $generalobj->fileupload($Photo_Gallery_folder,$image_object,$image_name,$prefix='', $vaildExt="pdf,doc,docx,jpg,jpeg,gif,png");
			$vImageName = $vFile[0];
			$Data_update_trips['vAfterImage']=$vImageName;
		}
		/*Code for Upload AfterImage of trip End */
		$id = $obj->MySQLQueryPerform("trips",$Data_update_trips,'update',$where);
		
		$trip_status    = "Not Active";
		
		$where = " iUserId = '$userId'";
		$Data_update_passenger['iTripId']=$tripId;
		$Data_update_passenger['vTripStatus']=$trip_status;
		$Data_update_passenger['vCallFromDriver']='Not Assigned';
		
		$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
		
		$where = " iDriverId = '$driverId'";
		$Data_update_driver['iTripId']=$tripId;
		$Data_update_driver['vTripStatus']=$trip_status;
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		
		if($id>0){
			
			$ENABLE_PUBNUB = $generalobj->getConfigurations("configurations","ENABLE_PUBNUB");
			$PUBNUB_PUBLISH_KEY = $generalobj->getConfigurations("configurations","PUBNUB_PUBLISH_KEY");
			$PUBNUB_SUBSCRIBE_KEY = $generalobj->getConfigurations("configurations","PUBNUB_SUBSCRIBE_KEY");
			
			/* For PubNub Setting */
			$tableName = "register_user";
			$iMemberId_VALUE = $userId;
			$iMemberId_KEY = "iUserId";
			$iAppVersion=get_value($tableName, 'iAppVersion', $iMemberId_KEY,$iMemberId_VALUE,'','true');
			$eDeviceType=get_value($tableName, 'eDeviceType', $iMemberId_KEY,$iMemberId_VALUE,'','true');
			$tLocationUpdateDate=get_value($tableName, 'tLocationUpdateDate', $iMemberId_KEY,$iMemberId_VALUE,'','true');
			$iGcmRegId=get_value($tableName, 'iGcmRegId', $iMemberId_KEY,$iMemberId_VALUE,'','true');
			/* For PubNub Setting Finished */
			
			$cmpMinutes = ceil((fetchtripstatustimeMAXinterval() + 60) / 60);
			$compare_date = @date('Y-m-d H:i:s', strtotime('-'.$cmpMinutes.' minutes'));
			
			$alertSendAllowed = false;
			
			if($isTripCanceled == "true"){
				// $alertMsg = $tripcancelbydriver;
				if($eType == "UberX"){
					$usercanceltriplabel = $result22[0]['driverName'].':'.$result22[0]['vRideNo'].'-'. $languageLabelsArr['LBL_PREFIX_TRIP_CANCEL_DRIVER'].' '.$driverReason;
				}else{
					$usercanceltriplabel = $languageLabelsArr['LBL_PREFIX_TRIP_CANCEL_DRIVER'].' '.$driverReason;
				}
			}else{
				if($eType == "UberX"){
					$alertMsg = $tripfinish;
				}else{
					$alertMsg = $tripfinish_ride;
				}
			}
			
			if($ENABLE_PUBNUB == "Yes"  && $PUBNUB_PUBLISH_KEY != "" && $PUBNUB_SUBSCRIBE_KEY != "" /* && $iAppVersion > 1 && $eDeviceType == "Android" */){
				
				//$pubnub = new Pubnub\Pubnub($PUBNUB_PUBLISH_KEY, $PUBNUB_SUBSCRIBE_KEY);
				$pubnub = new Pubnub\Pubnub(array("publish_key" => $PUBNUB_PUBLISH_KEY,"subscribe_key" => $PUBNUB_SUBSCRIBE_KEY, "uuid" => $uuid));
				
				$channelName = "PASSENGER_".$userId;
				
				$info = $pubnub->publish($channelName, $message);
				
				$message = $alertMsg;
				$tLocUpdateDate = date("Y-m-d H:i:s",strtotime($tLocationUpdateDate));
				if($tLocUpdateDate < $compare_date){
					$alertSendAllowed = true;
				}
			}else{
				$alertSendAllowed = true;
			}
			
			$deviceTokens_arr = array();
			
			if($alertSendAllowed == true){
				array_push($deviceTokens_arr, $iGcmRegId);
				
				if($eDeviceType == "Android" ){
					$Rmessage = array("message" => $message);
					
					send_notification($deviceTokens_arr, $Rmessage,0);
				}else{
					sendApplePushNotification(0,$deviceTokens_arr,$message,$alertMsg,0);
				}
				
			}
			
			$returnArr['Action'] = "1";
			$returnArr['iTripsLocationsID']=$id;
			// $returnArr['TotalFare']=round($Fare_data[0]['total_fare'] * $trip_start_data_arr[0]['fRatioDriver']);
			$returnArr['TotalFare']=round($Fare_data['total_fare'] * $trip_start_data_arr[0]['fRatioDriver'],1);
			// $returnArr['CurrencySymbol']=($obj->MySQLSelect("SELECT vSymbol FROM currency WHERE vName='".$trip_start_data_arr[0]['vCurrencyDriver']."' ")[0]['vSymbol']);
			$returnArr['CurrencySymbol']=$currencySymbolDriver;
			$returnArr['tripStartTime']=$startDate;
			$returnArr['TripPaymentMode']=$trip_start_data_arr[0]['vTripPaymentMode'];
			$returnArr['Discount']=round($Fare_data['fDiscount'] * $trip_start_data_arr[0]['fRatioDriver'],1);
			$returnArr['Message']="Data Updated";
			$returnArr['FormattedTripDate']=date('dS M Y \a\t h:i a',strtotime($startDate));
			
			
			$generalobj->get_benefit_amount($tripId);
			
			// Code for Check last logout date is update in driver_log_report
            $query = "SELECT * FROM driver_log_report WHERE iDriverId = '".$driverId."' ORDER BY iDriverLogId DESC LIMIT 0,1";
            $db_driver = $obj->MySQLSelect($query);
            if(count($db_driver) > 0) {
				$driver_lastonline = @date("Y-m-d H:i:s");
				$updateQuery = "UPDATE driver_log_report set dLogoutDateTime='".$driver_lastonline."' WHERE iDriverLogId = ".$db_driver[0]['iDriverLogId'];
				$obj->sql_query($updateQuery);
			}
            // Code for Check last logout date is update in driver_log_report Ends
			
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
		
	}
	
	###########################################################################
	
	if($type=="CollectPayment"){
		$iTripId     = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '';
		$isCollectCash     = isset($_REQUEST["isCollectCash"]) ? $_REQUEST["isCollectCash"] : '';
		
		$sql = "SELECT vTripPaymentMode,iUserId,iDriverId,iFare,vRideNo,fWalletDebit,fTripGenerateFare,fDiscount,fCommision,fTollPrice,eHailTrip FROM trips WHERE iTripId='$iTripId'";
		$tripData = $obj->MySQLSelect($sql);
		
		$vTripPaymentMode = $tripData[0]['vTripPaymentMode'];
		$data['vTripPaymentMode']=$vTripPaymentMode;
		$iUserId = $tripData[0]['iUserId'];
		//$iFare = $tripData[0]['iFare']+$tripData[0]['fTollPrice'];
		$iFare = $tripData[0]['iFare'];
		$vRideNo = $tripData[0]['vRideNo'];
		$eHailTrip = $tripData[0]['eHailTrip'];
		
		$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		
		$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
		
		if($vTripPaymentMode == "Card" && $isCollectCash == ""){
			
			$vStripeCusId = get_value('register_user', 'vStripeCusId', 'iUserId', $iUserId,'','true');
			
			$price_new = $iFare * 100;
      $price_new = round($price_new);
			$currency = get_value('currency', 'vName', 'eDefault', 'Yes','','true');
			
			$description = $languageLabelsArr['LBL_TRIP_PAYMENT_RECEIVED']." ".$vRideNo;
			
			try{
				if($iFare > 0){
					$charge_create = Stripe_Charge::create(array(
					"amount" => $price_new,
					"currency" => $currency,
					"customer" => $vStripeCusId,
					"description" =>  $description
					));
					
					$details = json_decode($charge_create);
					$result = get_object_vars($details);
				}
				
				
				if($iFare == 0 || ($result['status']=="succeeded" && $result['paid']=="1")){
					
					$pay_data['tPaymentUserID']= $iFare == 0? "":$result['id'];
					$pay_data['vPaymentUserStatus']="approved";
					$pay_data['iTripId']=$iTripId;
					$pay_data['iAmountUser']=$iFare;
					
					$id = $obj->MySQLQueryPerform("payments",$pay_data,'insert');
					
					}else{
					$returnArr['Action'] = "0";
					$returnArr['message']="LBL_CHARGE_COLLECT_FAILED";
					
					echo json_encode($returnArr);exit;
				}
				
				
				}catch(Exception $e){
				$error3 = $e->getMessage();
				$returnArr['Action'] = "0";
				$returnArr['message']=$error3;
				//$returnArr['message']="LBL_CHARGE_COLLECT_FAILED";
				
				echo json_encode($returnArr);exit;
			}
			$data['vTripPaymentMode']="Card";
			}else if($vTripPaymentMode == "Card" && $isCollectCash == "true"){
			// echo "else if";exit;
			$data['vTripPaymentMode']="Cash";
		}
		
		// echo "out";exit;
		$where = " iTripId = '$iTripId'";
		$data['ePaymentCollect']="Yes";
		
		$id = $obj->MySQLQueryPerform("trips",$data,'update',$where);
		
		$fWalletDebit = $tripData[0]['fWalletDebit'];
		$fDiscount = $tripData[0]['fDiscount'];
		$discountValue = $fWalletDebit + $fDiscount;
		//$discountValue = $tripData[0]['fDiscount'];
		//$walletamountofcreditcard = $tripData[0]['fTripGenerateFare']+$tripData[0]['fTollPrice'];
		$walletamountofcreditcard = $tripData[0]['fTripGenerateFare'];
		$driverId = $tripData[0]['iDriverId'];
		
		$COMMISION_DEDUCT_ENABLE=$generalobj->getConfigurations("configurations","COMMISION_DEDUCT_ENABLE");
		if($COMMISION_DEDUCT_ENABLE == 'Yes') {
			#Deduct Amount From Driver's Wallet Acount#
			$vTripPaymentMode = $data['vTripPaymentMode']; 
			if($vTripPaymentMode == "Cash"){
				$vRideNo = $tripData[0]['vRideNo'];
				$iBalance = $tripData[0]['fCommision'];
				$eFor = "Withdrawl";
				$eType = "Debit";
				$iTripId = $iTripId;	
				$tDescription = 'Debited for booking#'.$vRideNo;
				$ePaymentStatus = 'Settelled';
				$dDate =   Date('Y-m-d H:i:s');
				if($discountValue > 0){
					$eFor_credit = "Deposit";
					$eType_credit = "Credit";
					$tDescription_credit = 'Credited for booking#'.$vRideNo;
					$generalobj->InsertIntoUserWallet($driverId,"Driver",$discountValue,$eType_credit,$iTripId,$eFor_credit,$tDescription_credit,$ePaymentStatus,$dDate);
				}
				$generalobj->InsertIntoUserWallet($driverId,"Driver",$iBalance,$eType,$iTripId,$eFor,$tDescription,$ePaymentStatus,$dDate);
				$Where = " iTripId = '$iTripId'";
				$Data_update_driver_paymentstatus['eDriverPaymentStatus']="Settelled";
				$Update_Payment_Id = $obj->MySQLQueryPerform("trips",$Data_update_driver_paymentstatus,'update',$Where);
			}
			/* else{
				$vRideNo = $tripData[0]['vRideNo'];
				$iBalance = $walletamountofcreditcard-$tripData[0]['fCommision'];
				$eFor = "Deposit";
				$eType = "Credit";
				$iTripId = $iTripId;	
				$tDescription = ' Amount '.$iBalance.' Credited into your account for booking no#'.$vRideNo;
				$ePaymentStatus = 'Settelled';
				$dDate =   Date('Y-m-d H:i:s');
				$generalobj->InsertIntoUserWallet($driverId,"Driver",$iBalance,$eType,$iTripId,$eFor,$tDescription,$ePaymentStatus,$dDate);
				$Where = " iTripId = '$iTripId'";
				$Data_update_driver_paymentstatus['eDriverPaymentStatus']="Settelled";
				$Update_Payment_Id = $obj->MySQLQueryPerform("trips",$Data_update_driver_paymentstatus,'update',$Where);
			} */
			#Deduct Amount From Driver's Wallet Acount#
		}
		if($id >0){
			$returnArr['Action'] = "1";
			
			// Rating entry if trip is hail
			if($eHailTrip == "Yes"){
				
				$Data_update_ratings['iTripId']=$iTripId;
				$Data_update_ratings['vRating1']="5.0";
				$Data_update_ratings['vMessage']="";
				$Data_update_ratings['eUserType']="Driver";
				
				$obj->MySQLQueryPerform("ratings_user_driver",$Data_update_ratings,'insert');
				
				$Data_update_ratings['eUserType']="Passenger";
				
				$obj->MySQLQueryPerform("ratings_user_driver",$Data_update_ratings,'insert');
			}
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
	}
	
	###########################################################################
	
	###########################################################################
	
	if($type=="addMoneyUserWallet"){
		$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$eMemberType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';  //Passenger,Driver
		$fAmount = isset($_REQUEST["fAmount"]) ? $_REQUEST["fAmount"] : '';
		if($eMemberType == "Passenger"){
			$tbl_name = "register_user";
			$currencycode = "vCurrencyPassenger";
			$iUserId = "iUserId";
			$eUserType = "Rider";
			}else{
			$tbl_name = "register_driver";
			$currencycode = "vCurrencyDriver";
			$iUserId = "iDriverId";
			$eUserType = "Driver";
		}
		$vStripeCusId = get_value($tbl_name, 'vStripeCusId', $iUserId, $iMemberId,'','true');
		$vStripeToken = get_value($tbl_name, 'vStripeToken', $iUserId, $iMemberId,'','true');
		$userCurrencyCode = get_value($tbl_name, $currencycode, $iUserId, $iMemberId,'','true');
		$userCurrencyRatio = get_value('currency', 'Ratio', 'vName', $userCurrencyCode,'','true');
		$walletamount =  round($fAmount/$userCurrencyRatio,2);
		$currencyCode = get_value('currency', 'vName', 'eDefault', 'Yes','','true');
		$currencyratio = get_value('currency', 'Ratio', 'vName', $currencyCode,'','true');
		$price = $fAmount*$currencyratio;
		$price_new = $walletamount * 100;
		$price_new = round($price_new);
		if($vStripeCusId == "" || $vStripeToken == ""){
			$returnArr["Action"] = "0";
			$returnArr['message']="LBL_NO_CARD_AVAIL_NOTE";
			echo json_encode($returnArr);exit;
		}
		
		$dDate = Date('Y-m-d H:i:s');
		$eFor = 'Deposit';
		$eType = 'Credit';
		$iTripId = 0;
		$tDescription = "Amount credited";
		$ePaymentStatus = 'Unsettelled';
		
		try{
			$charge_create = Stripe_Charge::create(array(
			"amount" => $price_new,
			"currency" => $currencyCode,
			"customer" => $vStripeCusId,
			"description" =>  $tDescription
			));
			
			$details = json_decode($charge_create);
			$result = get_object_vars($details);
			//echo "<pre>";print_r($result);exit;
			if($result['status']=="succeeded" && $result['paid']=="1"){
				$generalobj->InsertIntoUserWallet($iMemberId,$eUserType,$walletamount,'Credit',0,$eFor,$tDescription,$ePaymentStatus,$dDate);
				$user_available_balance = $generalobj->get_user_available_balance($iMemberId,$eUserType);
				$returnArr["Action"] = "1";
				$returnArr["MemberBalance"] = strval($generalobj->userwalletcurrency(0,$user_available_balance,$userCurrencyCode));
				$returnArr['message1']= "LBL_WALLET_MONEY_CREDITED";
				
				if($eMemberType != "Driver"){
					$returnArr['message'] = getPassengerDetailInfo($iMemberId,"");
					}else{
					$returnArr['message'] = getDriverDetailInfo($iMemberId);
				}
				
				echo json_encode($returnArr);exit;
				}else{
				$returnArr['Action'] = "0";
				$returnArr['message']="LBL_WALLET_MONEY_CREDITED_FAILED";
				
				echo json_encode($returnArr);exit;
			}
			
			}catch(Exception $e){
			//echo "<pre>";print_r($e);exit;
			$error3 = $e->getMessage();
			$returnArr["Action"] = "0";
			$returnArr['message']=$error3;
			//$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
			
			echo json_encode($returnArr);exit;
		}
		
	}
	###########################################################################
	
	if($type == "GenerateCustomer"){
		$iUserId     = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$eMemberType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';  //Passenger,Driver
		$vStripeToken     = isset($_REQUEST["vStripeToken"]) ? $_REQUEST["vStripeToken"] : '';
		$CardNo     = isset($_REQUEST["CardNo"]) ? $_REQUEST["CardNo"] : '';
		
		if($eMemberType == "Passenger"){
			$tbl_name = "register_user";
			$vEmail = "vEmail";
			$iMemberId = "iUserId";
			$eUserType = "Rider";
			}else{
			$tbl_name = "register_driver";
			$vEmail = "vEmail";
			$iMemberId = "iDriverId";
			$eUserType = "Driver";
		}
		
		$vEmail = get_value($tbl_name, $vEmail, $iMemberId, $iUserId,'','true');
		$vStripeCusId = get_value($tbl_name, 'vStripeCusId', $iMemberId, $iUserId,'','true');
		
		try{
			if($vStripeCusId  != ""){
				$customer 	= Stripe_Customer::retrieve($vStripeCusId);
				$sources = $customer -> sources;
				$stripeData = $sources -> data;
				
				if(count($stripeData) >0 && $stripeData[0]['id'] != ''){
					$customer->sources->retrieve($stripeData[0]['id'])->delete();
				}
				
				$card = $customer->sources->create(array("source" => $vStripeToken));
				
			}else{
         try{
    			$customer 	= Stripe_Customer::create(array( "source" => $vStripeToken, "email" => $vEmail));
    			$vStripeCusId = $customer->id;
      	 } catch (Exception $e) {
      				$error3 = $e->getMessage();
      				$returnArr['Action'] = "0";
      				$returnArr['message']=$error3;
      				echo json_encode($returnArr);exit;
    		 }
      }
		}catch(Exception $e){
			$errMsg = $e->getMessage();
			if (strpos($errMsg, 'No such customer') !== false) {
				try{
					$customer 	= Stripe_Customer::create(array( "source" => $vStripeToken, "email" => $vEmail));
				}catch(Exception $e){
					$error3 = $e->getMessage();
					$returnArr['Action'] = "0";
					$returnArr['message']=$error3;
					
					echo json_encode($returnArr);exit;
				}
				
				$vStripeCusId = $customer->id;
			}else{
				$returnArr['Action'] = "0";
				$returnArr['message']=$errMsg;
				
				echo json_encode($returnArr);exit;
			}
		}
		
		$where = " $iMemberId = '$iUserId'";
		$updateData['vStripeToken']=$vStripeToken;
		$updateData['vStripeCusId']=$vStripeCusId;
		$updateData['vCreditCard']=$CardNo;
		
		$id = $obj->MySQLQueryPerform($tbl_name,$updateData,'update',$where);
		if($eMemberType == "Passenger"){
			$profileData =  getPassengerDetailInfo($iUserId);
		}else{
			$profileData =  getDriverDetailInfo($iUserId);
		}
		
		if($id >0){
			$returnArr['Action'] = "1";
			$returnArr['message'] = $profileData;
		}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
				
		/* if($vStripeCusId != ""){
			$customer 	= Stripe_Customer::retrieve($vStripeCusId);
			$sources = $customer -> sources;
			$data = $sources -> data;
			// print_r($sources);
			// echo "<br/>".$data[0]['id'];exit;
			
			if(count($data) >0 && $data[0]['id'] != ''){
				$customer->sources->retrieve($data[0]['id'])->delete();
			}
			
			$card = $customer->sources->create(array("source" => $vStripeToken));
			
			$where = " $iMemberId = '$iUserId'";
			$data_user['vStripeToken']=$vStripeToken;
			$data_user['vCreditCard']=$CardNo;
			
			$id = $obj->MySQLQueryPerform($tbl_name,$data_user,'update',$where);
			if($eMemberType == "Passenger"){
				$profileData =  getPassengerDetailInfo($iUserId);
				}else{
				$profileData =  getDriverDetailInfo($iUserId);
			}
			
			if($id >0){
				$returnArr['Action'] = "1";
				$returnArr['message'] = $profileData;
				}else{
				$returnArr['Action'] = "0";
				$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
			}
			
			}else{
			try{
				$customer 	= Stripe_Customer::create(array( "source" => $vStripeToken, "email" => $vEmail));
				$vStripeCustomerId = $customer->id;
				
				$where = " $iMemberId = '$iUserId'";
				$data['vStripeToken']=$vStripeToken;
				$data['vStripeCusId']=$vStripeCustomerId;
				$data['vCreditCard']=$CardNo;
				
				$id = $obj->MySQLQueryPerform($tbl_name,$data,'update',$where);
				if($eMemberType == "Passenger"){
					$profileData =  getPassengerDetailInfo($iUserId);
					}else{
					$profileData =  getDriverDetailInfo($iUserId);
				}
				
				if($id >0){
					$returnArr['Action'] = "1";
					$returnArr['message'] = $profileData;
					}else{
					$returnArr['Action'] = "0";
					$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
				}
				
				} catch (Exception $e) {
				$error3 = $e->getMessage();
				$returnArr['Action'] = "0";
				$returnArr['message']=$error3;
				//$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
			}
			
		} */
		echo json_encode($returnArr);
	}
	
	###########################################################################
	
	if($type == "CheckCard"){
		$iUserId     = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		
		$vStripeCusId = get_value('register_user', 'vStripeCusId', 'iUserId', $iUserId,'','true');
		
		if($vStripeCusId != ""){
			
			try{
				$customer 	= Stripe_Customer::retrieve($vStripeCusId);
				$sources = $customer -> sources;
				$data = $sources -> data;
				
				$cvc_check = $data[0]['cvc_check'];
				
				if($cvc_check && $cvc_check == "pass"){
					$returnArr['Action'] = "1";
					}else{
					$returnArr['Action'] = "0";
					$returnArr['message']="LBL_INVALID_CARD";
				}
				}catch (Exception $e) {
				$error3 = $e->getMessage();
				$returnArr['Action'] = "0";
				$returnArr['message']=$error3;
				//$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
				
			}
			
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
	}
	
	###########################################################################
	
	if($type == "getDriverRideHistory"){
		$iDriverId     = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$date     = isset($_REQUEST["date"]) ? $_REQUEST["date"] : '';
		$vTimeZone = isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		$date = $date." "."12:01:00";
		$date = date("Y-m-d H:i:s",strtotime($date)); 
		$serverTimeZone = date_default_timezone_get();
		$date = converToTz($date,$serverTimeZone,$vTimeZone,"Y-m-d");  
		
		$vCurrencyDriver=get_value('register_driver', 'vCurrencyDriver', 'iDriverId', $iDriverId,'','true');
		// $currencySymbol=get_value('currency', 'vSymbol', 'eDefault', 'Yes','','true');
		// $priceRatio=1;
		// $fRatioDriver = get_value('currency', 'Ratio', 'vName', $vCurrencyDriver,'','true');
		$currencySymbol = get_value('currency', 'vSymbol', 'vName', $vCurrencyDriver,'','true');
		
		$vLanguage=get_value('register_driver', 'vLang', 'iDriverId',$iDriverId,'','true');
		if($vLanguage == "" || $vLanguage == NULL){
			$vLanguage = "EN";
		}
		
		//$sql = "SELECT tr.*, rate.vRating1, rate.vMessage,ru.vName,ru.vLastName,ru.vImgName as vImage FROM trips as tr,ratings_user_driver as rate,register_user as ru WHERE tr.iDriverId='$iDriverId' AND tr.tTripRequestDate LIKE '".$date."%' AND tr.iActive='Finished' AND rate.iTripId = tr.iTripId AND rate.eUserType='Passenger' AND ru.iUserId=tr.iUserId";
		$sql = "SELECT tr.*, ru.vName,ru.vLastName,ru.vImgName as vImage FROM trips as tr,register_user as ru WHERE tr.iDriverId='$iDriverId' AND tr.tTripRequestDate LIKE '".$date."%' AND tr.iActive='Finished' AND ru.iUserId=tr.iUserId";
		
		$tripData = $obj->MySQLSelect($sql);
		
		$totalEarnings = 0;
		$avgRating = 0;
		
		if(count($tripData) > 0){
			
			for($i=0;$i<count($tripData);$i++){
				// $iFare = $tripData[$i]['fTripGenerateFare']-$tripData[$i]['fTollPrice'];
				$iFare = $tripData[$i]['fTripGenerateFare'];
				//$iFare = $tripData[$i]['fTripGenerateFare'];
				$fCommision = $tripData[$i]['fCommision'];
				$fDiscount = $tripData[$i]['fDiscount'];
				$fTipPrice = $tripData[$i]['fTipPrice']; 
				$fTollPrice = $tripData[$i]['fTollPrice'];
				//$vRating1 = $tripData[$i]['vRating1'];
				$priceRatio = $tripData[$i]['fRatio_'.$vCurrencyDriver];
				
				$sql = "SELECT vRating1, vMessage FROM ratings_user_driver WHERE iTripId = '".$tripData[$i]['iTripId']."' AND eUserType='Passenger'";
			    $tripData_rating = $obj->MySQLSelect($sql);
				if(count($tripData_rating) > 0){
					$tripData[$i]['vRating1'] = $tripData_rating[0]['vRating1'];
					$tripData[$i]['vMessage'] = $tripData_rating[0]['vMessage'];
					$vRating1 = $tripData_rating[0]['vRating1'];
					}else{
					$tripData[$i]['vRating1'] = "0";
					$tripData[$i]['vMessage'] = "";
					$vRating1 = 0;
				}
				
				if(($iFare == "" || $iFare == 0) && $fDiscount > 0){
					$incValue = ($fDiscount - $fCommision)+$fTipPrice;
					$totalEarnings = $totalEarnings + ($incValue * $priceRatio);
					}else if($iFare != "" && $iFare > 0){
					$incValue = ($iFare - $fCommision)+$fTipPrice;
					$totalEarnings = $totalEarnings + ($incValue * $priceRatio);
				}
				
				$avgRating = $avgRating + $vRating1;
				
				$returnArr = getTripPriceDetails($tripData[$i]['iTripId'],$iDriverId,"Driver");
				$tripData[$i] = array_merge($tripData[$i], $returnArr);
				
				if($APP_TYPE == 'UberX'){
					$tripData[$i]['tDaddress'] = "";
				}
			}
			
			$returnArr['Action'] = "1";
			$returnArr['message'] = $tripData;
			
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message'] = "LBL_NO_DATA_AVAIL";
		}
		$returnArr['TotalEarning'] = strval(round($totalEarnings,2));
		$returnArr['TripDate'] = date('l, dS M Y',strtotime($date));
		$returnArr['TripCount'] = strval(count($tripData));
		$returnArr['AvgRating'] = strval(round(count($tripData) == 0? 0 : ($avgRating/count($tripData)),2));
		$returnArr['CurrencySymbol'] = $currencySymbol;
		
		echo json_encode($returnArr);
		
	}
	###########################################################################
	
	if ($type == "loadDriverFeedBack") {
		global $generalobj,$tconfig;
		
		$page        = isset($_REQUEST['page']) ? trim($_REQUEST['page']) : 1;
		$iDriverId = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
		$vAvgRating =  get_value('register_driver', 'vAvgRating', 'iDriverId', $iDriverId,'','true');
		
		$per_page = 10;
		$sql_all  = "SELECT COUNT(iTripId) As TotalIds FROM trips WHERE  iDriverId='$iDriverId' AND iActive='Finished' AND eHailTrip='No'";
		
		
		$data_count_all = $obj->MySQLSelect($sql_all);
		
		$TotalPages = ceil($data_count_all[0]['TotalIds'] / $per_page);
		
		$start_limit = ($page - 1) * $per_page;
		$limit       = " LIMIT " . $start_limit . ", " . $per_page;
		
		//$sql  = "SELECT rate.*,DATE_FORMAT(rate.tDate, '%M, %Y') AS tDate FROM ratings_user_driver as rate, trips as tr WHERE  rate.iTripId = tr.iTripId AND tr.iDriverId='$iDriverId' AND tr.iActive='Finished' AND rate.eUserType='Passenger' ORDER BY tr.iTripId DESC". $limit;
		$sql  = "SELECT rate.*,CONCAT(ru.vName,' ',ru.vLastName) as vName,ru.iUserId as passengerid,ru.vImgName FROM ratings_user_driver as rate LEFT JOIN trips as tr ON tr.iTripId = rate.iTripId  LEFT JOIN register_user as ru ON ru.iUserId = tr.iUserId WHERE tr.iDriverId='$iDriverId' AND tr.iActive='Finished' AND tr.eHailTrip='No' AND rate.eUserType='Passenger' ORDER BY tr.iTripId DESC". $limit;
		
		$Data = $obj->MySQLSelect($sql);
		for($i=0;$i<count($Data);$i++){
			$Data[$i]['vImage'] = $tconfig["tsite_upload_images_passenger"].'/'.$Data[$i]['passengerid'].'/3_'.$Data[$i]['vImgName']; 
			$Data[$i]['tDateOrig'] = $Data[$i]['tDate'];
			$Data[$i]['tDate'] = $generalobj->DateTime($Data[$i]['tDate'], 14);
		}
		$totalNum = count($Data);
		
		if ( count($Data) > 0 ) {
			
			$returnData['message']=$Data;
			if ($TotalPages > $page) {
				$returnData['NextPage'] = $page + 1;
				} else {
				$returnData['NextPage'] = "0";
			}
			$returnData['vAvgRating']=strval($vAvgRating);
			$returnData['Action']="1";
			echo json_encode($returnData);
			
			}else{
			$returnData['Action']="0";
			$returnData['message']="LBL_NO_FEEDBACK";
			echo json_encode($returnData);
		}
		
	}
	
	###########################################################################
	
	if ($type == "loadEmergencyContacts") {
		global $generalobj;
		
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '0';
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : '';
		$GeneralUserType = isset($_REQUEST["GeneralUserType"]) ? $_REQUEST["GeneralUserType"] : 'Passenger';
		
		if($UserType == ""){
			$UserType = $GeneralUserType;
		}
		//$data = get_value('user_emergency_contact', '*', 'iUserId', $iUserId);
		//$data = get_value('user_emergency_contact', '*', 'eUserType', $UserType,'','true');
		$sql = "SELECT * FROM user_emergency_contact WHERE iUserId='".$iUserId."' AND eUserType = '".$UserType."'";
		$data = $obj->MySQLSelect($sql);
		
		if(count($data) >0){
			$returnData['Action']="1";
			$returnData['message']= $data;
			}else{
			$returnData['Action']="0";
		}
		echo json_encode($returnData);
	}
	
	###########################################################################
	
	if ($type == "addEmergencyContacts") {
		global $generalobj;
		
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '0';
		$Phone = isset($_REQUEST["Phone"]) ? $_REQUEST["Phone"] : '0';
		$vName = isset($_REQUEST["vName"]) ? $_REQUEST["vName"] : '0';
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
		$sql  = "SELECT vPhone FROM user_emergency_contact WHERE iUserId = '".$iUserId."' AND vPhone='".$Phone."' AND eUserType='".$UserType."'";
		
		$Data_Exist = $obj->MySQLSelect($sql);
		
		if(count($Data_Exist) > 0){
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_EME_CONTACT_EXIST";
			}else{
			$Data['vName']=$vName;
			$Data['vPhone']=$Phone;
			$Data['iUserId']=$iUserId;
			$Data['eUserType']=$UserType;
			
			$id = $obj->MySQLQueryPerform("user_emergency_contact",$Data,'insert');
			
			if($id >0){
				$returnArr['Action']="1";
				$returnArr['message']="LBL_EME_CONTACT_LIST_UPDATE";
				}else{
				$returnArr['Action'] = "0";
				$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
			}
		}
		
		echo json_encode($returnArr);
	}
	
	###########################################################################
	
	if ($type == "deleteEmergencyContacts") {
		global $generalobj;
		
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '0';
		$iEmergencyId = isset($_REQUEST["iEmergencyId"]) ? $_REQUEST["iEmergencyId"] : '0';
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
		$sql = "DELETE FROM user_emergency_contact WHERE `iEmergencyId`='".$iEmergencyId."' AND `iUserId`='".$iUserId."' AND eUserType = '".$UserType."'";
		$id = $obj->sql_query($sql);
		// echo "ID:".$id;exit;
		if($id >0){
			$returnArr['Action']="1";
			$returnArr['message']="LBL_EME_CONTACT_LIST_UPDATE";
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	
	###########################################################################
	if ($type == "sendAlertToEmergencyContacts") {
		global $generalobj,$obj;
		
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '0';
		$iTripId = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '0';
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
		$sql  = "SELECT * FROM user_emergency_contact WHERE iUserId = '".$iUserId."' AND eUserType='".$UserType."'";
		
		$dataArr = $obj->MySQLSelect($sql);
		if($iTripId == "" || $iTripId == "0"){
			$tableName = $UserType != "Driver"?"register_user":"register_driver";
			$iMemberId_KEY = $UserType != "Driver"?"iUserId":"iDriverId";
			$iTripId=get_value($tableName, 'iTripId', $iMemberId_KEY,$iUserId,'','true');
		}
		
		if(count($dataArr) >0){
			$sql = "SELECT tr.*,dv.vLicencePlate,CONCAT(rd.vName,' ',rd.vLastName) as vDriverName,rd.vPhone as DriverPhone,CONCAT(ru.vName,' ',ru.vLastName) as vPassengerName,ru.vPhone as PassengerPhone FROM trips as tr, register_driver as rd, register_user as ru, driver_vehicle as dv WHERE tr.iTripId = '".$iTripId."' AND rd.iDriverId = tr.iDriverId AND ru.iUserId = tr.iUserId AND dv.iDriverVehicleId = tr.iDriverVehicleId";
			
			$tripData= $obj->MySQLSelect($sql);
			
			$isdCode= $generalobj->getConfigurations("configurations","SITE_ISD_CODE");
			
			if($UserType == "Passenger"){
				$message = "Important: ".$tripData[0]['vPassengerName'].' ('.$tripData[0]['PassengerPhone'].') has reached out to you via '.$SITE_NAME.' SOS. Please reach out to him/her urgently. The details of the ride are: Trip start time: '.date('dS M Y \a\t h:i a',strtotime($tripData[0]['tStartDate'])).'. Pick up from: '.$tripData[0]['tSaddress'].'. Driver name: '.$tripData[0]['vDriverName'].'. Driver number:('.$tripData[0]['DriverPhone']."). Driver's car number: ".$tripData[0]['vLicencePlate'];
				}else{
				$message = "Important: ".$tripData[0]['vDriverName'].' ('.$tripData[0]['DriverPhone'].') has reached out to you via '.$SITE_NAME.' SOS. Please reach out to him/her urgently. The details of the ride are: Trip start time: '.date('dS M Y \a\t h:i a',strtotime($tripData[0]['tStartDate'])).'. Pick up from: '.$tripData[0]['tSaddress'].'. Passenger name: '.$tripData[0]['vPassengerName'].'. Passenger number:('.$tripData[0]['PassengerPhone']."). Driver's car number: ".$tripData[0]['vLicencePlate'];
			}
			
			
			for($i=0;$i<count($dataArr);$i++){
				$phone = preg_replace("/[^0-9]/", "", $dataArr[$i]['vPhone']);
				
				$toMobileNum= "+".$phone;
				
				$result = sendEmeSms($toMobileNum,$message);
				if($result ==0){
					$toMobileNum = "+".$isdCode.$phone;
					sendEmeSms($toMobileNum,$message);
				}
			}
			
			$returnArr['Action']="1";
			$returnArr['message']="LBL_EME_CONTACT_ALERT_SENT";
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_ADD_EME_CONTACTS";
		}
		
		
		echo json_encode($returnArr);
	}
	
	
	###########################################################################
	
	if($type== "ScheduleARide"){
		$iUserId =  isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$pickUpLocAdd =  isset($_REQUEST["pickUpLocAdd"]) ? $_REQUEST["pickUpLocAdd"] : '';
		$pickUpLatitude =  isset($_REQUEST["pickUpLatitude"]) ? $_REQUEST["pickUpLatitude"] : '';
		$pickUpLongitude =  isset($_REQUEST["pickUpLongitude"]) ? $_REQUEST["pickUpLongitude"] : '';
		$destLocAdd =  isset($_REQUEST["destLocAdd"]) ? $_REQUEST["destLocAdd"] : '';
		$destLatitude =  isset($_REQUEST["destLatitude"]) ? $_REQUEST["destLatitude"] : '';
		$destLongitude =  isset($_REQUEST["destLongitude"]) ? $_REQUEST["destLongitude"] : '';
		$scheduleDate =  isset($_REQUEST["scheduleDate"]) ? $_REQUEST["scheduleDate"] : '';
		$iVehicleTypeId =  isset($_REQUEST["iVehicleTypeId"]) ? $_REQUEST["iVehicleTypeId"] : '';
		// $timeZone =  isset($_REQUEST["TimeZone"]) ? $_REQUEST["TimeZone"] : '';
		$eType =  isset($_REQUEST["eType"]) ? $_REQUEST["eType"] : '';
		$iPackageTypeId =  isset($_REQUEST["iPackageTypeId"]) ? $_REQUEST["iPackageTypeId"] : '';
		$vReceiverName =  isset($_REQUEST["vReceiverName"]) ? $_REQUEST["vReceiverName"] : '';
		$vReceiverMobile =  isset($_REQUEST["vReceiverMobile"]) ? $_REQUEST["vReceiverMobile"] : '';
		$tPickUpIns =  isset($_REQUEST["tPickUpIns"]) ? $_REQUEST["tPickUpIns"] : '';
		$tDeliveryIns =  isset($_REQUEST["tDeliveryIns"]) ? $_REQUEST["tDeliveryIns"] : '';
		$tPackageDetails =  isset($_REQUEST["tPackageDetails"]) ? $_REQUEST["tPackageDetails"] : '';
		$vCouponCode =  isset($_REQUEST["PromoCode"]) ? $_REQUEST["PromoCode"] : '';
		$iUserPetId =  isset($_REQUEST["iUserPetId"]) ? $_REQUEST["iUserPetId"] : '';
		$cashPayment    =isset($_REQUEST["CashPayment"]) ? $_REQUEST["CashPayment"] : '';
		$quantity    =isset($_REQUEST["Quantity"]) ? $_REQUEST["Quantity"] : '';
		$fTollPrice = isset($_REQUEST["fTollPrice"]) ? $_REQUEST["fTollPrice"] : '';
		$vTollPriceCurrencyCode = isset($_REQUEST["vTollPriceCurrencyCode"]) ? $_REQUEST["vTollPriceCurrencyCode"] : '';
		$eTollSkipped = isset($_REQUEST["eTollSkipped"]) ? $_REQUEST["eTollSkipped"] : 'Yes';
		$HandicapPrefEnabled = isset($_REQUEST["HandicapPrefEnabled"]) ? $_REQUEST["HandicapPrefEnabled"] : '';
		$PreferFemaleDriverEnable = isset($_REQUEST["PreferFemaleDriverEnable"]) ? $_REQUEST["PreferFemaleDriverEnable"] : '';
		$eAutoAssign    = 'Yes';
		$iDriverId    =isset($_REQUEST["SelectedDriverId"]) ? $_REQUEST["SelectedDriverId"] : '';
		$vTimeZone    =isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		$Booking_Date_Time = $scheduleDate; 
		// $paymentMode =  isset($_REQUEST["paymentMode"]) ? $_REQUEST["paymentMode"] : 'Cash'; // Cash OR Card
		// $paymentMode = "Cash";
		// $paymentMode = $eType == "Deliver" ?"Card":"Cash";
		if($cashPayment=='true'){
			$paymentMode="Cash";
			}else{
			$paymentMode="Card";
		}
		
		checkmemberemailphoneverification($iUserId,"Passenger");
		## Check For PichUp/DropOff Location DisAllow ##
		$address_data = fetch_address_geocode($pickUpLocAdd); 
		$cityId = get_value('city', 'iCityId', 'LOWER(vCity)', strtolower($address_data['city']),'',true);
		$country = get_value('country', 'iCountryId,vCountryCode', 'LOWER(vCountry)', strtolower($address_data['country']));
		$countryId = $country[0]['iCountryId'];
		$vCountryCode = $country[0]['vCountryCode'];
		$stateId = get_value('state', 'iStateId', 'LOWER(vState)', strtolower($address_data['state']),'',true);
		
		if($countryId == '') {
			$country = get_value('country', 'iCountryId,vCountryCode', 'LOWER(vCountryCode)', strtolower($address_data['country_code']));
			$countryId = $country[0]['iCountryId'];
			$vCountryCode = $country[0]['vCountryCode'];
		}
		
		$address_data['cityId'] = $cityId;
		$address_data['countryId'] = $countryId;
		$address_data['stateId'] = $stateId;
		$address_data['PickUpAddress'] = $pickUpLocAdd;
		$address_data['DropOffAddress'] = $destLocAdd;
		$DataArr = getOnlineDriverArr($pickUpLatitude,$pickUpLongitude,$address_data,"Yes");
		if($DataArr['PickUpDisAllowed'] == "No" && $DataArr['DropOffDisAllowed'] == "No"){
			$returnArr['Action'] = "0";
  			$returnArr['message'] = "LBL_PICK_DROP_LOCATION_NOT_ALLOW";
  			echo json_encode($returnArr);
  			exit;
		}
		if($DataArr['PickUpDisAllowed'] == "Yes" && $DataArr['DropOffDisAllowed'] == "No"){
			$returnArr['Action'] = "0";
  			$returnArr['message'] = "LBL_DROP_LOCATION_NOT_ALLOW";
  			echo json_encode($returnArr);
  			exit;
		}
		if($DataArr['PickUpDisAllowed'] == "No" && $DataArr['DropOffDisAllowed'] == "Yes"){
			$returnArr['Action'] = "0";
  			$returnArr['message'] = "LBL_PICKUP_LOCATION_NOT_ALLOW";
  			echo json_encode($returnArr);
  			exit;
		}
		## Check For PichUp/DropOff Location DisAllow Ends##
		
		$systemTimeZone = date_default_timezone_get();
		// echo "hererrrrr:::".$systemTimeZone;exit;
		$scheduleDate = converToTz($scheduleDate,$systemTimeZone,$vTimeZone);
		// $pickUpDateTime = convertTimeZone("2016-29-14 15:29:41","Asia/Calcutta");
		
		// date_default_timezone_set($timeZone);
		// echo gmdate('Y-m-d H:i', strtotime($scheduleDate));exit;
		
		// echo "hererrrrr:::".$pickUpDateTime;exit;
		
		$address_data = fetch_address_geocode($pickUpLocAdd);
		$country = get_value('country', 'vCountryCode', 'LOWER(vCountry)', strtolower($address_data['country']));
		$vCountryCode = $country[0]['vCountryCode'];
		
		if($countryId == '') {
			$country = get_value('country', 'vCountryCode', 'LOWER(vCountryCode)', strtolower($address_data['country_code']));
			$vCountryCode = $country[0]['vCountryCode'];
		}
		
		$ePickStatus=get_value('vehicle_type', 'ePickStatus', 'iVehicleTypeId',$iVehicleTypeId,'','true');
		$eNightStatus=get_value('vehicle_type', 'eNightStatus', 'iVehicleTypeId',$iVehicleTypeId,'','true');
		
		$fPickUpPrice = 1;
		$fNightPrice = 1;
		
		$data_surgePrice = checkSurgePrice($selectedCarTypeID,$scheduleDate);
		
		if($data_surgePrice['Action'] == "0"){
			if($data_surgePrice['message'] == "LBL_PICK_SURGE_NOTE"){
				$fPickUpPrice=$data_surgePrice['SurgePriceValue'];
				}else{
				$fNightPrice=$data_surgePrice['SurgePriceValue'];
			}
		}
		/* if($eTollSkipped=='No')
			{
			$fTollPrice_Original = $fTollPrice;
			$vTollPriceCurrencyCode = strtoupper($vTollPriceCurrencyCode);
			$default_currency = get_value('currency', 'vName', 'eDefault', 'Yes','','true');
			$sql=" SELECT round(($fTollPrice/(SELECT Ratio FROM currency where vName='".$vTollPriceCurrencyCode."'))*(SELECT Ratio FROM currency where vName='".$default_currency."' ) ,2)  as price FROM currency  limit 1";
			$result = $obj->MySQLSelect($sql);
			$fTollPrice = $result[0]['price'];
			if($fTollPrice == 0){
			$fTollPrice = get_currency($vTollPriceCurrencyCode,$default_currency,$fTollPrice_Original);
			}
		} */
		if($eTollSkipped=='No' || $fTollPrice != "")
		{
			$fTollPrice_Original = $fTollPrice;
			$vTollPriceCurrencyCode = strtoupper($vTollPriceCurrencyCode);
			$default_currency = get_value('currency', 'vName', 'eDefault', 'Yes','','true');
			$sql=" SELECT round(($fTollPrice/(SELECT Ratio FROM currency where vName='".$vTollPriceCurrencyCode."'))*(SELECT Ratio FROM currency where vName='".$default_currency."' ) ,2)  as price FROM currency  limit 1";
			$result = $obj->MySQLSelect($sql);
			$fTollPrice = $result[0]['price'];
			if($fTollPrice == 0){
				$fTollPrice = get_currency($vTollPriceCurrencyCode,$default_currency,$fTollPrice_Original);
			}
			$Data['fTollPrice']=$fTollPrice;
			$Data['vTollPriceCurrencyCode']=$vTollPriceCurrencyCode;
			$Data['eTollSkipped']=$eTollSkipped;
			}else{
			$Data['fTollPrice']="0";
			$Data['vTollPriceCurrencyCode']="";
			$Data['eTollSkipped']="No";
		}
		
		$rand_num = rand ( 10000000 , 99999999 );
		
		/*$Booking_Date = @date('d-m-Y',strtotime($scheduleDate));    
		$Booking_Time = @date('H:i:s',strtotime($scheduleDate));*/
		$Booking_Date = @date('d-m-Y',strtotime($Booking_Date_Time));    
		$Booking_Time = @date('H:i:s',strtotime($Booking_Date_Time));
		$Data['iUserId']=$iUserId;
		$Data['vSourceLatitude']=$pickUpLatitude;
		$Data['vSourceLongitude']=$pickUpLongitude;
		$Data['vDestLatitude']=$destLatitude;
		$Data['vDestLongitude']=$destLongitude;
		$Data['vSourceAddresss']=$pickUpLocAdd;
		$Data['tDestAddress']=$destLocAdd;
		$Data['ePayType']=$paymentMode;
		$Data['iVehicleTypeId']=$iVehicleTypeId;
		$Data['vBookingNo']=$rand_num;
		$Data['dBooking_date']=date('Y-m-d H:i', strtotime($scheduleDate));
		$Data['eCancelBy']="";
		$Data['fPickUpPrice']=$fPickUpPrice;
		$Data['fNightPrice']=$fNightPrice;
		$Data['eType']=$eType;
		$Data['iUserPetId']=$iUserPetId;
		$Data['iQty']=$quantity;
		$Data['vCouponCode']=$vCouponCode;
		$Data['eAutoAssign']=$eAutoAssign;
		$Data['vRideCountry']=$vCountryCode;
		// $Data['fTollPrice']=$fTollPrice;
		// $Data['vTollPriceCurrencyCode']=$vTollPriceCurrencyCode;
		// $Data['eTollSkipped']=$eTollSkipped;
		$Data['iDriverId']=$iDriverId;
		$Data['vTimeZone']=$vTimeZone;
		$Data['eFemaleDriverRequest']=$PreferFemaleDriverEnable;
		$Data['eHandiCapAccessibility']=$HandicapPrefEnabled;
		if($eType == "Deliver"){
			$Data['iPackageTypeId']=$iPackageTypeId;
			$Data['vReceiverName']=$vReceiverName;
			$Data['vReceiverMobile']=$vReceiverMobile;
			$Data['tPickUpIns']=$tPickUpIns;
			$Data['tDeliveryIns']=$tDeliveryIns;
			$Data['tPackageDetails']=$tPackageDetails;
		}
		$id = $obj->MySQLQueryPerform("cab_booking",$Data,'insert');
		
		if($id > 0){
			$returnArr["Action"] = "1";
			$returnArr['message']= $eType == "Deliver" ?"LBL_DELIVERY_BOOKED":"LBL_RIDE_BOOKED";
			$sql = "SELECT concat(vName,' ',vLastName) as senderName,vEmail,vPhone,vPhoneCode from  register_user  WHERE iUserId ='".$iUserId."'";
			$userdetail = $obj->MySQLSelect($sql);
			$sql = "SELECT concat(vName,' ',vLastName) as drivername,vEmail,vPhone,vcode,iDriverVehicleId from  register_driver  WHERE iDriverId ='".$iDriverId."'";
			$driverdetail = $obj->MySQLSelect($sql);				
			$userPhoneNo = $userdetail[0]['vPhone'];
			$userPhoneCode = $userdetail[0]['vPhoneCode'];
			$DriverPhoneNo = $driverdetail[0]['vPhone'];
			$DriverPhoneCode = $driverdetail[0]['vcode'];
			$Data1['vRider']=$userdetail[0]['senderName'];
			$Data1['vDriver']=$driverdetail[0]['drivername'];
			$Data1['vDriverMail']=$driverdetail[0]['vEmail'];
			$Data1['vRiderMail']=$userdetail[0]['vEmail'];
			$Data1['vSourceAddresss']=$pickUpLocAdd;
			//$Data1['tDestAddress']=$destLocAdd;
			//$Data1['dBookingdate']=date('Y-m-d H:i', strtotime($scheduleDate));
			$Data1['dBookingdate']=date('Y-m-d H:i', strtotime($Booking_Date_Time));
			$Data1['vBookingNo']=$rand_num;	
			$query = "SELECT vLicencePlate FROM driver_vehicle WHERE iDriverVehicleId=".$iVehicleTypeId; 
			$db_driver_vehicles = $obj->MySQLSelect($query);
      if($Data1['vDriver'] != ""){
			$sendMailfromDriver = $generalobj->send_email_user("MANUAL_TAXI_DISPATCH_DRIVER_APP",$Data1);
			$sendMailfromUser = $generalobj->send_email_user("MANUAL_TAXI_DISPATCH_RIDER_APP",$Data1);			
      }else{
        $sendMailfromUser = $generalobj->send_email_user("MANUAL_TAXI_DISPATCH_RIDER_APP_NODRIVER",$Data1);
      }
      if($Data1['vDriver'] != ""){
			$maildata['DRIVER_NAME'] = $Data1['vDriver'];     
			//$maildata['PLATE_NUMBER'] = $db_driver_vehicles[0]['vLicencePlate'];
			$maildata['BOOKING_DATE'] = $Booking_Date;      
			$maildata['BOOKING_TIME'] =  $Booking_Time;      
			$maildata['BOOKING_NUMBER'] = $Data1['vBookingNo'];
			$message_layout = $generalobj->send_messages_user("USER_SEND_MESSAGE_APP",$maildata);			
			$UsersendMessage = $generalobj->sendUserSMS($userPhoneNo,$userPhoneCode,$message_layout,"");
			if($UsersendMessage == 0){
				$isdCode= $generalobj->getConfigurations("configurations","SITE_ISD_CODE");
				$userPhoneCode = $isdCode;
				$UsersendMessage = $generalobj->sendUserSMS($userPhoneNo,$userPhoneCode,$message_layout,"");
			}
			$maildata1['PASSENGER_NAME'] = $Data1['vRider'];      
			$maildata1['BOOKING_DATE'] = $Booking_Date;      
			$maildata1['BOOKING_TIME'] =  $Booking_Time;      
			$maildata1['BOOKING_NUMBER'] = $Data1['vBookingNo'];
			$message_layout = $generalobj->send_messages_user("DRIVER_SEND_MESSAGE",$maildata1);
			$DriversendMessage = $generalobj->sendUserSMS($DriverPhoneNo,$DriverPhoneCode,$message_layout,"");
			if($DriversendMessage == 0){
				$isdCode= $generalobj->getConfigurations("configurations","SITE_ISD_CODE");
				$DriverPhoneCode = $isdCode;
				$UsersendMessage = $generalobj->sendUserSMS($DriverPhoneNo,$DriverPhoneCode,$message_layout,"");
  			}
			}
			}else{
			$returnArr["Action"] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
			
		}
		
		echo json_encode($returnArr);
	}
	
	
	
	###########################################################################
	
	if ($type == "checkBookings") {
		global $generalobj;
		
		$page        = isset($_REQUEST['page']) ? trim($_REQUEST['page']) : 1;
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$iDriverId = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$bookingType = isset($_REQUEST["bookingType"]) ? $_REQUEST["bookingType"] : '';
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
		$per_page=10;
		$currDate = date('Y-m-d H:i:s');
		$ssql1 = " AND dBooking_date > '" . $currDate . "'";
		$ssql2 = " AND cb.dBooking_date > '" . $currDate . "'";
		
		if($UserType == "Driver"){
			$sql_all  = "SELECT COUNT(iCabBookingId) As TotalIds FROM cab_booking WHERE iDriverId != '' AND ( eStatus = 'Assign' OR eStatus = 'Pending' ) AND iDriverId='".$iDriverId."' $ssql1";
			}else{
			$sql_all  = "SELECT COUNT(iCabBookingId) As TotalIds FROM cab_booking WHERE  iUserId='$iUserId' AND  ( eStatus = 'Assign' OR eStatus = 'Pending' ) $ssql1";
		}
		
		$data_count_all = $obj->MySQLSelect($sql_all);
		$TotalPages = ceil($data_count_all[0]['TotalIds'] / $per_page);
		
		$start_limit = ($page - 1) * $per_page;
		$limit       = " LIMIT " . $start_limit . ", " . $per_page;
		
		if($UserType == "Driver"){
			//$sql = "SELECT cb.* FROM `cab_booking` as cb  WHERE cb.iDriverId != '' AND cb.eStatus = 'Assign' AND cb.iDriverId='$iDriverId' AND cb.eType='".$bookingType."' ORDER BY cb.iCabBookingId DESC" . $limit;
			$sql = "SELECT cb.* FROM `cab_booking` as cb  WHERE cb.iDriverId != '' AND  ( cb.eStatus = 'Assign' OR cb.eStatus = 'Pending' ) AND cb.iDriverId='$iDriverId' $ssql2 ORDER BY cb.iCabBookingId DESC" . $limit;
			}else{
			// $sql = "SELECT cb.* FROM `cab_booking` as cb  WHERE cb.iUserId='$iUserId' AND (cb.eStatus = 'Assign' OR cb.eStatus = 'Pending') ORDER BY cb.iCabBookingId DESC" . $limit;
			//$sql = "SELECT cb.* FROM `cab_booking` as cb  WHERE cb.iUserId='$iUserId' AND cb.eStatus != 'Completed' AND cb.eType='".$bookingType."' ORDER BY cb.iCabBookingId DESC" . $limit;
			$sql = "SELECT cb.* FROM `cab_booking` as cb  WHERE cb.iUserId='$iUserId' AND ( cb.eStatus = 'Assign' OR cb.eStatus = 'Pending' ) $ssql2 ORDER BY cb.iCabBookingId DESC" . $limit;
		}
		$Data = $obj->MySQLSelect($sql);
		$totalNum = count($Data);
		
		if ( count($Data) > 0 ) {
			
			for($i=0;$i<count($Data);$i++){
				$Data[$i]['dBooking_dateOrig']=$Data[$i]['dBooking_date'];
				// Convert Into Timezone
				$tripTimeZone = $Data[0]['vTimeZone'];
				if($tripTimeZone != ""){
					$serverTimeZone = date_default_timezone_get();
					$Data[$i]['dBooking_dateOrig'] = converToTz($Data[$i]['dBooking_dateOrig'],$tripTimeZone,$serverTimeZone);
				}
				// Convert Into Timezone
				$Data[$i]['dBooking_date']=date('dS M Y \a\t h:i a',strtotime($Data[$i]['dBooking_date']));
				if($APP_TYPE == 'UberX'){
					$Data[$i]['tDestAddress'] = "";
				}
			}
			$returnArr['Action'] ="1";
			$returnArr['message'] =$Data;
			
			if ($TotalPages > $page) {
				$returnArr['NextPage'] = $page + 1;
				} else {
				$returnArr['NextPage'] = "0";
			}
			
			}else{
			$returnArr['Action']="0";
			//$returnArr['message']= ($bookingType == "Ride" || $bookingType == "UberX")?"LBL_NO_BOOKINGS_AVAIL":"LBL_NO_DELIVERY_AVAIL";
			$returnArr['message']= "LBL_NO_DATA_AVAIL";
		}
		
		echo json_encode($returnArr);
	}
	
	/* if($type=="checkPassengerBookings"){
		$iUserId     = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		
		$sql = "SELECT * FROM cab_booking WHERE iUserId='$iUserId'";
		$data = $obj->MySQLSelect($sql);
		
		if(count($data)>0){
		
		for($i=0;$i<count($data);$i++){
		$eStatus = $data[$i]['eStatus'];
		
		if($eStatus == "Assign"){
		$iTripId = $data[$i]['iTripId'];
		
		$sql = "SELECT iActive,eCancelled FROM trips WHERE iTripId='$iTripId'";
		$trip_data_arr = $obj->MySQLSelect($sql);
		
		if($trip_data_arr[0]['iActive'] == "Finished" || $trip_data_arr[0]['iActive'] == "Canceled" || $trip_data_arr[0]['eCancelled'] == "Yes"){
		if($trip_data_arr[0]['eCancelled'] == "Yes"){
		$eStatus = "Cancelled by driver";
		}else{
		$eStatus = $trip_data_arr[0]['iActive'];
		}
		
		}
		}
		
		}
		$returnArr['Action'] ="1";
		$returnArr['Data'] =$data;
		}else{
		$returnArr['Action'] ="0";
		}
		
		echo json_encode($returnArr);
	} */
	
	###########################################################################
	if($type=="cancelBooking"){
		$iUserId     = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$userType     = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : '';
		$iCabBookingId     = isset($_REQUEST["iCabBookingId"]) ? $_REQUEST["iCabBookingId"] : '';
		$Reason     = isset($_REQUEST["Reason"]) ? $_REQUEST["Reason"] : '';
		
		$where = " iCabBookingId = '$iCabBookingId'";
		$data_update_booking['eStatus']="Cancel";
		$data_update_booking['vCancelReason']=$Reason;
		$data_update_booking['iCancelByUserId']=$iUserId;
		$data_update_booking['dCancelDate']=@date("Y-m-d H:i:s");
		$data_update_booking['eCancelBy']=$userType == "Driver" ? $userType:"Rider";
		$id = $obj->MySQLQueryPerform("cab_booking",$data_update_booking,'update',$where);
		
		if($userType == "Driver"){
			$sql="select cb.vBookingNo,concat(rd.vName,' ',rd.vLastName) as DriverName,concat(ru.vName,' ',ru.vLastName) as RiderName,cb.vSourceAddresss,cb.tDestAddress,cb.dBooking_date,cb.vCancelReason,cb.dCancelDate from cab_booking cb
			left join register_driver rd on rd.iDriverId = cb.iDriverId
			left join register_user ru on ru.iUserId = cb.iUserId where cb.iCabBookingId = '$iCabBookingId'";
			$data_cab = $obj->MySQLSelect($sql);
			if(count($data_cab) > 0){
				$Data['vBookingNo'] = $data_cab[0]['vBookingNo'];
				$Data['DriverName'] = $data_cab[0]['DriverName'];
				$Data['RiderName'] = $data_cab[0]['RiderName'];
				$Data['vSourceAddresss'] = $data_cab[0]['vSourceAddresss'];
				$Data['tDestAddress'] = $data_cab[0]['tDestAddress'];
				$Data['dBooking_date'] = $data_cab[0]['dBooking_date'];
				$Data['vCancelReason'] = $Reason;
				$Data['dCancelDate'] = $data_cab[0]['dCancelDate'];
				
				$generalobj->send_email_user("MANUAL_CANCEL_TRIP_ADMIN",$Data);
			}
		}
		
		if($id){
			$returnArr['Action']="1";
			$returnArr['message']="LBL_BOOKING_CANCELED";
			}else{
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
	}
	###########################################################################
	if($type == "loadPackageTypes"){
		$vehicleTypes = get_value('package_type', '*', 'eStatus', 'Active');
		
		if(count($vehicleTypes) >0){
			$returnArr['Action']="1";
			$returnArr['message']=$vehicleTypes;
			}else{
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	###########################################################################
	if($type == "loadDeliveryDetails"){
		$iDriverId     = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$iTripId     = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '';
		
		$sql = "SELECT tr.vReceiverName,tr.vReceiverMobile,tr.tPickUpIns,tr.tDeliveryIns,tr.tPackageDetails,pt.vName as packageType,concat(ru.vName,' ',ru.vLastName) as senderName, ru.vPhone as senderMobile from trips as tr, register_user as ru, package_type as pt WHERE ru.iUserId = tr.iUserId AND tr.iTripId = '".$iTripId."' AND pt.iPackageTypeId = tr.iPackageTypeId";
		$Data = $obj->MySQLSelect($sql);
		
		if(count($Data) >0 && $iTripId != ""){
			$returnArr['Action']="1";
			$returnArr['message']=$Data[0];
			}else{
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
	}
	
	###########################################################################
	
	if ($type == "checkSurgePrice") {
		$selectedCarTypeID    = isset($_REQUEST["SelectedCarTypeID"]) ? $_REQUEST["SelectedCarTypeID"] : '';
		$selectedTime    = isset($_REQUEST["SelectedTime"]) ? $_REQUEST["SelectedTime"] : '';
		
		$vTimeZone    =isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		
		if($selectedTime != '' && $vTimeZone != '') {
			$systemTimeZone = date_default_timezone_get();
			$selectedTime = converToTz($selectedTime,$systemTimeZone,$vTimeZone);
		}
		
		$APP_TYPE = $generalobj->getConfigurations("configurations","APP_TYPE");
		if($APP_TYPE == "UberX"){
			$data['Action'] = "1";
			}else{
			$data= checkSurgePrice($selectedCarTypeID,$selectedTime);
		}
		
		echo json_encode($data);
	}
	
	###########################################################################
	
	if ($type == "getTransactionHistory")
	{
		global $generalobj;
		#echo "hello"; exit;
		
		$page        = isset($_REQUEST['page']) ? trim($_REQUEST['page']) : 1;
		$iUserId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : '';
		$tripTimeZone = isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		$ListType = isset($_REQUEST["ListType"]) ? $_REQUEST["ListType"] : 'All';
		if($page == "0" || $page == 0){
			$page = 1;
		}
		if($UserType=="Passenger"){
			$UserType="Rider";
		}
		
		$ssql='';
		if($ListType != "All"){
			$ssql.= " AND eType ='".$ListType."'";
		}
		$per_page=10;
		$sql_all  = "SELECT COUNT(iUserWalletId) As TotalIds FROM user_wallet WHERE  iUserId='".$iUserId."' AND eUserType = '".$UserType."' ".$ssql." ";
		$data_count_all = $obj->MySQLSelect($sql_all);
		$TotalPages = ceil($data_count_all[0]['TotalIds'] / $per_page);
		
		$start_limit = ($page - 1) * $per_page;
		$limit       = " LIMIT " . $start_limit . ", " . $per_page;
		
		$user_available_balance = $generalobj->get_user_available_balance($iUserId,$UserType);
		
		//$sql = "SELECT tripRate.vRating1 as TripRating,tr.* FROM `trips` as tr,`ratings_user_driver` as tripRate  WHERE  tr.iUserId='$iUserId' AND tripRate.iTripId=tr.iTripId AND tripRate.eUserType='$UserType' AND (tr.iActive='Canceled' || tr.iActive='Finished') ORDER BY tr.iTripId DESC" . $limit;
		$sql = "SELECT * from user_wallet where iUserId='".$iUserId."' AND eUserType = '".$UserType."' ".$ssql." ORDER BY iUserWalletId DESC". $limit;
		$Data = $obj->MySQLSelect($sql);
		$totalNum = count($Data);
		
		$vSymbol = get_value('currency', 'vSymbol', 'eDefault','Yes','','true');
		if($UserType == 'Driver')
		{
			$uservSymbol = get_value('register_driver', 'vCurrencyDriver', 'iDriverId',$iUserId,'','true');
		}
		else
		{
			$uservSymbol = get_value('register_user', 'vCurrencyPassenger', 'iUserId',$iUserId,'','true');
		}
		
		$userCurrencySymbol = get_value('currency', 'vSymbol', 'vName',$uservSymbol,'','true');
		
		$i=0;
		if ( count($Data) > 0 ) {
			
			$row = $Data;
			$prevbalance = 0;
            while ( count($row)> $i ) {
				// Convert Into Timezone
				if($tripTimeZone != ""){
					$serverTimeZone = date_default_timezone_get();
					$row[$i]['dDate'] = converToTz($row[$i]['dDate'],$tripTimeZone,$serverTimeZone);
				}
				// Convert Into Timezone
            	if($row[$i]['eType'] == "Credit"){
					$row[$i]['currentbal'] = $prevbalance+$row[$i]['iBalance'];
					}else{
				    $row[$i]['currentbal'] = $prevbalance-$row[$i]['iBalance'];
				}
                $prevbalance = $row[$i]['currentbal'];
				$row[$i]['dDateOrig'] = $row[$i]['dDate'];
                $row[$i]['dDate'] = date('d-M-Y',strtotime($row[$i]['dDate']));
				
				//$row[$i]['currentbal'] = $vSymbol.$row[$i]['currentbal'];
				//$row[$i]['iBalance'] = $vSymbol.$row[$i]['iBalance'];
				$row[$i]['currentbal'] = $generalobj->userwalletcurrency($row[$i]['fRatio_'.$uservSymbol],$row[$i]['currentbal'],$uservSymbol);
				$row[$i]['iBalance'] = $generalobj->userwalletcurrency($row[$i]['fRatio_'.$uservSymbol],$row[$i]['iBalance'],$uservSymbol);
				$i++;
			}
			
			//$returnData['message'] = array_reverse($row);
			$returnData['message'] = $row;
			if ($TotalPages > $page) {
				$returnData['NextPage'] = $page + 1;
				} else {
				$returnData['NextPage'] = 0;
			}
			
			$returnData['user_available_balance_default']=$vSymbol.$user_available_balance;
			$returnData['user_available_balance'] = strval($generalobj->userwalletcurrency(0,$user_available_balance,$uservSymbol));
			$returnData['Action']="1";
			#echo "<pre>"; print_r($returnData); exit;
			echo json_encode($returnData);
			
			}else{
			$returnData['Action']="0";
			$returnData['message']="LBL_NO_TRANSACTION_AVAIL";
			$returnData['user_available_balance']= $userCurrencySymbol."0";
			echo json_encode($returnData);
		}
		
	}
	
	###########################################################################
	if($type=="loadPassengersLocation"){
		
		global $generalobj,$obj;
		
		/*$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
			$radius = isset($_REQUEST["Radius"]) ? $_REQUEST["Radius"] : '';
			$sourceLat = isset($_REQUEST["Latitude"]) ? $_REQUEST["Latitude"] : '';
			$sourceLon = isset($_REQUEST["Longitude"]) ? $_REQUEST["Longitude"] : '';
			
			$str_date = @date('Y-m-d H:i:s', strtotime('-5 minutes'));
			
			$sql = "SELECT ROUND(( 3959 * acos( cos( radians(".$sourceLat.") )
			* cos( radians( vLatitude ) )
			* cos( radians( vLongitude ) - radians(".$sourceLon.") )
			+ sin( radians(".$sourceLat.") )
			* sin( radians( vLatitude ) ) ) ),2) AS distance, register_driver.*  FROM `register_driver`
			WHERE (vLatitude != '' AND vLongitude != '' AND eStatus='Active' AND tLastOnline > '$str_date')
			HAVING distance < ".$radius." ORDER BY `register_driver`";
			
			
		$Data = $obj->MySQLSelect($sql);*/
		
		
		
		$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$radius = isset($_REQUEST["Radius"]) ? $_REQUEST["Radius"] : '';
		$sourceLat = isset($_REQUEST["Latitude"]) ? $_REQUEST["Latitude"] : '';
		$sourceLon = isset($_REQUEST["Longitude"]) ? $_REQUEST["Longitude"] : '';
		
		$str_date = @date('Y-m-d H:i:s', strtotime('-5 minutes'));
		
		// register_user table
		$sql = "SELECT ROUND(( 3959 * acos( cos( radians(".$sourceLat.") )
		* cos( radians( vLatitude ) )
		* cos( radians( vLongitude ) - radians(".$sourceLon.") )
		+ sin( radians(".$sourceLat.") )
		* sin( radians( vLatitude ) ) ) ),2) AS distance, register_user.*  FROM `register_user`
		WHERE (vLatitude != '' AND vLongitude != '' AND eStatus='Active' AND tLastOnline > '$str_date')
		HAVING distance < ".$radius." ORDER BY `register_user`.iUserId ASC";
		
		
		$Data = $obj->MySQLSelect($sql);
		$storeuser = array();
		$storetrip = array();
		
		foreach ($Data as $value) {
			
			$dataofuser = array("Type"=>'Online',"Latitude"=>$value['vLatitude'],"Longitude"=>$value['vLongitude'],"iUserId"=>$value['iUserId']);
			array_push($storeuser, $dataofuser);
			
		}
		
		// trip table
		$sql_trip = "SELECT ROUND(( 3959 * acos( cos( radians(".$sourceLat.") )
		* cos( radians( tStartLat ) )
		* cos( radians( tStartLong ) - radians(".$sourceLon.") )
		+ sin( radians(".$sourceLat.") )
		* sin( radians( tStartLat ) ) ) ),2) AS distance, trips.*  FROM `trips`
		WHERE (tStartLat != '' AND tStartLong != '' AND tTripRequestDate >= DATE_SUB(CURDATE(), INTERVAL 2500 HOUR))
		HAVING distance < ".$radius." ORDER BY `trips`.iTripId DESC";
		
		$Dataoftrips = $obj->MySQLSelect($sql_trip);
		
		foreach ($Dataoftrips as $value1) {
			
			$valuetrip = array("Type"=>'History',"Latitude"=>$value1['tStartLat'],"Longitude"=>$value1['tStartLong'],"iTripId"=>$value1['iTripId']);
			array_push($storetrip, $valuetrip);
			
		}
		
		$finaldata = array_merge($storeuser,$storetrip);
		//echo "<pre>"; print_r($finaldata); exit;
		
		if(count($finaldata)>0){
			$returnData['Action']="1";
			$returnData['message']=$finaldata;
			}else{
			$returnData['Action']="0";
		}
		echo json_encode($returnData);
		
	}
	###########################################################################
	###########################################################################
	if($type=="loadPetsType"){
		$iUserId     = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		
		if($iUserId != ""){
			$vLanguage=get_value('register_user', 'vLang', 'iUserId',$iUserId,'','true');
			
			$vLanguage = $vLanguage ==""?"EN":$vLanguage;
			
			$petTypes = get_value('pet_type', 'iPetTypeId, vTitle_'.$vLanguage.' as vTitle', 'eStatus', 'Active');
			
			$returnData['Action']="1";
			$returnData['message']=$petTypes;
			}else{
			$returnData['Action']="0";
		}
		echo json_encode($returnData);
	}
	###########################################################################
	
	if($type == "loadUserPets"){
		$iUserId     = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$page        = isset($_REQUEST['page']) ? trim($_REQUEST['page']) : 1;
		
		$vLanguage=get_value('register_user', 'vLang', 'iUserId',$iUserId,'','true');
		
		$vLanguage = $vLanguage ==""?"EN":$vLanguage;
		
		$per_page=10;
		$sql = "SELECT COUNT(iUserPetId) as TotalIds from user_pets WHERE iUserId='".$iUserId."'";
		
		$Data_all = $obj->MySQLSelect($sql);
		$TotalPages = ceil($Data_all[0]['TotalIds'] / $per_page);
		
		
		$start_limit = ($page - 1) * $per_page;
		$limit       = " LIMIT " . $start_limit . ", " . $per_page;
		
		$sql = "SELECT up.*,pt.vTitle_".$vLanguage." as petType from user_pets as up,  pet_type as pt WHERE pt.iPetTypeId = up.iPetTypeId AND up.iUserId='".$iUserId."'".$limit;
		$Data = $obj->MySQLSelect($sql);
		
		$totalNum = count($Data);
		
		if(count($Data) >0 && $iUserId != ""){
			$returnArr['Action']="1";
			$returnArr['message']=$Data;
			if ($TotalPages > $page) {
				$returnArr['NextPage'] = $page + 1;
				} else {
				$returnArr['NextPage'] = "0";
			}
			}else{
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
	}
	###########################################################################
	if($type == "deleteUserPets"){
		global $generalobj;
		
		$iUserPetId = isset($_REQUEST["iUserPetId"]) ? $_REQUEST["iUserPetId"] : '0';
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '0';
		
		$sql = "DELETE FROM user_pets WHERE `iUserPetId`='".$iUserPetId."' AND `iUserId`='".$iUserId."'";
		$id = $obj->sql_query($sql);
		// echo "ID:".$id;exit;
		if($id >0){
			$returnArr['Action']="1";
			$returnArr['message']="LBL_INFO_UPDATED_TXT";
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	###########################################################################
	if($type == "addUserPets"){
		global $generalobj;
		
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '0';
		$iPetTypeId = isset($_REQUEST["iPetTypeId"]) ? $_REQUEST["iPetTypeId"] : '0';
		$vTitle = isset($_REQUEST["vTitle"]) ? $_REQUEST["vTitle"] : '';
		$vWeight = isset($_REQUEST["vWeight"]) ? $_REQUEST["vWeight"] : '';
		$tBreed = isset($_REQUEST["tBreed"]) ? $_REQUEST["tBreed"] : '';
		$tDescription = isset($_REQUEST["tDescription"]) ? $_REQUEST["tDescription"] : '';
		
		$Data_pets['iUserId']=$iUserId;
		$Data_pets['iPetTypeId']=$iPetTypeId;
		$Data_pets['vTitle']=$vTitle;
		$Data_pets['vWeight']=$vWeight;
		$Data_pets['tBreed']=$tBreed;
		$Data_pets['tDescription']=$tDescription;
		
		$id = $obj->MySQLQueryPerform("user_pets",$Data_pets,'insert');
		
		if ($id > 0) {
			$returnArr['Action']="1";
			
			} else {
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	###########################################################################
	if($type == "editUserPets"){
		$iUserPetId = isset($_REQUEST["iUserPetId"]) ? $_REQUEST['iUserPetId'] : '';
		$iUserId    = isset($_REQUEST["iUserId"]) ? $_REQUEST['iUserId'] : '';
		$iPetTypeId = isset($_REQUEST["iPetTypeId"]) ? $_REQUEST['iPetTypeId'] : '';
		$vTitle 	= isset($_REQUEST["vTitle"]) ? $_REQUEST['vTitle'] : '';
		$vWeight 	= isset($_REQUEST["vWeight"]) ? $_REQUEST['vWeight'] : '';
		$tBreed 	= isset($_REQUEST["tBreed"]) ? $_REQUEST['tBreed'] : '';
		$tDescription 	= isset($_REQUEST["tDescription"]) ? $_REQUEST['tDescription'] : '';
		
		$where = " iUserPetId = '".$iUserPetId."' AND `iUserId`='".$iUserId."'";
		
		$Data['iUserId']=$iUserId;
		$Data['iPetTypeId']=$iPetTypeId;
		$Data['vTitle']=$vTitle;
		$Data['vWeight']=$vWeight;
		$Data['tBreed']=$tBreed;
		$Data['tDescription']=$tDescription;
		$id = $obj->MySQLQueryPerform("user_pets",$Data,'update',$where);
		
		
		if ($id) {
			$returnArr['Action']="1";
			$returnArr['message']="LBL_INFO_UPDATED_TXT";
			} else {
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	###########################################################################
	if($type == "loadPetDetail"){
		$iUserPetId = isset($_REQUEST["iUserPetId"]) ? $_REQUEST['iUserPetId'] : '';
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST['iUserId'] : '';
		$iDriverId = isset($_REQUEST["iDriverId"]) ? $_REQUEST['iDriverId'] : '';
		
		
		$vLanguage=get_value('register_user', 'vLang', 'iDriverId',$iDriverId,'','true');
		if($vLanguage == "" || $vLanguage == NULL){
			$vLanguage = "EN";
		}
		
		$sql = "SELECT up.*,pt.vTitle_".$vLanguage." as petTypeName from user_pets as up,  pet_type as pt WHERE pt.iPetTypeId = up.iPetTypeId AND up.iUserId='".$iUserId."' AND up.iUserPetId='".$iUserPetId."'";
		$Data = $obj->MySQLSelect($sql);
		
		if (count($Data) > 0) {
			$returnArr['Action']="1";
			$returnArr['message']=$Data[0];
			} else {
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	###########################################################################
	if($type=="collectTip"){
		$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$iTripId = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '';
		$fAmount = isset($_REQUEST["fAmount"]) ? $_REQUEST["fAmount"] : '';
		
		$tbl_name = "register_user";
		$currencycode = "vCurrencyPassenger";
		$iUserId = "iUserId";
		$eUserType = "Rider";
		
		if($iMemberId == "") {
			$iMemberId = get_value('trips', 'iUserId', 'iTripId', $iTripId,'','true');
		}
		$vStripeCusId = get_value($tbl_name, 'vStripeCusId', $iUserId, $iMemberId,'','true');
		$vStripeToken = get_value($tbl_name, 'vStripeToken', $iUserId, $iMemberId,'','true');
		$userCurrencyCode = get_value($tbl_name, $currencycode, $iUserId, $iMemberId,'','true');
		$currencyCode = get_value('currency', 'vName', 'eDefault', 'Yes','','true');
		$currencyratio = get_value('currency', 'Ratio', 'vName', $userCurrencyCode,'','true');
		//$price = $fAmount*$currencyratio;
		$price = $fAmount/$currencyratio;
		$price_new = $price * 100;
		$price_new = round($price_new);
		if($vStripeCusId == "" || $vStripeToken == ""){
			$returnArr["Action"] = "0";
			$returnArr['message']="LBL_NO_CARD_AVAIL_NOTE";
			echo json_encode($returnArr);exit;
		}
		
		$dDate = Date('Y-m-d H:i:s');
		$eFor = 'Deposit';
		$eType = 'Credit';
		$tDescription = "Amount debited";
		$ePaymentStatus = 'Unsettelled';
		
		$userAvailableBalance = $generalobj->get_user_available_balance($iMemberId,$eUserType);
		if($userAvailableBalance > $price){
			$where = " iTripId = '$iTripId'";
			$data['fTipPrice']= $price;
			
			$id = $obj->MySQLQueryPerform("trips",$data,'update',$where);
			
			$vRideNo = get_value('trips', 'vRideNo', 'iTripId',$tripId,'','true');
			$data_wallet['iUserId']=$iUserId;
			$data_wallet['eUserType']="Rider";
			$data_wallet['iBalance']=$price;
			$data_wallet['eType']="Debit";
			$data_wallet['dDate']=date("Y-m-d H:i:s");
			$data_wallet['iTripId']=$iTripId;
			$data_wallet['eFor']="Booking";
			$data_wallet['ePaymentStatus']="Unsettelled";
			$data_wallet['tDescription']="Debited for trip#".$vRideNo;
			
			$generalobj->InsertIntoUserWallet($data_wallet['iUserId'],$data_wallet['eUserType'],$data_wallet['iBalance'],$data_wallet['eType'],$data_wallet['iTripId'],$data_wallet['eFor'],$data_wallet['tDescription'],$data_wallet['ePaymentStatus'],$data_wallet['dDate']);
			
			$returnArr["Action"] = "1";
			echo json_encode($returnArr);exit;
			
			}else if($price > 0.51){
			try{
				$charge_create = Stripe_Charge::create(array(
				"amount" => $price_new,
				"currency" => $currencyCode,
				"customer" => $vStripeCusId,
				"description" =>  $tDescription
				));
				
				$details = json_decode($charge_create);
				$result = get_object_vars($details);
				//echo "<pre>";print_r($result);exit;
				if($result['status']=="succeeded" && $result['paid']=="1"){
					
					$where = " iTripId = '$iTripId'";
					$data['fTipPrice']= $price;
					
					$id = $obj->MySQLQueryPerform("trips",$data,'update',$where);
					
					$returnArr["Action"] = "1";
					echo json_encode($returnArr);exit;
					}else{
  					$returnArr['Action'] = "0";
  					$returnArr['message']="LBL_TRANS_FAILED";
					
  					echo json_encode($returnArr);exit;
				}
				
				}catch(Exception $e){
				//echo "<pre>";print_r($e);exit;
  				$error3 = $e->getMessage();
  				$returnArr["Action"] = "0";
				$returnArr['message']=$error3;
				//$returnArr['message']="LBL_TRANS_FAILED";
				
  				echo json_encode($returnArr);exit;
			}
			
			}else{
			$returnArr["Action"] = "0";
			$returnArr['message']="LBL_REQUIRED_MINIMUM_AMOUT";
			$returnArr['minValue'] = strval(round(51 * $currencyratio));
			
			echo json_encode($returnArr);exit;
		}
		
		
	}
	###########################################################################
	############################ UBER-For-X ################################
	
	/*if($type=="getServiceCategories"){
		global $generalobj;
		
		$parentId = isset($_REQUEST['parentId'])?clean($_REQUEST['parentId']):0;
		$userId = isset($_REQUEST['userId'])?clean($_REQUEST['userId']):'';
		if($userId != "") {
		$sql1 = "SELECT vLang FROM `register_user` WHERE iUserId='$userId'";
		$row = $obj->MySQLSelect($sql1);
		$lang = $row[0]['vLang'];
		if($lang == "") { $lang = "EN"; }
		
		//$vehicle_category = get_value('vehicle_category', 'iVehicleCategoryId, vLogo,vCategory_'.$row[0]['vLang'].' as vCategory', 'eStatus', 'Active');
		// $sql2 = "SELECT iVehicleCategoryId, vLogo,vCategory_".$lang." as vCategory FROM vehicle_category WHERE eStatus='Active' AND iParentId='$parentId'";
		if($parentId == 0){
		$sql2 = "SELECT vc.iVehicleCategoryId, vc.vLogo,vc.vCategory_".$lang." as vCategory FROM vehicle_category as vc WHERE vc.eStatus='Active' AND vc.iParentId='$parentId' and (select count(iVehicleCategoryId) from vehicle_category where iParentId=vc.iVehicleCategoryId) > 0";
		}else{
		$sql2 = "SELECT iVehicleCategoryId, vLogo,vCategory_".$lang." as vCategory FROM vehicle_category WHERE eStatus='Active' AND iParentId='$parentId'";
		}
		
		$Data = $obj->MySQLSelect($sql2);
		
		for($i=0;$i<count($Data);$i++){
		$Data[$i]['vLogo_image'] = $tconfig['tsite_upload_images_vehicle_category'].'/'.$Data[$i]['iVehicleCategoryId'].'/android/'.$Data[$i]['vLogo'];
		}
		
		// if(!empty($Data)){
		$returnArr['Action']="1";
		$returnArr['message'] = $Data;
		// }else{
		// $returnArr['Action']="0"; 
		// $returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		// }
		}else{
		$returnArr['Action']="0"; 
		$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}    */
	
	if($type=="getServiceCategories"){
		global $generalobj;
		
		$parentId = isset($_REQUEST['parentId'])?clean($_REQUEST['parentId']):0;
		$userId = isset($_REQUEST['userId'])?clean($_REQUEST['userId']):'';
		if($userId != "") {
			$sql1 = "SELECT vLang FROM `register_user` WHERE iUserId='$userId'";
			$row = $obj->MySQLSelect($sql1);
			$lang = $row[0]['vLang'];
			if($lang == "") { $lang = "EN"; }
			
			//$vehicle_category = get_value('vehicle_category', 'iVehicleCategoryId, vLogo,vCategory_'.$row[0]['vLang'].' as vCategory', 'eStatus', 'Active');
			// $sql2 = "SELECT iVehicleCategoryId, vLogo,vCategory_".$lang." as vCategory FROM vehicle_category WHERE eStatus='Active' AND iParentId='$parentId'";
			/*if($parentId == 0){
				$sql2 = "SELECT vc.iVehicleCategoryId, vc.vLogo,vc.vCategory_".$lang." as vCategory FROM vehicle_category as vc WHERE vc.eStatus='Active' AND vc.iParentId='$parentId' and (select count(iVehicleCategoryId) from vehicle_category where iParentId=vc.iVehicleCategoryId) > 0";
				}else{
				$sql2 = "SELECT iVehicleCategoryId, vLogo,vCategory_".$lang." as vCategory FROM vehicle_category WHERE eStatus='Active' AND iParentId='$parentId'";
			}   */
			$sql2 = "SELECT iVehicleCategoryId, vLogo,vCategory_".$lang." as vCategory FROM vehicle_category WHERE eStatus='Active' AND iParentId='$parentId' ";
			$Data = $obj->MySQLSelect($sql2);
			$Datacategory = array();
			if($parentId == 0){
				if(count($Data) > 0){
					$k = 0;
					for($i=0;$i<count($Data);$i++){
						$sql3 = "SELECT iVehicleCategoryId, vLogo,vCategory_".$lang." as vCategory FROM vehicle_category WHERE eStatus='Active' AND iParentId='".$Data[$i]['iVehicleCategoryId']."'";
						$Data2 = $obj->MySQLSelect($sql3);
						if(count($Data2) > 0){
							for($j=0;$j<count($Data2);$j++){
								$sql4 = "SELECT iVehicleTypeId FROM vehicle_type WHERE iVehicleCategoryId='".$Data2[$j]['iVehicleCategoryId']."'";
								$Data3 = $obj->MySQLSelect($sql4);
								if(count($Data3) > 0){
									$Datacategory[$k]['iVehicleCategoryId'] = $Data[$i]['iVehicleCategoryId'];
									$Datacategory[$k]['vLogo'] = $Data[$i]['vLogo'];
									$Datacategory[$k]['vLogo_image'] = $tconfig['tsite_upload_images_vehicle_category'].'/'.$Data[$i]['iVehicleCategoryId'].'/android/'.$Data[$i]['vLogo'];
									$Datacategory[$k]['vCategory'] = $Data[$i]['vCategory'];
									$k++; 
								}
							}
							//$Datacategory = array_map('unserialize', array_unique(array_map('serialize', $Datacategory)));
						}
						
					}
				}  
				}else{
				if(count($Data) > 0){
					$k = 0;
					for($j=0;$j<count($Data);$j++){
						$sql4 = "SELECT iVehicleTypeId FROM vehicle_type WHERE iVehicleCategoryId='".$Data[$j]['iVehicleCategoryId']."'";
						$Data3 = $obj->MySQLSelect($sql4);
						if(count($Data3) > 0){
							$Datacategory[$k]['iVehicleCategoryId'] = $Data[$j]['iVehicleCategoryId'];
							$Datacategory[$k]['vLogo'] = $Data[$j]['vLogo'];
							$Datacategory[$k]['vLogo_image'] = $tconfig['tsite_upload_images_vehicle_category'].'/'.$Data[$j]['iVehicleCategoryId'].'/android/'.$Data[$j]['vLogo'];
							$Datacategory[$k]['vCategory'] = $Data[$j]['vCategory'];
							$k++; 
						}
						//$unique = array_map('unserialize', array_unique(array_map('serialize', $array)));
					}
					//$Datacategory = array_map('unserialize', array_unique(array_map('serialize', $Datacategory)));
				} 
			} 
			
			$Datacategory1 = array_unique($Datacategory, SORT_REGULAR);
			$DatanewArr = array();
			foreach($Datacategory1 as $inner) {
				array_push($DatanewArr,$inner);
			}
			
			$returnArr['Action']="1";
			$returnArr['message'] = $DatanewArr;
			}else{
			$returnArr['Action']="0"; 
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	
	if($type=="getServiceCategoryTypes"){
		global $generalobj;
		
		$iVehicleCategoryId = isset($_REQUEST['iVehicleCategoryId'])?clean($_REQUEST['iVehicleCategoryId']):0;
		$userId = isset($_REQUEST['userId'])?clean($_REQUEST['userId']):'';
		if($userId != "") {
			$sql1 = "SELECT vLang,vCurrencyPassenger FROM `register_user` WHERE iUserId='$userId'";
			$row = $obj->MySQLSelect($sql1);
			$lang = $row[0]['vLang'];
			if($lang == "" || $lang == NULL) { $lang = "EN"; }
			
			
			$vCurrencyPassenger = $row[0]['vCurrencyPassenger'];
			if($vCurrencyPassenger == "" || $vCurrencyPassenger == NULL){
				$vCurrencyPassenger = get_value('currency', 'vName', 'eDefault', 'Yes','','true');
			}
			$UserCurrencyData = get_value('currency', 'vSymbol, Ratio', 'vName', $vCurrencyPassenger);
			$priceRatio = $UserCurrencyData[0]['Ratio'];
			$vSymbol = $UserCurrencyData[0]['vSymbol']; 
			
			$sql2 = "SELECT vc.iVehicleCategoryId, vc.vCategory_".$lang." as vCategory, vt.vVehicleType_".$lang." as vVehicleType, vt.eFareType, vt.fFixedFare, vt.fPricePerHour, vt.fPricePerKM, vt.fPricePerMin, vt.iBaseFare,vt.fCommision, vt.iMinFare,vt.iPersonSize, vt.vLogo as vVehicleTypeImage, vt.eType, vt.eIconType, vt.eAllowQty, vt.iMaxQty, vc.vCategoryTitle_".$lang." as vCategoryTitle, vc.tCategoryDesc_".$lang." as tCategoryDesc, vt.iVehicleTypeId, fFixedFare FROM vehicle_category as vc LEFT JOIN vehicle_type AS vt ON vt.iVehicleCategoryId = vc.iVehicleCategoryId WHERE vc.eStatus='Active' AND vt.iVehicleCategoryId='$iVehicleCategoryId'";
			//AND vt.eType='UberX'
			
			$Data = $obj->MySQLSelect($sql2);
			$ALLOW_SERVICE_PROVIDER_AMOUNT = $generalobj->getConfigurations("configurations","ALLOW_SERVICE_PROVIDER_AMOUNT");
			/* $ALLOW_SERVICE_PROVIDER_AMOUNT = $generalobj->getConfigurations("configurations","ALLOW_SERVICE_PROVIDER_AMOUNT");
				if($ALLOW_SERVICE_PROVIDER_AMOUNT == "Yes"){
				for($i=0;$i<count($Data);$i++){
				$iVehicleTypeId = $Data[$i]['iVehicleTypeId'];
				
				$sqlServicePro = "SELECT * FROM `service_pro_amount` WHERE iDriverVehicleId='".$iDriverVehicleId."' AND iVehicleTypeId='".$iVehicleTypeId."'";
				$serviceProData = $obj->MySQLSelect($sqlServicePro);
				
				if(count($serviceProData) > 0){
				$fAmount = $serviceProData[0]['fAmount'];
				}
				}
			} */
			
			if(!empty($Data)){
				for($i=0;$i<count($Data);$i++){
					$Data[$i]['fFixedFare_value']= round($Data[$i]['fFixedFare'] * $priceRatio,2);
					$Data[$i]['fFixedFare']= $vSymbol.round($Data[$i]['fFixedFare'] * $priceRatio,2);
					$Data[$i]['fPricePerHour']= $vSymbol.round($Data[$i]['fPricePerHour'] * $priceRatio,2);
					$Data[$i]['fPricePerKM']= $vSymbol.round($Data[$i]['fPricePerKM'] * $priceRatio,2);
					$Data[$i]['fPricePerMin']= $vSymbol.round($Data[$i]['fPricePerMin'] * $priceRatio,2);
					$Data[$i]['iBaseFare']= $vSymbol.round($Data[$i]['iBaseFare'] * $priceRatio,2);
					$Data[$i]['fCommision']= $vSymbol.round($Data[$i]['fCommision'] * $priceRatio,2);
					$Data[$i]['iMinFare']= $vSymbol.round($Data[$i]['iMinFare'] * $priceRatio,2);
					$Data[$i]['vSymbol']= $vSymbol;
				}
				
				$returnArr['Action']="1";
				$returnArr['message'] = $Data;
				$returnArr['ALLOW_SERVICE_PROVIDER_AMOUNT'] = $ALLOW_SERVICE_PROVIDER_AMOUNT;
				}else{
				$returnArr['Action']="0"; 
				$returnArr['message'] ="LBL_NO_DATA_AVAIL";
			}
			}else{
			$returnArr['Action']="0"; 
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr, JSON_HEX_QUOT | JSON_HEX_TAG);
	}
	
	if($type=="getBanners"){
		global $generalobj;
		
		$iMemberId = isset($_REQUEST['iMemberId'])?clean($_REQUEST['iMemberId']):'';
		if($iMemberId != "") {
			
			$vLanguage=get_value('register_user', 'vLang', 'iDriverId',$iDriverId,'','true');
			if($vLanguage == "" || $vLanguage == NULL){
				$vLanguage = "EN";
			}
			
			$banners= get_value('banners', 'vImage', 'vCode',$vLanguage,' ORDER BY iDisplayOrder ASC');
			
			$data = array();
			$count =0;
			for($i = 0; $i< count($banners); $i++){
				if($banners[$i]['vImage'] != ""){
					$data[$count]['vImage'] = $tconfig["tsite_url"].'assets/img/images/'.$banners[$i]['vImage'];
					$count++;
				}
			}
			
			$returnArr['Action']="1"; 
			$returnArr['message'] =$data;
			}else{
			$returnArr['Action']="0"; 
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
	}
	
	if($type=="getUserVehicleDetails"){
		global $generalobj;
		
		$iMemberId = isset($_REQUEST['iMemberId'])?clean($_REQUEST['iMemberId']):'';
		$user_type   = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Driver';
		$vCountry = '';
		if ($user_type == "Passenger") {
			$tblname = "register_user";
			$vLangCode = get_value('register_user', 'vLang', 'iUserId', $iMemberId, '', 'true');
			} else {
			$tblname = "register_driver";
			$driveData = get_value('register_driver', 'vLang,vCountry', 'iDriverId', $iMemberId);
			$vLangCode = $driveData[0]['vLang'];
			$vCountry = $driveData[0]['vCountry'];
		}
		if ($vLangCode == "" || $vLangCode == NULL) {
			$vLangCode = get_value('language_master', 'vCode', 'eDefault', 'Yes', '', 'true');
		}
		
		$APP_TYPE = $generalobj->getConfigurations("configurations", "APP_TYPE");
		if($APP_TYPE == "Delivery"){
			$ssql.= " AND eType = 'Deliver'";
			}else if($APP_TYPE == "Ride-Delivery"){
			$ssql.= " AND ( eType = 'Deliver' OR eType = 'Ride')";
			}else{
			$ssql.= " AND eType = '".$APP_TYPE."'";
		}
		
		if($vCountry != ""){
			$iCountryId = get_value('country', 'iCountryId', 'vCountryCode', $vCountry, '', 'true');
			$ssql.= " AND (iCountryId = '".$iCountryId."' OR iCountryId = '-1' OR iCountryId = '0')";
		}
		
		$sql = "SELECT iVehicleTypeId,vVehicleType_".$vLangCode." as vVehicleType FROM `vehicle_type` WHERE 1".$ssql;
		$db_vehicletype = $obj->MySQLSelect($sql);
		if($APP_TYPE == 'UberX'){
			$sql = "SELECT vCarType FROM `driver_vehicle` where iDriverId ='".$iMemberId."'";
			$db_vCarType = $obj->MySQLSelect($sql);
			
			/* if(count($db_vCarType) > 0){
				$vehicle_service_id= explode(",", $db_vCarType[0]['vCarType']);
				$data_service = array();
				for($i=0;$i<count($db_vehicletype); $i++){					
				$data_service[$i]=$db_vehicletype[$i];
				if(in_array($data_service[$i]['iVehicleTypeId'],$vehicle_service_id)){
				$data_service[$i]['VehicleServiceStatus']= 'true'; 							
				}else{
				$data_service[$i]['VehicleServiceStatus']= 'false'; 				
				}
				}	
			} */ 	
			if(count($db_vehicletype)>0 &&count($db_vCarType) > 0){
				$vehicle_service_id= explode(",", $db_vCarType[0]['vCarType']);
				for($i=0;$i<count($db_vehicletype); $i++){		
					if(in_array($db_vehicletype[$i]['iVehicleTypeId'],$vehicle_service_id)){
						$db_vehicletype[$i]['VehicleServiceStatus']= 'true'; 							
						}else{
						$db_vehicletype[$i]['VehicleServiceStatus']= 'false'; 				
					}
				}
			}
		}	
		$make = get_value('make', '*', 'eStatus', 'Active');
		$start = @date('Y');
		$end = '1970';
		$year = array();
		for($j = $start; $j >= $end; $j--) {
			$year[] = strval($j);
			//$year .= $j.",";  
		}
		
		//echo "<pre>";print_r($year);exit;
		$carlist = array();
		if(count($make) > 0){
			//echo "<pre>";print_r($make);exit;
			for($i = 0; $i< count($make); $i++){
				$ModelArr['List']=get_value('model', '*', 'iMakeId', $make[$i]['iMakeId']);
				$carlist[$i]['iMakeId'] = $make[$i]['iMakeId'];
				$carlist[$i]['vMake'] = $make[$i]['vMake'];
				$carlist[$i]['vModellist'] = $ModelArr['List'];
			}
			$data['year'] = $year;
			$data['carlist'] = $carlist;
			
			$data['vehicletypelist'] = $db_vehicletype; 
			
			
			$returnArr['Action']="1"; 
			$returnArr['message'] =$data;
			}else{
			$returnArr['Action']="0"; 
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}  
		//echo "<pre>";print_r($data);exit;
		echo json_encode($returnArr);exit;  
	}
	
	###########################Add/Edit Driver Vehicle#######################################################
	if ($type == "UpdateDriverVehicle") {
		$iDriverVehicleId = isset($_REQUEST['iDriverVehicleId']) ? $_REQUEST['iDriverVehicleId'] : '';
		$iDriverId = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$iMakeId = isset($_REQUEST["iMakeId"]) ? $_REQUEST["iMakeId"] : '';
		$iModelId = isset($_REQUEST["iModelId"]) ? $_REQUEST["iModelId"] : '';
		$iYear = isset($_REQUEST["iYear"]) ? $_REQUEST["iYear"] : '';
		$vLicencePlate = isset($_REQUEST["vLicencePlate"]) ? $_REQUEST["vLicencePlate"] : '';
		$eCarX = isset($_REQUEST["eCarX"]) ? $_REQUEST["eCarX"] : '';
		$eCarGo = isset($_REQUEST["eCarGo"]) ? $_REQUEST["eCarGo"] : '';
		$vColour = isset($_REQUEST["vColor"]) ? $_REQUEST["vColor"] : '';
		//$eStatus = ($generalobj->getConfigurations("configurations", "VEHICLE_AUTO_ACTIVATION") == 'Yes') ? 'Active' : 'Inactive';
		$vCarType = isset($_REQUEST["vCarType"]) ? $_REQUEST["vCarType"] : '';
		$handiCap = isset($_REQUEST["HandiCap"]) ? $_REQUEST["HandiCap"] : 'No';
		
		$APP_TYPE = $generalobj->getConfigurations("configurations", "APP_TYPE");
		if($APP_TYPE == "Ride-Delivery-UberX"){
			$query ="SELECT GROUP_CONCAT(iVehicleTypeId)as countId FROM `vehicle_type`";
			$result = $obj->MySQLSelect($query);
			$vCarType = $result[0]['countId']; 
		}
		
		$action = ($iDriverVehicleId != 0) ? 'Edit' : 'Add';
		if($action == "Add"){
			$eStatus = "Inactive";	
		}
		$sql = "select iCompanyId from `register_driver` where iDriverId = '" . $iDriverId . "'";
		$db_usr = $obj->MySQLSelect($sql);
		$iCompanyId = $db_usr[0]['iCompanyId'];
		
		$Data_Driver_Vehicle['iDriverId'] = $iDriverId;
		$Data_Driver_Vehicle['iCompanyId'] = $iCompanyId;
		
		if(SITE_TYPE == "Demo"){
			$Data_Driver_Vehicle['eStatus'] = "Active"; 
			}else{
			if($action == "Add"){
				$Data_Driver_Vehicle['eStatus'] = $eStatus;
			}
		}
		$Data_Driver_Vehicle['eCarX'] = $eCarX;
		$Data_Driver_Vehicle['eCarGo'] = $eCarGo;
		$Data_Driver_Vehicle['vCarType'] = $vCarType;
		$Data_Driver_Vehicle['eHandiCapAccessibility'] = $handiCap;
		
		if($iMakeId != ""){
			$Data_Driver_Vehicle['iMakeId'] = $iMakeId;
		}
		if($iModelId != ""){
			$Data_Driver_Vehicle['iModelId'] = $iModelId;
		}
		
		if($iYear != ""){
			$Data_Driver_Vehicle['iYear'] = $iYear;
		}
		
		if($vColour != ""){
			$Data_Driver_Vehicle['vColour'] = $vColour;
		}
		if($vLicencePlate != ""){
			$Data_Driver_Vehicle['vLicencePlate'] = $vLicencePlate;
		}
		
		if($APP_TYPE == 'UberX'){
			$Data_Driver_Vehicle['iCompanyId'] = "1";
			$Data_Driver_Vehicle['iMakeId'] = "3";
			$Data_Driver_Vehicle['iModelId'] = "1";
			$Data_Driver_Vehicle['iYear'] = Date('Y');
			$Data_Driver_Vehicle['vLicencePlate'] = "My Services";
			$Data_Driver_Vehicle['eStatus'] = "Active";
			$Data_Driver_Vehicle['eCarX'] = "Yes";
			$Data_Driver_Vehicle['eCarGo'] = "Yes";
		}
		
		// $Data_Driver_Vehicle['vColour'] = $vColour;
		// $Data_Driver_Vehicle['vLicencePlate'] = $vLicencePlate;
		
		if ($action == "Add") {
			
			
			$id = $obj->MySQLQueryPerform("driver_vehicle", $Data_Driver_Vehicle, 'insert');
			} else {
			$where = " iDriverVehicleId = '" . $iDriverVehicleId . "'";
			$id = $obj->MySQLQueryPerform("driver_vehicle", $Data_Driver_Vehicle, 'update', $where);
		}
		
		if ($id > 0) {
			$returnArr['Action'] = "1";
			//$returnArr['message'] = GetDriverDetail($iDriverId);
			$returnArr['message'] = ($action == 'Add') ? 'LBL_VEHICLE_ADD_SUCCESS' : 'LBL_VEHICLE_UPDATE_SUCCESS';
			//$eStatus = ($generalobj->getConfigurations("configurations", "VEHICLE_AUTO_ACTIVATION") == 'Yes') ? 'Active' : 'Inactive';
			} else {
			$returnArr['Action'] = "0";
			$returnArr['message'] = "LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
		exit;
	}
	###########################Add/Edit Driver Vehicle End#######################################################
	################################Delete Driver Vehicle###############################################################
	
	################################Delete Driver Vehicle #######################################################
	if ($type == 'deletedrivervehicle') {
		global $generalobj, $tconfig;
		$returnArr = array();
		$iMemberCarId = isset($_REQUEST['iDriverVehicleId']) ? clean($_REQUEST['iDriverVehicleId']) : '';
		$iDriverId = isset($_REQUEST['iDriverId']) ? clean($_REQUEST['iDriverId']) : '';
		// getLanguageCode($iMemberId); //create array of language_label
		$iDriverVehicleId = get_value('register_driver', 'iDriverVehicleId', 'iDriverId', $iDriverId, '', 'true');
    	if($iDriverVehicleId == $iMemberCarId) {
    		$returnArr['Action'] = 0;
    		$returnArr['message'] = "LBL_DELETE_VEHICLE_ERROR";
    		echo json_encode($returnArr);
    		exit;
		}
		
		$sql = "DELETE FROM driver_vehicle WHERE iDriverVehicleId='" . $iMemberCarId . "' AND iDriverId='" . $iDriverId . "'";
		$db_sql = $obj->sql_query($sql);
		if (mysql_affected_rows() > 0) {
			$returnArr['Action'] = 1;
			$returnArr['message'] = "LBL_DELETE_VEHICLE";
			} else {
			$returnArr['Action'] = 0;
			$returnArr['message'] = "LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
		exit;
	}
	
	###########################displayDocList##########################################################
	
	if ($type == "displayDocList") {
		global $generalobj, $tconfig;
		
		$iMemberId = isset($_REQUEST['iMemberId']) ? clean($_REQUEST['iMemberId']) : '';
		$memberType = isset($_REQUEST['MemberType']) ? clean($_REQUEST['MemberType']) : 'Driver';
		$iDriverVehicleId = isset($_REQUEST['iDriverVehicleId']) ? clean($_REQUEST['iDriverVehicleId']) : '';
		$doc_usertype = isset($_REQUEST['doc_usertype']) ? clean(strtolower($_REQUEST['doc_usertype'])) : 'driver';
		
		if($doc_usertype == "vehicle"){
			$doc_usertype = "car";
		}
		$doc_userid = ($doc_usertype == 'car') ? $iDriverVehicleId : $iMemberId;
		$APP_TYPE = $generalobj->getConfigurations("configurations", "APP_TYPE");
		
		$vCountry = get_value('register_driver', 'vCountry', 'iDriverId', $iMemberId,'',true);
		
		$sql1= "SELECT dm.doc_masterid masterid, dm.doc_usertype , dm.doc_name ,dm.ex_status,dm.status, COALESCE(dl.doc_id,  '' ) as doc_id,COALESCE(dl.doc_masterid, '') as masterid_list ,COALESCE(dl.ex_date, '') as ex_date,COALESCE(dl.doc_file, '') as doc_file, COALESCE(dl.status, '') as status FROM document_master dm left join (SELECT * FROM `document_list` where doc_userid='" . $doc_userid . "' ) dl on dl.doc_masterid=dm.doc_masterid  
		where dm.doc_usertype='".$doc_usertype."' AND (dm.country='".$vCountry."' OR dm.country='All') and dm.status='Active' ";
		$db_vehicle = $obj->MySQLSelect($sql1);
		if (count($db_vehicle) > 0) {
			//$Photo_Gallery_folder = $tconfig['tsite_upload_driver_doc']."/".$iMemberId."/";
			if($doc_usertype == "driver") {
				$Photo_Gallery_folder = $tconfig['tsite_upload_driver_doc'] . "/" . $iMemberId . "/"; 
				} else {
				$Photo_Gallery_folder = $tconfig['tsite_upload_vehicle_doc_panel'] . "/" . $iDriverVehicleId . "/";
			}
			for($i=0;$i<count($db_vehicle);$i++){
				if($db_vehicle[$i]['doc_file'] != ""){
					$db_vehicle[$i]['vimage'] = $Photo_Gallery_folder.$db_vehicle[$i]['doc_file'];
					}else{
					$db_vehicle[$i]['vimage'] = "";
				}
			}
			$returnArr['Action'] = "1";
			$returnArr['message'] = $db_vehicle;
			} else {
			$returnArr['Action'] = "0";
			$returnArr['message'] = "LBL_NO_DATA_AVAIL";
		}
		echo json_encode($returnArr);
		exit;
	}
	####################################################################################################
	
	###########################displaydrivervehicles##########################################################
	
	if ($type == "displaydrivervehicles") {
		global $generalobj, $tconfig;
		
		$iMemberId = isset($_REQUEST['iMemberId']) ? clean($_REQUEST['iMemberId']) : '';
		$memberType = isset($_REQUEST['MemberType']) ? clean($_REQUEST['MemberType']) : 'Driver';
		
		$sql = "select iCompanyId from `register_driver` where iDriverId = '" . $iMemberId . "'";
		$db_usr = $obj->MySQLSelect($sql);
		$iCompanyId = $db_usr[0]['iCompanyId'];
		$APP_TYPE = $generalobj->getConfigurations("configurations", "APP_TYPE");
		
		if ($APP_TYPE == 'UberX') {
			$sql = "SELECT * FROM driver_vehicle where iCompanyId = '" . $iCompanyId . "' and iDriverId = '" . $iMemberId . "' and eStatus != 'Deleted'";
			$db_vehicle = $obj->MySQLSelect($sql);
			} else {
			
			$sql="SELECT m.vTitle, mk.vMake,dv.* ,case WHEN (dv.vInsurance='' OR dv.vPermit='' OR dv.vRegisteration='') THEN 'TRUE' ELSE 'FALSE' END as 'VEHICLE_DOCUMENT'
			FROM driver_vehicle as dv JOIN model m ON dv.iModelId=m.iModelId JOIN make mk ON dv.iMakeId=mk.iMakeId where iCompanyId = '".$iCompanyId."' and iDriverId = '".$iMemberId."' and dv.eStatus != 'Deleted'";  
			// echo   $sql = "SELECT m.vTitle, mk.vMake,dv.*  FROM driver_vehicle as dv JOIN model m ON dv.iModelId=m.iModelId JOIN make mk ON dv.iMakeId=mk.iMakeId where iCompanyId = '" . $iCompanyId . "' and iDriverId = '" . $iMemberId . "' and dv.eStatus != 'Deleted'";
			$db_vehicle = $obj->MySQLSelect($sql);
		}
		
		if (count($db_vehicle) > 0) {
			$returnArr['Action'] = "1";
			$returnArr['message'] = $db_vehicle;
			} else {
			$returnArr['Action'] = "0";
			$returnArr['message'] = "LBL_NO_VEHICLES_FOUND";
		}
		echo json_encode($returnArr);
		exit;
	} 
	
	###########################Display Driver's Vehicle Listing End##########################################################
	###########################Add/Update Driver's Document and Vehilcle Document ##########################################################
	if ($type == "uploaddrivedocument") {
		global $generalobj, $tconfig;
		$iMemberId = isset($_REQUEST['iMemberId']) ? clean($_REQUEST['iMemberId']) : '';
		$iDriverVehicleId = isset($_REQUEST['iDriverVehicleId']) ? clean($_REQUEST['iDriverVehicleId']) : '';
		//$doc_userid = isset($_REQUEST['doc_userid']) ? clean($_REQUEST['doc_userid']) : '';
		$memberType = isset($_REQUEST['MemberType']) ? clean($_REQUEST['MemberType']) : 'Driver';
		$doc_usertype = isset($_REQUEST['doc_usertype']) ? clean(strtolower($_REQUEST['doc_usertype'])) : 'driver';     // vehicle OR driver
		$doc_masterid = isset($_REQUEST['doc_masterid']) ? clean($_REQUEST['doc_masterid']) : '';
		$doc_name = isset($_REQUEST['doc_name']) ? clean($_REQUEST['doc_name']) : '';
		$doc_id = isset($_REQUEST['doc_id']) ? clean($_REQUEST['doc_id']) : '';
		$doc_file = isset($_REQUEST['doc_file']) ? clean($_REQUEST['doc_file']) : '';
		$ex_date = isset($_REQUEST['ex_date']) ? clean($_REQUEST['ex_date']) : '';
		$ex_status = isset($_REQUEST['ex_status']) ? clean($_REQUEST['ex_status']) : '';
		if($doc_usertype == "vehicle"){
			$doc_usertype = "car";
		}
		$doc_userid = ($doc_usertype == 'car') ? $iDriverVehicleId : $iMemberId;
		$status = ($doc_usertype == 'car') ? "Active" : "Inctive"; 
		$image_name = $vImage = isset($_FILES['vImage']['name']) ? $_FILES['vImage']['name'] : '';
		$image_object = isset($_FILES['vImage']['tmp_name']) ? $_FILES['vImage']['tmp_name'] : '';
		//$image_name = "123.jpg";
		$action = ($doc_id != '') ? 'Edit' : 'Add';
		$addupdatemode = ($action == 'Add') ? 'insert' : 'update'; 
		
		if($doc_file != ""){
			$vImageName = $doc_file;
			}else{
			if($doc_usertype == "driver") {
				$Photo_Gallery_folder = $tconfig['tsite_upload_driver_doc_path'] . "/" . $iMemberId . "/"; 
				} else {
				$Photo_Gallery_folder = $tconfig['tsite_upload_vehicle_doc'] . "/" . $iDriverVehicleId . "/";
			}
			if(!is_dir($Photo_Gallery_folder)) {
				mkdir($Photo_Gallery_folder, 0777);
			}
			$vFile = $generalobj->fileupload($Photo_Gallery_folder, $image_object, $image_name, $prefix = '', $vaildExt = "pdf,doc,docx,jpg,jpeg,gif,png");
			$vImageName = $vFile[0];
		}
		
		if ($vImageName != '') {
			$Data_Update["doc_masterid"] = $doc_masterid;
			$Data_Update["doc_usertype"] = $doc_usertype;
			$Data_Update["doc_userid"] = $doc_userid;
			$Data_Update["ex_date"] = $ex_date;
			$Data_Update["doc_file"] = $vImageName;
			$Data_Update["edate"] = @date("Y-m-d H:i:s");  
			if($action == "Add"){
				$Data_Update["status"] = $status; 
				$id = $obj->MySQLQueryPerform("document_list", $Data_Update, 'insert'); 
				}else{
				$where = " doc_id = '" . $doc_id . "'";
				$id = $obj->MySQLQueryPerform("document_list", $Data_Update, 'update', $where);
			}
			$generalobj->save_log_data('0', $iMemberId, 'driver', $doc_name, $vImageName);
			if ($id > 0) {
				$returnArr['Action'] = "1";
				//$returnArr['message'] = getDriverDetailInfo($iMemberId);
				} else {
				$returnArr['Action'] = "0";
				$returnArr['message'] = "LBL_TRY_AGAIN_LATER_TXT";
			}
			} else {
			$returnArr['Action'] = "0";
			$returnArr['message'] = "LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
		exit;
	}
	###########################Add/Update Driver's Document and Vehilcle Document Ends##########################################################
    ###########################Add/Update User's Vehicle Listing End##########################################################
	
	if($type=="UpdateUserVehicleDetails"){
		global $generalobj,$tconfig;
		$iUserVehicleId = isset($_REQUEST['iUserVehicleId'])?$_REQUEST['iUserVehicleId']:'';
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		$iMakeId = isset($_REQUEST["iMakeId"]) ? $_REQUEST["iMakeId"] : '';
		$iModelId = isset($_REQUEST["iModelId"]) ? $_REQUEST["iModelId"] : '';
		$iYear = isset($_REQUEST["iYear"]) ? $_REQUEST["iYear"] : '';
		$vLicencePlate = isset($_REQUEST["vLicencePlate"]) ? $_REQUEST["vLicencePlate"] : '';
		$vColour = isset($_REQUEST["vColour"]) ? $_REQUEST["vColour"] : '';
		$eStatus = isset($_REQUEST["eStatus"]) ? $_REQUEST["eStatus"] : 'Inactive';
		//$vImage = isset($_REQUEST["vImage"]) ? $_REQUEST["vImage"] : '';
		
		$image_name = $vImage = isset($_FILES['vImage']['name'])?$_FILES['vImage']['name']:'';
		$image_object	= isset($_FILES['vImage']['tmp_name'])?$_FILES['vImage']['tmp_name']:'';
		
		$Photo_Gallery_folder = $tconfig['tsite_upload_images_passenger_vehicle']."/".$iUserVehicleId."/"; // /webimages/upload/uservehicle
		
		// echo $Photo_Gallery_folder."===";
		if(!is_dir($Photo_Gallery_folder))
		mkdir($Photo_Gallery_folder, 0777);
		
		$action = ($iUserVehicleId != '')?'Edit':'Add';
		
		$Data_User_Vehicle['iUserId']=$iUserId;
		$Data_User_Vehicle['iMakeId']=$iMakeId;
		$Data_User_Vehicle['iModelId']=$iModelId;
		$Data_User_Vehicle['iYear']=$iYear;
		$Data_User_Vehicle['vLicencePlate']=$vLicencePlate;
		$Data_User_Vehicle['eStatus']=$eStatus;
		$Data_User_Vehicle['vColour']=$vColour;
		//$Data_User_Vehicle['vImage']=$vImage;
		
		if($action == "Add"){
			$id = $obj->MySQLQueryPerform("user_vehicle",$Data_User_Vehicle,'insert');
			$updateimageid = $id; 
			}else{
			$where = " iUserVehicleId = '".$iUserVehicleId."'";
			$updateimageid = $iUserVehicleId;
			$id = $obj->MySQLQueryPerform("user_vehicle",$Data_User_Vehicle,'update',$where);
		}
		
		if($image_name != ""){
			$vFile = $generalobj->fileupload($Photo_Gallery_folder,$image_object,$image_name,$prefix='', $vaildExt="pdf,doc,docx,jpg,jpeg,gif,png");
			$vImageName = $vFile[0];
			$Data_passenger["vImage"]=$vImageName;
			$where_image = " iUserVehicleId = '".$updateimageid."'";
			$id = $obj->MySQLQueryPerform("user_vehicle",$Data_passenger,'update',$where_image);
		}
		
		if($id > 0){
			$returnArr['Action'] = "1";
			$returnArr['message'] = getPassengerDetailInfo($iUserId);
			}else{
			$returnArr['Action'] ="0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		echo json_encode($returnArr);
		exit;
	}
	
	if($type == "displayuservehicles"){
		global $generalobj,$tconfig;
		
		$iUserId 	= isset($_REQUEST['iUserId'])?clean($_REQUEST['iUserId']):'';
		$sql = "SELECT m.vTitle, mk.vMake,uv.*  FROM user_vehicle as uv JOIN model m ON uv.iModelId=m.iModelId JOIN make mk ON uv.iMakeId=mk.iMakeId where iUserId = '".$iUserId."' and uv.eStatus != 'Deleted'";
		$db_vehicle = $obj->MySQLSelect($sql);
		
		if(count($db_vehicle) > 0){
			$returnArr['Action'] = "1";
			$returnArr['message'] = $db_vehicle;
			}else{
			$returnArr['Action'] ="0";
			$returnArr['message'] ="No Vehicles Found";
		}
		echo json_encode($returnArr);
		exit;
	}
	
	if($type == 'changelanguagelabel') { 
		$vLang = isset($_REQUEST['vLang'])?clean($_REQUEST['vLang']):'';
		$UpdatedLanguageLabels = getLanguageLabelsArr($vLang,"1");
		
		$lngData = get_value('language_master', 'vCode, vGMapLangCode, eDirectionCode as eType, vTitle', 'vCode', $vLang);
		
		$returnArr['Action'] = "1";
		$returnArr['message'] = $UpdatedLanguageLabels;
		$returnArr['vCode'] =  $lngData[0]['vCode'];
		$returnArr['vGMapLangCode'] = $lngData[0]['vGMapLangCode'];
		$returnArr['eType'] = $lngData[0]['eType'];
		$returnArr['vTitle'] = $lngData[0]['vTitle'];
		
		echo json_encode($returnArr);
		exit;
	}
	
	if($type == 'displaytripcharges') {
		$TripID = isset($_REQUEST["TripID"]) ? $_REQUEST["TripID"] : '';
		$destination_lat = isset($_REQUEST["dest_lat"]) ? $_REQUEST["dest_lat"] : '';
		$destination_lon = isset($_REQUEST["dest_lon"]) ? $_REQUEST["dest_lon"] : '';
		$iTripTimeId = isset($_REQUEST["iTripTimeId"]) ? $_REQUEST["iTripTimeId"] : '';
		$ALLOW_SERVICE_PROVIDER_AMOUNT = $generalobj->getConfigurations("configurations", "ALLOW_SERVICE_PROVIDER_AMOUNT");
		
		$where = " iTripId = '".$TripID."'";
		$data_update['tEndDate']=@date("Y-m-d H:i:s");
		$data_update['tEndLat']=$destination_lat;
		$data_update['tEndLong']=$destination_lon;
		$obj->MySQLQueryPerform("trips",$data_update,'update',$where);
		
		if($iTripTimeId != "") {
			$where = " iTripTimeId = '$iTripTimeId'";
			$Data_update['dPauseTime']=$data_update['tEndDate'];
			$Data_update['iTripId']=$TripID;
			$id = $obj->MySQLQueryPerform("trip_times",$Data_update,'update',$where);
		}
		
		$sql = "SELECT * from trips WHERE iTripId = '".$TripID."'";
		$tripData = $obj->MySQLSelect($sql);
		// echo "<pre>"; print_r($tripData); die;
		$iDriverVehicleId = $tripData[0]['iDriverVehicleId'];
		$iVehicleTypeId = $tripData[0]['iVehicleTypeId'];
		$fVisitFee = $tripData[0]['fVisitFee'];
		$startDate = $tripData[0]['tStartDate'];
		$endDateOfTrip = $tripData[0]['tEndDate']; 
		$iQty = $tripData[0]['iQty']; 
		//$endDateOfTrip=@date("Y-m-d H:i:s");
		
		if($tripData[0]['eFareType'] == 'Hourly'){
			$sql22 = "SELECT * FROM `trip_times` WHERE iTripId='$TripID'";
			$db_tripTimes = $obj->MySQLSelect($sql22);
			
			$totalSec = 0;
			$iTripTimeId = '';
			foreach($db_tripTimes as $dtT){
				if($dtT['dPauseTime'] != '' && $dtT['dPauseTime'] != '0000-00-00 00:00:00') {
					$totalSec += strtotime($dtT['dPauseTime']) - strtotime($dtT['dResumeTime']);
				}
			}
			$totalTimeInMinutes_trip=@round(abs($totalSec) / 60,2);
			}else {
			$totalTimeInMinutes_trip=@round(abs(strtotime($startDate) - strtotime($endDateOfTrip)) / 60,2);
		}
		$totalHour = $totalTimeInMinutes_trip / 60;
		$tripDistance=calcluateTripDistance($tripId);
		$sourcePointLatitude=$tripData[0]['tStartLat'];
		$sourcePointLongitude=$tripData[0]['tStartLong'];
		if($totalTimeInMinutes_trip <= 1){
			$FinalDistance = $tripDistance;
			}else{
			$FinalDistance = checkDistanceWithGoogleDirections($tripDistance,$sourcePointLatitude,$sourcePointLongitude,$destination_lat,$destination_lon);
		}
		$tripDistance=$FinalDistance;
		$fPickUpPrice = $tripData[0]['fPickUpPrice'];
		$fNightPrice = $tripData[0]['fNightPrice'];
		$eFareType = get_value('trips', 'eFareType', 'iTripId', $TripID, '', 'true');
		$surgePrice = $fPickUpPrice > 1 ? $fPickUpPrice : ($fNightPrice > 1 ? $fNightPrice : 1);
		$fAmount = 0;
		$Fare_data = getVehicleFareConfig("vehicle_type", $iVehicleTypeId);
		// echo "<pre>"; print_r($tripData); die;
		$Minute_Fare = round($Fare_data[0]['fPricePerMin'] * $totalTimeInMinutes_trip * $surgePrice,2);
		$Distance_Fare = round($Fare_data[0]['fPricePerKM'] * $tripDistance * $surgePrice,2);
		$iBaseFare = round($Fare_data[0]['iBaseFare'] * $surgePrice,2);
		$iMinFare = round($Fare_data[0]['iMinFare'] * $surgePrice,2); 
		$total_fare = $iBaseFare + $Minute_Fare + $Distance_Fare; 
		if($iMinFare > $total_fare){
            $total_fare = $iMinFare; 
		}
		if($ALLOW_SERVICE_PROVIDER_AMOUNT == "Yes"){
			
			$sqlServicePro = "SELECT * FROM `service_pro_amount` WHERE iDriverVehicleId='".$iDriverVehicleId."' AND iVehicleTypeId='".$iVehicleTypeId."'";
			$serviceProData = $obj->MySQLSelect($sqlServicePro);
            
            if(count($serviceProData) > 0){
				$fAmount = $serviceProData[0]['fAmount'];
				if($eFareType == "Fixed"){
					$fAmount = $fAmount * $iQty;
					}else if($eFareType == "Hourly"){
					$fAmount = $fAmount * $totalHour;
					}else{
					$fAmount = $total_fare;
				}
				}else{ 
				if($eFareType == "Fixed"){
					$fAmount = round($Fare_data[0]['fFixedFare'] * $iQty,2);
					}else if($eFareType == "Hourly"){
					$fAmount = round($Fare_data[0]['fPricePerHour'] * $totalHour,2);
					}else{
					$fAmount = $total_fare;
				}
			}
			}else{   
			if($eFareType == "Fixed"){
				$fAmount = round($Fare_data[0]['fFixedFare'] * $iQty,2);
				}else if($eFareType == "Hourly"){
				$fAmount = round($Fare_data[0]['fPricePerHour'] * $totalHour,2);
				}else{
				$fAmount = $total_fare;
			}
		}
		
		$final_display_charge = $fAmount+$fVisitFee;
		$returnArr['Action']="1";
		/*$vCurrencyDriver=get_value('register_driver', 'vCurrencyDriver', 'iDriverId', $tripData[0]['iDriverId'],'','true');
			$currencySymbolRationDriver = get_value('currency', 'vSymbol,Ratio', 'vName', $vCurrencyDriver);
		$returnArr['message']=$currencySymbolRationDriver[0]['vSymbol']." ".number_format(round($final_display_charge * $currencySymbolRationDriver[0]['Ratio'],1),2);*/
		$currencySymbol = get_value('currency', 'vSymbol', 'eDefault', 'Yes','',true);
		$returnArr['message']=$currencySymbol.' '.$final_display_charge;
		$returnArr['FareValue']=$final_display_charge;
		$returnArr['CurrencySymbol']=$currencySymbol;
		echo json_encode($returnArr);
	}
	
	########################### UBER-For-X ######################################
	###########################################################################
	###########################################################################
	if($type=="checkUserStatus"){
		$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$APP_TYPE = $generalobj->getConfigurations("configurations", "APP_TYPE");
		
		if($UserType == "Passenger"){
			// $tblname = "register_user";
			// $fields = 'iUserId as iMemberId, vPhone,vPhoneCode as vPhoneCode, vEmail, vName, vLastName,vPassword, vLang';
			$condfield = 'iUserId';
			}else{
			// $tblname = "register_driver";
			// $fields = 'iDriverId  as iMemberId, vPhone,vCode as vPhoneCode, vEmail, vName, vLastName,vPassword, vLang';
			$condfield = 'iDriverId';
		}
		
		if($APP_TYPE == "UberX"){
			$sql = "SELECT iTripId FROM trips WHERE 1=1 AND $condfield = '".$iMemberId."' AND vTripPaymentMode != 'Cash' AND eType!='Ride' AND (iActive=	'Active' OR iActive='On Going Trip')";
			$checkStatus = $obj->MySQLSelect($sql);
			}else {
			$sql = "SELECT iTripId FROM trips WHERE 1=1 AND $condfield = '".$iMemberId."' AND vTripPaymentMode != 'Cash' AND eType='Ride' AND (iActive=	'Active' OR iActive='On Going Trip') order by iTripId DESC limit 1";
			$checkStatus = $obj->MySQLSelect($sql);
		}
		
		if(count($checkStatus) > 0){
			$returnArr['Action']="0";
			$returnArr['message'] = 'LBL_DIS_ALLOW_EDIT_CARD';
			}else {
			$returnArr['Action']="1";
		}
		echo json_encode($returnArr);
		exit;
	}
	###########################################################################
	###########################################################################
	
	###########################################################################
	
	#########################################################################
	## NEW WEBSERVICE END ##
	##########################################################################
	############################ language_master #############################
	if($type == 'language_master') {
		
		$sql = "SELECT * FROM  `language_master` WHERE eStatus = 'Active' ";
		$all_label = $obj->MySQLSelect($sql);
		$returnArr['language_master_code'] = $all_label;
		echo json_encode($returnArr);
		exit;
	}
	##########################################################################
	
	if($type == 'GetLinksConfiguration'){
		$UserType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):'';
		
		if($UserType=='Passenger'){
			$DataArr['LINK_FORGET_PASS_PAGE_PASSENGER']=$tconfig["tsite_url"].$generalobj->getConfigurations("configurations","LINK_FORGET_PASS_PAGE_PASSENGER");
			$DataArr['FACEBOOK_APP_ID']=$generalobj->getConfigurations("configurations","FACEBOOK_APP_ID");
			$DataArr['CONFIG_CLIENT_ID']=$generalobj->getConfigurations("configurations","CONFIG_CLIENT_ID");
			$DataArr['GOOGLE_SENDER_ID']=$generalobj->getConfigurations("configurations","GOOGLE_SENDER_ID");
			$DataArr['MOBILE_VERIFICATION_ENABLE']=$generalobj->getConfigurations("configurations","MOBILE_VERIFICATION_ENABLE");
			
			echo json_encode($DataArr);
			}else if($UserType=='Driver'){
			$DataArr['LINK_FORGET_PASS_PAGE_DRIVER']=$tconfig["tsite_url"].$generalobj->getConfigurations("configurations","LINK_FORGET_PASS_PAGE_DRIVER");
			$DataArr['LINK_SIGN_UP_PAGE_DRIVER']=$tconfig["tsite_url"].$generalobj->getConfigurations("configurations","LINK_SIGN_UP_PAGE_DRIVER");
			$DataArr['GOOGLE_SENDER_ID']=$generalobj->getConfigurations("configurations","GOOGLE_SENDER_ID");
			$DataArr['MOBILE_VERIFICATION_ENABLE']=$generalobj->getConfigurations("configurations","MOBILE_VERIFICATION_ENABLE");
			
			echo json_encode($DataArr);
		}
	}
	
	##########################################################################
	
	if($type=='UpdateLanguageCode'){
		
		$lCode = isset($_REQUEST['vCode'])?clean(strtoupper($_REQUEST['vCode'])):''; // User's prefered language
		$UserID = isset($_REQUEST['UserID'])?clean($_REQUEST['UserID']):'';
		$UserType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):'';
		
		if($UserType=="Passenger"){
			
			$where = " iUserId = '$UserID'";
			$Data_update_passenger['vLang']=$lCode;
			
			$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
			// echo $id; exit;
			if($id<0){
				echo "UpdateFailed";
				exit;
			}
			}else if($UserType=="Driver"){
			$where = " iDriverId = '$UserID'";
			$Data_update_driver['vLang']=$lCode;
			
			$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
			// echo $id; exit;
			if($id<0){
				echo "UpdateFailed";
				exit;
			}
		}
		
		/* find default language of website set by admin */
		if($lCode == '') {
			$sql = "SELECT  `vCode` FROM  `language_master` WHERE eStatus = 'Active' AND `eDefault` = 'Yes' ";
			$default_label = $obj->MySQLSelect($sql);
			
			$lCode = (isset($default_label[0]['vCode']) && $default_label[0]['vCode'])?$default_label[0]['vCode']:'EN';
		}
		
		$sql = "SELECT  `vLabel` , `vValue`  FROM  `language_label`  WHERE  `vCode` = '".$lCode."' ";
		$all_label = $obj->MySQLSelect($sql);
		
		$x = array();
		for($i=0; $i<count($all_label); $i++){
			$vLabel = $all_label[$i]['vLabel'];
			$vValue = $all_label[$i]['vValue'];
			$x[$vLabel]=$vValue;
		}
		$x['vCode'] = $lCode; // to check in which languge code it is loading
		
		echo json_encode($x);
		
	}
	
	##########################################################################
	
	
	
	/* get variables value directly */
	if($type == 'get_value') {
		global $obj;
		$returnArr 	= array();
		$table 				= isset($_REQUEST['table'])?clean($_REQUEST['table']):'';
		$field_name 		= isset($_REQUEST['field_name'])?clean($_REQUEST['field_name']):'';
		$condition_field 	= isset($_REQUEST['condition_field'])?clean($_REQUEST['condition_field']):'';
		$condition_value 	= isset($_REQUEST['condition_value'])?clean($_REQUEST['condition_value']):'';
		
		$where		= ($condition_field != '')?' WHERE '.$condition_field:'';
		$where		.= ($where != '' && $condition_value != '')?' = "'.$condition_value.'"':'';
		
		$returnArr = get_value($table, $field_name, $condition_field, $condition_value);
		
		echo json_encode($returnArr);
		exit;
	}
	
	
	############################## Get DriverDetail ###################################
	if ($type == "getDriverDetail") {
		
		
		$Did = isset($_REQUEST["DriverAutoId"]) ? $_REQUEST["DriverAutoId"] : '';
		$GCMID = isset($_REQUEST["GCMID"]) ? $_REQUEST["GCMID"] : '';
		
		$sql = "SELECT iGcmRegId FROM `register_driver` WHERE iDriverId='$Did'";
		$Data = $obj->MySQLSelect($sql);
		
		if(count($Data)>0){
			
			$iGCMregID=$Data[0]['iGcmRegId'];
			
			if($GCMID!=''){
				
				if($iGCMregID!=$GCMID){
					$where = " iDriverId = '$Did' ";
					
					$Data_update_driver['iGcmRegId']=$GCMID;
					
					$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
				}
				
			}
			
		}
		
		echo json_encode(getDriverDetailInfo($Did));
		
		exit;
		
	}
	###########################################################################
	
	######################## Get Driver Car Detail ############################
	if ($type == "getDriverCarDetail")
	{
		$Did = isset($_REQUEST["DriverAutoId"]) ? $_REQUEST["DriverAutoId"] : '';
		
		$sql = "SELECT make.vMake, model.vTitle, dv.*  FROM `driver_vehicle` dv, make, model WHERE dv.iDriverId='$Did' AND dv.`iMakeId` = make.`iMakeId` AND dv.`iModelId` = model.`iModelId` AND dv.`eStatus`='Active'";
		
		$Data = $obj->MySQLSelect($sql);
		if (count($Data) > 0)
		{
			
			$i=0;
			while(count($Data)>$i){
				
				$Data[$i]['vModel']=$Data[$i]['vTitle'];
				$i++;
			}
			
			$returnArr['carList'] = $Data;
			
			echo json_encode($returnArr);
		}
		else
		{
			$returnArr['action'] = 0; //duplicate entry
			$returnArr['message'] = 'Fail';
			
			echo json_encode($returnArr);
		}
		
	}
	###########################################################################
	
	
	###########################################################################
	
	
	############################ checkUser_FB ################################
	
	if ($type == "checkUser_FB") {
		
		$fbid = isset($_REQUEST["fbid"]) ? $_REQUEST["fbid"] : '';
		$cityName = isset($_REQUEST["cityName"]) ? $_REQUEST["cityName"] : '';
		$emailId = isset($_REQUEST["emailId"]) ? $_REQUEST["emailId"] : '';
		$GCMID = isset($_REQUEST["GCMID"]) ? $_REQUEST["GCMID"] : '';
		$autoSign = isset($_REQUEST["autoSign"]) ? $_REQUEST["autoSign"] : '';
		$vFirebaseDeviceToken = isset($_REQUEST["vFirebaseDeviceToken"]) ? $_REQUEST["vFirebaseDeviceToken"] : '';
		
		
		if($fbid==''){
			echo "LBL_NO_REG_FOUND";
			exit;
		}
		
		$sql    = "SELECT iUserId,eStatus,iGcmRegId FROM `register_user` WHERE vFbId=" . $fbid . " OR vEmail='$emailId'";
		$row = $obj->MySQLSelect($sql);
		
		if (count($row) > 0) {
			if($row[0]['eStatus']=="Active"){
				if($autoSign=="true"){
					$iGCMregID=$row[0]['iGcmRegId'];
					
					if($GCMID!=''){
						
						if($iGCMregID!=$GCMID){
							
							$iUserID_passenger=$row[0]['iUserId'];
							$where = " iUserId = '$iUserID_passenger' ";
							$Data_update_passenger['tSessionId'] = session_id().time();
							$Data_update_passenger['iGcmRegId']=$GCMID;
							$Data_update_passenger['vFirebaseDeviceToken']=$vFirebaseDeviceToken;
							
							$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
						}
						
					}
					
					}else{
					if($GCMID!=''){
						$iUserId_passenger=$row[0]['iUserId'];
						$where = " iUserId = '$iUserId_passenger' ";
						$Data_update_passenger['tSessionId'] = session_id().time();
						$Data_update_passenger['iGcmRegId']=$GCMID;
						$Data_update_passenger['vFirebaseDeviceToken']=$vFirebaseDeviceToken;
						
						$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
					}
				}
				
				echo json_encode(getPassengerDetailInfo($row[0]['iUserId'],$cityName));
				}else{
				echo "LBL_CONTACT_US_STATUS_NOTACTIVE_PASSENGER";
			}
			
			} else {
			echo "LBL_NO_REG_FOUND";
		}
		
	}
	
	###########################################################################
	
	if($type=='checkFacebookUser'){
		$FbID = isset($_REQUEST["FbID"]) ? $_REQUEST["FbID"] : '';
		$EmailID = isset($_REQUEST["EmailID"]) ? $_REQUEST["EmailID"] : '';
		
		$sql    = "SELECT iUserId FROM `register_user` WHERE vFbId=" . $FbID . " OR vEmail='$EmailID' ";
		$row = $obj->MySQLSelect($sql);
		
		if(count($row)>0){
			echo "Failed";
			}else{
			echo "success";
		}
		exit;
	}
	
	###########################################################################
	
	######################### checkUser_passenger #############################
	
	if ($type == "checkUser_passenger") {
		
		$Emid = isset($_REQUEST["Email"]) ? $_REQUEST["Email"] : '';
		$Phone = isset($_REQUEST["Phone"]) ? $_REQUEST["Phone"] : '';
		
		$sql    = "SELECT vEmail,vPhone FROM `register_user` WHERE vEmail = '$Emid' OR vPhone = '$Phone'";
		$Data = $obj->MySQLSelect($sql);
		
		if ( count($Data) >0  )
		{
			
			if($Emid==$Data[0]['vEmail']){
				echo "EMAIL_EXIST";
				}else {
				echo "MOBILE_EXIST";
			}
		}
		else
		{
			echo "NO_REG_FOUND";
		}
		
	}
	###########################################################################
	
	######################## getDriverDetail_signIN ###########################
	
	if ($type == "getDriverDetail_signIN") {
		$Driver_email = $_REQUEST["DriverId"];
		$Password_driver = $generalobj->encrypt($_REQUEST["Pass"]);
		$GCMID = isset($_REQUEST["GCMID"]) ? $_REQUEST["GCMID"] : '';
		$vFirebaseDeviceToken = isset($_REQUEST["vFirebaseDeviceToken"]) ? $_REQUEST["vFirebaseDeviceToken"] : '';
		
		
		$DeviceType = "Android";
		$sql = "SELECT rd.iDriverId,rd.eStatus,cmp.eStatus as cmpEStatus FROM `register_driver` as rd,`company` as cmp WHERE rd.vEmail='$Driver_email'  AND rd.vPassword='$Password_driver' AND cmp.iCompanyId=rd.iCompanyId";
		$Data = $obj->MySQLSelect($sql);
		
		if ( count($Data) > 0 ) {
			
			
			if($Data[0]['eStatus'] !="Deleted"){
				if($GCMID!=''){
					
					$iDriverId_driver=$Data[0]['iDriverId'];
					$where = " iDriverId = '$iDriverId_driver' ";
					
					$Data_update_driver['iGcmRegId']=$GCMID;
					$Data_update_driver['eDeviceType']=$DeviceType;
					$Data_update_driver['vFirebaseDeviceToken']=$vFirebaseDeviceToken;
					$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
					
				}
				echo json_encode(getDriverDetailInfo($Data[0]['iDriverId'],1));
				
				}else{
				echo "ACC_DELETED";
			}
		}
		else
		{
			$sql = "SELECT * FROM `register_driver` WHERE vEmail='$Driver_email'";
			$num_rows_Email = $obj->MySQLSelect($sql);
			if ( count($num_rows_Email) == 1 )
			{
				echo "LBL_PASSWORD_ERROR_TXT";
			}
			else
			{
				echo "LBL_NO_REG_FOUND";
			}
		}
		
	}
	
	###########################################################################
	
	###########################################################################
	if ($type == "getDetail_signIN_passenger") {
		
		$Emid = isset($_REQUEST["Email"]) ? $_REQUEST["Email"] : '';
		$Password_user = isset($_REQUEST["Pass"]) ? $_REQUEST["Pass"] : '';
		$cityName = isset($_REQUEST["cityName"]) ? $_REQUEST["cityName"] : '';
		$GCMID = isset($_REQUEST["GCMID"]) ? $_REQUEST["GCMID"] : '';
		$vFirebaseDeviceToken = isset($_REQUEST["vFirebaseDeviceToken"]) ? $_REQUEST["vFirebaseDeviceToken"] : '';
		
		$Password_passenger = $generalobj->encrypt($Password_user);
		
		$DeviceType = "Android";
		
		$sql = "SELECT iUserId,eStatus,vLang,vTripStatus FROM `register_user` WHERE vEmail='$Emid'  && vPassword='$Password_passenger'";
		$Data = $obj->MySQLSelect($sql);
		
		$iCabRequestId= get_value('cab_request_now', 'max(iCabRequestId)', 'iUserId',$Data[0]['iUserId'],'','true');
		$eStatus_cab= get_value('cab_request_now', 'eStatus', 'iCabRequestId',$iCabRequestId,'','true');
		if ( count($Data) > 0 ) {
			if($Data[0]['eStatus']=="Active"){
				
				
                $iUserId_passenger=$Data[0]['iUserId'];
                $where = " iUserId = '$iUserId_passenger' ";
				
				if($GCMID!=''){
					$Data_update_passenger['tSessionId'] = session_id().time();
					$Data_update_passenger['iGcmRegId']=$GCMID;
					$Data_update_passenger['eDeviceType']=$DeviceType;
					$Data_update_passenger['vFirebaseDeviceToken']=$vFirebaseDeviceToken;
					
					$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
				}
				
				if($eStatus_cab == "Requesting"){
					$where1 = " iCabRequestId = '$iCabRequestId' ";
					$Data_update_cab_now['eStatus']="Cancelled";
					
					$id = $obj->MySQLQueryPerform("cab_request_now",$Data_update_cab_now,'update',$where1);
				}
				
				$sql_checkLangCode = "SELECT  vCode FROM  language_master WHERE `eStatus` = 'Active' AND `eDefault` = 'Yes' ";
				$Data_checkLangCode = $obj->MySQLSelect($sql_checkLangCode);
				
				
				if($Data_checkLangCode[0]['vCode'] != $Data[0]['vLang']){
					$returnArr['changeLangCode'] ="Yes";
					$returnArr['UpdatedLanguageLabels'] = getLanguageLabelsArr($Data[0]['vLang'],"1");
					}else{
					$returnArr['changeLangCode'] ="No";
				}
				$returnArr['ProfileData'] = getPassengerDetailInfo($Data[0]['iUserId'],$cityName);
				echo json_encode($returnArr);
				}else{
				if($Data[0]['eStatus'] !="Deleted"){
					echo "LBL_CONTACT_US_STATUS_NOTACTIVE_PASSENGER";
					}else{
					echo "ACC_DELETED";
				}
			}
			
		}
		else
		{
			$sql = "SELECT * FROM `register_user` WHERE vEmail='$Emid'";
			$num_rows_Email = $obj->MySQLSelect($sql);
			if ( count($num_rows_Email) == 1 )
			{
				echo "LBL_PASSWORD_ERROR_TXT";
			}
			else
			{
				echo "LBL_NO_REG_FOUND";
			}
		}
		
	}
	###########################################################################
	
	
	###########################################################################
	
	###########################################################################
	if($type == "getFareConfigurations"){
		
		
		$configurations=array();
		$configurations["LBL_PAYMENT_ENABLED"] = $generalobj->getConfigurations("configurations","PAYMENT_ENABLED");
		$configurations["LBL_BASE_FARE"]= $generalobj->getConfigurations("configurations","BASE_FARE");
		$configurations["LBL_FARE_PER_MINUTE"]= $generalobj->getConfigurations("configurations","FARE_PER_MINUTE");
		$configurations["LBL_FARE_PAR_KM"]= $generalobj->getConfigurations("configurations","FARE_PAR_KM");
		$configurations["LBL_SERVICE_TAX"]= $generalobj->getConfigurations("configurations","SERVICE_TAX");
		
		echo json_encode($configurations);
	}
	###########################################################################
	
	
	//**********************Update Details************************************//
	###########################################################################
	
	if ($type == "updatePassengerGcmID") {
		$user_id_auto = isset($_REQUEST["UidAuto"]) ? $_REQUEST['UidAuto'] : '';
		$GcmID        = isset($_REQUEST["GcmId"]) ? $_REQUEST['GcmId'] : '';
		
		$where = " iUserId = '".$user_id_auto."'";
		$Data['iGcmRegId']=$GcmID;
		$id = $obj->MySQLQueryPerform("register_user",$Data,'update',$where);
		
		
		if ($id) {
			echo "Update Successful..";
			} else {
			echo "No Update.";
		}
		
	}
	###########################################################################
	
	###########################################################################
	if ($type == "updateDriverGcmID") {
		$user_id_auto = isset($_REQUEST["UidAuto"]) ? $_REQUEST['UidAuto'] : '';
		$GcmID        = isset($_REQUEST["GcmId"]) ? $_REQUEST['GcmId'] : '';
		
		$where = " iDriverId = '".$user_id_auto."'";
		$Data['iGcmRegId']=$GcmID;
		$id = $obj->MySQLQueryPerform("register_driver",$Data,'update',$where);
		
		
		if ($id) {
			echo "Update Successful..";
			} else {
			echo "No Update.";
		}
		
	}
	###########################################################################
	
	
	
	###########################################################################
	
	if ($type == "getTripIdFor_driver") {
		
		
		$driver_id = isset($_REQUEST["driver_id"]) ? $_REQUEST["driver_id"] : '';
		
		$sql = "SELECT iTripId FROM `register_driver` WHERE iDriverId = '$driver_id'";
		$Data = $obj->MySQLSelect($sql);
		
		if ( count($Data) == 1 )
		{
			$current_trip_id = $Data[0]['iTripId'];
		}
		echo $current_trip_id;
		
	}
	
	###########################################################################
	if ($type == "updateUserImage") {
		
		$user_id_auto = isset($_REQUEST["user_id"]) ? $_REQUEST["user_id"] : '';
		$UIpath       = isset($_REQUEST["Path"]) ? $_REQUEST["Path"] : '';
		
		$where = " iUserId = '$user_id_auto'";
		$Data_update_passenger['vImgName']=$UIpath;
		
		$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
		
		
		if ($id) {
			echo "Update Successful..";
			} else {
			
			echo "Failed.";
		}
		
	}
	###########################################################################
	
	if ($type == "updateDriverImage") {
		
		$user_id_auto = isset($_REQUEST["user_id"]) ? $_REQUEST["user_id"] : '';
		$UIpath       = isset($_REQUEST["Path"]) ? $_REQUEST["Path"] : '';
		
		$where = " iDriverId = '$user_id_auto'";
		$Data_update_driver['vImage']=$UIpath;
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		
		if ($id) {
			echo "Update Successful..";
			} else {
			
			echo "Failed.";
		}
		
	}
	
	###########################################################################
	
	if($type=="UpdateLastOnline_Driver"){
		
		$Did              = isset($_REQUEST["DriverAutoId"]) ? $_REQUEST["DriverAutoId"] : '';
		$availabilityStatus              = isset($_REQUEST["Status"]) ? $_REQUEST["Status"] : '';
		
		$where = " iDriverId='$Did'";
		
		$Data_update_driver['tLastOnline']=@date("Y-m-d H:i:s");
		$Data_update_driver['vAvailability']=$availabilityStatus;
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		
		if($id>0){
			echo "UpdateSuccessful";
			}else{
			echo "Failed";
		}
	}
	###########################################################################
	###########################################################################
	
	if ($type == "update_pass_passenger_Detail") {
		$user_id_auto = isset($_REQUEST["user_id"]) ? $_REQUEST["user_id"] : '';
		$Upass        = isset($_REQUEST["pass"]) ? $_REQUEST["pass"] : '';
		
		$Password_passenger = $generalobj->encrypt($Upass);
		$where = " iUserId = '$user_id_auto'";
		$Data_update_passenger['vPassword']=$Password_passenger;
		
		$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
		
		
		if ($id >0) {
			
			echo json_encode(getPassengerDetailInfo($user_id_auto,"none"));
			
			} else {
			
			echo "Failed.";
		}
		
	}
	
	###########################################################################
	
	if ($type == "update_pass_Detail_driver") {
		$user_id_auto = isset($_REQUEST["user_id"]) ? $_REQUEST["user_id"] : '';
		$Upass        = isset($_REQUEST["pass"]) ? $_REQUEST["pass"] : '';
		
		$Password_driver = $generalobj->encrypt($Upass);
		
		$where = " iDriverId = '$user_id_auto'";
		$Data_update_driver['vPassword']=$Password_driver;
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		
		
		if ($id >0) {
			echo json_encode(getDriverDetailInfo($user_id_auto));
			} else {
			echo "Failed.";
		}
		
	}
	
	###########################################################################
	
	if ($type == "update_payment_Detail_passenger") {
		
		$user_id_auto = isset($_REQUEST["user_id"]) ? $_REQUEST["user_id"] : '';
		$UcrdNO       = isset($_REQUEST["crd_no"]) ? $_REQUEST["crd_no"] : '';
		$UexMonth     = isset($_REQUEST["expMonth"]) ? $_REQUEST["expMonth"] : '';
		$UexYear      = isset($_REQUEST["expYear"]) ? $_REQUEST["expYear"] : '';
		$UCVV         = isset($_REQUEST["cvv_no"]) ? $_REQUEST['cvv_no'] : '';
		
		
		$where = " iUserId = '$user_id_auto'";
		$Data_update_passenger['vCreditCard']=$UcrdNO;
		$Data_update_passenger['vExpMonth']=$UexMonth;
		$Data_update_passenger['vExpYear']=$UexYear;
		$Data_update_passenger['vCvv']=$UCVV;
		
		$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
		
		
		if ($id) {
			echo "Update Successful..";
			} else {
			
			echo "No Update.";
		}
		
	}
	
	###########################################################################
	
	if ($type == "update_payment_Detail_driver") {
		
		$user_id_auto = isset($_REQUEST["user_id"]) ? $_REQUEST["user_id"] : '';
		$UcrdNO       = isset($_REQUEST["crd_no"]) ? $_REQUEST["crd_no"] : '';
		$UexMonth     = isset($_REQUEST["expMonth"]) ? $_REQUEST["expMonth"] : '';
		$UexYear      = isset($_REQUEST["expYear"]) ? $_REQUEST["expYear"] : '';
		$UCVV         = isset($_REQUEST["cvv_no"]) ? $_REQUEST['cvv_no'] : '';
		
		
		$where = " iDriverId = '$user_id_auto'";
		$Data_update_driver['vCreditCard']=$UcrdNO;
		$Data_update_driver['vExpMonth']=$UexMonth;
		$Data_update_driver['vExpYear']=$UexYear;
		$Data_update_driver['vCvv']=$UCVV;
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		
		
		if ($id) {
			echo "Update Successful..";
			} else {
			
			echo "No Update.";
		}
		
	}
	
	###########################################################################
	
	if ($type == "updateName_Mobile_Detail_passenger") {
		
		$Fname        = isset($_REQUEST["Fname"]) ? $_REQUEST["Fname"] : '';
		$Lname        = isset($_REQUEST["Lname"]) ? $_REQUEST["Lname"] : '';
		$Umobile      = isset($_REQUEST["mobile"]) ? $_REQUEST["mobile"] : '';
		$user_id_auto = isset($_REQUEST["user_id"]) ? $_REQUEST['user_id'] : '';
		$phoneCode = isset($_REQUEST["phoneCode"]) ? $_REQUEST['phoneCode'] : '';
		
		
		$where = " iUserId = '$user_id_auto'";
		$Data_update_passenger['vName']=$Fname;
		$Data_update_passenger['vLastName']=$Lname;
		$Data_update_passenger['vPhone']=$Umobile;
		$Data_update_passenger['vPhoneCode']=$phoneCode;
		
		$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
		
		if ($id >0) {
			echo json_encode(getPassengerDetailInfo($user_id_auto,"none"));
			} else {
			echo "Failed.";
		}
		
	}
	
	
	
	###########################################################################
	
	if ($type == "updateName_Mobile_Detail_driver") {
		
		$Fname        = isset($_REQUEST["Fname"]) ? $_REQUEST["Fname"] : '';
		$Lname        = isset($_REQUEST["Lname"]) ? $_REQUEST["Lname"] : '';
		$Umobile      = isset($_REQUEST["mobile"]) ? $_REQUEST["mobile"] : '';
		$user_id_auto = isset($_REQUEST["user_id"]) ? $_REQUEST['user_id'] : '';
		$phoneCode = isset($_REQUEST["phoneCode"]) ? $_REQUEST['phoneCode'] : '';
		
		
		$where = " iDriverId = '$user_id_auto'";
		$Data_update_driver['vName']=$Fname;
		$Data_update_driver['vLastName']=$Lname;
		$Data_update_driver['vPhone']=$Umobile;
		$Data_update_driver['vCode']=$phoneCode;
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		
		
		if ($id >0) {
			echo json_encode(getDriverDetailInfo($user_id_auto));
			} else {
			echo "Failed.";
		}
	}
	
	###########################################################################
	
	if ($type == "uploadImage_driver") {
		
		$target_path      = "webimages/upload/";
		$user_id          = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
		$base             = isset($_REQUEST['image']) ? $_REQUEST['image'] : '';
		$name             = isset($_REQUEST['cmd']) ? $_REQUEST['cmd'] : '';
		$target_path_temp = $target_path . "Driver/";
		$target_path      = $target_path_temp . $user_id . "/";
		
		if (is_dir($target_path) === false) {
			mkdir($target_path, 0755);
		}
		// base64 encoded utf-8 string
		$binary = base64_decode($base);
		
		header('Content-Type: bitmap; charset=utf-8');
		
		$time_val = time();
		$img_arr = explode(".", $name);
		$fileextension = $img_arr[count($img_arr) - 1];
		
		$Random_filename = mt_rand(11111, 99999);
		// $ImgFileName="3_".$name;
		$ImgFileName=$time_val . "_" . $Random_filename . "." . $fileextension;
		
		$file = fopen($target_path . '/' . $ImgFileName, "w");
		
		fwrite($file, $binary);
		fclose($file);
		
		$path = $target_path . $ImgFileName;
		
		
		if (file_exists($path)) {
			
			$where = " iDriverId = '".$user_id."'";
			$Data_Driver['vImage']=$ImgFileName;
			$id = $obj->MySQLQueryPerform("register_driver",$Data_Driver,'update',$where);
			
			if($id > 0){
				// echo "UPLOADSUCCESS";
				$thumb->createthumbnail($target_path . '/' . $ImgFileName); // generate image_file, set filename to resize/resample
				$thumb->size_auto($tconfig["tsite_upload_images_member_size1"]);    // set the biggest width or height for thumbnail
				$thumb->jpeg_quality(100);
				$thumb->save($target_path . "1" . "_" . $time_val . "_" . $Random_filename . "." . $fileextension);
				
				$thumb->createthumbnail($target_path . "/" . $ImgFileName);   // generate image_file, set filename to resize/resample
				$thumb->size_auto($tconfig["tsite_upload_images_member_size2"]);       // set the biggest width or height for thumbnail
				$thumb->jpeg_quality(100);      // [OPTIONAL] set quality for jpeg only (0 - 100) (worst - best), default = 75
				$thumb->save($target_path . "2" . "_" . $time_val . "_" . $Random_filename . "." . $fileextension);
				
				$thumb->createthumbnail($target_path . "/" . $ImgFileName);   // generate image_file, set filename to resize/resample
				$thumb->size_auto($tconfig["tsite_upload_images_member_size3"]);       // set the biggest width or height for thumbnail
				$thumb->jpeg_quality(100);      // [OPTIONAL] set quality for jpeg only (0 - 100) (worst - best), default = 75
				$thumb->save($target_path . "3" . "_" . $time_val . "_" . $Random_filename . "." . $fileextension);
				
				$returnArrayImg['Action']="SUCCESS";
				$returnArrayImg['ImgName']='3_'.$ImgFileName;
				echo json_encode($returnArrayImg);
				}else{
				echo "Failed";
			}
			
			} else {
			// handle the error
			
			echo "Failed";
		}
		
		exit;
		
	}
	
	###########################################################################
	
	if ($type == "uploadImage_passenger") {
		
		$target_path      = "webimages/upload/";
		$user_id          = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
		$base             = isset($_REQUEST['image']) ? $_REQUEST['image'] : '';
		$name             = isset($_REQUEST['cmd']) ? $_REQUEST['cmd'] : '';
		
		$target_path_temp = $target_path . "Passenger/";
		$target_path      = $target_path_temp . $user_id . "/";
		
		if (is_dir($target_path) === false) {
			mkdir($target_path, 0777);
		}
		// base64 encoded utf-8 string
		$binary = base64_decode($base);
		// binary, utf-8 bytes
		header('Content-Type: bitmap; charset=utf-8');
		
		$time_val = time();
		$img_arr = explode(".", $name);
		$fileextension = $img_arr[count($img_arr) - 1];
		
		$Random_filename = mt_rand(11111, 99999);
		// $ImgFileName="3_".$name;
		$ImgFileName=$time_val . "_" . $Random_filename . "." . $fileextension;
		
		$file = fopen($target_path . '/' . $ImgFileName, "w");
		
		fwrite($file, $binary);
		fclose($file);
		
		$path = $target_path . $ImgFileName;
		
		if (file_exists($path)) {
			
			$where = " iUserId = '".$user_id."'";
			$Data_passenger['vImgName']=$ImgFileName;
			$id = $obj->MySQLQueryPerform("register_user",$Data_passenger,'update',$where);
			
			if($id > 0){
				// echo "UPLOADSUCCESS";
				$thumb->createthumbnail($target_path . '/' . $ImgFileName); // generate image_file, set filename to resize/resample
				$thumb->size_auto($tconfig["tsite_upload_images_member_size1"]);    // set the biggest width or height for thumbnail
				$thumb->jpeg_quality(100);
				$thumb->save($target_path . "1" . "_" . $time_val . "_" . $Random_filename . "." . $fileextension);
				
				$thumb->createthumbnail($target_path . "/" . $ImgFileName);   // generate image_file, set filename to resize/resample
				$thumb->size_auto($tconfig["tsite_upload_images_member_size2"]);       // set the biggest width or height for thumbnail
				$thumb->jpeg_quality(100);      // [OPTIONAL] set quality for jpeg only (0 - 100) (worst - best), default = 75
				$thumb->save($target_path . "2" . "_" . $time_val . "_" . $Random_filename . "." . $fileextension);
				
				$thumb->createthumbnail($target_path . "/" . $ImgFileName);   // generate image_file, set filename to resize/resample
				$thumb->size_auto($tconfig["tsite_upload_images_member_size3"]);       // set the biggest width or height for thumbnail
				$thumb->jpeg_quality(100);      // [OPTIONAL] set quality for jpeg only (0 - 100) (worst - best), default = 75
				$thumb->save($target_path . "3" . "_" . $time_val . "_" . $Random_filename . "." . $fileextension);
				
				$returnArrayImg['Action']="SUCCESS";
				$returnArrayImg['ImgName']='3_'.$ImgFileName;
				echo json_encode($returnArrayImg);
				//exit;
				}else{
				echo "Failed";
			}
			} else {
			echo "Failed";
		}
		
	}
	
	
	
	###########################################################################
	
	
	###########################################################################
	
	if($type=="registerFbUser"){
		$fbid             = isset($_REQUEST["fbid"]) ? $_REQUEST["fbid"] : '';
		$Fname             = isset($_REQUEST["Fname"]) ? $_REQUEST["Fname"] : '';
		$Lname             = isset($_REQUEST["Lname"]) ? $_REQUEST["Lname"] : '';
		$email            = isset($_REQUEST["email"]) ? $_REQUEST["email"] : '';
		$GCMID            = isset($_REQUEST["GCMID"]) ? $_REQUEST["GCMID"] : '';
		$phone_mobile     = isset($_REQUEST["phone"]) ? $_REQUEST["phone"] : '';
		$CountryCode = isset($_REQUEST["CountryCode"]) ? $_REQUEST["CountryCode"] : '';
		$PhoneCode = isset($_REQUEST["PhoneCode"]) ? $_REQUEST["PhoneCode"] : '';
		$vFirebaseDeviceToken = isset($_REQUEST["vFirebaseDeviceToken"]) ? $_REQUEST["vFirebaseDeviceToken"] : '';
		
		// $Language_Code=($obj->MySQLSelect("SELECT `vCode` FROM `language_master` WHERE `eDefault`='Yes'")[0]['vCode']);
		$Language_Code= get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		
		$deviceType="Android";
		
		$sql = "SELECT * FROM `register_user` WHERE vEmail = '$email' OR vPhone = '$phone_mobile'";
		$check_passenger    = $obj->MySQLSelect($sql);
		
		if(count($check_passenger)>0){
			if($email==$check_passenger[0]['vEmail']){
				echo "EMAIL_EXIST";
				}else {
				echo "MOBILE_EXIST";
			}
			}else{
			
			$Data_passenger['vFbId']=$fbid;
			$Data_passenger['vName']=$Fname;
			$Data_passenger['vLastName']=$Lname;
			$Data_passenger['vEmail']=$email;
			$Data_passenger['vPhone']=$phone_mobile;
			$Data_passenger['vPassword']='';
			$Data_passenger['iGcmRegId']=$GCMID;
			$Data_passenger['vLang']=$Language_Code;
			$Data_passenger['vPhoneCode']=$PhoneCode;
			$Data_passenger['vCountry']=$CountryCode;
			$Data_passenger['vFirebaseDeviceToken']=$vFirebaseDeviceToken;
			$Data_passenger['eDeviceType']=$deviceType;
			// $Data_passenger['vCurrencyPassenger']=($obj->MySQLSelect("SELECT vName FROM currency WHERE eDefault='Yes'")[0]['vName']);
			$Data_passenger['vCurrencyPassenger']=get_value('currency', 'vName', 'eDefault','Yes','','true');
			
			$id = $obj->MySQLQueryPerform("register_user",$Data_passenger,'insert');
			
			if ($id > 0) {
				/*new added*/
				
				echo json_encode(getPassengerDetailInfo($id,$cityName));
				
				$maildata['EMAIL'] = $email;
				$maildata['NAME'] = $Fname;
				$maildata['PASSWORD'] = $password;
				$generalobj->send_email_user("MEMBER_REGISTRATION_USER",$maildata);
				} else {
				echo "Registration UnSuccessful.";
			}
		}
	}
	
	###########################################################################
	###########################################################################
	
	###########################################################################
	
	
	
	if($type=="setVehicleTypes"){
		// $startDate="2016-04-04 14:33:58";
		
		// echo date('dS M \a\t h:i a',strtotime($startDate));
		// $value= get_value('user_emergency_contact', 'COUNT(iEmergencyId) as Count', 'iUserId', "34");
		// echo $value[0]['Count'];
		// echo $res = preg_replace("/[^0-9]/", "", "Every 6.1,0--//+2 Months" );
		
		/* $tripID    = isset($_REQUEST["tripID"]) ? $_REQUEST["tripID"] : '';
			$rating  = isset($_REQUEST["rating"]) ? $_REQUEST["rating"] : '';
			
			$iUserId =get_value('trips', 'iUserId', 'iTripId',$tripID,'','true');
			$tableName = "register_user";
			$where = " WHERE iUserId='".$iUserId."'";
			
			$sql = "SELECT vAvgRating FROM ".$tableName.' '.$where;
            $fetchAvgRating= $obj->MySQLSelect($sql);
			
			
			
            $fetchAvgRating[0]['vAvgRating'] = floatval($fetchAvgRating[0]['vAvgRating']);
			// echo  "Fetch:".$fetchAvgRating[0]['vAvgRating'];exit;
			
			if($fetchAvgRating[0]['vAvgRating'] > 0){
			$average_rating = round(($fetchAvgRating[0]['vAvgRating'] + $rating) / 2,1);
			}else{
			$average_rating = round($fetchAvgRating[0]['vAvgRating'] + $rating,1);
			}
			
            $Data_update['vAvgRating']=$average_rating;
			
		echo "AvgRate:".$Data_update['vAvgRating']; */
		
		$langCodesArr = get_value('language_master', 'vCode', '', '');
		
		print_r($langCodesArr);
		
		echo "<BR/>";
		
		for($i=0;$i<count($langCodesArr);$i++){
			$currLngCode = $langCodesArr[$i]['vCode'];
			$vVehicleType = $langCodesArr[$i]['vVehicleType'];
			$fieldName = "vVehicleType_".$currLngCode;
			$suffixName = $i==0?"vVehicleType":"vVehicleType_".$langCodesArr[$i-1]['vCode'];
			
			
			$sql = "ALTER TABLE vehicle_type ADD ".$fieldName." VARCHAR(50) AFTER"." ".$suffixName;
			$id= $obj->sql_query($sql);
		}
		
		
		$vehicleTypesArr = get_value('vehicle_type', 'vVehicleType,iVehicleTypeId', '', '');
		
		for($j=0;$j<count($vehicleTypesArr);$j++){
			$vVehicleType = $vehicleTypesArr[$j]['vVehicleType'];
			$iVehicleTypeId = $vehicleTypesArr[$j]['iVehicleTypeId'];
			
			echo "vVehicleType:".$vVehicleType."<BR/>";
			for($k=0;$k<count($langCodesArr);$k++){
				$currLngCode = $langCodesArr[$k]['vCode'];
				$fieldName = "vVehicleType_".$currLngCode;
				$suffixName = $k==0?"vVehicleType":"vVehicleType_".$langCodesArr[$k-1]['vCode'];
				
				
				// $sql = "ALTER TABLE vehicle_type ADD ".$fieldName." VARCHAR(50) AFTER"." ".$suffixName;
				// $id= $obj->sql_query($sql);
				echo $sql = "UPDATE `vehicle_type` SET ".$fieldName." = '".$vVehicleType."' WHERE iVehicleTypeId = '$iVehicleTypeId'";
				echo "<br/>";
				$id1= $obj->sql_query($sql);
				
				echo "<br/>".$id1;
			}
			
		}
		
		// echo $sql = "UPDATE `vehicle_type` SET ".$fieldName." = ".$vVehicleType;
		// $id1= $obj->sql_query($sql);
		// echo "<br/>".$id;
		
	}
	###########################################################################
	
	if ($type == "callToDriver_Message") {
		
		$driver_id_auto = isset($_REQUEST["DautoId"])?$_REQUEST["DautoId"]:'';
		$user_id_auto   =isset($_REQUEST["UautoId"])?$_REQUEST["UautoId"]:'';
		$message_rec    = isset($_REQUEST["message_rec"])?$_REQUEST["message_rec"]:'';
		$message        = isset($_REQUEST["message"])?$_REQUEST["message"]:'';
		$tripID   = isset($_REQUEST["tripID"])?$_REQUEST["tripID"]:'';
		
		$sender_type = "Passenger";
		
		$where = " iUserId = '$user_id_auto'";
		
		$Data_update_Messages['tMessage']=$message;
		$Data_update_Messages['tSendertype']=$sender_type;
		$Data_update_Messages['iTripId']=$tripID;
		
		$id = $obj->MySQLQueryPerform("driver_user_messages",$Data_update_Messages,'insert');
		
		$message_new_combine = $message_rec . $message;
		
		$DArray = explode(',', $driver_id_auto);
		
		foreach ($DArray as $key => $val) {
			
			$sql = "SELECT iGcmRegId FROM register_driver WHERE iDriverId='$val'  AND eDeviceType = 'Android'";
			$result = $obj->MySQLSelect($sql);
			
			$rows[] = $result[0];
			
		}
		
		
		foreach ($rows as $item) {
			
			$registatoin_ids = $item['iGcmRegId'];
			
			
			$Rregistatoin_ids = array(
            $registatoin_ids
			);
			
			$Rmessage = array(
            "message" => $message_new_combine
			);
			$result   = send_notification($Rregistatoin_ids, $Rmessage);
			
			echo $result;
		}
		
	}
	
	###########################################################################
	
	if ($type == "callToUser_Message") {
		
		$driver_id_auto = isset($_REQUEST["DautoId"])?$_REQUEST["DautoId"]:'';
		$user_id_auto   = isset($_REQUEST["UautoId"])?$_REQUEST["UautoId"]:'';
		$message_rec    = isset($_REQUEST["message_rec"])?$_REQUEST["message_rec"]:'';
		$message        = isset($_REQUEST["message"])?$_REQUEST["message"]:'';
		$tripID   = isset($_REQUEST["tripID"])?$_REQUEST["tripID"]:'';
		
		$sender_type = "Driver";
		
		$Data_update_Messages['tMessage']=$message;
		$Data_update_Messages['tSendertype']=$sender_type;
		$Data_update_Messages['iTripId']=$tripID;
		
		$id = $obj->MySQLQueryPerform("driver_user_messages",$Data_update_Messages,'insert');
		
		$message_new_combine = $message_rec . $message;
		
		$sql = "SELECT iGcmRegId FROM register_user WHERE iUserId='$user_id_auto'  AND eDeviceType = 'Android'";
		$result = $obj->MySQLSelect($sql);
		
		$registatoin_ids = $result[0]['iGcmRegId'];
		
		$Rregistatoin_ids = array(
        $registatoin_ids
		);
		$Rmessage         = array(
        "message" => $message_new_combine
		);
		$result           = send_notification($Rregistatoin_ids, $Rmessage);
		
		echo $result;
		
		
	}
	
	###########################################################################
	
	if ($type == "submit_rating_user") {
		
		$usr_email = isset($_REQUEST["usr_email"]) ? $_REQUEST["usr_email"] : '';
		$driver_id = isset($_REQUEST["driver_id"]) ? $_REQUEST["driver_id"] : '';
		$tripID    = isset($_REQUEST["tripID"]) ? $_REQUEST["tripID"] : '';
		$rating_1  = isset($_REQUEST["rating_1"]) ? $_REQUEST["rating_1"] : '';
		
		$message   = isset($_REQUEST["message"]) ? $_REQUEST['message'] : '';
		$tripVerificationCode   = isset($_REQUEST["verification_code"]) ? $_REQUEST['verification_code'] : '';
		
		$average_rating = $rating_1;
		
		$sql = "SELECT iVerificationCode FROM `trips`  WHERE  iTripId='$tripID'";
		$row_code = $obj->MySQLSelect($sql);
		
		$verificationCode=$row_code[0]['iVerificationCode'];
		
		// if($tripVerificationCode==$verificationCode){
		
		$VerificationStatus="Verified";
		
		
		$where = " iTripId = '$tripID'";
		
		$Data_update_trips['eVerified']=$VerificationStatus;
		
		$id = $obj->MySQLQueryPerform("trips",$Data_update_trips,'update',$where);
		
		
		$sql = "SELECT vAvgRating FROM `register_user` WHERE iUserId='$usr_email'";
		$row = $obj->MySQLSelect($sql);
		
		
		$average_rating = ($row[0]['vAvgRating'] + $average_rating) / 2;
		
		$usrType   = "Driver";
		
		$sql = "SELECT * FROM `ratings_user_driver` WHERE iTripId = '$tripID' && eUserType = '$usrType'";
		$row = $obj->MySQLSelect($sql);
		
		
		if (count($row) > 0) {
			echo "LBL_RATING_EXIST";
			
			} else {
			
			$where = " iUserId = '$usr_email'";
			
			$Data_update_passenger['vAvgRating']=round($average_rating,1);
			
			$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
			
			
			$Data_update_ratings['iTripId']=$tripID;
			$Data_update_ratings['vRating1']=$rating_1;
			$Data_update_ratings['vMessage']=$message;
			$Data_update_ratings['eUserType']=$usrType;
			
			$id = $obj->MySQLQueryPerform("ratings_user_driver",$Data_update_ratings,'insert');
			
			
			if ($id >0) {
				echo "Ratings Successful.";
				} else {
				
				echo "Ratings UnSuccessful.";
			}
			sendTripReceiptAdmin($tripID);
		}
		
		
	}
	
	###########################################################################
	
	if($type=="submit_rating_driver"){
		
		$usr_email = isset($_REQUEST["usr_email"]) ? $_REQUEST["usr_email"] : '';
		$driver_id = isset($_REQUEST["driver_id"]) ? $_REQUEST["driver_id"] : '';
		$tripID    = isset($_REQUEST["tripID"]) ? $_REQUEST["tripID"] : '';
		$rating_1  = isset($_REQUEST["rating_1"]) ? $_REQUEST["rating_1"] : '';
		$message   = isset($_REQUEST["message"]) ? $_REQUEST['message'] : '';
		$tripVerificationCode   = isset($_REQUEST["verification_code"]) ? $_REQUEST['verification_code'] : '';
		//$average_rating=($rating_1+$rating_2+$rating_3+$rating_4)/4 ;
		
		$average_rating = $rating_1;
		
		$usrType   = "Passenger";
		
		$sql = "SELECT * FROM `ratings_user_driver` WHERE iTripId = '$tripID' and eUserType = '$usrType'";
		$row_check = $obj->MySQLSelect($sql);
		
		$sql = "SELECT vAvgRating FROM `register_driver` WHERE iDriverId = '$driver_id'";
		$row = $obj->MySQLSelect($sql);
		
		$average_rating = ($row[0]['vAvgRating'] + $average_rating) / 2;
		
		
		if (count($row_check) > 0) {
			
			echo "LBL_RATING_EXIST";
			
			} else {
			
			$where = " iDriverId = '$driver_id'";
			
			$Data_update_driver['vAvgRating']=round($average_rating,1);
			
			$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
			
			$Data_update_ratings['iTripId']=$tripID;
			$Data_update_ratings['vRating1']=$rating_1;
			$Data_update_ratings['vMessage']=$message;
			$Data_update_ratings['eUserType']=$usrType;
			
			$id = $obj->MySQLQueryPerform("ratings_user_driver",$Data_update_ratings,'insert');
			
			
			if ($id) {
				echo "Ratings Successful.";
				} else {
				
				echo "Ratings UnSuccessful.";
			}
			
			sendTripReceipt($tripID);
			
		}
		
	}
	
	###########################################################################
	
	if ($type == "updateLog") {
		$Uid          = isset($_REQUEST["access_sign_token_user_id_auto"]) ? $_REQUEST["access_sign_token_user_id_auto"] : '';
		
		$where = " iUserId='$Uid'";
		$Data_update_passenger['vLogoutDev']="false";
		
		$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
		
		if ($id) {
			echo "Update Successful";
		}
		
	}
	
	
	
	###########################################################################
	
	if($type=='getCarTypes'){
		$sql="SELECT * FROM vehicle_type";
		
		$row_result_vehivle_type = $obj->MySQLSelect($sql);
		
		$arr_temp['Types']=$row_result_vehivle_type;
		echo  json_encode($arr_temp);
	}
	
	
	###########################################################################
	
	###########################################################################
	
	if($type=='CheckVerificationCode'){
		$tripId          = isset($_REQUEST["TripId"]) ? $_REQUEST["TripId"] : '';
		
		$sql="SELECT eVerified FROM trips WHERE iTripId=$tripId";
		
		$result_eVerified = $obj->MySQLSelect($sql);
		
		if($result_eVerified[0]['eVerified']=="Verified"){
            echo "Verified";
			}else{
			echo "Not Verified";
		}
		
	}
	###########################################################################
	###########################################################################
	
	if($type=='AddPaypalPaymentData'){
		$tripId          = isset($_REQUEST["TripId"]) ? $_REQUEST["TripId"] : '';
		$PayPalPaymentId          = isset($_REQUEST["PayPalPaymentId"]) ? $_REQUEST["PayPalPaymentId"] : '';
		$PaidAmount          = isset($_REQUEST["PaidAmount"]) ? $_REQUEST["PaidAmount"] : '';
		
		$Data_payments['tPaymentUserID']=$PayPalPaymentId;
		$Data_payments['vPaymentUserStatus']="approved";
		$Data_payments['iTripId']=$tripId;
		$Data_payments['iAmountUser']=$PaidAmount;
		
		$id = $obj->MySQLQueryPerform("payments",$Data_payments,'insert');
		if($id>0){
			echo "PaymentSuccessful";
			}else{
			echo "PaymentUnSuccessful";
		}
	}
	
	####################### To get Currency Values ##############################
	
	if($type =="getCurrencyList"){
		// $returnArr['List']=($obj->MySQLSelect("SELECT * FROM currency WHERE eStatus='Active'"));
		$returnArr['List']=get_value('currency', '*', 'eStatus', 'Active');
		echo json_encode($returnArr);
	}
	
	####################### To get Currency Values END############################
	
	
	####################### Update Currency Values ##############################
	
	if($type =="updateCurrencyValue"){
		$Uid          = isset($_REQUEST["UserID"]) ? $_REQUEST["UserID"] : '';
		$currencyCode = isset($_REQUEST["vCurrencyCode"]) ? $_REQUEST["vCurrencyCode"] : '';
		$UserType     = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : '';
		
		if($UserType == "Driver"){
			$where = " iDriverId = '$Uid'";
			$Data_update_user['vCurrencyDriver']=$currencyCode;
			$id = $obj->MySQLQueryPerform("register_driver",$Data_update_user,'update',$where);
			}else{
			$where = " iUserId = '$Uid'";
			$Data_update_user['vCurrencyPassenger']=$currencyCode;
			$id = $obj->MySQLQueryPerform("register_user",$Data_update_user,'update',$where);
		}
		
		
		if($id){
			echo "SUCCESS";
			}else{
			echo "UpdateFailed";
		}
	}
	
	####################### To get Currency Values END############################
	
	
	if($type=="enc_pass"){
		$pass 		= isset($_REQUEST['pass'])?clean($_REQUEST['pass']):'';
		
		echo $generalobj->encrypt($pass);
	}
    
    if($type=="DeclineTripRequest"){
  		$passenger_id = isset($_REQUEST["PassengerID"]) ? $_REQUEST["PassengerID"] : '';
		$driver_id      = isset($_REQUEST["DriverID"]) ? $_REQUEST["DriverID"] : '';
		$vMsgCode      = isset($_REQUEST["vMsgCode"]) ? $_REQUEST["vMsgCode"] : '';
		
		$request_count = UpdateDriverRequest2($driver_id,$passenger_id,"0","Decline");
		
		echo $request_count;
	}
	
	###########################################################################
	###########################################################################
	###########################################################################
	###########################################################################
	
	
	if($type =="getOngoingUserTrips"){
		global $generalobj,$obj;
		$iUserId = isset($_REQUEST["iUserId"]) ? $_REQUEST["iUserId"] : '';
		
		$vLangCode=get_value('register_user', 'vLang', 'iUserId',$iUserId,'','true');
		if($vLangCode == "" || $vLangCode == NULL){
			$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		
		$Data1 = array();
		if($iUserId != "") {
			$sql1 = "SELECT rd.iDriverId,rd.vImage as driverImage,concat(rd.vName,' ',rd.vLastName) as driverName, rd.vCode ,rd.vPhone as driverMobile ,rd.vLatitude as driverLatitude,rd.vLongitude as driverLongitude,rd.vTripStatus as driverStatus, rd.vAvgRating as driverRating, tr.`vRideNo`, tr.tSaddress,tr.iTripId, tr.iVehicleTypeId,tr.tTripRequestDate,tr.eFareType,tr.vTimeZone from trips as tr 
			LEFT JOIN register_driver as rd ON rd.iDriverId=tr.iDriverId
			WHERE tr.iActive != 'Canceled' AND iActive != 'Finished' AND iUserId='".$iUserId."' ORDER BY tr.iTripId DESC";
			
			$Data1 = $obj->MySQLSelect($sql1);
			if(count($Data1) > 0){
				for($i=0;$i<count($Data1);$i++){
					$iVehicleCategoryId=get_value('vehicle_type', 'iVehicleCategoryId', 'iVehicleTypeId',$Data1[$i]['iVehicleTypeId'],'','true');
					$vVehicleTypeName=get_value('vehicle_type', 'vVehicleType_'.$vLangCode, 'iVehicleTypeId',$Data1[$i]['iVehicleTypeId'],'','true');
					if($iVehicleCategoryId != 0){
						$vVehicleCategoryName = get_value('vehicle_category', 'vCategory_'.$vLangCode, 'iVehicleCategoryId',$iVehicleCategoryId,'','true');
						$vVehicleTypeName = $vVehicleCategoryName . "-" . $vVehicleTypeName;
					}
					$Data1[$i]['SelectedTypeName'] = $vVehicleTypeName;
					// Convert Into Timezone
					$tripTimeZone = $Data1[$i]['vTimeZone'];
					if($tripTimeZone != ""){
						$serverTimeZone = date_default_timezone_get();
						$Data1[$i]['tTripRequestDate'] = converToTz($Data1[$i]['tTripRequestDate'],$tripTimeZone,$serverTimeZone);
					}
					// Convert Into Timezone
					$Data1[$i]['dDateOrig'] = $Data1[$i]['tTripRequestDate'];
				}
				$returnArr['Action'] = "1";
				$returnArr['SERVER_TIME'] = date('Y-m-d H:i:s');
				$returnArr['message'] = $Data1;
				}else {
				$returnArr['Action'] = "0"; 
				$returnArr['message'] = "LBL_NO_ONGOING_TRIPS_AVAIL";
			}
			}else {
			$returnArr['Action'] = "0"; 
			$returnArr['message'] = "LBL_NO_ONGOING_TRIPS_AVAIL";
		}
		echo json_encode($returnArr);
	}
	
	if($type =="getTripDeliveryLocations"){
		global $generalobj,$obj;
		$iTripId = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '';
		$userType = isset($_REQUEST["userType"]) ? $_REQUEST["userType"] : 'Passenger';
		$Data = array();
		if($iTripId != "") {
			if($userType != 'Passenger') {
				$sql = "SELECT ru.iUserId,ru.vimgname as riderImage,concat(ru.vName,' ',ru.vLastName) as riderName, ru.vPhoneCode ,ru.vPhone as riderMobile,ru.vTripStatus as driverStatus, ru.vAvgRating as riderRating, tr.* from trips as tr 
				LEFT JOIN register_user as ru ON ru.iUserId=tr.iUserId
				WHERE tr.iTripId = '".$iTripId."'";
				}else {
				$sql = "SELECT rd.iDriverId,rd.vImage as driverImage,concat(rd.vName,' ',rd.vLastName) as driverName, rd.vCode ,rd.vPhone as driverMobile,rd.vTripStatus as driverStatus, rd.vAvgRating as driverRating, tr.* from trips as tr 
				LEFT JOIN register_driver as rd ON rd.iDriverId=tr.iDriverId
				WHERE tr.iTripId = '".$iTripId."'";
			}
			$dataUser = $obj->MySQLSelect($sql);
			$Data['driverDetails'] =$dataUser[0];
			$testBool = 1;
			
			if(count($dataUser) > 0){
				$Data['States'] = array();
				$Data_tTripRequestDate= $dataUser[0]['tTripRequestDate'];
				$Data_tDriverArrivedDate= $dataUser[0]['tDriverArrivedDate'];
				$Data_dDeliveredDate= $dataUser[0]['dDeliveredDate'];
				$Data_tStartDate= $dataUser[0]['tStartDate'];
				$Data_tEndDate= $dataUser[0]['tEndDate'];
				$i=0;
				
				if($Data_tTripRequestDate != "" && $Data_tTripRequestDate != "0000-00-00 00:00:00" && $testBool == 1){
					$msg = 'Provider accepted the request.';
					if($userType != 'Passenger'){
						$msg = 'You accepted the request.';
					}
					$Data['States'][$i]['text'] = $msg;
					$Data['States'][$i]['time'] = "at ".date("h:i A",strtotime($Data_tTripRequestDate)); 
					$Data['States'][$i]['timediff'] = @round(abs(strtotime($Data_tTripRequestDate) - strtotime(date("Y-m-d H:i:s"))) / 60,0)." mins ago"; 
					$Data['States'][$i]['type'] = "Accept";
					$i++;
					}else {
					$testBool = 0;
				}
				
				if($Data_tDriverArrivedDate != "" && $Data_tDriverArrivedDate != "0000-00-00 00:00:00" && $testBool == 1){
					$msg = "Provider arrived to your location.";
					if($userType != 'Passenger'){
						$msg = "You arrived to user's location.";
					}
					$Data['States'][$i]['text'] = $msg;
					$Data['States'][$i]['time'] = "at ".date("h:i A",strtotime($Data_tDriverArrivedDate)); 
					$Data['States'][$i]['timediff'] = @round(abs(strtotime($Data_tDriverArrivedDate) - strtotime(date("Y-m-d H:i:s"))) / 60,0)." mins ago"; 
					$Data['States'][$i]['type'] = "Arrived"; 
					$i++;
					}else {
					$testBool = 0;
				}
				
				if($Data_tStartDate != "" && $Data_tStartDate != "0000-00-00 00:00:00" && $testBool == 1){
					$msg = 'Provider has started the job.';
					if($userType != 'Passenger'){
						$msg = 'You started the job.';
					}
					$Data['States'][$i]['text'] = $msg; 
					$Data['States'][$i]['time'] = "at ".date("h:i A",strtotime($Data_tStartDate)); 
					$Data['States'][$i]['timediff'] = @round(abs(strtotime($Data_tStartDate) - strtotime(date("Y-m-d H:i:s"))) / 60,0)." mins ago"; 
					$Data['States'][$i]['type'] = "Onway";
					$i++;
					}else {
					$testBool = 0;
				}
				
				if($Data_tEndDate != "" && $Data_tEndDate != "0000-00-00 00:00:00" && $testBool == 1 && $dataUser[0]['iActive'] == "Finished"){
					$msg = 'Provider has completed the job.';
					if($userType != 'Passenger'){
						$msg = 'You completed the job.';
					}
					$Data['States'][$i]['text'] = $msg; 
					$Data['States'][$i]['time'] = "at ".date("h:i A",strtotime($Data_tEndDate)); 
					$Data['States'][$i]['timediff'] = @round(abs(strtotime($Data_tEndDate) - strtotime(date("Y-m-d H:i:s"))) / 60,0)." mins ago"; 
					$Data['States'][$i]['type'] = "Delivered"; 
					$i++;
				}
				}else{
				$Data['States'] = array();
			}
			if(count($Data) > 0){
				$returnArr['Action'] = "1";
				$returnArr['message'] = $Data;
				}else {
				$returnArr['Action'] = "0"; 
				$returnArr['message'] = "LBL_NO_DRIVER_FOUND";
			}
			}else {
			$returnArr['Action'] = "0"; 
			$returnArr['message'] = "LBL_NO_TRIP_FOUND";
		}
		echo json_encode($returnArr);
	}
	
	if($type =="SetTimeForTrips"){
		global $generalobj,$obj;
		$iTripId = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '';
		$iTripTimeId = isset($_REQUEST["iTripTimeId"]) ? $_REQUEST["iTripTimeId"] : '';
		$dTime = date('Y-m-d H:i:s');
		
		if($iTripTimeId == '') {
			$Data_update['dResumeTime']=$dTime;
			$Data_update['iTripId']=$iTripId;
			$id = $obj->MySQLQueryPerform("trip_times",$Data_update,'insert');
			$returnArr['Action'] = "1";
			$returnArr['message'] = $id;
			}else {
			$where = " iTripTimeId = '$iTripTimeId'";
			$Data_update['dPauseTime']=$dTime;
			$Data_update['iTripId']=$iTripId;
			$id = $obj->MySQLQueryPerform("trip_times",$Data_update,'update',$where);
			$returnArr['Action'] = "1";
			$returnArr['message'] = $id;
		}
		$sql22 = "SELECT * FROM `trip_times` WHERE iTripId='$iTripId'";
		$db_tripTimes = $obj->MySQLSelect($sql22);
		
		$totalSec = 0;
		$timeState = 'Pause';
		$iTripTimeId = '';
		foreach($db_tripTimes as $dtT){
			if($dtT['dPauseTime'] != '' && $dtT['dPauseTime'] != '0000-00-00 00:00:00') {
				$totalSec += strtotime($dtT['dPauseTime']) - strtotime($dtT['dResumeTime']);
				}else {
				$totalSec += strtotime(date('Y-m-d H:i:s')) - strtotime($dtT['dResumeTime']);
			}
		}
		$returnArr['totalTime'] = $totalSec;
		echo json_encode($returnArr); exit;
	}
	
	if($type == "getYearTotalEarnings"){
		global $generalobj,$obj;
		
		$iDriverId = isset($_REQUEST['iDriverId'])?clean($_REQUEST['iDriverId']):'';
		$year  = isset($_REQUEST["year"]) ? $_REQUEST["year"] : @date('Y');
		if($year == ""){
			$year = @date('Y'); 
		}
		
		$vCurrencyDriver=get_value('register_driver', 'vCurrencyDriver', 'iDriverId', $iDriverId,'','true');
		$vCurrencySymbol=get_value('currency', 'vSymbol', 'vName', $vCurrencyDriver,'','true');
		
		$start = @date('Y');
		$end = '1970';
		$year_arr = array();
		for($j = $start; $j >= $end; $j--) {
  			$year_arr[] = strval($j);
		}
		
		$Month_Array = array('01' => 'Jan', '02' =>'Feb', '03' =>'Mar', '04' =>'Apr', '05' =>'May', '06' =>'Jun', '07' =>'Jul', '08' =>'Aug', '09' =>'Sep', '10' =>'Oct', '11' =>'Nov', '12' =>'Dec');
		
		$sql = "SELECT * FROM trips WHERE iDriverId='".$iDriverId."' AND tTripRequestDate LIKE '".$year."%'";
		$tripData = $obj->MySQLSelect($sql);
		$totalEarnings = 0;
		
		//if(count($tripData) > 0){
		for($i=0;$i<count($tripData);$i++){
			$iFare = $tripData[$i]['fTripGenerateFare'];
			$fCommision = $tripData[$i]['fCommision'];
			$priceRatio = $tripData[$i]['fRatio_'.$vCurrencyDriver];
			$totalEarnings += ($iFare - $fCommision) * $priceRatio;
		}
		
		$yearmontharr = array();
		$yearmontearningharr_Max = array();
		foreach ($Month_Array as $key=>$value) {
			$tripyearmonthdate = $year."-".$key;
			$sql_Month = "SELECT * FROM trips WHERE iDriverId='".$iDriverId."' AND tTripRequestDate LIKE '".$tripyearmonthdate."%'";
			$tripyearmonthData = $obj->MySQLSelect($sql_Month);
			$tripData_M = strval(count($tripyearmonthData));
			$yearmontearningharr = array();
			$totalEarnings_M = 0;
			for($j=0;$j<count($tripyearmonthData);$j++){
				$iFare_M = $tripData[$j]['fTripGenerateFare'];
				$fCommision_M = $tripData[$j]['fCommision'];
				$priceRatio_M = $tripData[$j]['fRatio_'.$vCurrencyDriver];
				$totalEarnings_M += ($iFare_M - $fCommision_M) * $priceRatio_M;  
			}
			$yearmontearningharr_Max[] = $totalEarnings_M;
			$yearmontearningharr["CurrentMonth"] = $value;
			$yearmontearningharr["TotalEarnings"] = strval(round($totalEarnings_M < 0 ? 0 : $totalEarnings_M,1));
			$yearmontearningharr["TripCount"] = strval(round($tripData_M,1));
			array_push($yearmontharr,$yearmontearningharr);
		}
		foreach ($yearmontearningharr_Max as $key => $value) {
			if ($value >= $max) 
            $max = $value;     
		}
		$returnArr['Action'] = "1";
		$returnArr['TotalEarning'] = $vCurrencySymbol." ".strval(round($totalEarnings,1));
		$returnArr['TripCount'] = strval(count($tripData));
		$returnArr["CurrentYear"] = $year;
		$returnArr['MaxEarning'] = strval($max);
		$returnArr['YearMonthArr'] = $yearmontharr;
		$returnArr['YearArr'] = $year_arr;
		/*}else{
			$returnArr['Action'] = "0";
		} */
		
		echo json_encode($returnArr);
	}     
	/* For Forgot Password */
	if($type == 'requestResetPassword'){ 
		global $generalobj,$obj,$tconfig;
		$Emid = isset($_REQUEST["vEmail"]) ? $_REQUEST["vEmail"] : '';
		$userType = isset($_REQUEST["UserType"])?clean($_REQUEST["UserType"]):''; // UserType = Driver/Passenger
		if($userType == "" || $userType == NULL){
			$userType = "Passenger";    
		}
		if($userType == "Passenger"){
			$tblname = "register_user";
			$fields = 'iUserId as iMemberId, vPhone,vPhoneCode as vPhoneCode, vEmail, vName, vLastName, vPassword, vLang';
			$condfield = 'iUserId';
			$EncMembertype= base64_encode(base64_encode('rider'));
			}else{
			$tblname = "register_driver";
			$fields = 'iDriverId  as iMemberId, vPhone,vCode as vPhoneCode, vEmail, vName, vLastName,	vPassword, vLang';
			$condfield = 'iDriverId';
			$EncMembertype= base64_encode(base64_encode('driver'));
		}
		$sql="select $fields from $tblname where vEmail = '".$Emid."'";
		$db_member = $obj->MySQLSelect($sql);
		if(count($db_member) > 0){
			$vLangCode = $db_member[0]['vLang'];
			if($vLangCode == "" || $vLangCode == NULL){
		    	$vLangCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
			}
			$languageLabelsArr= getLanguageLabelsArr($vLangCode,"1");
			$clickherelabel = $languageLabelsArr['LBL_CLICKHERE_SIGNUP'];
			
			$milliseconds = time();
			$tempGenrateCode = substr($milliseconds, 1);
			$Today=Date('Y-m-d H:i:s');
			$today= base64_encode(base64_encode($Today));
			$type= $EncMembertype;
			$id= $generalobj->encrypt($db_member[0]["iMemberId"]);
			$newToken = $generalobj->RandomString(32);
			$url = $tconfig["tsite_url"].'reset_password.php?type='.$type.'&id='.$id.'&_token='.$newToken;
			$activation_text = '<a href="'.$url.'" target="_blank"> '.$clickherelabel.' </a>';
			$maildata['EMAIL'] = $db_member[0]["vEmail"];
			$maildata['NAME'] = $db_member[0]["vName"]." ".$db_member[0]["vLastName"];				
			$maildata['LINK'] = $activation_text; 
			$status = $generalobj->send_email_user("CUSTOMER_RESET_PASSWORD",$maildata);
			if($status == 1){
				$sql = "UPDATE $tblname set vPassword_token='".$newToken."' WHERE vEmail='".$Emid."' and eStatus != 'Deleted'";  
				$obj->sql_query($sql);
				
				$returnArr['Action'] = "1";
				$returnArr['message'] = "LBL_PASSWORD_SENT_TXT";
				}else{
				$returnArr['Action'] = "0";
				$returnArr['message'] = "LBL_ERROR_PASSWORD_MAIL";
			}
			}else{
			$returnArr['Action'] = "0";
  			$returnArr['message'] = "LBL_WRONG_EMAIL_PASSWORD_TXT";
		}  
		echo json_encode($returnArr);exit;  
	}
	/* For Forgot Password */
	
  	###########################################################################
	/* For WayBill */
	if($type == "displayWayBill"){
		$driverId = isset($_REQUEST['iDriverId'])?clean($_REQUEST['iDriverId']):'';
		$userType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):'Driver';
		$driver_detail = get_value('register_driver', 'vName,vLastName,vCurrencyDriver,vLang', 'iDriverId', $driverId);
		$sql = "SELECT * from trips WHERE iDriverId = '".$driverId."' ORDER BY iTripId DESC LIMIT 0,1";
		$tripData = $obj->MySQLSelect($sql);
		if(count($tripData) > 0){
			$passenger_detail = get_value('register_user', 'vName,vLastName,eHail', 'iUserId', $tripData[0]['iUserId']);
			if($passenger_detail[0]['eHail'] == "Yes"){
				$passengername = "--";
				}else{
				$passengername = $passenger_detail[0]['vName']." ".$passenger_detail[0]['vLastName'];
			}
			## get fare details ##
			$vLang =$driver_detail[0]['vLang'];
			if($vLang == "" || $vLang == NULL){
				$vLang = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
			} 
			$languageLabelsArr= getLanguageLabelsArr($vLang,"1");
			$vehicleTypes = get_value('vehicle_type', '*', 'iVehicleTypeId', $tripData[0]['iVehicleTypeId']);
			$priceRatio=get_value('currency', 'Ratio', 'vName', $driver_detail[0]['vCurrencyDriver'],'','true');
			$vCurrencySymbol=get_value('currency', 'vSymbol', 'vName', $driver_detail[0]['vCurrencyDriver'],'','true');
			$eFareType = $vehicleTypes[0]['eFareType']; 
			$fPricePerKM= round($vehicleTypes[0]['fPricePerKM'] * $priceRatio,2);
			$fPricePerMin= round($vehicleTypes[0]['fPricePerMin'] * $priceRatio,2);
			$iBaseFare= round($vehicleTypes[0]['iBaseFare'] * $priceRatio,2);
			$fCommision= round($vehicleTypes[0]['fCommision'] * $priceRatio,2);
			$iMinFare= round($vehicleTypes[0]['iMinFare'] * $priceRatio,2);
			$fFixedFare= round($vehicleTypes[0]['fFixedFare'] * $priceRatio,2);
			$fPricePerHour= round($vehicleTypes[0]['fPricePerHour'] * $priceRatio,2);
			if($eFareType == "Regular"){
				$Rate = $vCurrencySymbol." ".$iBaseFare." ".$languageLabelsArr['LBL_BASE_FARE_SMALL_TXT']."+".$vCurrencySymbol." ".$fPricePerMin." ".$languageLabelsArr['LBL_PRICE_PER_MINUTE_SMALL_TXT']."+".$vCurrencySymbol." ".$fPricePerKM." ".$languageLabelsArr['LBL_PRICE_PER_KM_SMALL_TXT'];
			}
			if($eFareType == "Fixed"){
				$Rate = $vCurrencySymbol." ".$fFixedFare." ".$languageLabelsArr['LBL_FIXED_FARE_TXT_ADMIN'];
			}
			if($eFareType == "Hourly"){
				$Rate = $vCurrencySymbol." ".$fPricePerHour." ".$languageLabelsArr['LBL_PER_HOUR_SMALL_TXT'];
			}
			## get fare details ##
			$tripArr['DriverName'] = $driver_detail[0]['vName']." ".$driver_detail[0]['vLastName'];
			$tripArr['vRideNo'] = $tripData[0]['vRideNo'];
			$tripArr['tTripRequestDate'] = $tripData[0]['tTripRequestDate']; 
			$tripArr['ProjectName'] = $generalobj->getConfigurations("configurations", "SITE_NAME");
			$tripArr['tSaddress'] = $tripData[0]['tSaddress'];
			$tripArr['tDaddress'] = $tripData[0]['tDaddress'];
			$tripArr['PassengerName'] = $passengername;
			$tripArr['Licence_Plate'] = get_value('driver_vehicle', 'vLicencePlate', 'iDriverVehicleId', $tripData[0]['iDriverVehicleId'],'','true');
			$tripArr['PassengerCapacity'] = get_value('vehicle_type', 'iPersonSize', 'iVehicleTypeId', $tripData[0]['iVehicleTypeId'],'','true');
			$tripArr['Rate'] = $Rate;
			
			$returnArr['Action'] = "1"; 
			$returnArr['message'] = $tripArr;
			}else{
			$returnArr['Action'] = "0"; 
			$returnArr['message'] = "LBL_NO_TRIP_FOUND";
		}
		echo json_encode($returnArr);exit;  
	}
	/* For WayBill */
	###########################################################################
  	###########################################################################
	/* For Driver Vehicle Details */
	if($type=="getDriverVehicleDetails"){
		$driverId = isset($_REQUEST['iDriverId'])?clean($_REQUEST['iDriverId']):'';
		$userType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):'Driver';
		$distance = isset($_REQUEST["distance"]) ? $_REQUEST["distance"] : '';
		$time = isset($_REQUEST["time"]) ? $_REQUEST["time"] : '';
		$StartLatitude    = isset($_REQUEST["StartLatitude"]) ? $_REQUEST["StartLatitude"] : '0.0';
		$EndLongitude    =isset($_REQUEST["EndLongitude"]) ? $_REQUEST["EndLongitude"] : '0.0';
		$time = round(($time / 60),2);
		$distance = round(($distance / 1000),2);
		
		$vLang = get_value('register_driver', 'vLang', 'iDriverId', $driverId,'','true');
		if($vLang == "" || $vLang == NULL){
			$vLang = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
		$iDriverVehicleId = get_value('register_driver', 'iDriverVehicleId', 'iDriverId', $driverId,'','true');
		if($iDriverVehicleId > 0){
			$Fare_Data = array();
			$vCarType = get_value('driver_vehicle', 'vCarType', 'iDriverVehicleId', $iDriverVehicleId,'','true');
			
			
			$sql11 = "SELECT vVehicleType_".$vLang." as vVehicleTypeName, iVehicleTypeId, vLogo, iPersonSize FROM `vehicle_type`  WHERE  iVehicleTypeId IN (".$vCarType.") AND eType='Ride'";
			
			// $sql11 = "SELECT iVehicleTypeId, vLogo FROM vehicle_type WHERE iVehicleTypeId IN (".$vCarType.")";
			$vCarType_Arr = $obj->MySQLSelect($sql11);
			
			// $vCarType_Arr = explode(",",$vCarType);
			for($i=0;$i<count($vCarType_Arr);$i++){    
				// $iVehicleTypeId = $vCarType_Arr[$i];    
				// $vVehicleTypeName=get_value('vehicle_type', 'vVehicleType_'.$vLang, 'iVehicleTypeId',$iVehicleTypeId,'','true');
				// $vLogo=get_value('vehicle_type', 'vLogo', 'iVehicleTypeId',$iVehicleTypeId,'','true');
				// $vehicleTypes = get_value('vehicle_type', 'iVehicleTypeId,', 'iVehicleTypeId', $vCarType_Arr[$i]['iVehicleTypeId']);
				
				$Fare_Single_Vehicle_Data=calculateFareEstimateAll($time,$distance,$vCarType_Arr[$i]['iVehicleTypeId'],$driverId,1,"","","",1,0,0,0,"DisplySingleVehicleFare","Driver",1);
				$Fare_Data[$i]['iVehicleTypeId'] = $vCarType_Arr[$i]['iVehicleTypeId'];
				$Fare_Data[$i]['vVehicleTypeName'] = $vCarType_Arr[$i]['vVehicleTypeName'];
				$Fare_Data[$i]['vLogo'] = $vCarType_Arr[$i]['vLogo'];
				$Fare_Data[$i]['iPersonSize'] = $vCarType_Arr[$i]['iPersonSize'];
				$lastvalue = end($Fare_Single_Vehicle_Data);
				$lastvalue1 = array_shift($lastvalue);
				$Fare_Data[$i]['SubTotal'] = $lastvalue1;                      
				$Fare_Data[$i]['VehicleFareDetail'] = $Fare_Single_Vehicle_Data;  
				//array_push($Fare_Data, $Fare_Single_Vehicle_Data);
				
			} 
			$returnArr['Action'] = "1"; 
			$returnArr['message'] = $Fare_Data; 
			}else{
			$returnArr['Action'] = "0"; 
			$returnArr['message'] = "LBL_NO_VEHICLE_SELECTED";
		}
		echo json_encode($returnArr);exit;
	}
	/* For Driver Vehicle Details */
	###########################################################################
	
	if($type == "updateuserPref"){
		$iMemberId = isset($_REQUEST['iMemberId'])?clean($_REQUEST['iMemberId']):'';
		$userType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):'Driver';
		$eFemaleOnly = isset($_REQUEST['eFemaleOnly'])?clean($_REQUEST['eFemaleOnly']):'No';
		
		$where = " iDriverId = '$iMemberId'";
		$Data_update_User['eFemaleOnlyReqAccept']=$eFemaleOnly;
		
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_User,'update',$where);
		
		if ($id >0) {
			$returnArr['Action']="1";
			$returnArr['message'] = getDriverDetailInfo($iMemberId);
			} else {
			$returnArr['Action']="0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
	}
  	###########################################################################
	###########################################################################
	
	if($type == "updateUserGender"){
		$iMemberId = isset($_REQUEST['iMemberId'])?clean($_REQUEST['iMemberId']):'';
		$userType = isset($_REQUEST['UserType'])?clean($_REQUEST['UserType']):'Driver';
		$eGender = isset($_REQUEST['eGender'])?clean($_REQUEST['eGender']):'';
		
		if($userType == "Driver"){
			$where = " iDriverId = '$iMemberId'";
			$Data_update_User['eGender']=$eGender;
			
			$id = $obj->MySQLQueryPerform("register_driver",$Data_update_User,'update',$where);
			}else{
			$where = " iUserId = '$iMemberId'";
			$Data_update_User['eGender']=$eGender;
			
			$id = $obj->MySQLQueryPerform("register_user",$Data_update_User,'update',$where);
		}
		
		
		if ($id >0) {
			$returnArr['Action']="1";
			if($userType != "Driver"){
				$returnArr['message'] = getPassengerDetailInfo($iMemberId,"");
				}else{
				$returnArr['message'] = getDriverDetailInfo($iMemberId);
			}
			} else {
			$returnArr['Action']="0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
	}
  	###########################################################################
	/* For Generate Hail Trip */
	if($type=="StartHailTrip"){
		$driverId = isset($_REQUEST['iDriverId'])?clean($_REQUEST['iDriverId']):'';
		$selectedCarTypeID    = isset($_REQUEST["SelectedCarTypeID"]) ? $_REQUEST["SelectedCarTypeID"] : '';
		$PickUpLatitude    = isset($_REQUEST["PickUpLatitude"]) ? $_REQUEST["PickUpLatitude"] : '0.0';
		$PickUpLongitude    =isset($_REQUEST["PickUpLongitude"]) ? $_REQUEST["PickUpLongitude"] : '0.0';
		$PickUpAddress = isset($_REQUEST["PickUpAddress"]) ? $_REQUEST["PickUpAddress"] : '';
		$DestLatitude    = isset($_REQUEST["DestLatitude"]) ? $_REQUEST["DestLatitude"] : '';
		$DestLongitude    =isset($_REQUEST["DestLongitude"]) ? $_REQUEST["DestLongitude"] : '';
		$DestAddress    =isset($_REQUEST["DestAddress"]) ? $_REQUEST["DestAddress"] : '';
		$DriverMessage= "CabRequestAccepted";
		
		## Check For PichUp/DropOff Location DisAllow ##
  		$address_data = fetch_address_geocode($PickUpAddress);
  		$cityId = get_value('city', 'iCityId', 'LOWER(vCity)', strtolower($address_data['city']),'',true);
  		$country = get_value('country', 'iCountryId,vCountryCode', 'LOWER(vCountry)', strtolower($address_data['country']));
  		$countryId = $country[0]['iCountryId'];
  		$vCountryCode = $country[0]['vCountryCode'];
  		$stateId = get_value('state', 'iStateId', 'LOWER(vState)', strtolower($address_data['state']),'',true);
  		
  		if($countryId == '') {
  			$country = get_value('country', 'iCountryId,vCountryCode', 'LOWER(vCountryCode)', strtolower($address_data['country_code']));
  			$countryId = $country[0]['iCountryId'];
  			$vCountryCode = $country[0]['vCountryCode'];
		}
  		
  		$address_data['cityId'] = $cityId;
  		$address_data['countryId'] = $countryId;
  		$address_data['stateId'] = $stateId;
  		$address_data['PickUpAddress'] = $PickUpAddress;
  		$address_data['DropOffAddress'] = $DestAddress;
  		$DataArr = getOnlineDriverArr($PickUpLatitude,$PickUpLongitude,$address_data,"Yes");
  		if($DataArr['PickUpDisAllowed'] == "No" && $DataArr['DropOffDisAllowed'] == "No"){
			$returnArr['Action'] = "0";
			$returnArr['message'] = "LBL_PICK_DROP_LOCATION_NOT_ALLOW";
			echo json_encode($returnArr);
			exit;
		}
  		if($DataArr['PickUpDisAllowed'] == "Yes" && $DataArr['DropOffDisAllowed'] == "No"){
			$returnArr['Action'] = "0";
			$returnArr['message'] = "LBL_DROP_LOCATION_NOT_ALLOW";
			echo json_encode($returnArr);
			exit;
		}
  		if($DataArr['PickUpDisAllowed'] == "No" && $DataArr['DropOffDisAllowed'] == "Yes"){
			$returnArr['Action'] = "0";
			$returnArr['message'] = "LBL_PICKUP_LOCATION_NOT_ALLOW";
			echo json_encode($returnArr);
			exit;
		}
		## Check For PichUp/DropOff Location DisAllow Ends##
		
		
		$sql = "SELECT * FROM `register_user` WHERE eHail = 'Yes' ORDER BY iUserId DESC";
		$hailpassenger = $obj->MySQLSelect($sql);
		
		if(count($hailpassenger)>0){
			$iUserId = $hailpassenger[0]['iUserId'];  
			## Update Trip Status ##
			
			$where = " iUserId='".$iUserId."'";
			$Data_passenger['iTripId']= "0";
			$Data_passenger['vTripStatus']="NONE";
			$Data_passenger['vCallFromDriver']="";
			
			$sql = "UPDATE register_user set iTripId='0', vTripStatus = 'NONE', vCallFromDriver = '' WHERE iUserId='".$iUserId."'";
			$id=$obj->sql_query($sql);
			
			// $id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
			// echo "hello";exit;
			## Update Trip Status ## 
			$iTripID = GenerateHailTrip($iUserId,$driverId,$selectedCarTypeID,$PickUpLatitude,$PickUpLongitude,$PickUpAddress,$DestLatitude,$DestLongitude,$DestAddress);
			}else{
			$Data["vName"] = "Hail";
			$Data["vLastName"] = "Passenger";
			$Data["vEmail"] = "hailrider@demo.com";
			$Data["tDestinationLatitude"] = $DestLatitude;
			$Data["tDestinationLongitude"] = $DestLongitude;
			$Data["tDestinationAddress"] = $DestAddress;
			$Data["vLang"] = get_value('language_master', 'vCode', 'eDefault', 'Yes', '', 'true');
			$Data["eStatus"] = "Active";
			$Data["vCurrencyPassenger"] = get_value('currency', 'vName', 'eDefault', 'Yes', '', 'true');
			$Data["tRegistrationDate"] = @date("Y-m-d H:i:s");
			$Data["eEmailVerified"] = "Yes";
			$Data["ePhoneVerified"] = "Yes";
			$Data['eDeviceType'] = "Ios";
			$Data['eType'] = "Ride";
			$Data['vCountry'] = $vCountryCode;
			$Data['tSessionId'] = session_id();
			$random = substr( md5(rand()), 0, 7);
			$Data['tDeviceSessionId'] = session_id().time().$random;
			$Data['eHail'] = "Yes";
			$id = $obj->MySQLQueryPerform("register_user", $Data, 'insert');
			if($id > 0){
				$iTripID = GenerateHailTrip($id,$driverId,$selectedCarTypeID,$PickUpLatitude,$PickUpLongitude,$PickUpAddress,$DestLatitude,$DestLongitude,$DestAddress);
				$iUserId = $id;
			}
		}
		#### Update Driver Request Status of Trip ####
		UpdateDriverRequest($driverId,$iUserId,$iTripID,"Accept");
		#### Update Driver Request Status of Trip ####
		$trip_status = "On Going Trip";
		$where = " iUserId = '$iUserId'";
		/*$Data_update_passenger['iTripId']=$iTripID;
		$Data_update_passenger['vTripStatus']=$trip_status;*/
		$Data_update_passenger['iTripId']=0;
		$Data_update_passenger['vTripStatus']="NONE";
		$Data_update_passenger['vCallFromDriver']="";
		$id = $obj->MySQLQueryPerform("register_user",$Data_update_passenger,'update',$where);
		
		$where = " iDriverId = '$driverId'";
		$Data_update_driver['iTripId']=$iTripID;
		$Data_update_driver['vTripStatus']=$trip_status;
		$Data_update_driver['vAvailability']="Not Available";
		$id = $obj->MySQLQueryPerform("register_driver",$Data_update_driver,'update',$where);
		$sql = "SELECT iDriverVehicleId,vCurrencyDriver,iAppVersion,vName,vLastName FROM `register_driver` WHERE iDriverId = '$driverId'";
		$Data_vehicle = $obj->MySQLSelect($sql);
		$message_arr = array();
		$message_arr['iDriverId'] = $driverId;
		$message_arr['Message'] = $DriverMessage;
		$message_arr['iTripId'] = strval($iTripID);
		$message_arr['DriverAppVersion'] = strval($Data_vehicle[0]['iAppVersion']);
		$message_arr['iTripVerificationCode'] = get_value('trips', 'iVerificationCode', 'iTripId', $iTripID, '', 'true');
		$message = json_encode($message_arr);
		if ($iTripID > 0) {
			$returnArr['Action'] = "1";
			$data['iTripId'] = $iTripID;
			$data['tEndLat'] = $DestLatitude;
			$data['tEndLong'] = $DestLongitude;
			$data['tDaddress'] = $DestAddress;
			$data['PAppVersion'] = get_value('register_user', 'iAppVersion', 'iUserId', $iUserId, '', 'true');
			$data['eFareType'] = get_value('trips', 'eFareType', 'iTripId', $iTripID, '', 'true');
			$returnArr['APP_TYPE'] = $generalobj->getConfigurations("configurations","APP_TYPE");
			$returnArr['message']=$data;
			echo json_encode($returnArr); exit;
			} else {
			$data['Action'] = "0";
			$data['message']="LBL_TRY_AGAIN_LATER_TXT";
			echo json_encode($data);
			exit;
		}
	}
	/* For Generate Hail Trip */
	###########################################################################
	/* For Sending Trip Message and Notification  */
	if($type=="SendTripMessageNotification"){
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$iFromMemberId = isset($_REQUEST["iFromMemberId"]) ? $_REQUEST["iFromMemberId"] : '';
		$iToMemberId = isset($_REQUEST['iToMemberId'])?clean($_REQUEST['iToMemberId']):'';
		$iTripId = isset($_REQUEST['iTripId'])?clean($_REQUEST['iTripId']):'';
		$tMessage = isset($_REQUEST['tMessage'])?stripslashes($_REQUEST['tMessage']):'';
		
		$Data['iTripId'] = $iTripId;
		$Data['iFromMemberId'] = $iFromMemberId;
		$Data['iToMemberId'] = $iToMemberId;
		$Data['tMessage'] = $tMessage;
		$Data['dAddedDate'] = @date("Y-m-d H:i:s");
		$Data['eStatus'] = "Unread";
		$Data['eUserType'] = $UserType;
		$id = $obj->MySQLQueryPerform('trip_messages',$Data,'insert');
		if($id > 0){
			$returnArr['Action'] ="1";
			// $message = sendTripMessagePushNotification($iFromMemberId,$UserType,$iToMemberId,$iTripId,$tMessage);
			// if($message == 1){
			// $returnArr['Action'] ="1";
			// }else{
			// $returnArr['Action'] ="0";
			// $returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
			// }
			sendTripMessagePushNotification($iFromMemberId,$UserType,$iToMemberId,$iTripId,$tMessage);
			echo json_encode($returnArr);
			exit; 
			}else{
			$returnArr['Action'] ="0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
			echo json_encode($returnArr);
			exit;
		}
	}
	/* For Sending Trip Message and Notification  */
	###########################################################################
	###########################################################################
	/* For Update values of Language Labels */
	if($type=="UpdateLanguageLabelsValue"){
		//echo "Try Later";exit;
		$UserType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$vLangLabel = isset($_REQUEST['vLangLabel'])?$_REQUEST['vLangLabel']:'';
		$vLangLabel = urldecode(stripslashes($vLangLabel));
		//$vLangLabel = '{"LBL_NO_REFERRAL_CODES":"No Referral Code Found"}';
		$vCode = isset($_REQUEST['vCode'])?clean($_REQUEST['vCode']):'';
		$vLangLabelArr = json_decode($vLangLabel,TRUE);   //echo "<pre>";print_r($vLangLabelArr);exit;
		if(count($vLangLabelArr) > 0){  
			foreach($vLangLabelArr as $key => $val){
				$vLabel = $key;
				$vValue = $val;
				$sql = "SELECT LanguageLabelId FROM `language_label` where vLabel = '".$vLabel."' AND vCode = '".$vCode."'";
				$db_language_label = $obj->MySQLSelect($sql);
				$count = count($db_language_label);
				if($count > 0){
					$where = " LanguageLabelId = '".$db_language_label[0]['LanguageLabelId']."'";
					$data_label_update['vValue']=$vValue;
					$obj->MySQLQueryPerform("language_label",$data_label_update,'update',$where);
					//UpdateOtherLanguage($vLabel,$vValue,$vCode,'language_label');
					}else{
					$sql = "SELECT LanguageLabelId FROM `language_label_other` where vLabel = '".$vLabel."' AND vCode = '".$vCode."'";
					$db_language_label_other = $obj->MySQLSelect($sql);
					$countOther = count($db_language_label_other);
					if($countOther > 0){
						$where = " LanguageLabelId = '".$db_language_label_other[0]['LanguageLabelId']."'";
						$data_label_update_other['vValue']=$vValue;
						$obj->MySQLQueryPerform("language_label_other",$data_label_update_other,'update',$where);
						//UpdateOtherLanguage($vLabel,$vValue,$vCode,'language_label_other');
					}
				}
			} 
			$returnArr['Action'] = "1";
			$returnArr['UpdatedLanguageLabels'] = getLanguageLabelsArr($vCode,"1");
			$returnArr['message'] ="LBL_UPDATE_MSG_TXT";
			echo json_encode($returnArr);
			exit;
			}else{
			$returnArr['Action'] = "0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";
			echo json_encode($returnArr);
			exit;
		}  
	}
	/* For Update values of Language Labels */
	#############################################################################
	#############################################################################
	#############################################################################
    if($type == "pushNotification"){
		
		//echo $pass= $generalobj->decrypt("XcIZDZwoXA==");exit;
        $deviceToken = $_REQUEST['Token'];
        //5240381e085cf439d5bda4f322440fc0b9cd750315b91c725cfdc12996545eb1
		
        // Put your private key's passphrase here:
        $passphrase = '123456';
		
        // Put your alert message here:
        $message['key'] = 'push notification!';
		
        $message_json = json_encode($message);
        ////////////////////////////////////////////////////////////////////////////////
		
        $ctx = stream_context_create();
		//        stream_context_set_option($ctx, 'ssl', 'local_cert', 'apn-dev-uberapp.pem');'driver_apns_dev.pem'
        stream_context_set_option($ctx, 'ssl', 'local_cert', $_REQUEST['pemName']);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		
        // Open a connection to the APNS server
        $fp = stream_socket_client(
		'ssl://gateway.push.apple.com:2195', $err,
		$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		echo "<BR/>fp:".$fp."<BR/>";
		
        if (!$fp)
		exit("Failed to connect: $err $errstr" . PHP_EOL);
		
        echo 'Connected to APNS' . PHP_EOL;
        // $msg = "{\"iDriverId\":\"20\"}";
        // Create the payload body
        $body['aps'] = array(
		'alert' => $_REQUEST['message'],
		'content-available' => 1,
		'body'  => $_REQUEST['message'],
		'sound' => 'default'
		
		);
		
        // Encode the payload as JSON
        $payload = json_encode($body);
		
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
		
        if (!$result)
		echo 'Message not delivered' . PHP_EOL;
        else
		echo 'Message successfully delivered' . PHP_EOL;
		
        // Close the connection to the server
        fclose($fp);
	}
	
	###########################################################################
	###########################################################################
	
    if($type == "pushNotificationGCM"){
		
		$deviceToken = $_REQUEST['Token'];
        $registation_ids_new = array();
		
		array_push($registation_ids_new, $deviceToken);
		
		$Rmessage = array("message" => $_REQUEST['message']);
		
		$result = send_notification($registation_ids_new, $Rmessage,0);
        echo "<pre>";print_r($result);exit;
		
	}
	
	if($type == "checkTripstatus"){
		$iTripId = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '';
		$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$userType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$vLatitude = isset($_REQUEST["vLatitude"]) ? $_REQUEST["vLatitude"] : '';
		$vLongitude = isset($_REQUEST["vLongitude"]) ? $_REQUEST["vLongitude"] : '';
		$isSubsToCabReq = isset($_REQUEST["isSubsToCabReq"]) ? $_REQUEST["isSubsToCabReq"] : '';
		$APP_TYPE = $generalobj->getConfigurations("configurations", "APP_TYPE");
		$vTimeZone = isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		
		if($iMemberId != ""){
			if (!empty($isSubsToCabReq) && $isSubsToCabReq == 'true') {
				$driver_update['tLastOnline'] = date('Y-m-d H:i:s');
				$driver_update['tOnline'] = date('Y-m-d H:i:s');
			}
			
			if (!empty($vLatitude) && !empty($vLongitude)) {
				$driver_update['vLatitude'] = $vLatitude;
				$driver_update['vLongitude'] = $vLongitude;
				$user_update['vLatitude'] = $vLatitude;
				$user_update['vLongitude'] = $vLongitude;
			}
			
			if ($isSubsToCabReq == 'true' || !empty($vLatitude) || !empty($vLongitude)) {
				if($userType == "Driver"){
					$where = " iDriverId = '".$iMemberId."'";
					$Update_driver = $obj->MySQLQueryPerform("register_driver", $driver_update, "update", $where);
					}else {
					$where = " iUserId = '".$iMemberId."'";
					$Update_driver = $obj->MySQLQueryPerform("register_user", $user_update, "update", $where);
				}
			}
		}
		# Update User Location Date #
		Updateuserlocationdatetime($iMemberId,$userType,$vTimeZone);    
		# Update User Location Date #
		
		if($userType == "Passenger"){
			$condfield = 'iUserId';
			if($iTripId != ""){
				$sql = "SELECT t.*, CONCAT(rd.vName,' ',rd.vLastName) AS driverName, rd.vTripStatus, rd.iDriverId, rd.iAppVersion FROM trips AS t LEFT JOIN register_driver AS rd ON rd.iDriverId=t.iDriverId WHERE t.iTripId='".$iTripId."'";
				$msg = $obj->MySQLSelect($sql);
				
				if(!empty($msg)){
					if($msg[0]['iActive'] == 'Active'){
						$DriverMessage= "CabRequestAccepted";
						
						$message_arr = array();
						$message_arr['iDriverId'] = $msg[0]['iDriverId'];
						$message_arr['Message'] = $DriverMessage;
						$message_arr['iTripId'] = strval($msg[0]['iTripId']);
						$message_arr['DriverAppVersion'] = strval($msg[0]['iAppVersion']);
						$message_arr['iTripVerificationCode'] = $msg[0]['iVerificationCode'];
						
						$returnArr['Action'] = "1";
						$returnArr['message'] = $message_arr;
						}else if($msg[0]['iActive'] == 'Canceled' && $msg[0]['eCancelledBy'] == 'Driver'){
						$message  = "TripCancelledByDriver";
						$message_arr = array();
						$message_arr['Message'] = $message;
						$message_arr['Reason'] = $msg[0]['vCancelReason'];
						$message_arr['isTripStarted'] = "false";
						$message_arr['iUserId'] = $msg[0]['iUserId'];
						$message_arr['driverName'] = $msg[0]['driverName'];
						$message_arr['vRideNo'] = $msg[0]['vRideNo'];
						
						$returnArr['Action'] = "1";
						$returnArr['message'] = $message_arr;
						}else if($msg[0]['vTripStatus'] == 'Arrived'){
						$message_arr = array();
						$message_arr['MsgType'] = "DriverArrived";
						$message_arr['iDriverId'] = $msg[0]['iDriverId'];
						$message_arr['driverName'] = $msg[0]['driverName'];
						$message_arr['vRideNo'] = $msg[0]['vRideNo'];
						
						$returnArr['Action'] = "1";
						$returnArr['message'] = $message_arr;
						
						}else if($msg[0]['iActive'] == 'On Going Trip'){
						$message      = "TripStarted";
						$message_arr = array();
						$message_arr['Message'] = $message;
						$message_arr['iDriverId'] = $msg[0]['iDriverId'];
						$message_arr['driverName'] = $msg[0]['driverName'];
						$message_arr['vRideNo'] = $msg[0]['vRideNo'];
						if($msg[0]['eType'] == "Deliver"){
							$message_arr['VerificationCode'] = $msg[0]['vDeliveryConfirmCode'];
							}else{
							$message_arr['VerificationCode'] = "";
						}
						
						$returnArr['Action'] = "1";
						$returnArr['message'] = $message_arr;
						}else if($msg[0]['iActive'] == 'Finished'){
						$message_arr = array();
						if($msg[0]['eCancelled'] == "true"){
							$message  = "TripCancelledByDriver";
							$message_arr['Reason'] = $msg[0]['vCancelReason'];
							$message_arr['isTripStarted'] = "true";
							}else{
							$message      = "TripEnd";
						}
						$message_arr['Message'] = $message;
						$message_arr['iDriverId'] = $msg[0]['iDriverId'];
						$message_arr['driverName'] = $msg[0]['driverName'];
						$message_arr['vRideNo'] = $msg[0]['vRideNo'];
						
						$returnArr['Action'] = "1";
						$returnArr['message'] = $message_arr;
					}
					}else {
					$returnArr['Action'] = "0";
					$returnArr['message'] = "LBL_NO_TRIP_FOUND";
				}
				}else {
				$sql = "SELECT t.*, CONCAT(rd.vName,' ',rd.vLastName) AS driverName, rd.vTripStatus, rd.iDriverId, rd.iAppVersion FROM trips AS t LEFT JOIN register_driver AS rd ON rd.iDriverId=t.iDriverId WHERE t.iUserId='".$iMemberId."' ORDER BY t.iTripId DESC limit 1";
				$msg = $obj->MySQLSelect($sql);
				
				if(!empty($msg)){
					
					// Cab Accepted MEssage
					$DriverMessage= "CabRequestAccepted";
					
					$message_arr1 = array();
					$message_arr1['iDriverId'] = $msg[0]['iDriverId'];
					$message_arr1['Message'] = $DriverMessage;
					$message_arr1['iTripId'] = strval($msg[0]['iTripId']);
					$message_arr1['DriverAppVersion'] = strval($msg[0]['iAppVersion']);
					$message_arr1['iTripVerificationCode'] = $msg[0]['iVerificationCode'];
					$returnArr['message']['Accepted'] = $message_arr1;
					
					// Trip Cancelled Message
					$message  = "TripCancelledByDriver";
					$message_arr2 = array();
					$message_arr2['Message'] = $message;
					$message_arr2['Reason'] = $msg[0]['vCancelReason'];
					$message_arr2['isTripStarted'] = "false";
					$message_arr2['iUserId'] = $msg[0]['iUserId'];
					$message_arr2['driverName'] = $msg[0]['driverName'];
					$message_arr2['vRideNo'] = $msg[0]['vRideNo'];
					$returnArr['message']['Cancel'] = $message_arr2;
					
					// Driver Arrived Message
					$message_arr3 = array();
					$message_arr3['MsgType'] = "DriverArrived";
					$message_arr3['iDriverId'] = $msg[0]['iDriverId'];
					$message_arr3['driverName'] = $msg[0]['driverName'];
					$message_arr3['vRideNo'] = $msg[0]['vRideNo'];
					$returnArr['message']['Arrived'] = $message_arr3;
					
					// Trip Started Message
					$message      = "TripStarted";
					$message_arr4 = array();
					$message_arr4['Message'] = $message;
					$message_arr4['iDriverId'] = $msg[0]['iDriverId'];
					$message_arr4['driverName'] = $msg[0]['driverName'];
					$message_arr4['vRideNo'] = $msg[0]['vRideNo'];
					if($msg[0]['eType'] == "Deliver"){
						$message_arr4['VerificationCode'] = $msg[0]['vDeliveryConfirmCode'];
						}else{
						$message_arr4['VerificationCode'] = "";
					}
					$returnArr['message']['Started'] = $message_arr4;
					
					// Trip Finished Message
					$message_arr = array();
					if($msg[0]['eCancelled'] == "true"){
						$message  = "TripCancelledByDriver";
						$message_arr5['Reason'] = $msg[0]['vCancelReason'];
						$message_arr5['isTripStarted'] = "true";
						}else{
						$message      = "TripEnd";
					}
					$message_arr5['Message'] = $message;
					$message_arr5['iDriverId'] = $msg[0]['iDriverId'];
					$message_arr5['driverName'] = $msg[0]['driverName'];
					$message_arr5['vRideNo'] = $msg[0]['vRideNo'];
					$returnArr['message']['TripEnd'] = $message_arr5;
					
					$returnArr['Action'] = "1";
					}else {
					$returnArr['Action'] = "0";
					$returnArr['message'] = "LBL_NO_TRIP_FOUND";
				}
			}
			}else{
			if($iTripId != ""){
				$sql = "SELECT t.iTripId, t.iUserId, t.vRideNo, CONCAT(rd.vName,' ',rd.vLastName) AS driverName FROM trips AS t LEFT JOIN register_driver AS rd ON rd.iDriverId=t.iDriverId WHERE t.iTripId='".$iTripId."' AND t.iActive='Canceled' AND t.eCancelledBy='Passenger'";
				$msg = $obj->MySQLSelect($sql);
				
				if(!empty($msg)){
					$message  = "TripCancelled";
					$message_arr = array();
					$message_arr['Message'] = $message;
					$message_arr['iUserId'] = $msg[0]['iUserId'];
					$message_arr['driverName'] = $msg[0]['driverName'];
					$message_arr['vRideNo'] = $msg[0]['vRideNo'];
					
					$returnArr['Action'] = "1";
					$returnArr['message'] = $message_arr;
					}else {
					$returnArr['Action'] = "0";
					$returnArr['message'] = "LBL_NO_TRIP_FOUND";
				}
				}else {
				$sql = "SELECT tMessage as msg FROM passenger_requests WHERE iDriverId='".$iMemberId."' ORDER BY iRequestId DESC LIMIT 1 ";
				$msg = $obj->MySQLSelect($sql);
				
				
				if(!empty($msg)){
					$returnArr['Action'] = "1";
					$returnArr['message'] = $msg;
					}else {
					$returnArr['Action'] = "0";
					$returnArr['message'] = "LBL_NO_TRIP_FOUND";
				}
			}
		}
		echo json_encode($returnArr);
		exit;
	}
	
	if($type=="configDriverTripStatus"){
		$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$userType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$vLatitude = isset($_REQUEST["vLatitude"]) ? $_REQUEST["vLatitude"] : '';
		$vLongitude = isset($_REQUEST["vLongitude"]) ? $_REQUEST["vLongitude"] : '';
		$iTripId = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '';
		$isSubsToCabReq = isset($_REQUEST["isSubsToCabReq"]) ? $_REQUEST["isSubsToCabReq"] : '';
		$vTimeZone = isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		
		if($iMemberId != ""){
			if (!empty($isSubsToCabReq) && $isSubsToCabReq == 'true') {
				$driver_update['tLastOnline'] = date('Y-m-d H:i:s');
				$driver_update['tOnline'] = date('Y-m-d H:i:s');
			}
			
			if (!empty($vLatitude) && !empty($vLongitude)) {
				$driver_update['vLatitude'] = $vLatitude;
				$driver_update['vLongitude'] = $vLongitude;
			}
			
			if(count($driver_update) > 0){
				$where = " iDriverId = '".$iMemberId."'";
				$Update_driver = $obj->MySQLQueryPerform("register_driver", $driver_update, "update", $where);
				# Update User Location Date #
				Updateuserlocationdatetime($iMemberId,"Driver",$vTimeZone);    
				# Update User Location Date #
			}
			
		}
		if($iTripId != ""){
			$sql = "SELECT tMessage as msg, iStatusId FROM trip_status_messages WHERE iDriverId='".$iMemberId."' AND eToUserType='Driver' AND eReceived='No' ORDER BY iStatusId DESC LIMIT 1 ";
			$msg = $obj->MySQLSelect($sql);
			}else{
			$date = @date("Y-m-d");
			$sql = "SELECT passenger_requests.tMessage as msg  FROM passenger_requests LEFT JOIN driver_request ON  driver_request.iRequestId=passenger_requests.iRequestId  LEFT JOIN register_driver ON register_driver.iDriverId=passenger_requests.iDriverId where date_format(passenger_requests.dAddedDate,'%Y-%m-%d')= '" . $date . "' AND  passenger_requests.iDriverId=" . $iMemberId . " AND driver_request.eStatus='Timeout' AND register_driver.vTripStatus IN ('Not Active','NONE','Cancelled') ORDER BY passenger_requests.iRequestId DESC LIMIT 1 "; 
			$msg = $obj->MySQLSelect($sql);
		}
		
		
		$returnArr['Action'] = "0";
		if(!empty($msg)){
			
			$returnArr['Action'] = "1";
			
			if($iTripId != ""){
				$updateQuery = "UPDATE trip_status_messages SET eReceived = 'Yes' WHERE iStatusId='".$msg[0]['iStatusId']."'";
				$obj->sql_query($updateQuery);
				
				$returnArr['Action'] = "1";
				$returnArr['message'] = $msg[0]['msg'];
				}else{
				
				$driver_request['eStatus'] = "Received";				
				$where = " iDriverId =" . $iMemberId . " and date_format(tDate,'%Y-%m-%d') = '" . $date . "' AND eStatus = 'Timeout' ";
				$obj->MySQLQueryPerform("driver_request", $driver_request, "update", $where);
				
				
				// $updatequery = "update driver_request set eStatus='Received' where iDriverId='".$iMemberId."' AND   date_format(tDate,'%Y-%m-%d') = '" . $date . "'  AND eStatus = 'Timeout'";
				// $obj->sql_query($updateQuery);
				
				
				$returnArr['Action'] = "1";
				$dataArr = array();
				for($i=0;$i<count($msg); $i++){
					$dataArr[$i] = $msg[$i]['msg'];
				}
				$returnArr['message'] = $dataArr;
			}
			
		}
		echo json_encode($returnArr);
		exit;
	}
	
	if($type=="configPassengerTripStatus"){
		$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$userType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		$vLatitude = isset($_REQUEST["vLatitude"]) ? $_REQUEST["vLatitude"] : '';
		$vLongitude = isset($_REQUEST["vLongitude"]) ? $_REQUEST["vLongitude"] : '';
		$iTripId = isset($_REQUEST["iTripId"]) ? $_REQUEST["iTripId"] : '';
		$CurrentDriverIds = isset($_REQUEST["CurrentDriverIds"]) ? explode(',',$_REQUEST["CurrentDriverIds"]) : '';
		$vTimeZone = isset($_REQUEST["vTimeZone"]) ? $_REQUEST["vTimeZone"] : '';
		
		if($iMemberId != ""){
			
			if (!empty($vLatitude) && !empty($vLongitude)) {
				$user_update['vLatitude'] = $vLatitude;
				$user_update['vLongitude'] = $vLongitude;
				$where = " iUserId = '".$iMemberId."'";
				$Update_driver = $obj->MySQLQueryPerform("register_user", $user_update, "update", $where);
				# Update User Location Date #
				Updateuserlocationdatetime($iMemberId,"Passenger",$vTimeZone);    
				# Update User Location Date #
			}
		}
		
		$currDriver = array();
		if(!empty($CurrentDriverIds)){
			$k=0;
			foreach($CurrentDriverIds as $cDriv){
				$driverDetails = array();
				$driverDetails=get_value('register_driver', 'iDriverId,vLatitude,vLongitude', 'iDriverId', $cDriv);
				$currDriver[$k]['iDriverId'] = $driverDetails[0]['iDriverId'];
				$currDriver[$k]['vLatitude'] = $driverDetails[0]['vLatitude'];
				$currDriver[$k]['vLongitude'] = $driverDetails[0]['vLongitude'];
				$k++;
			}
		}
		
		$sql = "SELECT tMessage as msg, iStatusId FROM trip_status_messages WHERE iUserId='".$iMemberId."' AND eToUserType='Passenger' AND eReceived='No' ORDER BY iStatusId DESC LIMIT 1 ";
		$msg = $obj->MySQLSelect($sql);
		
		$returnArr['Action'] = "0";
		if(!empty($msg)){
			$updateQuery = "UPDATE trip_status_messages SET eReceived ='Yes' WHERE iStatusId='".$msg[0]['iStatusId']."'";
			$obj->sql_query($updateQuery);
			
			$returnArr['Action'] = "1";
			$returnArr['message'] = $msg[0]['msg'];
		}
		
		$returnArr['currentDrivers'] = $currDriver;
		
		echo json_encode($returnArr);
		exit;
	}
	
	if($type == "callOnLogout"){
		global $generalobj,$obj;
		
		$iMemberId = isset($_REQUEST["iMemberId"]) ? $_REQUEST["iMemberId"] : '';
		$userType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Passenger';
		
		$Data_logout = array();
		
		if($userType == "Passenger"){
			$Data_logout['eLogout'] = 'Yes';
			$tableName = "register_user";
			$where = " iUserId='".$iMemberId."'";
			}else{
			$Data_logout['vAvailability'] = 'Not Available';
			$Data_logout['eLogout'] = 'Yes';
			$tableName = "register_driver";
			$where = " iDriverId='".$iMemberId."'";
		}
		$id = $obj->MySQLQueryPerform($tableName,$Data_logout,'update', $where);
		
		if($id){
			$returnArr['Action']="1";
			}else{
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
		exit;
	}
	
	if($type == "getCabRequestAddress"){
		global $generalobj,$obj;
		
		$iCabRequestId = isset($_REQUEST["iCabRequestId"]) ? $_REQUEST["iCabRequestId"] : '';
		$fields="tSourceAddress,tDestAddress";
		
		$Data_cab_request = get_value('cab_request_now', $fields, 'iCabRequestId', $iCabRequestId,'','');
		
		if(!empty($Data_cab_request)){
			$returnArr['Action']="1";
			$returnArr['message']=$Data_cab_request[0];
			}else{
			$returnArr['Action']="0";
			$returnArr['message']="LBL_TRY_AGAIN_LATER_TXT";
		}
		
		echo json_encode($returnArr);
		exit;
	}
	
	###########################################################################
	########################Get Driver Bank Details############################  
	
	if($type == "DriverBankDetails"){
		global $generalobj,$obj;
		
		$iDriverId = isset($_REQUEST["iDriverId"]) ? $_REQUEST["iDriverId"] : '';
		$userType = isset($_REQUEST["UserType"]) ? $_REQUEST["UserType"] : 'Driver';
		$eDisplay = isset($_REQUEST["eDisplay"]) ? $_REQUEST["eDisplay"] : 'Yes';
		$vPaymentEmail = isset($_REQUEST["vPaymentEmail"]) ? $_REQUEST["vPaymentEmail"] : '';
		$vBankAccountHolderName = isset($_REQUEST["vBankAccountHolderName"]) ? $_REQUEST["vBankAccountHolderName"] : '';
		$vAccountNumber = isset($_REQUEST["vAccountNumber"]) ? $_REQUEST["vAccountNumber"] : '';
		$vBankLocation = isset($_REQUEST["vBankLocation"]) ? $_REQUEST["vBankLocation"] : '';
		$vBankName = isset($_REQUEST["vBankName"]) ? $_REQUEST["vBankName"] : '';
		$vBIC_SWIFT_Code = isset($_REQUEST["vBIC_SWIFT_Code"]) ? $_REQUEST["vBIC_SWIFT_Code"] : ''; 
		
		if($eDisplay == "" || $eDisplay == NULL){
			$eDisplay = "Yes";
		}
		$returnArr = array();
		if($eDisplay == "Yes"){
			$Driver_Bank_Arr=  get_value('register_driver', 'vPaymentEmail, vBankAccountHolderName, vAccountNumber, vBankLocation, vBankName, vBIC_SWIFT_Code', 'iDriverId', $iDriverId);
			$returnArr['Action']="1";
			$returnArr['message']=$Driver_Bank_Arr[0];
			echo json_encode($returnArr);exit;
			}else{
			$Data_Update['vPaymentEmail'] = $vPaymentEmail;
			$Data_Update['vBankAccountHolderName'] = $vBankAccountHolderName;
			$Data_Update['vAccountNumber'] = $vAccountNumber;
			$Data_Update['vBankLocation'] = $vBankLocation;
			$Data_Update['vBankName'] = $vBankName;
			$Data_Update['vBIC_SWIFT_Code'] = $vBIC_SWIFT_Code;
			
			$where = " iDriverId = '".$iDriverId."'";
    		$obj->MySQLQueryPerform("register_driver",$Data_Update,'update',$where);
			
			$returnArr['Action']="1";
			$returnArr['message']=getDriverDetailInfo($iDriverId);
			echo json_encode($returnArr);exit;
		}
	}
	########################Get Driver Bank Details############################
	
	if($type == "getvehicleCategory"){ 
		 $iDriverId = isset($_REQUEST['iDriverId']) ? $_REQUEST['iDriverId'] : '';
  		$page        = isset($_REQUEST['page']) ? trim($_REQUEST['page']) : 1;
		
		$languageCode="";
		if($iDriverId !=""){
			$languageCode =  get_value('register_driver', 'vLang', 'iDriverId',$iDriverId,'','true');		
		}			
		
		if($languageCode == "" || $languageCode == NULL){
			$languageCode = get_value('language_master', 'vCode', 'eDefault','Yes','','true');
		}
				
		
		$per_page=10;
  		$sql_all  = "SELECT COUNT(iVehicleCategoryId) As TotalIds FROM vehicle_category as vc WHERE vc.eStatus='Active' AND vc.iParentId='0' and (select count(iVehicleCategoryId) from vehicle_category where iParentId=vc.iVehicleCategoryId AND eStatus='Active') > 0";
  		$data_count_all = $obj->MySQLSelect($sql_all);
  		$TotalPages = ceil($data_count_all[0]['TotalIds'] / $per_page);
		
  		$start_limit = ($page - 1) * $per_page;
  		$limit       = " LIMIT " . $start_limit . ", " . $per_page;
		
		 $sql = "SELECT vc.iVehicleCategoryId, vc.vCategory_".$languageCode." as vCategory FROM vehicle_category as vc WHERE vc.eStatus='Active' AND vc.iParentId='0' and (select count(iVehicleCategoryId) from vehicle_category where iParentId=vc.iVehicleCategoryId AND eStatus='Active') > 0". $limit;
		$vehicleCategoryDetail = $obj->MySQLSelect($sql);
				
		$vehicleCategoryData = array();
			
		if(count($vehicleCategoryDetail) >0){
			$vehicleCategoryData = $vehicleCategoryDetail;
			$i=0;
			while (count($vehicleCategoryDetail)> $i) {
				
				$iVehicleCategoryId = $vehicleCategoryDetail[$i]['iVehicleCategoryId'];
				
				$sql = "SELECT vCategory_" .$languageCode. " as vTitle,iVehicleCategoryId FROM `vehicle_category` WHERE iParentId='".$iVehicleCategoryId."'";
				$subCategoryData = $obj->MySQLSelect($sql);
				
				$vehicleCategoryData[$i]['SubCategory'] = $subCategoryData;
				$i++;
			}
				
						
			$returnArr['Action'] = "1";	
			if ($TotalPages > $page) {
				$returnArr['NextPage'] = "".($page + 1);
    			} else {
				$returnArr['NextPage'] = "0";
			}
			$returnArr['message'] = $vehicleCategoryData;			
			
		}else{
			$returnArr['Action'] = "0";
			$returnArr['message'] ="LBL_TRY_AGAIN_LATER_TXT";		
		
		}
		echo json_encode($returnArr);
		
	 }	
	 
	###########################################################################
	###########################################################################     
?>