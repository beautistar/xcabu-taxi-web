<?php
include_once("../common.php");
include_once("../generalFunctions.php");

if (!isset($generalobjAdmin)) {
    require_once(TPATH_CLASS . "class.general_admin.php");
    $generalobjAdmin = new General_admin();
}
$generalobjAdmin->check_member_login();

$fromLoc = isset($_REQUEST['fromLoc'])?$_REQUEST['fromLoc']:'';
$toLoc = isset($_REQUEST['toLoc'])?$_REQUEST['toLoc']:'';
$response = isset($_REQUEST['response'])?$_REQUEST['response']:'';

$allowed_ans = 'Yes';
$allowed_ans_drop = 'Yes';

if($fromLoc != "")
	$allowed_ans = checkRestrictedArea($fromLoc,"No");
if($toLoc != "")
	$allowed_ans_drop = checkRestrictedArea($toLoc,"Yes");

if($allowed_ans == 'Yes' && $allowed_ans_drop == 'Yes'){
	echo "yes";
}else {
	echo "no";
}
?>