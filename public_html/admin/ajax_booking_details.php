<?php
include_once('../common.php');

if (!isset($generalobjAdmin)) {
    require_once(TPATH_CLASS . "class.general_admin.php");
    $generalobjAdmin = new General_admin();
}
$generalobjAdmin->check_member_login();

$countryId = isset($_REQUEST['countryId']) ? $_REQUEST['countryId'] : ''; 
$iVehicleTypeId = isset($_REQUEST['iVehicleTypeId']) ? $_REQUEST['iVehicleTypeId'] : ''; 
$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';

if ($type == 'getVehicles') {

	$sql23 = "SELECT vt.* FROM `vehicle_type` AS vt 
	LEFT JOIN `country` AS c ON c.iCountryId=vt.iCountryId
	WHERE (c.vCountryCode='".$countryId."' OR vt.iCountryId = '-1') AND eType='Ride' ORDER BY iVehicleTypeId ASC";
	$db_carType = $obj->MySQLSelect($sql23);

	echo '<option value="" >Select Vehicle Type</option>';
	foreach ($db_carType as $db_car) {
		$selected='';
		if($db_car['iVehicleTypeId'] == $iVehicleTypeId){
			$selected = "selected=selected";
		}
		echo "<option value=".$db_car['iVehicleTypeId']." ".$selected.">".$db_car['vVehicleType']."</option>";
	}
	exit;
}
?>